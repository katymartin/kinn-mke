<div class="h-wrapper">
<?php
/*
 Template Name: Gallery
 *
 * This is your custom page template. You can create as many of these as you need.
 * Simply name is "page-whatever.php" and in add the "Template Name" title at the
 * top, the same way it is here.
 *
 * When you create your page, you can just select the template and viola, you have
 * a custom page template to call your very own. Your mother would be so proud.
 *
 * For more info: http://codex.wordpress.org/Page_Templates
*/
?>
    <?php get_header(); ?>


   <div class="prepad">
        </div>

<div class="intro-narrow">
                <h1 class="lead" style="color:black">Gallery</h1>

</div>

<div class="galleria">
        <div class="container">
            <h2>Views from the Guesthouse</h2>
            <p>Here's a glimpse into what Kinn Guesthouse has to offer. Tag your photos with #kinnmke to show off your experience in Milwaukee.</p>
        </div>
    </div>

</div>

    <div id="insta-gal">

<?php echo do_shortcode('[ess_grid alias="gallery"]'); ?>

    </div>

    <div id="booker">
        <div class="container">
            <h3>Ready to Book?</h3>
            <p>Want to have your own experience at Kinn MKE? Click below to see our rooms and book today.</p>

            <a href="/rooms" class="bttn-grayoutline">View Our Rooms</a>
        </div>
    </div>


    <?php get_footer(); ?>

</div>
