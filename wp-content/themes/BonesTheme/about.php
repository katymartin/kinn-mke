<div class="h-wrapper">

    <?php
/*
 Template Name: About
 *
 * This is your custom page template. You can create as many of these as you need.
 * Simply name is "page-whatever.php" and in add the "Template Name" title at the
 * top, the same way it is here.
 *
 * When you create your page, you can just select the template and viola, you have
 * a custom page template to call your very own. Your mother would be so proud.
 *
 * For more info: http://codex.wordpress.org/Page_Templates
*/
?>
        <?php get_header(); ?>

        <section id="hero" style="background: linear-gradient(
      rgba(0, 0, 0, 0.35), 
      rgba(0, 0, 0, 0.35)
    ), url(' <?php the_field('hero_image') ;?>');">

            <div class="intro" data-aos="fade-up">

                <h1 class="lead">About Us</h1>


                <div class="container-in transparent">
                    <div class="container">
                        <?php the_field('hero_copy') ;?>
                    </div>
                </div>
            </div>




</div>

</section>

<section id="grid1">

    <div class="flex-container" data-aos="fade-up">
        <div class="flex-image" style="background: url(' <?php the_field('large_image') ;?>');">
        </div>
    </div>

    <div class="flex-container b" data-aos="fade-up">
        <div class="fullbox">
            <div class="wrapper">
                <?php the_field('about_main_copy') ;?>
            </div>
        </div>

        <div class="gallery" style="background: url('<?php the_field('secondary_image_1') ;?>')">
        </div>

        <div class="gallery" style="background: url('<?php the_field('secondary_image_2') ;?>')">
        </div>

    </div>

</section>

<!--
<section id="connect">
    <div class="flex-container">

        <?php the_field('past_experience_copy') ;?>

        <a class="btn-gray" style="margin: 0 auto;" href="/">Past Experiences</a>
    </div>



</section>


<section id="gallery">
    <div class="flex-container">
        <div class="gal-item">
            <img src="/wp-content/uploads/2016/12/mosaic4-filler.jpg">
        </div>

        <div class="gal-item">
            <img src="/wp-content/uploads/2016/12/mosaic1-filler.jpg">
        </div>

        <div class="gal-item">
            <img src="/wp-content/uploads/2016/12/mosaic2-filler.jpg">
        </div>

        <div class="gal-item">
            <img src="/wp-content/uploads/2016/12/mosaic5.jpg">
        </div>

        <div class="gal-item">
            <img src="/wp-content/uploads/2016/12/mosaic6-filler.jpg">
        </div>


        <div class="gal-item hide">
            <img src="/wp-content/uploads/2016/12/mosaic3-filler.jpg">
        </div>

    </div>


</section>
-->


<section id="dinein" data-aos="fade-up">
    <div class="flex-container">
        <div class="copy">
            <div class="wrapper">
                <?php the_field('dining_copy');?>


                <div class="bttn-group" style="justify-content: inherit;">

                    <div class="bttn-grayline"><a class="btn-gray" href="/book">Learn More</a></div>

                </div>
            </div>
        </div>



        <div class="img" style="background: url('<?php the_field('dining_image');?>');"></div>

    </div>

</section>
</div>

<?php get_footer(); ?>
