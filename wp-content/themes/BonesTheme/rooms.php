<div class="h-wrapper">
<?php
/*
 Template Name: Rooms
 *
 * This is your custom page template. You can create as many of these as you need.
 * Simply name is "page-whatever.php" and in add the "Template Name" title at the
 * top, the same way it is here.
 *
 * When you create your page, you can just select the template and viola, you have
 * a custom page template to call your very own. Your mother would be so proud.
 *
 * For more info: http://codex.wordpress.org/Page_Templates
*/
?>
    <?php get_header(); ?>

    <section id="hero">


        <div class="intro" data-aos="fade-up">
            <h1 class="lead">Our Rooms</h1>
            
                    <div class="container-in transparent">
            <div class="container">
                <h2>Amenities</h2>
                <p>Not your traditional hotel experience. Located in the heart of Bay View; Kinn offers Milwaukee’s most unique lodging experience.</p>
            </div>
        </div>
            
        </div>




    </section>

    <section id="amenities">
        <div class="flex-container" data-aos="fade-up">

            <div class="am-item"><div class="caption"><div class="overlayer"></div><h2>Decorated by local artists</h2><a href="/artists">Learn More</a></div><img src="<?php the_field('amenities_image_1');?>"> </div>

            <div class="am-item"><div class="caption"><div class="overlayer"></div><h2>AppleTV Equipped</h2></div><img src="<?php the_field('amenities_image_2');?>"></div>

            <div class="am-item"><div class="caption"><div class="overlayer"></div><h2>Self Serve Laundry</h2></div><img src="<?php the_field('amenities_image_3');?>"> </div>

            <div class="am-item"><div class="caption"><div class="overlayer"></div><h2>Brooklinen Bedding</h2><a href="https://www.brooklinen.com/" target="_blank">Learn More</a></div><img src="<?php the_field('amenities_image_4');?>"> </div>

        </div>

    </section>
    
    <section id="room">
        
			<?php if( have_rows('rooms') ): ?>

			<?php while( have_rows('rooms') ): the_row(); 

		// vars
       $featured_image = get_sub_field('featured_image');
		$room_name = get_sub_field('room_name');
        $room_description = get_sub_field('room_description');
        $airbnb = get_sub_field('airbnb_link');
        $image1 = get_sub_field('room_image_1');
        $image2 = get_sub_field('room_image_2');
        $image3 = get_sub_field('room_image_3');
        $image4 = get_sub_field('room_image_4');

		?>
        
        <div class="flex-container" data-aos="fade-up">

            <div class="main_visual">
                <a href="<?php echo $featured_image ;?>" data-featherlight="image"><img src="<?php echo $featured_image ;?>"></a>
                
                                    <div class="room-gal">
                        <div class="item" style="background: url('<?php echo $image1 ;?>')"><a href="<?php echo $image1 ;?>" data-featherlight="image"><h2>...</h2></a></div>

                        <div class="item" style="background: url('<?php echo $image2 ;?>')"><a href="<?php echo $image2 ;?>" data-featherlight="image"><h2>...</h2></a></div>

                        <div class="item" style="background: url('<?php echo $image3 ;?>')"><a href="<?php echo $image3 ;?>" data-featherlight="image"><h2>...</h2></a></div>

                    </div>
                
            </div>

            <div class="sidebox">
                <div class="textbox">
                    <div class="container">

                        <h2>
                            <?php echo $room_name ;?>
                        </h2>
                        
                            <?php echo $room_description ;?>
                 
                       <a class="btn-gray" href="<?php echo $airbnb ;?>">Book on AirBnB</a>
                    </div>

                </div>
            </div>
            <hr>
        </div>

			<?php endwhile; ?>

			<?php endif; ?>

    </section>

    <?php get_footer(); ?>
</div>

<script>

    jQuery("#hero").vegas({
        delay: 3000,
        overlay: true,
        overlay: '/js/vegas/dist/overlays/06.png',
    slides: [
        { src: "/wp-content/themes/BonesTheme/library/images/rooms04.jpg" },
        { src: "/wp-content/themes/BonesTheme/library/images/rooms02.jpg" },
        { src: "/wp-content/themes/BonesTheme/library/images/rooms03.jpg" },
        { src: "/wp-content/themes/BonesTheme/library/images/rooms01.jpg" }
    ]
        
    
});
</script>