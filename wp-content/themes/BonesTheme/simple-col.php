<?php
/*
 Template Name: Simple Column
*/
?>
	<?php get_header(); ?>



	<section id="hero-narrow" style="background: linear-gradient(
      rgba(0, 0, 0, 0.35), 
      rgba(0, 0, 0, 0.35)
    ), url('<?php the_field('hero_image_short') ?>');">

		<div class="intro narrow">

			<h1 class="lead"><?php the_field('page_title') ;?></h1>


			<div class="container-in transparent">
				<div class="container">
					<?php the_field('hero_copy_short') ;?>
				</div>
			</div>
		</div>

	</section>


	<section id="content-short">
		<div class="flex-container">

			<?php if( have_rows('new_field') ): ?>

			<?php while( have_rows('new_field') ): the_row(); 

		// vars
       $image = get_sub_field('image_short');
	   $content = get_sub_field('content_short');

		?>

			<div class="flex-item">
				<div class="boxer">
				<div class="headphoto"><img src="<?php echo $image ;?>"></div>

				<div class="thecontent">
					<?php echo $content ; ?>
				</div>
					</div>
			</div>
<?php endwhile; ?>
			<?php endif; ?>
			


		</div>

	</section>
	<?php get_footer(); ?>
