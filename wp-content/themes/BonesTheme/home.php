<div class="h-wrapper">
<?php
/*
 Template Name: Home
*/
?>
    <?php get_header(); ?>
    
    
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCvA0Wk96OzNrcwoz7tl-ojpnB8nQDxbKw"></script>

        
        <script type="text/javascript">
            // When the window has finished loading create our google map below
            google.maps.event.addDomListener(window, 'load', init);
        
            function init() {
                // Basic options for a simple Google Map
                // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
                var mapOptions = {
                    // How zoomed in you want the map to start at (always required)
                    zoom: 15,
                    scrollwheel: false,
                    
                    // The latitude and longitude to center the map (always required)
                    center: new google.maps.LatLng(42.998631, -87.901124), // New York

                    // How you would like to style the map. 
                    // This is where you would paste any style found on Snazzy Maps.
                    styles: [{"featureType":"water","elementType":"geometry","stylers":[{"color":"#e9e9e9"},{"lightness":17}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#ffffff"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":16}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":21}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#dedede"},{"lightness":21}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"lightness":16}]},{"elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#333333"},{"lightness":40}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#f2f2f2"},{"lightness":19}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#fefefe"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#fefefe"},{"lightness":17},{"weight":1.2}]}]
                };

                // Get the HTML DOM element that will contain your map 
                // We are using a div with id="map" seen below in the <body>
                var mapElement = document.getElementById('map');

                // Create the Google Map using our element and options defined above
                var map = new google.maps.Map(mapElement, mapOptions);

                // Let's also add a marker while we're at it
                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(42.998631, -87.901124),
                    map: map,
                    title: 'Snazzy!'
                });
                
                
            }
        </script>

    
    

    <section id="hero">
        
        <div class="content" data-aos="fade-up">

        <div class="logo"><img src="/wp-content/themes/BonesTheme/library/images/logo.svg" alt="Kinn Milwaukee"></div>

        <div class="searchform" >
<!--			<div class="bttn-whiteline"><a href="/rooms">Book Today</a></div>-->
			<?php echo do_shortcode('[hb_booking_form all_accom="yes" search_only="yes" redirection_url="/book"]'); ?>
<!--
            <h2>Find a room</h2>
      
-->
        </div>
            
            </div>

    </section>

    <section id="landing" data-aos="fade-up">
        <div class="container">
            <h2>Your Home Away</h2>
            <p>Kinn Guesthouse is a perfect hybrid of a standard hotel and a residential apartment designed w/ needs the Air BnB traveler in mind where we strive to celebrate community.  Whether you choose to enjoy some seasonal fare and a craft cocktail at the long awaited Kindred Restaurant & Bar on the first floor or to prepare you own creation upstairs, at Kinn you’ll discover a highly tailorable living experience with a bit of something for everyone.  Come to Kinn and visit Milwaukee on your own terms.</p>
        </div>

    </section>

    <section id="gallery" data-aos="fade-up">
        <div class="container">
            <div class="gal-item" style="background: url('<?php the_field('gallery_image_1')?>')"></div>
            <div class="gal-item" style="background: url('<?php the_field('gallery_image_2')?>')"></div>
            <div class="gal-item" style="background: url('<?php the_field('gallery_image_3')?>')"></div>
            <div class="gal-item" style="background: url('<?php the_field('gallery_image_4')?>')"></div>
            <div class="gal-item" style="background: url('<?php the_field('gallery_image_5')?>')"></div>
            <div class="gal-item" style="background: url('<?php the_field('gallery_image_6')?>')"></div>
        </div>
    </section>


<div id="wrapper" data-aos="fade-up">
    <div id="a" style="background: linear-gradient(
      rgba(0, 0, 0, 0.35), 
      rgba(0, 0, 0, 0.35)
    ),url('<?php the_field('mosaic_1')?>')">
        <div class="container">
        <h2>Our City</br/>Your House</h2>
        <h3>Immerse Yourself</h3>
        <div class="bttn-whiteline"><a href="/things-to-do/">Things To Do</a></div>
    </div>
        </div>
 <div id="a2">
    <div id="b" style="background:url('<?php the_field('mosaic_2')?>');"></div>
     <div class="flex-container">
        <div class="c">
            <div class="container">
            <h4>Community</h4>
            <p>We wanted to mimic this sense of openness and community which we feel is important these days. To achieve this, we created a community lounge w/ a TV area, mini library, laundry and a complete, pro-series kitchen where one can share in the creature comforts of home. </p>
            </div>
            </div>
            <div class="d" style="background:url('<?php the_field('mosaic_3')?>')"></div>
  </div>
</div>
    </div>

<section id="map" data-aos="fade-up">

</section>
</div>
    <?php get_footer(); ?>

    <script>
    
    
        //BG Slider
    
    jQuery("#hero").vegas({
        delay: 3000,
    slides: [
        { src: "/wp-content/themes/BonesTheme/library/images/home-bg04.jpg" },
        { src: "/wp-content/themes/BonesTheme/library/images/home-bg02.jpg" },
        { src: "/wp-content/themes/BonesTheme/library/images/home-bg03.jpg" },
        { src: "/wp-content/themes/BonesTheme/library/images/home-bg01.jpg" }
    ]
        
    
});
        
    </script>
