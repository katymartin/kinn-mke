<?php
/*
 Template Name: Contact
 *
 * This is your custom page template. You can create as many of these as you need.
 * Simply name is "page-whatever.php" and in add the "Template Name" title at the
 * top, the same way it is here.
 *
 * When you create your page, you can just select the template and viola, you have
 * a custom page template to call your very own. Your mother would be so proud.
 *
 * For more info: http://codex.wordpress.org/Page_Templates
*/
?>
    <?php get_header(); ?>

<style>

    #map{
        margin-top: 0px !important;
    }
</style>

 <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCvA0Wk96OzNrcwoz7tl-ojpnB8nQDxbKw"></script>

        
        <script type="text/javascript">
            // When the window has finished loading create our google map below
            google.maps.event.addDomListener(window, 'load', init);
        
            function init() {
                // Basic options for a simple Google Map
                // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
                var mapOptions = {
                    // How zoomed in you want the map to start at (always required)
                    zoom: 15,
                    scrollwheel: false,
                    
                    // The latitude and longitude to center the map (always required)
                    center: new google.maps.LatLng(42.998631, -87.901124), // New York

                    // How you would like to style the map. 
                    // This is where you would paste any style found on Snazzy Maps.
                    styles: [{"featureType":"water","elementType":"geometry","stylers":[{"color":"#e9e9e9"},{"lightness":17}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#ffffff"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":16}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":21}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#dedede"},{"lightness":21}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"lightness":16}]},{"elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#333333"},{"lightness":40}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#f2f2f2"},{"lightness":19}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#fefefe"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#fefefe"},{"lightness":17},{"weight":1.2}]}]
                };

                // Get the HTML DOM element that will contain your map 
                // We are using a div with id="map" seen below in the <body>
                var mapElement = document.getElementById('map');

                // Create the Google Map using our element and options defined above
                var map = new google.maps.Map(mapElement, mapOptions);

                // Let's also add a marker while we're at it
                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(42.998631, -87.901124),
                    map: map,
                    title: 'Snazzy!'
                });
                
                
            }
        </script>

    <div id="contact-bg">
        
        <div class="prepad">
        </div>
        <div class="contact">

            <div class="flex-container">
<div id="map">
                
                </div>
                
                <div class="form" data-aos="fade-right">
                    <div class="container">
                <h1 style="color: white">Contact Us</h1>
                    <div class="address">
                    <ul>
                        <li>2535 S Kinnickinnic Ave</li>
                        <li>Milwaukee, WI 53207</li>
                        </ul>
                    <ul>
                        <li><a href="tel:+17739094947">(773) 909-4947</a></li>
                        <li><a href="mailto:info@kinnmke.com">info@kinnmke.com</a></li>
                        </ul>
                    </div>
                    
                    <div class="cf7">
                    
                    <?php echo do_shortcode('[contact-form-7 id="124" title="Contact form 1"]') ;?>
                    </div>
                    </div>
                
                </div>
            </div>
        </div>

    </div>
       
        </div></div>



    <?php get_footer(); ?>
