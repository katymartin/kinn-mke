<div class="h-wrapper">
    <?php
/*
 Template Name: Accomodation Results
 *
 * This is your custom page template. You can create as many of these as you need.
 * Simply name is "page-whatever.php" and in add the "Template Name" title at the
 * top, the same way it is here.
 *
 * When you create your page, you can just select the template and viola, you have
 * a custom page template to call your very own. Your mother would be so proud.
 *
 * For more info: http://codex.wordpress.org/Page_Templates
*/
?>
        <?php get_header(); ?>

        <style>
            .hb-search-result-title-section h3 {
                display: none !important;
            }
            
            p.hb-select-accom.hb-select-accom-single {
    display: none !important;
}
            
            .hb-search-result-title-section p {
                background: whitesmoke;
                padding: 15px 15px 10px;
                line-height: 1.2;
            }
            
            p.hb-select-accom.hb-select-accom-multiple {
                display: none !important;
            }
            
            div#results label {
                color: black !important;
            }
            
            .hb-accom {
                border-radius: 0px !important;
                background: #fbfbfb;
            }
            
            .hb-details-fields {
                display: flex !important;
                flex-wrap: wrap;
                justify-content: space-around;
                width: 90%;
                margin: 0 auto;
                max-width: 1080px;
            }
            
            .hb-details-fields h3 {
                width: 100%;
                text-align: center;
            }
            
            .hbook-wrapper input[type="text"] {
                border-radius: 0px !important;
            }

        </style>


        <div class="prepad">
        </div>

        <section id="narrow-hero">
            <div class="overlayer"></div>
            <div class="flex-container">
                <h1 class="lead">Book a Room</h1>
            </div>

        </section>


        <div id="results">

            <div class="container">
                <?php echo do_shortcode('[hb_booking_form]') ;?>
            </div>
            
            <div class="alert">
                
                <button class="closer">&#xf00d;</button>
                
                Due to group demand during Summerfest and other high volume weekends, we’ve purposely blocked out certain dates.  If interested in any date which is shown as booked or blocked, please don’t be deterred, simply <a href="/contact">click here to contact us.</a></br></br> 
Best,</br>
Charles & Connie
        
            </div>

        </div>

        <?php get_footer(); ?>

</div>
