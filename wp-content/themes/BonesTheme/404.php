<?php get_header(); ?>

<div id="missing">
	<h2>Whoops!</h2>
	<img src="/wp-content/themes/BonesTheme/library/images/404.svg">
	<p>
		<?php _e( 'The page you were looking for was not found. Please try again.', 'bonestheme' ); ?>
	</p>
	
	<a href="/" class="btn btn-gray">Back to Home</a>

</div>



<?php get_footer(); ?>
