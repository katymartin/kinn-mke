<div class="h-wrapper">


<?php
/*
 Template Name: Things to Do
 *
 * This is your custom page template. You can create as many of these as you need.
 * Simply name is "page-whatever.php" and in add the "Template Name" title at the
 * top, the same way it is here.
 *
 * When you create your page, you can just select the template and viola, you have
 * a custom page template to call your very own. Your mother would be so proud.
 *
 * For more info: http://codex.wordpress.org/Page_Templates
*/
?>
    <?php get_header(); ?>

    <section id="hero">


        <div class="intro" data-aos="fade-up">

            <h1 class="lead">Explore MKE</h1>
            
            <div class="container-in transparent" style="display:none;">
            <div class="container">
                  <?php the_field('hero_copy');?>
                
                <div class="icon-scroll">
  <div class="mouse">
    <div class="wheel"></div>
  </div>
  <div>
    <span class="a1"></span>
    <span class="a2"></span>
    <span class="a3"></span>
  </div>
											</div>
                </div>
            </div>
        </div>
        
    </section>

    
    
        <section id="amenities">
        <div class="flex-container" data-aos="fade-up">

            <div class="am-item"><img src="<?php the_field('amenities_image_1');?>"> </div>

            <div class="am-item"><img src="<?php the_field('amenities_image_2');?>"></div>

            <div class="am-item"><img src="<?php the_field('amenities_image_3');?>"> </div>

            <div class="am-item"><img src="<?php the_field('amenities_image_4');?>"> </div>

        </div>

    </section>
    
    <div class="guidebar">
    <div class="flex-container" data-aos="fade-right">
        <div><a href="/things-to-do/#nightlife">Nightlife</a></div>
        <div><a href="/things-to-do/#casualdining">Casual Dining</a></div>
        <div><a href="/things-to-do/#finedining">Fine Dining</a></div>
        <div><a href="/things-to-do/#events">Events</a></div>
        </div>
    </div>
    
    
        <section id="nightlife" data-aos="fade-up">
            
            
                 <div class="flex-container-2col">
        <h2>Night Life</h2>
			<?php if( have_rows('nl_d') ): ?>

			<?php while( have_rows('nl_d') ): the_row(); 

		// vars
        $image = get_sub_field('business_image_nl');
		$name = get_sub_field('business_name_nl');
        $descript = get_sub_field('business_copy_nl');
        $url = get_sub_field('business_url_nl');

		?>
        
   <div class="flex-item">
            
            <div class="biz-img">
            <img src="<?php echo $image ;?>">
            </div>
            
            <div class="biz-copy">
            <h3><?php echo $name ;?></h3>
                <?php echo $descript ;?>
                <a href="<?php echo $url ;?>" class="btn-gray">Visit Website</a>
            </div>
            
            </div>
            
            
            <?php endwhile; ?>
            <?php endif; ?>
                     
            </div>
            
    </section>
    
    
            <section id="casualdining" data-aos="fade-up">
            
            
                 <div class="flex-container-2col">
        <h2>Casual Dining</h2>
			<?php if( have_rows('cd_d') ): ?>

			<?php while( have_rows('cd_d') ): the_row(); 

		// vars
        $image = get_sub_field('business_image_cd');
		$name = get_sub_field('business_name_cd');
        $descript = get_sub_field('business_copy_cd');
        $url = get_sub_field('business_url_cd');

		?>
        
   <div class="flex-item">
            
            <div class="biz-img">
            <img src="<?php echo $image ;?>">
            </div>
            
            <div class="biz-copy">
            <h3><?php echo $name ;?></h3>
                <?php echo $descript ;?>
                <a href="<?php echo $url ;?>" class="btn-gray">Visit Website</a>
            </div>
            
            </div>
            
            
            <?php endwhile; ?>
            <?php endif; ?>
                     
            </div>
            
    </section>
    
    
            <section id="finedining" data-aos="fade-up">
            
            
                 <div class="flex-container-2col">
        <h2>Fine Dining</h2>
			<?php if( have_rows('fd_d') ): ?>

			<?php while( have_rows('fd_d') ): the_row(); 

		// vars
        $image = get_sub_field('business_image_fd');
		$name = get_sub_field('business_name_fd');
        $descript = get_sub_field('business_copy_fd');
        $url = get_sub_field('business_url_fd');

		?>
        
   <div class="flex-item">
            
            <div class="biz-img">
            <img src="<?php echo $image ;?>">
            </div>
            
            <div class="biz-copy">
            <h3><?php echo $name ;?></h3>
                <?php echo $descript ;?>
                <a href="<?php echo $url ;?>" class="btn-gray">Visit Website</a>
            </div>
            
            </div>
            
            
            <?php endwhile; ?>
            <?php endif; ?>
                     
            </div>
            
    </section>
    
    
            <section id="events" data-aos="fade-up">
            
            
                 <div class="flex-container-2col">
        <h2>Events</h2>
			<?php if( have_rows('e_d') ): ?>

			<?php while( have_rows('e_d') ): the_row(); 

		// vars
        $image = get_sub_field('business_image_e');
		$name = get_sub_field('business_name_e');
        $descript = get_sub_field('business_copy_e');
        $url = get_sub_field('business_url_e');

		?>
        
   <div class="flex-item">
            
            <div class="biz-img">
            <img src="<?php echo $image ;?>">
            </div>
            
            <div class="biz-copy">
            <h3><?php echo $name ;?></h3>
                <?php echo $descript ;?>
                <a href="<?php echo $url ;?>" class="btn-gray">Visit Website</a>
            </div>
            
            </div>
            
            
            <?php endwhile; ?>
            <?php endif; ?>
                     
            </div>
            
    </section>
    
            <?php get_footer(); ?>

</div>

<script>

    jQuery("#hero").vegas({
        delay: 3000,
        overlay: true,
        overlay: '/js/vegas/dist/overlays/06.png',
    slides: [
        { src: "/wp-content/themes/BonesTheme/library/images/events04.jpg" },
        { src: "/wp-content/themes/BonesTheme/library/images/events02.jpg" },
        { src: "/wp-content/themes/BonesTheme/library/images/events03.jpg" },
        { src: "/wp-content/themes/BonesTheme/library/images/events01.jpg" }
    ]
        
    
});
</script>
    