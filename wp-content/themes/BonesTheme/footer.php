			<footer class="footer" role="contentinfo" itemscope itemtype="http://schema.org/WPFooter">

				<div id="inner-footer" class="wrap cf">
                      <div class="center"><img src="/wp-content/themes/BonesTheme/library/images/logomark.svg" ></div>

                  
                    <div class="right">All Rights Reserved Kinn <?php echo date("Y"); ?></div>
				</div>

			</footer>

		</div>
<script src="//cdn.rawgit.com/noelboss/featherlight/1.7.0/release/featherlight.min.js" type="text/javascript" charset="utf-8"></script>

<script src="/wp-content/themes/BonesTheme/library/js/featherlight.gallery.min.js"></script>
<script src="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.js"></script>

<script src="/wp-content/themes/BonesTheme/library/js/vegas.min.js"></script>

<script>

	
	AOS.init({
  duration: 1200,
})    


</script>


		<?php // all js scripts are loaded in library/bones.php ?>
		<?php wp_footer(); ?>

	</body>

</html> <!-- end of site. what a ride! -->
