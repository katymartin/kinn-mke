<?php get_header(); ?>
<link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
		
<body>

<section class="top">

<div class="comingSoon "><h3>COMING TO MILWAUKEE 2017</h3></div>

	<div class="logo"><img src="/wp-content/themes/BonesTheme/library/images/KinnLogo.png"></div>
	
</section>

<section class="bottom">
	<div class="info"><p>Prepare to visit Milwaukee with a unique lodging experience. Kinn is a guesthouse coming to Milwaukee's up-and-coming Bay View neighborhood in 2017.</p></div>

	
	<div class="chimpFoot">
		<!-- Begin MailChimp Signup Form -->
<link href="//cdn-images.mailchimp.com/embedcode/horizontal-slim-10_7.css" rel="stylesheet" type="text/css">
<style type="text/css">
	#mc_embed_signup{background:#fff; clear:left; font:14px Helvetica,Arial,sans-serif; width:100%;}
	/* Add your own MailChimp form style overrides in your site stylesheet or in this style block.
	   We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */
</style>
<div id="mc_embed_signup">
<form action="//gogeddit.us2.list-manage.com/subscribe/post?u=4c27f09ab6a622c6340c21d60&amp;id=5558be1ed9" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
    <div id="mc_embed_signup_scroll">
	<label for="mce-EMAIL">sign up for updates</label>
	<input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="email address" required>
    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_4c27f09ab6a622c6340c21d60_5558be1ed9" tabindex="-1" value=""></div>
    <div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
    </div>
</form>
</div>

<!--End mc_embed_signup-->
	</div>
</section>
</body>



<?php get_footer(); ?>
