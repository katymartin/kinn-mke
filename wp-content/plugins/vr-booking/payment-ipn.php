 <?php


 Global $VRBooking_var;

 //error_log( print_r( $_POST, true ) );

    if ( ! empty( $_POST )) {
    $posted = wp_unslash( $_POST );

      $validate_ipn = array( 'cmd' => '_notify-validate' );
		$validate_ipn += wp_unslash( $_POST );

		// Send back post vars to paypal
		$params = array(
			'body'        => $validate_ipn,
			'timeout'     => 60,
			'httpversion' => '1.1',
			'compress'    => false,
			'decompress'  => false,
			'user-agent'  => 'VR-Booking'
		);

		// Post back to get a response.

        $sandbox =1;

        if ( $sandbox ) {
			$link = 'https://www.sandbox.paypal.com/cgi-bin/webscr';
		} else {
			$link = 'https://www.paypal.com/cgi-bin/webscr';
		}

		$response = wp_safe_remote_post( $link, $params );

		// Check to see if the request was valid.
		if ( ! is_wp_error( $response ) && $response['response']['code'] >= 200 && $response['response']['code'] < 300 && strstr( $response['body'], 'VERIFIED' ) ) {
			$validate = true;
         //   error_log( print_r( $response, true ) );
         //   $VRBooking_var->cancell_booking($posted['custom']);
		} else $validate = false;

        if (($validate)and( ! empty( $posted['custom'] ) )) {

        // Lowercase returned variables.
			$posted['payment_status'] = strtolower( $posted['payment_status'] );

		   //	 Sandbox fix.
		 	if ( isset( $posted['test_ipn'] ) && 1 == $posted['test_ipn'] && 'pending' == $posted['payment_status'] ) {
		 		$posted['payment_status'] = 'completed';
		 	}

          if ( 'completed' === $posted['payment_status'] ) {
              $VRBooking_var->accept_booking($posted['custom'], $posted['mc_gross'], $posted['txn_id'], 'paypal');
              $VRBooking_var->thanks_email($posted['custom']);
          } else $VRBooking_var->cancell_booking($posted['custom']);
     }
  }