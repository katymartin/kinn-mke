<?php

//      General functions

if ( ! defined( 'ABSPATH' ) )
	exit;

// Add support for Post Thumbnails.
add_theme_support( 'post-thumbnails' );
//set_post_thumbnail_size( 200, 150, true ); // Normal post thumbnails
add_image_size( 'post1-thumbnail', 320, 200, true );

///////////////////////////

add_filter( 'body_class', function( $classes ) {
    $pref = explode("/", $_SERVER['REQUEST_URI']);
    if ($pref[1] == 'search-result')
    return array_merge( $classes, array( 'search-result' ) );
    elseif ($pref[1] == 'invoice')
    return array_merge( $classes, array( 'invoice' ) );
    elseif ($pref[1] == 'thank-you')
    return array_merge( $classes, array( 'thank-you' ) );
    else
    return $classes;
} );

/////////////////////////////

add_filter( 'ajax_query_attachments_args', 'show_current_post_attachments', 10, 1 );
function show_current_post_attachments( $query = array() ) {
    add_filter( 'posts_where', 'wpse156319_posts_where', 10, 2 );
    return $query;
}

function wpse156319_posts_where( $where, $query ) {
    global $wpdb;
    $where .= $wpdb->prepare( ' AND (' . $wpdb->posts . '.post_parent = %s )', $_POST['post_id'] );
    return $where;
}

//Manage Your Media Only
function mymo_parse_query_unattach( $wp_query ) {
    if ( strpos( $_SERVER[ 'REQUEST_URI' ], '/wp-admin/upload.php' ) !== false ) {
            $wp_query->set( 'post_parent', 0 );
    }
}
add_filter('parse_query', 'mymo_parse_query_unattach' );

/***Tuning****/

add_action( 'do_meta_boxes', 'wpdocs_remove_plugin_metaboxes' );

/**
 * Remove meta boxes
 */
function wpdocs_remove_plugin_metaboxes(){
        remove_meta_box( 'locationsdiv', 'property', 'side' );
        remove_meta_box( 'slugdiv', 'property', 'normal' );
}

//////////////////////////
function get_property_cats($post_id){
    $categories_list = wp_get_post_terms($post_id, 'categories', array("fields" => "all"));
    $categories_output = '';
    $sep = ', ';
    foreach ( $categories_list as $category ) {
        $categories_output .= $category->name.$sep;
    }
    if ($categories_output){
       $categories_output = substr($categories_output, 0, -2);
    }

    return $categories_output;
}

//////// CMB2 general functions  ////////////////////////
function cmb2_render_term_tree($termid, $taxonomy, $level = 0){
    $prepend = '';
    for ($i = 0; $i< $level; $i++)
    $prepend .= '-';

    if($prepend) $prepend .= ' ';

	$single_term = get_term($termid, $taxonomy);
    $options[ $termid ] = $prepend . $single_term->name;

    $level = $level + 1;
	$childrens = get_term_children($termid, $taxonomy);
	if($childrens){
		foreach ($childrens as $term_child) {
			$arr2 = cmb2_render_term_tree($term_child, $taxonomy, $level);
            $options = $options + $arr2;
		}
	}
   return $options;
}


function cmb2_get_locations_options() {

    $terms = get_terms( array(
    'taxonomy' => 'locations',
    'hide_empty' => false,
    'parent' => 0,
    ) );

    $options = array();
    foreach ($terms as $term) {
       $arr = cmb2_render_term_tree($term->term_id, 'locations');
       $options = $options + $arr;
    }
    return $options;
}


function cmb2_get_property_options() {
    $args = array(
        'post_type'   => 'property',
        'numberposts' => -1,
    );
    $posts = get_posts( $args );
    $post_options = array();
    if ( $posts ) {
        foreach ( $posts as $post ) {
          $post_options[ $post->ID ] = $post->post_title;
        }
    }
    return $post_options;
}

function cmb2_get_policies_options() {
    $args = array(
        'post_type'   => 'page',
        'post_parent' => get_option( 'VR_BOOKING_page_policies' ),
        'numberposts' => -1,
    );
    $posts = get_posts( $args );
    $post_options = array();
    if ( $posts ) {
        foreach ( $posts as $post ) {
          $post_options[ $post->ID ] = $post->post_title;
        }
    }
    return $post_options;
}

//////////////////////////

function cmb2_get_services_options() {
    global $VRBooking_var;

    $args = array(
        'post_type'   => 'service',
        'numberposts' => -1,
    );
    $posts = get_posts( $args );
    $post_options = array();
    if ( $posts ) {
        foreach ( $posts as $post ) {
        $price = '';
        if (isset($_GET['post']))
          $price = get_post_meta($_GET['post'], '_property_service_'.$post->ID, true);
        if ($price == '')
         $price = get_post_meta($post->ID, '_service_price', true);

          $post_options[ $post->ID ] = $post->post_title.', '.__('Price(').$VRBooking_var->settings['currency'].'): <span id="service_price_'.$post->ID.'">'.$price.'</span>'.'<input class="set-price-button" name="set_price['.$post->ID.']" id="set_price_'.$post->ID.'" type="button" data-a="'.$post->ID.'" data-s="'.$post->post_title.'" value="'.__('Change price').'">';
        }
    }
    return $post_options;
}

//////////////////////////

function cmb2_get_extra_bed_options() {
    global $VRBooking_var;

    $args = array(
        'post_type'   => 'extra_bed',
        'numberposts' => -1,
    );
    $posts = get_posts( $args );
    $post_options = array();
    if ( $posts ) {
        foreach ( $posts as $post ) {
        $price = '';
        if (isset($_GET['post']))
          $price = get_post_meta($_GET['post'], '_property_extra_bed_'.$post->ID, true);
        if ($price == '')
         $price = get_post_meta($post->ID, '_extra_bed_price', true);

          $post_options[ $post->ID ] = $post->post_title.', '.__('Price(').$VRBooking_var->settings['currency'].'): <span id="extra_bed_price_'.$post->ID.'">'.$price.'</span>'.'<input class="set-price-button2" name="set_price['.$post->ID.']" id="set_price_'.$post->ID.'" type="button" data-a="'.$post->ID.'" data-s="'.$post->post_title.'" value="'.__('Change price').'">';
        }
    }
    return $post_options;
}

//////////////////////////

function cmb2_get_services_book_options() {
    global $VRBooking_var;

    if (isset($_GET['post']))
         $prop_id = get_post_meta($_GET['post'], '_booking_prop_id', true);
    else $prop_id = '';

    $args = array(
        'post_type'   => 'service',
        'numberposts' => -1,
    );
    $posts = get_posts( $args );
    $post_options = array();
    if ( $posts ) {
        foreach ( $posts as $post ) {
        $price = '';
        if ($prop_id)
         $price = get_post_meta($prop_id, '_property_service_'.$post->ID, true);
        if ($price == '')
         $price = get_post_meta($post->ID, '_service_price', true);

          $post_options[ $post->ID ] = $post->post_title.', '.__('Price(').$VRBooking_var->settings['currency'].'): <span id="service_price_'.$post->ID.'">'.$price.'</span>';
        }
    }
    return $post_options;
}

////////////////////////////////////

function cmb2_get_extra_bed_book_options() {
    global $VRBooking_var;

    if (isset($_GET['post']))
         $prop_id = get_post_meta($_GET['post'], '_booking_prop_id', true);
    else $prop_id = '';

    $args = array(
        'post_type'   => 'extra_bed',
        'numberposts' => -1,
    );
    $posts = get_posts( $args );
    $post_options = array();
    if ( $posts ) {
        foreach ( $posts as $post ) {
        $price = '';
        if ($prop_id)
         $price = get_post_meta($prop_id, '_property_extra_bed_'.$post->ID, true);
        if ($price == '')
         $price = get_post_meta($post->ID, '_extra_bed_price', true);

          $post_options[ $post->ID ] = $post->post_title.', '.__('Price(').$VRBooking_var->settings['currency'].'): <span id="extra_bed_price_'.$post->ID.'">'.$price.'</span>';
        }
    }
    return $post_options;
}

////////////////////////////////////

add_action( 'admin_head-post.php', 'set_price_custom_button' );
add_action( 'admin_head-post-new.php', 'set_price_custom_button' );
function set_price_custom_button(){
    global $current_screen;
    global $VRBooking_var;

    // Not our post type, exit earlier
    if( 'property' != $current_screen->post_type )
        return;

echo '<div id="assign_price"><span id="modal_close"><i class="fa fa-remove"></i></span>
      <h2>Assign price to this service:</h2>
      <h3></h3>
      <div>
      <label for="service_price_prop">Set new price: </label>
      <input id="service_price_prop" name="service_price_prop" type="text" value="">
      <button id="service-price-prop-button" type="button" title="Assign">Assign</button>
      </div>
      <div id="ap_res">
      </div>
    </div>
  <div id="overlay"></div>';

}

/////////// admin columns  ////////////////

add_filter('manage_booking_posts_columns', 'booking_table_head');
function booking_table_head( $defaults ) {
    global $VRBooking_var;
    $defaults['date_created']   = 'Date of Booking';
    $defaults['appartment']  = 'Property';          
    $defaults['date_from']    = 'Date from';
    $defaults['date_to']   = 'Date to';
    $defaults['price'] = 'Total amount, '.$VRBooking_var->settings['currency'];
    if ( isset( $_GET['subpage'] ))
    $defaults['invoice'] = '';

    unset($defaults['date']);
    return $defaults;
}

add_action( 'manage_booking_posts_custom_column', 'booking_table_content', 10, 2 );
function booking_table_content( $column_name, $post_id ) {
    if ($column_name == 'appartment') {
    $app_id = get_post_meta( $post_id, '_booking_prop_id', true );
      echo  get_the_title($app_id);
    }
    if ($column_name == 'date_created') {
      if (get_post_meta( $post_id, '_booking_created', true ))
    echo date( "d/m/Y", get_post_meta( $post_id, '_booking_created', true ));
    }
    if ($column_name == 'date_from') {
      if (get_post_meta( $post_id, '_booking_date_from', true ))
    echo date( "d/m/Y", get_post_meta( $post_id, '_booking_date_from', true ));
    }
    if ($column_name == 'date_to') {
      if (get_post_meta( $post_id, '_booking_date_to', true ))
    echo date( "d/m/Y", get_post_meta( $post_id, '_booking_date_to', true ));
    }

    if ($column_name == 'price') {
    echo get_post_meta( $post_id, '_booking_price', true );
    }

    if (($column_name == 'invoice')&&( isset( $_GET['subpage'] ))) {
    $home_url = get_home_url();
    $Path='/invoice/'.get_the_title($post_id);
    echo '<button class="invoice-button" type="button" data-u="'.$home_url.$Path.'" title="Invoice">Invoice</button>';
    }
}

add_filter('manage_rate_posts_columns', 'price_table_head');
function price_table_head( $defaults ) {
//    $defaults['appartment']  = 'Appartment';
    $defaults['color']    = 'Color';
    $defaults['price_sun'] = 'Sun';
    $defaults['price_mon'] = 'Mon';
    $defaults['price_tue'] = 'Tue';
    $defaults['price_wed'] = 'Wed';
    $defaults['price_thu'] = 'Thu';
    $defaults['price_fri'] = 'Fri';
    $defaults['price_sat'] = 'Sat';
    $defaults['price_fri_sat'] = 'Fri + Sat';
    $defaults['price_7'] = '7 nights';
    unset($defaults['language']);
    return $defaults;
}

add_action( 'manage_rate_posts_custom_column', 'price_table_content', 10, 2 );
function price_table_content( $column_name, $post_id ) {
    global $VRBooking_var;

    if ($column_name == 'appartment') {
    $app_id = get_post_meta( $post_id, '_price_prop_id', true );
      echo  get_the_title($app_id);
    }
    if ($column_name == 'date_from') {
    echo date( "d/m/Y", get_post_meta( $post_id, '_price_date_from', true ));
    }
    if ($column_name == 'date_to') {
    echo date( "d/m/Y", get_post_meta( $post_id, '_price_date_to', true ));
    }

    if ($column_name == 'price') {
    echo get_post_meta( $post_id, '_price_price', true );
    }

    if ($column_name == 'color') {
    echo '<div style="background-color:'.get_post_meta( $post_id, '_price_color', true ).'; color:#fff;">'.get_post_meta( $post_id, '_price_color', true ).'</div>';
    }

    if ($column_name == 'price_sun') {
    echo $VRBooking_var->format_currency(get_post_meta( $post_id, '_price_price_sun', true ));
    }
    if ($column_name == 'price_mon') {
    echo $VRBooking_var->format_currency(get_post_meta( $post_id, '_price_price_mon', true ));
    }
    if ($column_name == 'price_tue') {
    echo $VRBooking_var->format_currency(get_post_meta( $post_id, '_price_price_tue', true ));
    }
    if ($column_name == 'price_wed') {
    echo $VRBooking_var->format_currency(get_post_meta( $post_id, '_price_price_wed', true ));
    }
    if ($column_name == 'price_thu') {
    echo $VRBooking_var->format_currency(get_post_meta( $post_id, '_price_price_thu', true ));
    }
    if ($column_name == 'price_fri') {
    echo $VRBooking_var->format_currency(get_post_meta( $post_id, '_price_price_fri', true ));
    }
    if ($column_name == 'price_sat') {
    echo $VRBooking_var->format_currency(get_post_meta( $post_id, '_price_price_sat', true ));
    }
    if ($column_name == 'price_fri_sat') {
    echo $VRBooking_var->format_currency(get_post_meta( $post_id, '_price_price_fri_sat', true ));
    }
    if ($column_name == 'price_7') {
    echo $VRBooking_var->format_currency(get_post_meta( $post_id, '_price_price_7', true ));
    }
}

////////////////////////////

function floatvaldec($v, $dec=',') {
  return floatval(preg_replace("/[,]/" , "." , preg_replace("/[^-0-9$dec]/","",$v)));
}

///////////////////////////

function get_excerpt_by_id($post_id, $length){
    $the_post = get_post($post_id); //Gets post ID
    $the_excerpt = $the_post->post_content; //Gets post_content to be used as a basis for the excerpt
    $excerpt_length = $length; //Sets excerpt length by word count
    $the_excerpt = strip_tags(strip_shortcodes($the_excerpt), '<p><ul><li><strong><a>'); //Strips tags and images
    $words = explode(' ', $the_excerpt, $excerpt_length + 1);

    if(count($words) > $excerpt_length) :
        array_pop($words);
       // array_push($words, '…');
        $the_excerpt = implode(' ', $words);
    endif;

    $the_excerpt = '<p>'.closetags($the_excerpt).'</p>';

    return $the_excerpt;
}

function closetags($html) {
    preg_match_all('#<(?!meta|img|br|hr|input\b)\b([a-z]+)(?: .*)?(?<![/|/ ])>#iU', $html, $result);
    $openedtags = $result[1];
    preg_match_all('#</([a-z]+)>#iU', $html, $result);
    $closedtags = $result[1];
    $len_opened = count($openedtags);
    if (count($closedtags) == $len_opened) {
        return $html;
    }
    $openedtags = array_reverse($openedtags);
    for ($i=0; $i < $len_opened; $i++) {
        if (!in_array($openedtags[$i], $closedtags)) {
            $html .= '</'.$openedtags[$i].'>';
        } else {
            unset($closedtags[array_search($openedtags[$i], $closedtags)]);
        }
    }
    return $html;
}

//////////////////////////////

//add_filter( 'views_edit-booking', 'booking_add_button_to_views' );
function booking_add_button_to_views( $views )
{
    $home_url = get_home_url();
    $Path='/invoice/';
    $views['my-button'] = '<button id="invoice-button" type="button" data-u="'.$home_url.$Path.'" title="Invoice">Invoice</button>';
    return $views;
}

add_action( 'admin_head-edit.php', 'booking_move_custom_header' );
function booking_move_custom_header()
{
    global $current_screen;

    // Not our post type, exit earlier
    if(( 'booking' != $current_screen->post_type )||( !isset( $_GET['subpage'] )))
    return;
    ?>
    <script type="text/javascript">
        jQuery(document).ready( function($)
        {
            $('#wpbody-content h1').html("<?php echo 'My Invoices';?>");
        });
    </script>
    <?php
}

function booking_move_custom_button()
{
    global $current_screen;

    // Not our post type, exit earlier
    if( 'booking' != $current_screen->post_type )
        return;
    ?>
    <script type="text/javascript">
        jQuery(document).ready( function($)
        {
            $('#invoice-button').appendTo(".wrap h1");
        });
    </script>
    <?php
}
//////////////////////////////
add_filter( 'views_edit-rate', 'rate_add_button_to_views' );
function rate_add_button_to_views( $views )
{
    $views['my-button'] = '<button id="assign-rates-button" type="button"  title="Assign rates">Assign Rates</button>';
    return $views;
}

add_action( 'admin_head-edit.php', 'rate_move_custom_button' );
function rate_move_custom_button()
{
    global $current_screen;
    global $VRBooking_var;

    // Not our post type, exit earlier
    if( 'rate' != $current_screen->post_type )
        return;
    ?>
    <script type="text/javascript">
        jQuery(document).ready( function($)
        {
            $('#assign-rates-button').appendTo(".wrap h1");
        });
    </script>
    <?php

    $apartments_arr = cmb2_get_property_options();

    $tmp = '';

    foreach($apartments_arr as $ap_id => $ap_title){
        $tmp .= '<option value="'.$ap_id.'">'.__($ap_title).'</option>';
    }

    if ($tmp) $tmp = '<label for="select_ap">Select property: </label><select size="1" id="select_ap" name="select_ap">'.$tmp.'</select>';

    $rates_list = '';
    $rates_select = '';
    $rates_table = '';
    $rates_arr = booking_get_rates();
     foreach($rates_arr as $i => $rate){
        $rates_list .= '<span style="background-color:'.$rate['color'].';"></span>'.__($rate['title']);
        $rates_select .= '<option value="'.$rate['id'].'">'.__($rate['title']).'</option>';

        if ($i === 0) $tr_class = ' class="active"';
        else $tr_class = '';

        $rates_table .= '<tr'.$tr_class.' data-i="'.$rate['id'].'"><td class="rate_color" style="background-color:'.$rate['color'].';"></td><td>'.$VRBooking_var->format_currency($rate['price_sun']).'</td><td>'.$VRBooking_var->format_currency($rate['price_mon']).'</td><td>'.$VRBooking_var->format_currency($rate['price_tue']).'</td><td>'.$VRBooking_var->format_currency($rate['price_wed']).'</td><td>'.$VRBooking_var->format_currency($rate['price_thu']).'</td><td>'.$VRBooking_var->format_currency($rate['price_fri']).'</td><td>'.$VRBooking_var->format_currency($rate['price_sat']).'</td><td>'.$VRBooking_var->format_currency($rate['price_fri_sat']).'</td><td>'.$VRBooking_var->format_currency($rate['price_7']).'</td></tr>';
     }

     if ($rates_select){
         $rates_select = '<label for="select_rate"></label><select size="1" id="select_rate" name="select_rate">'.$rates_select.'</select>';
         $rates_table = '<table><thead><tr class="tr_header"><th class="rate_color"></th><th>Sun</th><th>Mon</th><th>Tue</th><th>Wed</th><th>Thu</th><th>Fri</th><th>Sat</th><th>Fri + Sat</th><th>7 nights</th></tr></thead><tbody>'.$rates_table.'</tbody></table>';
     }

    echo '<div id="assign_rates"><span id="modal_close"><i class="fa fa-remove"></i></span>
      <h2>Assign Rates</h2>
      <div>
      '.$tmp.'
      <button id="select-ap-button" type="button" title="Select">Select</button>
      </div><br>
      <div id="ap_res">
      <label for="general_price">Set standart rate, '.$VRBooking_var->settings['currency'].' per day: </label>
      <input id="general_price" name="general_price" type="text" value="">
      <button id="update-general-button" type="button" title="Update">Update standart rate</button>
      <h3 id="ap_title"></h3>
      <label for="range_rates_from">Start date (dd/mm/yyyy)</label>
      <input id="range_rates_from" name="range_rates_from" type="text" value="">
      <label for="range_rates_to">End date (dd/mm/yyyy)</label>
      <input id="range_rates_to" name="range_rates_to" type="text" value="">
      </div>
      <div id="ap_res_calendar">
      </div>
      <div id="rates_list">Rates: '.$rates_list.'</div>
      <div><span id="choose_rate_span">Choose a Rate</span></div>
      <div id="choose_rate">
      '.$rates_select.$rates_table.'
      </div>
      <button id="assign-button" type="button" title="Assign">Assign</button>
    </div>
  <div id="overlay"></div>';

}

function booking_get_rates() {
    $args = array(
        'post_type'   => 'rate',
        'numberposts' => -1,
    );

    $posts = get_posts( $args );

    $post_options = array();
    if ( $posts ) {
        $i = 0;
        foreach ( $posts as $post ) {
          $post_options[$i][ 'id' ] = $post->ID;
          $post_options[$i][ 'title' ] = $post->post_title;
          $post_options[$i][ 'color' ] = get_post_meta( $post->ID, '_price_color', true );
          $post_options[$i][ 'price_sun' ] = get_post_meta( $post->ID, '_price_price_sun', true );
          $post_options[$i][ 'price_mon' ] = get_post_meta( $post->ID, '_price_price_mon', true );
          $post_options[$i][ 'price_tue' ] = get_post_meta( $post->ID, '_price_price_tue', true );
          $post_options[$i][ 'price_wed' ] = get_post_meta( $post->ID, '_price_price_wed', true );
          $post_options[$i][ 'price_thu' ] = get_post_meta( $post->ID, '_price_price_thu', true );
          $post_options[$i][ 'price_fri' ] = get_post_meta( $post->ID, '_price_price_fri', true );
          $post_options[$i][ 'price_sat' ] = get_post_meta( $post->ID, '_price_price_sat', true );
          $post_options[$i][ 'price_fri_sat' ] = get_post_meta( $post->ID, '_price_price_fri_sat', true );
          $post_options[$i][ 'price_7' ] = get_post_meta( $post->ID, '_price_price_7', true );
          $i++;
        }
    }

    return $post_options;
}

////////////////////////////////

