<?php
/*
 * The Template for ical-feed.
 *
 */
 Global $VRBooking_var;
 
    require_once VR_BOOKING_PLUGIN_DIR . '/includes/icalendar/zapcallib.php';
    
    $ical_prop = get_query_var( 'ical_prop' );
    
   $now = time() - 60*24*60*60;
   $to =  time() + 2*365*24*60*60;
   $begin_date = date("Y-m-d", $now);
   $end_date = date("Y-m-d", $to);

   $args = array(
               'post_type' => 'booking',
               'posts_per_page'=> -1,
               'meta_query' => array(
                  'relation' => 'AND',
                  'date_from' => array(
                    'key'     => '_booking_date_from',
                    'value'   => $to,
                    'compare' => '<=',
                    'type'    => 'NUMERIC',
                    ),
                  'date_to' => array(
                    'key'     => '_booking_date_to',
                    'value'   => $now,
                    'compare' => '>=',
                    'type'    => 'NUMERIC',
                    ),
                  'ap_id' => array(
                    'key'     => '_booking_prop_id',
                    'value'   => $ical_prop,
                    'compare' => '=',
                    ),
                   ),
                  );    

// create the ical object
$icalobj = new ZCiCal();
    
$the_query = new WP_Query( $args );
     while ( $the_query->have_posts() ) : $the_query->the_post();
      $event_start = date("Y-m-d", get_post_meta( get_the_ID(), '_booking_date_from', true ));
      $event_end = date("Y-m-d", get_post_meta( get_the_ID(), '_booking_date_to', true ));
      $title = "Booked";
      
 // create the event within the ical object
$eventobj = new ZCiCalNode("VEVENT", $icalobj->curnode);
// add title
$eventobj->addNode(new ZCiCalDataNode("SUMMARY:" . $title));
// add start date
$eventobj->addNode(new ZCiCalDataNode("DTSTART:" . ZCiCal::fromSqlDateTime($event_start)));
// add end date
$eventobj->addNode(new ZCiCalDataNode("DTEND:" . ZCiCal::fromSqlDateTime($event_end)));
// UID is a required item in VEVENT, create unique string for this event
// Adding your domain to the end is a good way of creating uniqueness
$uid = date('Y-m-d-H-i-s') . "@vrbooking.icalendar.away";
$eventobj->addNode(new ZCiCalDataNode("UID:" . $uid));
// DTSTAMP is a required item in VEVENT
$eventobj->addNode(new ZCiCalDataNode("DTSTAMP:" . ZCiCal::fromSqlDateTime()));
// Add description
$eventobj->addNode(new ZCiCalDataNode("Description:" . ZCiCal::formatContent(
	"booked")));

     endwhile;
wp_reset_postdata();    
    //echo 'Here is iCal feed for property id = '.$ical_prop;
// write iCalendar feed to stdout
echo $icalobj->export();

    exit();
?>