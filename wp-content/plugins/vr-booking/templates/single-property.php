<?php
/**
 * The Template for displaying all single properties.
 */
 
// No direct access, please
if ( ! defined( 'ABSPATH' ) ) exit;

get_header(); ?>

	<div id="primary">
		<main id="main">
		<?php do_action('generate_before_main_content'); ?>
		<?php while ( have_posts() ) : the_post(); ?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="inside-article">

		<header class="entry-header">
			<?php do_action( 'generate_before_entry_title'); ?>
				<?php the_title( '<h1 class="entry-title" itemprop="headline">', '</h1>' );

                global $VRBooking_var;

                if ($VRBooking_var->settings['reviews_active'])
                echo do_shortcode('[RICH_REVIEWS_SNIPPET category="page"]');


                $categories_output = get_property_cats(get_the_ID());
                if ($categories_output){
                     $categories_output = '<div id="property_cats">'.__('Property categories: ', VR_BOOKING_TEXTDOMAIN).$categories_output.'</div>';
                }
                   echo $categories_output;

                 ?>

			<?php do_action( 'generate_after_entry_title'); ?>
		</header><!-- .entry-header -->

		<?php do_action( 'generate_after_entry_header'); ?>
		<div class="entry-content" itemprop="text">
			<?php //the_content();

               echo do_shortcode('[booking-property]');

            ?>

			<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'booking' ),
				'after'  => '</div>',
			) );
			?>
		</div><!-- .entry-content -->

		<?php do_action( 'generate_after_entry_content' ); ?>
		<?php do_action( 'generate_after_content' ); ?>
	</div><!-- .inside-article -->
</article><!-- #post-## -->

			<?php
				// If comments are open or we have at least one comment, load up the comment template
				if ( comments_open() || '0' != get_comments_number() ) : ?>
					<div class="comments-area">
						<?php comments_template(); ?>
					</div>
			<?php endif; ?>

		<?php endwhile; // end of the loop. ?>
		<?php do_action('generate_after_main_content'); ?>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php 
do_action('generate_sidebars');
get_footer();