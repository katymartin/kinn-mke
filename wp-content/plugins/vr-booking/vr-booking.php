<?php
/**
 * @wordpress-plugin
 * Plugin Name:       VR Booking
 * Plugin URI: https://www.myhotelzone.com/wordpress-booking-plugin-vacation-holiday-rentals
 * Description: VR Booking, 2 in 1 WordPress Booking Plugin for property rentals, is an advanced solution for all types of property rental businesses, including vacation and apartment rentals, holiday houses and villas, cabins and cottages, timeshares and AirBnB hosts, etc. This customizable solution allows property rental business owners to boost bookings on their websites, efficiently manage business with PMS and significantly increase revenue.
 * Version:           1.1
 * Author:            MyHotelZone
 * Author URI:        https://themeforest.net/user/myhotelzone
 * License: GPLv2 or later
 * License URI: http://www.gnu.org/licenses/gpl-2.0.html
 */

if ( ! defined( 'ABSPATH' ) )
	exit;

define( 'VR_BOOKING_VERSION', '1.1.1' );
define( 'VR_BOOKING_PLUGIN', __FILE__ );
define( 'VR_BOOKING_PLUGIN_DIR', untrailingslashit( dirname( VR_BOOKING_PLUGIN ) ) );
define( 'VR_BOOKING_TEXTDOMAIN', 'booking' );

if ( file_exists(  VR_BOOKING_PLUGIN_DIR . '/includes/plugins/cmb2/init.php' ) ) {
  require_once  VR_BOOKING_PLUGIN_DIR . '/includes/plugins/cmb2/init.php';
}

include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
if (!is_plugin_active('rich-reviews/rich-reviews.php')) {
	require_once  VR_BOOKING_PLUGIN_DIR . '/includes/plugins/rich-reviews/rich-reviews.php';
}

include_once VR_BOOKING_PLUGIN_DIR . '/includes/functions.php';

if ( is_admin() ) {
   	include_once VR_BOOKING_PLUGIN_DIR . '/admin/booking-admin.php';
}

include_once VR_BOOKING_PLUGIN_DIR . '/vr-booking-main.php';


