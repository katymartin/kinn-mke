<?php

//     Plugin Main class

if ( ! defined( 'ABSPATH' ) )
	exit;

/////// make our main plugin class

class VRBooking {
    public $settings = array();
    public $markers_urls = array();
    public function __construct() {

        global $wpdb;
        $this->db = $wpdb;

        define( 'BOOKING_TABLE_RATES', $this->db->prefix . 'booking_acc_rates' );

        $this->home_url = get_home_url();

        add_action( 'plugins_loaded', array( $this, 'load_textdomain' ));
        add_action( 'wp_enqueue_scripts', array( $this, 'enqueued_assets') );

        add_action( 'trash_rate', array( $this, 'delete_rate_from_db'), 1, 1);

        add_filter( 'redirect_canonical', array( $this, 'disable_my_canonical_redirect_for_front_page' ), 10, 2);
        add_filter( 'query_vars', array( $this, 'register_query_var') );
        //add rewrite rules in case another plugin flushes rules
        // Call when plugin is initialized on every page load
        add_action( 'init', array( $this, 'rewrite_rule'), 10, 0 );

        add_action( 'init', array( $this, 'create_booking_post_type'), 0);
        add_action( 'init', array( $this, 'create_rate_post_type'), 0);
        add_action( 'init', array( $this, 'create_property_post_type'), 0);
        add_action( 'init', array( $this, 'create_services_post_type'), 0);
        add_action( 'init', array( $this, 'create_extra_bed_post_type'), 0);

// This filter replaces a complete file from the parent theme or child theme with your file (in this case the archive page).
// Whenever the archive is requested, it will use YOUR archive.php instead of that of the parent or child theme.
//add_filter ('single-property_template', create_function ('', 'return plugin_dir_path(__FILE__)."templates/single-property.php";'));

        add_filter( 'single_template',  array( $this, 'custom_property_template') );

        add_action( 'cmb2_admin_init', array( $this, 'cmb2_metaboxes') );

        add_action( 'template_redirect', array( $this, 'redirect_after_post'));

        add_action('wp_ajax_assign_rate', array( $this, 'assign_rate_callback'));
        add_action('wp_ajax_select_ap', array( $this, 'select_ap_callback'));
        add_action('wp_ajax_select_ap2', array( $this, 'select_ap2_callback'));
        add_action('wp_ajax_update_general_p', array( $this, 'update_general_p_callback'));
        add_action('wp_ajax_get_reserv_code', array( $this, 'get_reserv_code_callback'));
        add_action('wp_ajax_get_amount_booking', array( $this, 'get_amount_booking_callback'));

        add_action('wp_ajax_check_ap', array( $this, 'check_ap_callback'));
        add_action('wp_ajax_nopriv_check_ap', array( $this, 'check_ap_callback'));

        add_action('wp_footer', array( $this, 'add_chat_script_footer'));
        
        add_filter( 'cron_schedules', array( $this, 'cron_add_ten_min' ));
        add_action( 'add_booking_event', array( $this, 'add_booking_event'));

    //    add_action('wp_ajax_nopriv_get_ext_url', array( $this, 'get_ext_url_callback'));

        add_action( 'save_post', array( $this, 'update_booking_title'), 10, 3 );
        add_action( 'save_post', array( $this, 'update_property_location'), 10, 3 );

        register_activation_hook( VR_BOOKING_PLUGIN, array( $this, 'install') );
        register_deactivation_hook( VR_BOOKING_PLUGIN, array( $this, 'deactivation') );

        add_shortcode( 'booking-home', array( $this, 'home_page_shortcode') );
        add_shortcode( 'booking-search', array( $this, 'search_page_shortcode') );
        add_shortcode( 'booking-services', array( $this, 'services_page_shortcode') );
        add_shortcode( 'booking-booking', array( $this, 'booking_page_shortcode') );
        add_shortcode( 'booking-thanks', array( $this, 'thanks_page_shortcode') );
        add_shortcode( 'booking-invoice', array( $this, 'invoice_page_shortcode') );

        add_shortcode( 'booking-property', array( $this, 'property_page_shortcode') );

        /**************** init settings *******************/
       $this->settings = get_option('booking_settings');
       if(empty($this->settings)) {
       $this->settings['tax'] = '';
       $this->settings['paypal_activate'] = 0;
       $this->settings['stripe_activate'] = 0;
       $this->settings['paypal_sandbox'] = 1;
       $this->settings['stripe_sandbox'] = 1;
       $this->settings['paypal_email'] = '';
       $this->settings['stripe_test_secret_key'] = '';
       $this->settings['stripe_test_publishable_key'] = '';
       $this->settings['stripe_live_secret_key'] = '';
       $this->settings['stripe_live_publishable_key'] = '';
       $this->settings['currency'] = '$';
       $this->settings['currency_code'] = 'USD';
       $this->settings['currency_place'] = 1;
       $this->settings['deposit_active'] = 0;
       $this->settings['deposit_amount'] = '';
       $this->settings['payment_mode1'] = 1;
       $this->settings['payment_mode2'] = '';
       $this->settings['payment_mode3'] = '';
       $this->settings['invoice_title'] = '';
       $this->settings['invoice_logo'] = '';
       $this->settings['invoice_header'] = '';
       $this->settings['invoice_footer'] = '';
       $this->settings['add_accept_term'] = 1;
       $this->settings['add_accept_term_adds'] = 1;
       $this->settings['print_button'] = 0;
       $this->settings['confirm_email_header'] = __('Thank you for your request, one of our staff will respond shortly.', VR_BOOKING_TEXTDOMAIN);
       $this->settings['confirm_email_subject'] = __('You have new reservation', VR_BOOKING_TEXTDOMAIN);
       $this->settings['confirm_email'] = get_bloginfo('admin_email');
       $this->settings['chat_active'] = 0;
       $this->settings['chat_id'] = '';
       $this->settings['reviews_active'] = 1;
       $this->settings['reviews_mode'] = 2;
       $this->settings['google_map_active'] = 1;
       $this->settings['google_api'] = '';
       $this->settings['map_zoom'] = 13;
       $this->settings['map_marker'] = 2;
       $this->settings['max_select_adults'] = 7;
       $this->settings['max_select_children'] = 7;
       $this->settings['max_select_rooms'] = 7;
       $this->settings['search_res_front'] = 6;
       $this->settings['search_res_other'] = 10;
       $this->settings['color_button'] = '#81d742';
       update_option('booking_settings', $this->settings);
       }

       //////////// $markers_urls ///////
       $this->markers_urls[1] = 'css/img/mm-blue.png';
       $this->markers_urls[2] = 'css/img/mm-green.png';
       $this->markers_urls[3] = 'css/img/mm-l-blue.png';
       $this->markers_urls[4] = 'css/img/mm-orange.png';
       $this->markers_urls[5] = 'css/img/mm-purple.png';
       $this->markers_urls[6] = 'css/img/mm-red.png';
       $this->markers_urls[7] = 'css/img/mm-turquoise.png';
       $this->markers_urls[8] = 'css/img/mm-violet.png';
       $this->markers_urls[9] = 'css/img/mm-yellow.png';
    }

public function install() {
    
    global $wpdb;

	$table_name = $wpdb->prefix . 'booking_acc_rates';

    if (!empty ($wpdb->charset))
        $charset_collate = "DEFAULT CHARACTER SET {$wpdb->charset}";
    if (!empty ($wpdb->collate))
        $charset_collate .= " COLLATE {$wpdb->collate}";

	$sql = "CREATE TABLE IF NOT EXISTS {$table_name} (
           id bigint(20) NOT NULL AUTO_INCREMENT ,
           ap_id bigint(20) ,
           rate_id bigint(20) ,
           range_from date DEFAULT NULL ,
           range_to date DEFAULT NULL ,
           except_price float DEFAULT 0 ,
	   	   UNIQUE KEY id (id)
        ) {$charset_collate};";
    
    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

	dbDelta($sql);

    $this->create_page( 'search-result', 'VR_BOOKING_page_search_result', __('Available properties', 'booking'), '[booking-search]');
    $this->create_page( 'booking', 'VR_BOOKING_page_booking', __('Booking', VR_BOOKING_TEXTDOMAIN), '[booking-booking]');
    $this->create_page( 'services', 'VR_BOOKING_page_services', __('Services', VR_BOOKING_TEXTDOMAIN), '[booking-services]');
    $this->create_page( 'thank-you', 'VR_BOOKING_page_thank_you', __('Confirmation', VR_BOOKING_TEXTDOMAIN), '<h3>'.__('Thank you for your request, one of our staff will respond shortly.', VR_BOOKING_TEXTDOMAIN).'</h3>
[booking-thanks]');
    $this->create_page( 'invoice', 'VR_BOOKING_page_invoice', __('Invoice', VR_BOOKING_TEXTDOMAIN), '[booking-invoice]');
    $this->create_page( 'terms-and-conditions', 'VR_BOOKING_page_terms', __('Terms and Conditions', VR_BOOKING_TEXTDOMAIN), '');
    $this->create_page( 'additional-conditions', 'VR_BOOKING_page_terms_adds', __('Additional conditions', VR_BOOKING_TEXTDOMAIN), '');
    $this->create_page( 'policies', 'VR_BOOKING_page_policies', __('Policies', VR_BOOKING_TEXTDOMAIN), '');
    $this->create_page( 'cancellation-policy', 'VR_BOOKING_page_policy_cancel', __('Cancellation', VR_BOOKING_TEXTDOMAIN), '', get_option( 'VR_BOOKING_page_policies' ));
    $this->create_page( 'check-in-policy', 'VR_BOOKING_page_policy_checkin', __('Check in', VR_BOOKING_TEXTDOMAIN), '', get_option( 'VR_BOOKING_page_policies' ));
    $this->create_page( 'check-out-policy', 'VR_BOOKING_page_policy_checkout', __('Check out', VR_BOOKING_TEXTDOMAIN), '', get_option( 'VR_BOOKING_page_policies' ));
 //   $this->create_page( 'price-includes', 'VR_BOOKING_page_price_includes', __('Price includes', VR_BOOKING_TEXTDOMAIN), '');
 
 
   $this->create_booking_post_type();
   $this->create_rate_post_type();
   $this->create_property_post_type();
   $this->create_services_post_type();
    $this->rewrite_rule();
   // flush_rewrite_rules();
    
    global $wp_rewrite;
    $wp_rewrite->flush_rules();
    
    wp_schedule_event( time(), 'ten_min', 'add_booking_event' );
}

function deactivation() {
   	flush_rewrite_rules();
 /* 
    wp_delete_post(get_option('VR_BOOKING_page_search_result'));
    wp_delete_post(get_option('VR_BOOKING_page_booking'));
    wp_delete_post(get_option('VR_BOOKING_page_thank_you'));
    wp_delete_post(get_option('VR_BOOKING_page_invoice')); 
 //   wp_delete_post(get_option('terms-and-conditions'));
    delete_option('VR_BOOKING_page_search_result');
    delete_option('VR_BOOKING_page_booking');
    delete_option('VR_BOOKING_page_thank_you');
    delete_option('VR_BOOKING_page_invoice');  
    */
 //   delete_option('VR_BOOKING_page_price_includes'); 
 
     wp_clear_scheduled_hook( 'add_booking_event' );
}

// add 10 min interval
function cron_add_ten_min( $schedules ) {
	$schedules['ten_min'] = array(
		'interval' => 60 * 10,
		'display' => 'One time per 10 min'
	);
	return $schedules;
}

function delete_rate_from_db ( $pid ) {
    global $wpdb;
    if ( $wpdb->get_var( $wpdb->prepare( 'SELECT rate_id FROM '.BOOKING_TABLE_RATES.' WHERE rate_id = %d', $pid ) ) ) {
        $wpdb->query( $wpdb->prepare( 'DELETE FROM '.BOOKING_TABLE_RATES.' WHERE rate_id = %d', $pid ) );
    }
}

function disable_my_canonical_redirect_for_front_page( $redirect_url, $requested_url ) {
    if( is_front_page() )
        return $requested_url;
    else
        return $redirect_url;
}

function create_page( $slug, $option, $page_title = '', $page_content = '', $post_parent = 0 ) {
    global $wpdb;
    $option_value = get_option( $option );
    if ( $option_value > 0 && get_post( $option_value ) )
      return;
    $page_found = $wpdb->get_var("SELECT ID FROM " . $wpdb->posts . " WHERE post_name = '$slug' LIMIT 1;");
    if ( $page_found ) :
      if ( ! $option_value )
        update_option( $option, $page_found );
      return;
    endif;
    $page_data = array(
          'post_status' 		=> 'publish',
          'post_type' 		=> 'page',
          'post_author' 		=> 1,
          'post_name' 		=> $slug,
          'post_title' 		=> $page_title,
          'post_content' 		=> $page_content,
          'post_parent' 		=> $post_parent,
          'comment_status' 	=> 'closed'
      );
      $page_id = wp_insert_post( $page_data );
      update_option( $option, $page_id );
}

////////////////////////////////////////////

function load_textdomain() {
      load_plugin_textdomain( VR_BOOKING_TEXTDOMAIN,  false, VR_BOOKING_PLUGIN_DIR . '/languages' );
}

function enqueued_assets() {
      wp_enqueue_style( 'datetimepicker-style', plugins_url( "js/jquery.datetimepicker.css", VR_BOOKING_PLUGIN ));
      wp_enqueue_script( 'datetimepicker-js', plugins_url( "js/jquery.datetimepicker.full.min.js", VR_BOOKING_PLUGIN ), array('jquery'), '1.0', true );
     wp_enqueue_script( 'jqvalidate-js', plugins_url( "js/jquery.validate.min.js", VR_BOOKING_PLUGIN ), array('jquery'), '1.0', true );
     // //wp_enqueue_script( 'jqvalidate-local-js', plugins_url( "js/localization/messages_es.min.js", VR_BOOKING_PLUGIN ), array('jquery'), '1.0', true );

     //wp_enqueue_script( 'number-min', plugins_url( "js/jquery.number.min.js", VR_BOOKING_PLUGIN ), array('jquery'), '1.0', true );

     wp_enqueue_script( 'cycle2-js', plugins_url( "js/jquery.cycle2.min.js", VR_BOOKING_PLUGIN ), array('jquery'), '1.0', true );

     wp_enqueue_script( 'cycle2-swipe-js', plugins_url( "js/jquery.cycle2.swipe.min.js", VR_BOOKING_PLUGIN ), array('jquery'), '1.0', true );

     wp_enqueue_script( 'cycle2-ios6fix-js', plugins_url( "js/ios6fix.js", VR_BOOKING_PLUGIN ), array('jquery'), '1.0', true );

     wp_enqueue_script( 'lightslider-js', plugins_url( "js/lightslider.min.js", VR_BOOKING_PLUGIN ), array('jquery'), '1.0', true );

     wp_enqueue_script( 'lightgallery-js', plugins_url( "js/lightgallery-all.min.js", VR_BOOKING_PLUGIN ), array('jquery'), '1.0', true );

    $pref = explode("/", $_SERVER['REQUEST_URI']);
     if ($pref[1] == 'booking'){
        wp_enqueue_script( 'stripe', 'https://checkout.stripe.com/checkout.js', '', '1.0', true );
        wp_enqueue_script( 'stripe-custom-js', plugins_url( "js/stripe_checkout.js", VR_BOOKING_PLUGIN ), array('jquery'), '1.0', true );

            if ($this->settings['stripe_sandbox'])
        	$stripe_params = array(
			 'key' => $this->settings['stripe_test_publishable_key'],
		    );
            else
            $stripe_params = array(
			 'key' => $this->settings['stripe_live_publishable_key'],
		    );

		wp_localize_script( 'stripe-custom-js', 'wc_stripe_params', $stripe_params );
     }

     wp_enqueue_script( 'my-custom-js', plugins_url( "js/booking-settings.js", VR_BOOKING_PLUGIN ), array('jquery'), '1.0', true );

     wp_localize_script( 'my-custom-js', 'lst', array(
            'ajax_url' => admin_url( 'admin-ajax.php' ),
            'nonce' => wp_create_nonce('lst-nonce')
         )
        );

   //   wp_enqueue_style('fontawesome2', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css', '', '4.6.3', 'all');

     wp_enqueue_style( 'lightslider-style', plugins_url( "js/lightslider.min.css", VR_BOOKING_PLUGIN ));

     wp_enqueue_style( 'lightgallery-style', plugins_url( "js/lightgallery.min.css", VR_BOOKING_PLUGIN ));

     wp_enqueue_style( 'booking-style', plugins_url( "css/booking.css", VR_BOOKING_PLUGIN ));

     $color = $this->settings['color_button'];
        $custom_css = "
                input[type=\"button\"].bc-button-color, input[type=\"submit\"].bc-button-color, .rr_form_input input[type=\"submit\"], #pager span.cur_page, #app-page .doc_tab.cur_tab {
                        background-color: {$color};
                }
                #sort_by_block{
                   background-color: {$color};
                   color: #fff;
                }

                #app-page .doc_tab{
                        color: {$color};
                        border-color: {$color};
                }

                #app-page .doc_tab:nth-child(1){
                        border-color: {$color};
                }
                .add_service_block{
                       border-color: {$color};
                }";
        wp_add_inline_style( 'booking-style', $custom_css );

}

function add_chat_script_footer(){

  if (($this->settings['chat_active'])and($this->settings['chat_id'])):
  ?>
<!-- begin olark code -->
<script data-cfasync="false" type='text/javascript'>/*<![CDATA[*/window.olark||(function(c){var f=window,d=document,l=f.location.protocol=="https:"?"https:":"http:",z=c.name,r="load";var nt=function(){
f[z]=function(){
(a.s=a.s||[]).push(arguments)};var a=f[z]._={
},q=c.methods.length;while(q--){(function(n){f[z][n]=function(){
f[z]("call",n,arguments)}})(c.methods[q])}a.l=c.loader;a.i=nt;a.p={
0:+new Date};a.P=function(u){
a.p[u]=new Date-a.p[0]};function s(){
a.P(r);f[z](r)}f.addEventListener?f.addEventListener(r,s,false):f.attachEvent("on"+r,s);var ld=function(){function p(hd){
hd="head";return["<",hd,"></",hd,"><",i,' onl' + 'oad="var d=',g,";d.getElementsByTagName('head')[0].",j,"(d.",h,"('script')).",k,"='",l,"//",a.l,"'",'"',"></",i,">"].join("")}var i="body",m=d[i];if(!m){
return setTimeout(ld,100)}a.P(1);var j="appendChild",h="createElement",k="src",n=d[h]("div"),v=n[j](d[h](z)),b=d[h]("iframe"),g="document",e="domain",o;n.style.display="none";m.insertBefore(n,m.firstChild).id=z;b.frameBorder="0";b.id=z+"-loader";if(/MSIE[ ]+6/.test(navigator.userAgent)){
b.src="javascript:false"}b.allowTransparency="true";v[j](b);try{
b.contentWindow[g].open()}catch(w){
c[e]=d[e];o="javascript:var d="+g+".open();d.domain='"+d.domain+"';";b[k]=o+"void(0);"}try{
var t=b.contentWindow[g];t.write(p());t.close()}catch(x){
b[k]=o+'d.write("'+p().replace(/"/g,String.fromCharCode(92)+'"')+'");d.close();'}a.P(2)};ld()};nt()})({
loader: "static.olark.com/jsclient/loader0.js",name:"olark",methods:["configure","extend","declare","identify"]});
/* custom configuration goes here (www.olark.com/documentation) */
olark.identify('<?php echo $this->settings['chat_id'];?>');/*]]>*/</script><noscript><a href="https://www.olark.com/site/9372-370-10-5639/contact" title="Contact us" target="_blank">Questions? Feedback?</a> powered by <a href="http://www.olark.com?welcome" title="Olark live chat software">Olark live chat software</a></noscript>
<!-- end olark code -->

<?php
  endif;

}

function custom_property_template($single_template) {
     global $post;

     if ($post->post_type == 'property' ) {
          $single_template = plugin_dir_path(__FILE__) . 'templates/single-property.php';
     }
     return $single_template;
     wp_reset_postdata();
}

function register_query_var( $vars ) {
      // $vars[] = 'user_login';
      $vars[] = 'reserv_code';
      $vars[] = 'sort_by';
      $vars[] = 'page_num';
      $vars[] = 'ical_prop';
       return $vars;
}

function rewrite_rule() {

        add_rewrite_rule( 'payment-api/?', 'index.php?payment_api=1', 'top' );

        add_rewrite_rule( 'thank-you/reserv-code/([^/]+)/?', 'index.php?pagename=thank-you&reserv_code=$matches[1]', 'top' );

        add_rewrite_rule( 'invoice/([^/]+)/?', 'index.php?pagename=invoice&reserv_code=$matches[1]', 'top' );

        add_rewrite_rule( 'page_num/([^/]+)/sort_by/([^/]+)/?', 'index.php?pagename=home-page&page_num=$matches[1]&sort_by=$matches[2]', 'top' );
        add_rewrite_rule( 'page_num/([^/]+)/?', 'index.php?pagename=home-page&page_num=$matches[1]', 'top' );

        add_rewrite_rule( 'search-result/page_num/([^/]+)/sort_by/([^/]+)/?', 'index.php?pagename=search-result&page_num=$matches[1]&sort_by=$matches[2]', 'top' );
        add_rewrite_rule( 'search-result/page_num/([^/]+)/?', 'index.php?pagename=search-result&page_num=$matches[1]', 'top' );
        
        add_rewrite_rule( 'ical/([^/]+)/?', 'index.php?ical_prop=$matches[1]', 'top' );

     }

///////////////////

function redirect_after_post(){
    
    if (get_query_var( 'ical_prop' )){
        add_filter( 'template_include', function() {
            return plugin_dir_path(__FILE__) . 'templates/ical-feed.php';
        });
    }

if (!empty($_POST['guote'])){
   if (((empty($_POST['adults']))or(empty($_POST['location']))or(empty($_POST['date_from']))or(empty($_POST['date_to'])))or(strtotime($this->date_to_sql($_POST['date_from']) . ' 00:00:00') >= strtotime($this->date_to_sql($_POST['date_to']) . ' 00:00:00'))){
     wp_safe_redirect(get_home_url());
     exit;
   }
}

    if ((empty($_POST['reserve']))and(!empty($_POST['book']))){
    //print_r($_POST);
if ((!empty($_POST['first_name']))and(!empty($_POST['last_name']))and(!empty($_POST['email']))and(!empty($_POST['tel1']))and(!empty($_POST['accept_term']))){

      if ($this->is_free_appartment($_POST['date_from'], $_POST['date_to'], $_POST['id'])){
      $reserv_id = $this->create_booking();

      $reserv_code = get_the_title($reserv_id);

      if (($_POST['payment_method'] == 'paypal')and($this->settings['paypal_activate'])){

      $paypal_args = http_build_query( $this->create_paypal_arr($reserv_code), '', '&' );

		if ( $this->settings['paypal_sandbox'] ) {
			$link = 'https://www.sandbox.paypal.com/cgi-bin/webscr?test_ipn=1&' . $paypal_args;
		} else {
			$link = 'https://www.paypal.com/cgi-bin/webscr?' . $paypal_args;
		}
        wp_redirect($link);
         }
       if (($_POST['payment_method'] == 'cash')and($this->settings['payment_mode1'])){

           $this->accept_booking($reserv_code, 0, '', 'cash');
           $home_url = get_home_url();
           $Path='/thank-you/reserv-code/'.$reserv_code;

           $this->thanks_email($reserv_code);

           wp_safe_redirect($home_url.$Path);
           exit;
       }
       if (($_POST['payment_method'] == 'stripe')and($this->settings['stripe_activate'])){
           $this->charge_stripe($reserv_code);
           exit;
       }

       }
    }
  }

}

///////////////

public function format_currency($amount){
  $output = '';

  if ( $this->settings['currency_place'] == 1 )
    $output .= $this->settings['currency'].$amount;
  else
    $output .= $amount.' '.$this->settings['currency'];

  return $output;
}

///////////////////////////////////

function charge_stripe($reserv_code){

    if (!empty($_POST['stripe_token'])){

      // charge
       require_once VR_BOOKING_PLUGIN_DIR . '/includes/stripe-php/init.php';

       // Set your secret key: remember to change this to your live secret key in production
// See your keys here: https://dashboard.stripe.com/account/apikeys
 if ($this->settings['stripe_sandbox'])
     $key = $this->settings['stripe_test_secret_key'];
    else
     $key = $this->settings['stripe_live_secret_key'];

  $price_arr = $this->get_all_rates_by_ap_id($_POST['id'], $this->date_to_sql($_POST['date_from']), $this->date_to_sql($_POST['date_to']));

  $amount = round($price_arr['total'], 2);
  if ($_POST['amount'] != 'full') $amount = round($price_arr['deposit'], 2);

  $price = $amount*100;  // Amount in cents

\Stripe\Stripe::setApiKey($key);

// Get the credit card details submitted by the form
$token = $_POST['stripe_token'];

// Create a charge: this will charge the user's card
try {
  $charge = \Stripe\Charge::create(array(
    "amount" => $price, // Amount in cents
    "currency" => strtolower($this->settings['currency_code']),
    "source" => $token,
    "description" => "Reservation code #".$reserv_code
    ));
} catch(\Stripe\Error\Card $e) {
  // The card has been declined
  $this->cancell_booking($reserv_code);
  exit;
}
      // if charge
    $this->thanks_email($reserv_code);
    $this->accept_booking($reserv_code, $amount, $token, 'stripe');
    $home_url = get_home_url();
    $Path='/thank-you/reserv-code/'.$reserv_code;
    wp_safe_redirect($home_url.$Path);
    exit;
    }
}

///////////////////

function create_paypal_arr($reserv_code){
  $output = array();

  $home_url = get_home_url();

  $price_arr = $this->get_all_rates_by_ap_id($_POST['id'], $this->date_to_sql($_POST['date_from']), $this->date_to_sql($_POST['date_to']));

  $amount = $price_arr['total'];

  if ($_POST['amount'] != 'full') $amount = $price_arr['deposit'];

			$phone_args = array(
				'night_phone_b' => $_POST['tel1'],
				'day_phone_b' 	=> $_POST['tel1']
			);

  $output = array_merge( array(
				'cmd'           => '_xclick',
				'business'      => $this->settings['paypal_email'],
				'no_note'       => 1,
				'currency_code' => $this->settings['currency_code'],
				'charset'       => 'utf-8',
				'rm'            => 2,
				'return'        => $home_url . '/thank-you',
				'cancel_return' => $home_url . '/',
				'bn'            => 'Bookingacc_BuyNow',
                'no_shipping'   => 1,
				'custom'        => $reserv_code,
				'notify_url'    => $home_url . '/payment-api',
				'first_name'    => $_POST['first_name'],
				'last_name'     => $_POST['last_name'],
				'address1'      => '',
				'address2'      => '',
				'city'          => '',
				'state'         => '',
				'country'       => '',
				'email'         => $_POST['email'],
                'amount'        => $amount,
                'item_name'     => 'Reservation code: '.$reserv_code
			),
            $phone_args);

  return $output;
}

//////////////////////

public function accept_booking($reserv_code, $paid = 0, $token = '', $paid_by = ''){

     $page = get_page_by_title( $reserv_code, OBJECT, 'booking' );

     if (!empty($page)){
     $booking_id = $page->ID;

     $post_id = get_post_meta($booking_id, '_booking_prop_id', true);

     update_post_meta($post_id, '_booking_accepted', 1);
     update_post_meta($post_id, '_booking_paid', $paid);
     update_post_meta($post_id, '_booking_paid_by', $paid_by);
     update_post_meta($post_id, '_booking_token', $token);
    }
}

//////////////

public function cancell_booking($reserv_code){
  $output = 0;
  $page = get_page_by_title( $reserv_code, OBJECT, 'booking' );
     if (!empty($page)){
     wp_delete_post( $page->ID );
     $output = 1;
    }
  return $output;
}

public function clear_booking(){

  $args = array(
               'post_type' => 'booking',
               'posts_per_page'=> -1,
               'meta_query' => array(
                  'relation' => 'AND',
                  'date_from' => array(
                    'key'     => '_booking_created',
                    'value'   => (time() - 10*60),
                    'compare' => '<',
                    'type'    => 'NUMERIC',
                    ),
                  'accepted' => array(
                    'key'     => '_booking_accepted',
                    'value'   => 0,
                    'compare' => '=',
                    'type'    => 'NUMERIC',
                    ),
                    ),
                  );

    $the_query = new WP_Query( $args );
     while ( $the_query->have_posts() ) : $the_query->the_post();
          wp_delete_post( get_the_ID() );
     endwhile;
wp_reset_postdata();
}

////////////////

function add_booking_event(){
    
    // foreach prop
    $args = array(
        'post_type'   => 'property',
        'numberposts' => -1,
    );
    $posts = get_posts( $args );
    if ( $posts ) {
        foreach ( $posts as $post ) {
            $this->import_icals($post->ID);
        }
    }
    
    return;
}

public function import_icals($prop_id){
    require_once VR_BOOKING_PLUGIN_DIR . '/includes/icalendar/zapcallib.php';
    
    $icals_list = maybe_unserialize(get_post_meta( $prop_id, '_icals_import_list', true));
    foreach ($icals_list as $ical_name => $ical_url){
        // import bookings from each iCalendar
      //  $icalfile = VR_BOOKING_PLUGIN_DIR . "/listing-15831052.ics";
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_URL, $ical_url);
    curl_setopt($ch, CURLOPT_REFERER, $ical_url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    $icalfeed = curl_exec($ch);
    curl_close($ch);       
        
     //  print_r($icalfeed); 
       // $icalfeed = file_get_contents($ical_url);

// create the ical object
$icalobj = new ZCiCal($icalfeed);
$ecount = 0;
// read back icalendar data that was just parsed
if(isset($icalobj->tree->child))
{
	foreach($icalobj->tree->child as $node)
	{
		if($node->getName() == "VEVENT")
		{
			$ecount++;
		//	echo "<br />";
            $summary = '';
			foreach($node->data as $key => $value)
			{
			//	echo "  $key: " . $value->getValues() . "<br />";
             if ($key == 'DTSTART') {
                $ical_date_start = $value->getValues();
                $date_start = $this->ical_to_sql($ical_date_start);
              //  print_r('<pre>'.$date_start.'</pre>');
                }  
             if ($key == 'DTEND') $date_end = $this->ical_to_sql($value->getValues());
             if ($key == 'SUMMARY') $summary = $value->getValues();   
			}
            $this->create_booking_ical($prop_id, $date_start, $date_end, $ical_name, $ical_date_start, $summary); 
		}
	}
}       
    }
    return;
}

function ical_to_sql($date_ical){
    $date = substr($date_ical,0,4).'-'.substr($date_ical,4,2).'-'.substr($date_ical,6,2);
    return $date;
}

///////////////

function get_reserv_code_callback(){

    $home_url = get_home_url();
    $Path='/invoice/'.get_the_title($_POST['id']);

    echo $home_url.$Path;
    wp_die();
}

///////////////

function update_general_p_callback(){

   $ap_id = $_POST['select_ap'];
   $general_price = floatval($_POST['general_price']);

  update_post_meta($ap_id, 'general_price', $general_price);

  $rates = $this->get_all_rates_by_ap_id($ap_id);

  echo json_encode($rates);

  wp_die();
}

function check_ap_callback(){

  if ($this->is_free_appartment($_POST['date_from'], $_POST['date_to'], $_POST['id']))
  echo 1;
  else
  echo 0;

  wp_die();
}

function select_ap_callback(){

   $ap_id = $_POST['select_ap'];

    // get new rates dates array, output

    $rates = $this->get_all_rates_by_ap_id($ap_id);

  echo json_encode($rates);
  wp_die();
}

function select_ap2_callback(){

   $ap_id = $_POST['select_ap'];

    // get new rates dates array, output

    $rates = $this->get_all_rates_by_ap_id($ap_id);
    $av = $this->get_av_dates_by_ap_id($ap_id);

    $output = array_merge($rates, $av);

  echo json_encode($output);
  wp_die();
}

function get_amount_booking_callback(){

   $ap_id = $_POST['id'];

   $output = array();

   if ((!empty($_POST['date_from']))and(!empty($_POST['date_to']))){
    $rates = $this->get_all_rates_by_ap_id($ap_id, $this->date_to_sql($_POST['date_from']), $this->date_to_sql($_POST['date_to']), $_POST['extra_bed']);
   $output = $rates;
   } else
   $output = array('total' => '', 'total_tax' => '', 'total_clear' => '');

   echo json_encode($output);

  wp_die();
}

function assign_rate_callback(){
  $output = array();
   $range_rates_from = $this->date_to_sql($_POST['range_rates_from']);
   $range_rates_to = $this->date_to_sql($_POST['range_rates_to']);
   $rate_id = $_POST['select_rate'];
   $ap_id = $_POST['select_ap'];

   // update other rates with range includes that dates

   // cut new dates from existing rates    86400

   $time_from = strtotime($range_rates_from . ' 00:00:00')-86400;
   $time_to = strtotime($range_rates_to . ' 00:00:00')+86400;

   $check_rates = $this->db->get_results("SELECT * FROM ".BOOKING_TABLE_RATES." WHERE ap_id = '".$ap_id."' AND range_to >= '".$range_rates_from."' AND range_from <= '".$range_rates_to."' ORDER BY range_from ASC", ARRAY_A);

           if ($this->db->num_rows){
              foreach ($check_rates as $key => $rate){

                   if (strtotime($rate['range_from'] . ' 00:00:00') <= $time_from){
                         // first part to save: $rate['range_from'] - date("Y-m-d", $time_from)
                      $this->db->insert(
                       BOOKING_TABLE_RATES,
                       array(
                        'ap_id' => $rate['ap_id'],
                        'rate_id' => $rate['rate_id'],
                        'range_from' => $rate['range_from'],
                        'range_to' => date("Y-m-d", $time_from),
                        'except_price' => $rate['except_price']
                        )
                       );

                   }
                   if (strtotime($rate['range_to'] . ' 00:00:00') >= $time_to){
                         // second part to save: date("Y-m-d", $time_to) - $rate['range_to']
                       $this->db->insert(
                       BOOKING_TABLE_RATES,
                       array(
                        'ap_id' => $rate['ap_id'],
                        'rate_id' => $rate['rate_id'],
                        'range_from' => date("Y-m-d", $time_to),
                        'range_to' => $rate['range_to'],
                        'except_price' => $rate['except_price']
                        )
                       );
                   }

                   // delete $rate['id'] from BOOKING_TABLE_RATES
                   $this->db->delete(
                     BOOKING_TABLE_RATES,
                      array(
                       'id' => $rate['id']
                       )
                      );
              }
           }

   $insert_arr = array(
          'ap_id' => $ap_id,
          'rate_id' => $rate_id,
          'range_from' => $range_rates_from,
          'range_to' => $range_rates_to
	      );

    if (!empty($_POST['except_price'])) $insert_arr['except_price'] = $_POST['except_price'];

    $this->db->insert(
	     BOOKING_TABLE_RATES,
	     $insert_arr
         );

         $assign_id = $this->db->insert_id;

    // get new rates dates array, output

    $rates = $this->get_all_rates_by_ap_id($ap_id);

  echo json_encode($rates);
  wp_die();
}

function get_av_dates_by_ap_id($ap_id){
   $output = array();
   $av_arr = array();

   $now = time() - 365*24*60*60;
   $to =  time() + 2*365*24*60*60;
   $begin_date = date("Y-m-d", $now);
   $end_date = date("Y-m-d", $to);

      $begin = new DateTime( $begin_date );
      $end = new DateTime( $end_date );
      $end = $end->modify( '+1 day' );

      $interval = new DateInterval('P1D');
      $daterange = new DatePeriod($begin, $interval ,$end);
      foreach($daterange as $date){
       $av_arr[strtotime($date->format("Y-m-d"))] = 0;
      }

   $args = array(
               'post_type' => 'booking',
               'posts_per_page'=> -1,
               'meta_query' => array(
                  'relation' => 'AND',
                  'date_from' => array(
                    'key'     => '_booking_date_from',
                    'value'   => $to,
                    'compare' => '<=',
                    'type'    => 'NUMERIC',
                    ),
                  'date_to' => array(
                    'key'     => '_booking_date_to',
                    'value'   => $now,
                    'compare' => '>=',
                    'type'    => 'NUMERIC',
                    ),
                  'ap_id' => array(
                    'key'     => '_booking_prop_id',
                    'value'   => $ap_id,
                    'compare' => '=',
                    ),
                   ),
                  );

    $the_query = new WP_Query( $args );
     while ( $the_query->have_posts() ) : $the_query->the_post();
      $begin_date = date("Y-m-d", get_post_meta( get_the_ID(), '_booking_date_from', true ));
      $end_date = date("Y-m-d", get_post_meta( get_the_ID(), '_booking_date_to', true ));

      $begin = new DateTime( $begin_date );
      $end = new DateTime( $end_date );
      $end = $end->modify( '+1 day' );

      $interval = new DateInterval('P1D');
      $daterange = new DatePeriod($begin, $interval ,$end);

      $first = true;
      //$last = false;
      foreach($daterange as $date){
         $cur = strtotime($date->format("Y-m-d"));
         $prev = $av_arr[$cur];
      if ($first){
         if ($av_arr[$cur]) $av_arr[$cur] = 4;
         else
         $av_arr[$cur] = 2;
         $first = false;
       } else
       $av_arr[$cur] = 3;
      }
       if ($prev > 0)
       $av_arr[$cur] = 4;
       else
       $av_arr[$cur] = 1;

     endwhile;
wp_reset_postdata();

   $output['av_arr'] = $av_arr;
   return $output;
}

function get_all_rates_by_ap_id($ap_id, $range_from = '', $range_to = '', $extra_bed = ''){
    $output = array();
    $price_arr = array();
    $color_arr = array();
    $price = 0;
    $price_tax = 0;
    $price_clear = 0;
    $min_price = 1000000;
    $general_price = 0;
    $general_price_clear = 0;
    $total_count = 0;
    $nights_total = 0;
    $tax = 1;

    $deposit = 0;

    if (!empty($this->settings['tax']))
    $tax_am = floatval($this->settings['tax'])/100;
    else $tax_am = 0;

    $tax = $tax + $tax_am;

              $ap_fields = get_post_custom($ap_id);
              if (isset($ap_fields['general_price'])){
              $general_price = round($ap_fields['general_price'][0]*$tax, 2);
              $general_price_clear = round($ap_fields['general_price'][0], 2);
              }

  if (!$range_from){
  $range_rates_from = date("Y-m-d", time() - 365*24*60*60);
  $range_rates_to = date("Y-m-d", time() + 2*365*24*60*60);
  } else {
   $range_rates_from = $range_from;
   $range_rates_to = $range_to;
  }

    $nights_total = date_diff(date_create($range_rates_from. ' 00:00:00'), date_create($range_rates_to. ' 00:00:00'))->days;

  $check_rates = $this->db->get_results("SELECT * FROM ".BOOKING_TABLE_RATES." WHERE ap_id = '".$ap_id."' AND range_to >= '".$range_rates_from."' AND range_from <= '".$range_rates_to."' ORDER BY range_from ASC", ARRAY_A);

           if ($this->db->num_rows){
              foreach ($check_rates as $key => $rate){
             ////////////
      $rate_fields = get_post_custom($rate['rate_id']);
      if (!isset($rate_fields['_price_price_mon'])) $rate_fields['_price_price_mon'][0] = 0;
      if (!isset($rate_fields['_price_price_tue'])) $rate_fields['_price_price_tue'][0] = 0;
      if (!isset($rate_fields['_price_price_wed'])) $rate_fields['_price_price_wed'][0] = 0;
      if (!isset($rate_fields['_price_price_thu'])) $rate_fields['_price_price_thu'][0] = 0;
      if (!isset($rate_fields['_price_price_fri'])) $rate_fields['_price_price_fri'][0] = 0;
      if (!isset($rate_fields['_price_price_sat'])) $rate_fields['_price_price_sat'][0] = 0;
      if (!isset($rate_fields['_price_price_sun'])) $rate_fields['_price_price_sun'][0] = 0;
      if (!isset($rate_fields['_price_price_fri_sat'])) $rate_fields['_price_price_fri_sat'][0] = 0;
      if (!isset($rate_fields['_price_price_7'])) $rate_fields['_price_price_7'][0] = 0;

      $week_amount_by_days =  round($rate_fields['_price_price_mon'][0]*$tax, 2) + round($rate_fields['_price_price_tue'][0]*$tax, 2) + round($rate_fields['_price_price_wed'][0]*$tax, 2) + round($rate_fields['_price_price_thu'][0]*$tax, 2) + round($rate_fields['_price_price_fri'][0]*$tax, 2) + round($rate_fields['_price_price_sat'][0]*$tax, 2) + round($rate_fields['_price_price_sun'][0]*$tax, 2);

      $week_amount_by_days_tax =  round($rate_fields['_price_price_mon'][0]*$tax_am, 2) + round($rate_fields['_price_price_tue'][0]*$tax_am, 2) + round($rate_fields['_price_price_wed'][0]*$tax_am, 2) + round($rate_fields['_price_price_thu'][0]*$tax_am, 2) + round($rate_fields['_price_price_fri'][0]*$tax_am, 2) + round($rate_fields['_price_price_sat'][0]*$tax_am, 2) + round($rate_fields['_price_price_sun'][0]*$tax_am, 2);

      $week_amount_by_days_clear =  round($rate_fields['_price_price_mon'][0], 2) + round($rate_fields['_price_price_tue'][0], 2) + round($rate_fields['_price_price_wed'][0], 2) + round($rate_fields['_price_price_thu'][0], 2) + round($rate_fields['_price_price_fri'][0], 2) + round($rate_fields['_price_price_sat'][0], 2) + round($rate_fields['_price_price_sun'][0], 2);

      if ($range_from){
        $begin_date = $range_from;
        $end_date = $range_to;
      } else {
        $begin_date = $rate['range_from'];
        $end_date = $rate['range_to'];
      }

      $begin = new DateTime( $begin_date );
      $end = new DateTime( $end_date );

      if (!$range_from)
      $end = $end->modify( '+1 day' );

      $interval = new DateInterval('P1D');
      $daterange = new DatePeriod($begin, $interval ,$end);
         $nights = 1;
         $price_local = 0;
         $price_local_tax = 0;
         $price_local_clear = 0;
      foreach($daterange as $date){
            $w_day = strtolower ( $date->format("D") );
            $cur_time = strtotime($date->format("Y-m-d"). ' 00:00:00');

            if($cur_time > strtotime($rate['range_to'] . ' 00:00:00')) break;

            if ($cur_time >= strtotime($rate['range_from'] . ' 00:00:00')){
            $total_count++;

            $price_val = round($rate_fields['_price_price_'.$w_day][0]*$tax, 2);
            $price_val_tax = round($rate_fields['_price_price_'.$w_day][0]*$tax_am, 2);
            $price_val_clear = round($rate_fields['_price_price_'.$w_day][0], 2);

       // fri + sat
           if (($w_day == 'sat')and($price_local)and($nights%7)){
             $price_local = $price_local - round($rate_fields['_price_price_fri'][0]*$tax, 2);
             $price_local = $price_local + round($rate_fields['_price_price_fri_sat'][0]*$tax, 2);

             $price_local_tax = $price_local_tax - round($rate_fields['_price_price_fri'][0]*$tax_am, 2);
             $price_local_tax = $price_local_tax + round($rate_fields['_price_price_fri_sat'][0]*$tax_am, 2);

             $price_local_clear = $price_local_clear - round($rate_fields['_price_price_fri'][0], 2);
             $price_local_clear = $price_local_clear + round($rate_fields['_price_price_fri_sat'][0], 2);
           } else {
             $price_local = $price_local + $price_val;
             $price_local_tax = $price_local_tax + $price_val_tax;
             $price_local_clear = $price_local_clear + $price_val_clear;
            }

       // 7 nights
           if (!($nights%7)){
              $price_local = $price_local - $week_amount_by_days;
              $price_local = $price_local + round($rate_fields['_price_price_7'][0]*$tax, 2);

              $price_local_tax = $price_local_tax - $week_amount_by_days_tax;
              $price_local_tax = $price_local_tax + round($rate_fields['_price_price_7'][0]*$tax_am, 2);

              $price_local_clear = $price_local_clear - $week_amount_by_days_clear;
              $price_local_clear = $price_local_clear + round($rate_fields['_price_price_7'][0], 2);
           }

       if ($min_price > $price_val) $min_price = $price_val;

       $price_arr[strtotime($date->format("Y-m-d"))] = $price_val;

       if (!isset($rate_fields['_price_color'])) $rate_fields['_price_color'][0] = '#eee';
       $color_arr[strtotime($date->format("Y-m-d"))] = $rate_fields['_price_color'][0];

       $nights++;
              }
      }
            ////////////////////
          $price = $price + $price_local;
          $price_tax = $price_tax + $price_local_tax;
          $price_clear = $price_clear + $price_local_clear;
              }

           } else {
           // no rates, so get general price

        $begin_date = $range_rates_from;
        $end_date = $range_rates_to;

      $begin = new DateTime( $begin_date );
      $end = new DateTime( $end_date );

      if (!$range_from)
      $end = $end->modify( '+1 day' );

      $interval = new DateInterval('P1D');
      $daterange = new DatePeriod($begin, $interval ,$end);

      foreach($daterange as $date){
            $total_count++;
            $price_arr[strtotime($date->format("Y-m-d"))] = $general_price;
      }
          $min_price = $general_price;
          $price = $general_price*$nights_total;
          $price_tax = round($general_price_clear*$tax_am, 2)*$nights_total;
          $price_clear = $general_price_clear*$nights_total;
           }

   if (($range_from)and($total_count < $nights_total)){
       $price = $price + ($nights_total - $total_count)*$general_price;
       $price_tax = $price_tax + ($nights_total - $total_count)*round($general_price_clear*$tax_am, 2);
       $price_clear = $price_clear + ($nights_total - $total_count)*$general_price_clear;
   }

   if ($total_count < $nights_total){
       if ($min_price > $general_price) $min_price = $general_price;
   }

  if ($min_price == 1000000) $min_price = 0;

  if ($this->settings['payment_mode2']) $deposit = round($price*($this->settings['deposit_amount']/100), 2);

  $output['nights_total_clear'] = $price_clear;
  $output['nights_total_tax'] = $price_tax;
  $output['nights_total'] = $price;
  $output['total'] = $price;
  $output['total_tax'] = $price_tax;
  $output['total_clear'] = $price_clear;
  $output['deposit'] = $deposit;
  $output['price_arr'] = $price_arr;
  $output['min'] = $min_price;
  $output['general_price'] = $general_price;
  $output['color_arr'] = $color_arr;
  
  ///////////////////////////////

  if (isset($_POST['services'])){
    $services_arr = array_keys($_POST['services']);
    $args = array(
        'post_type'   => 'service',
        'numberposts' => -1,
    );
    $posts = get_posts( $args );
    $services_price_clear = 0;
    $services_price_tax = 0;
    $services_price = 0;
    if ( $posts ) {
        foreach ( $posts as $post ) {
          if (in_array( $post->ID, $services_arr )){
            $price = 0;
            $price = get_post_meta($ap_id, '_property_service_'.$post->ID, true);
            if ($price == '')
            $price = get_post_meta($post->ID, '_service_price', true);
            $services_price_clear = $services_price_clear + $price;
            $services_price_tax = $services_price_tax + round($price*$tax_am,2);
            $services_price = $services_price + round($price*$tax,2);
          }
        }
      }

    if ($services_price){
      $output['total'] = $output['total'] + $services_price;
      $output['total_tax'] = $output['total_tax'] + $services_price_tax;
      $output['total_clear'] = $output['total_clear'] + $services_price_clear;
      if ($this->settings['payment_mode2']) $deposit = round($output['total']*($this->settings['deposit_amount']/100), 2);
      $output['deposit'] = $deposit;
    }
  }
  
  ////////////  Extra bed   //////////
  
  if ($extra_bed){
     $extra_bed_id = get_post_meta($ap_id, '_property_extra_bed', true);
     $extra_price = get_post_meta($ap_id, '_property_extra_bed_'.$extra_bed_id, true);
     if ($extra_price == '')
       $extra_price = get_post_meta($extra_bed_id, '_extra_bed_price', true);
       
       $extra_price_clear = $extra_price*$nights_total;
       $extra_price_tax = round($extra_price_clear*$tax_am,2);
       $extra_price_total = round($extra_price_clear*$tax,2);
       
     if ($extra_price_total){
      $output['total'] = $output['total'] + $extra_price_total;
      $output['total_tax'] = $output['total_tax'] + $extra_price_tax;
      $output['total_clear'] = $output['total_clear'] + $extra_price_clear;
      if ($this->settings['payment_mode2']) $deposit = round($output['total']*($this->settings['deposit_amount']/100), 2);
      $output['deposit'] = $deposit;
    }  
  }
  
  ////////////////////////////////////

  return $output;
}

/////////////////

function create_services_post_type(){

// Set UI labels for Custom Post Type
	$labels = array(
		'name'                => _x( 'Services', 'Post Type General Name', VR_BOOKING_TEXTDOMAIN),
		'singular_name'       => _x( 'Service', 'Post Type Singular Name', VR_BOOKING_TEXTDOMAIN),
		'menu_name'           => __( 'Services', VR_BOOKING_TEXTDOMAIN),
		'parent_item_colon'   => __( 'Parent Service', VR_BOOKING_TEXTDOMAIN),
		'all_items'           => __( 'All Services', VR_BOOKING_TEXTDOMAIN),
		'view_item'           => __( 'View Service', VR_BOOKING_TEXTDOMAIN),
		'add_new_item'        => __( 'Add New Service', VR_BOOKING_TEXTDOMAIN),
		'add_new'             => __( 'Add New', VR_BOOKING_TEXTDOMAIN),
		'edit_item'           => __( 'Edit Service', VR_BOOKING_TEXTDOMAIN),
		'update_item'         => __( 'Update Service', VR_BOOKING_TEXTDOMAIN),
		'search_items'        => __( 'Search Services', VR_BOOKING_TEXTDOMAIN),
		'not_found'           => __( 'Not Found', VR_BOOKING_TEXTDOMAIN),
		'not_found_in_trash'  => __( 'Not found in Trash', VR_BOOKING_TEXTDOMAIN),
	);

// Set other options for Custom Post Type

	$args = array(
		'label'               => __( 'services', VR_BOOKING_TEXTDOMAIN),
		'description'         => __( 'Services', VR_BOOKING_TEXTDOMAIN),
		'labels'              => $labels,
		// Features this CPT supports
		'supports'            => array( 'title', 'editor', 'thumbnail' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
        'menu_position'        => 30,
        'menu_icon'           => 'dashicons-products',
		'show_in_nav_menus'   => false,
		'show_in_admin_bar'   => true,
		'can_export'          => true,
		'has_archive'         => false,
		'exclude_from_search' => true,
		'publicly_queryable'  => true,
		'capability_type'     => 'post',
	);

	// Registering your Custom Post Type
	register_post_type( 'service', $args );
}

/////////////////

function create_extra_bed_post_type(){

// Set UI labels for Custom Post Type
	$labels = array(
		'name'                => _x( 'Extra Bed', 'Post Type General Name', VR_BOOKING_TEXTDOMAIN),
		'singular_name'       => _x( 'Extra Bed', 'Post Type Singular Name', VR_BOOKING_TEXTDOMAIN),
		'menu_name'           => __( 'Extra Bed', VR_BOOKING_TEXTDOMAIN),
		'parent_item_colon'   => __( 'Parent Extra Bed', VR_BOOKING_TEXTDOMAIN),
		'all_items'           => __( 'All Extra Beds', VR_BOOKING_TEXTDOMAIN),
		'view_item'           => __( 'View Extra Bed', VR_BOOKING_TEXTDOMAIN),
		'add_new_item'        => __( 'Add New Extra Bed', VR_BOOKING_TEXTDOMAIN),
		'add_new'             => __( 'Add New', VR_BOOKING_TEXTDOMAIN),
		'edit_item'           => __( 'Edit Extra Bed', VR_BOOKING_TEXTDOMAIN),
		'update_item'         => __( 'Update Extra Bed', VR_BOOKING_TEXTDOMAIN),
		'search_items'        => __( 'Search Extra Bed', VR_BOOKING_TEXTDOMAIN),
		'not_found'           => __( 'Not Found', VR_BOOKING_TEXTDOMAIN),
		'not_found_in_trash'  => __( 'Not found in Trash', VR_BOOKING_TEXTDOMAIN),
	);

// Set other options for Custom Post Type

	$args = array(
		'label'               => __( 'extra_bed', VR_BOOKING_TEXTDOMAIN),
		'description'         => __( 'Extra Bed', VR_BOOKING_TEXTDOMAIN),
		'labels'              => $labels,
		// Features this CPT supports
		'supports'            => array( 'title' ),
		'hierarchical'        => false,
		'public'              => false,
		'show_ui'             => true,
		'show_in_menu'        => true,
        'menu_position'        => 29,
        'menu_icon'           => 'dashicons-groups',
		'show_in_nav_menus'   => false,
		'show_in_admin_bar'   => true,
		'can_export'          => true,
		'has_archive'         => false,
		'exclude_from_search' => true,
		'publicly_queryable'  => true,
		'capability_type'     => 'post',
	);

	// Registering your Custom Post Type
	register_post_type( 'extra_bed', $args );
}

/////////////////////////////////////

function create_property_post_type(){

// Set UI labels for Custom Post Type
	$labels = array(
		'name'                => _x( 'Properties', 'Post Type General Name', VR_BOOKING_TEXTDOMAIN),
		'singular_name'       => _x( 'Property', 'Post Type Singular Name', VR_BOOKING_TEXTDOMAIN),
		'menu_name'           => __( 'Properties', VR_BOOKING_TEXTDOMAIN),
		'parent_item_colon'   => __( 'Parent Property', VR_BOOKING_TEXTDOMAIN),
		'all_items'           => __( 'All Properties', VR_BOOKING_TEXTDOMAIN),
		'view_item'           => __( 'View Property', VR_BOOKING_TEXTDOMAIN),
		'add_new_item'        => __( 'Add New Property', VR_BOOKING_TEXTDOMAIN),
		'add_new'             => __( 'Add New', VR_BOOKING_TEXTDOMAIN),
		'edit_item'           => __( 'Edit Property', VR_BOOKING_TEXTDOMAIN),
		'update_item'         => __( 'Update Property', VR_BOOKING_TEXTDOMAIN),
		'search_items'        => __( 'Search Properties', VR_BOOKING_TEXTDOMAIN),
		'not_found'           => __( 'Not Found', VR_BOOKING_TEXTDOMAIN),
		'not_found_in_trash'  => __( 'Not found in Trash', VR_BOOKING_TEXTDOMAIN),
	);

// Set other options for Custom Post Type

	$args = array(
		'label'               => __( 'properties', VR_BOOKING_TEXTDOMAIN),
		'description'         => __( 'Properties', VR_BOOKING_TEXTDOMAIN),
		'labels'              => $labels,
		// Features this CPT supports
		'supports'            => array( 'title', 'editor', 'thumbnail' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
        'menu_position'        => 27,
        'menu_icon'           => 'dashicons-building',
		'show_in_nav_menus'   => false,
		'show_in_admin_bar'   => true,
		'can_export'          => true,
		'has_archive'         => false,
		'exclude_from_search' => true,
		'publicly_queryable'  => true,
		'capability_type'     => 'post',
	);

	// Registering your Custom Post Type
	register_post_type( 'property', $args );

    $property_locations_labels = array(
			'name'              => __( 'Locations', VR_BOOKING_TEXTDOMAIN ),
			'singular_name'     => __( 'Location', VR_BOOKING_TEXTDOMAIN ),
			'search_items'      => __( 'Search Location', VR_BOOKING_TEXTDOMAIN ),
			'all_items'         => __( 'All Locations', VR_BOOKING_TEXTDOMAIN ),
			'parent_item'       => __( 'Parent Location', VR_BOOKING_TEXTDOMAIN ),
			'parent_item_colon' => __( 'Parent Location:', VR_BOOKING_TEXTDOMAIN ),
			'edit_item'         => __( 'Edit Location', VR_BOOKING_TEXTDOMAIN ),
			'update_itm'        => __( 'Update Location', VR_BOOKING_TEXTDOMAIN ),
			'add_new_item'      => __( 'Add New Location', VR_BOOKING_TEXTDOMAIN ),
			'new_item_name'     => __( 'New Location', VR_BOOKING_TEXTDOMAIN ),
			'menu_name'         => __( 'Locations', VR_BOOKING_TEXTDOMAIN ),
		);

		register_taxonomy( 'locations', 'property', array(
			'labels'            => $property_locations_labels,
			'hierarchical'      => true,
			'query_var'         => 'location',
			'rewrite'           => array( 'slug' => 'location', 'hierarchical' => true ),
			'public'            => true,
			'show_ui'           => true,
			'show_admin_column' => true,
		) );

            $property_categories_labels = array(
			'name'              => __( 'Categories', VR_BOOKING_TEXTDOMAIN ),
			'singular_name'     => __( 'Category', VR_BOOKING_TEXTDOMAIN ),
			'search_items'      => __( 'Search Category', VR_BOOKING_TEXTDOMAIN ),
			'all_items'         => __( 'All Categories', VR_BOOKING_TEXTDOMAIN ),
			'parent_item'       => __( 'Parent Category', VR_BOOKING_TEXTDOMAIN ),
			'parent_item_colon' => __( 'Parent Category:', VR_BOOKING_TEXTDOMAIN ),
			'edit_item'         => __( 'Edit Category', VR_BOOKING_TEXTDOMAIN ),
			'update_itm'        => __( 'Update Category', VR_BOOKING_TEXTDOMAIN ),
			'add_new_item'      => __( 'Add New Category', VR_BOOKING_TEXTDOMAIN ),
			'new_item_name'     => __( 'New Category', VR_BOOKING_TEXTDOMAIN ),
			'menu_name'         => __( 'Categories', VR_BOOKING_TEXTDOMAIN ),
		);

		register_taxonomy( 'categories', 'property', array(
			'labels'            => $property_categories_labels,
			'hierarchical'      => true,
			'query_var'         => 'category-ap',
			'public'            => true,
			'show_ui'           => true,
			'show_admin_column' => true,
		) );

        $property_amenities_labels = array(
			'name'              => __( 'Features', VR_BOOKING_TEXTDOMAIN ),
			'singular_name'     => __( 'Feature', VR_BOOKING_TEXTDOMAIN ),
			'search_items'      => __( 'Search Feature', VR_BOOKING_TEXTDOMAIN ),
			'all_items'         => __( 'All Features', VR_BOOKING_TEXTDOMAIN ),
			'parent_item'       => __( 'Parent Feature', VR_BOOKING_TEXTDOMAIN ),
			'parent_item_colon' => __( 'Parent Feature:', VR_BOOKING_TEXTDOMAIN ),
			'edit_item'         => __( 'Edit Feature', VR_BOOKING_TEXTDOMAIN ),
			'update_itm'        => __( 'Update Feature', VR_BOOKING_TEXTDOMAIN ),
			'add_new_item'      => __( 'Add New Feature', VR_BOOKING_TEXTDOMAIN ),
			'new_item_name'     => __( 'New Feature', VR_BOOKING_TEXTDOMAIN ),
			'menu_name'         => __( 'Features', VR_BOOKING_TEXTDOMAIN ),
		);

		register_taxonomy( 'amenities', 'property', array(
			'labels'            => $property_amenities_labels,
			'hierarchical'      => true,
			'query_var'         => 'feature',
			'public'            => false,
			'show_ui'           => true,
			'show_admin_column' => true,
		) );
}

function create_booking_post_type(){

// Set UI labels for Custom Post Type
	$labels = array(
		'name'                => _x( 'All Reservations', 'Post Type General Name', VR_BOOKING_TEXTDOMAIN),
		'singular_name'       => _x( 'Reservation', 'Post Type Singular Name', VR_BOOKING_TEXTDOMAIN),
		'menu_name'           => __( 'PMS', VR_BOOKING_TEXTDOMAIN),
		'parent_item_colon'   => __( 'Parent Reservation', VR_BOOKING_TEXTDOMAIN),
		'all_items'           => __( 'All Reservations', VR_BOOKING_TEXTDOMAIN),
		'view_item'           => __( 'View Reservation', VR_BOOKING_TEXTDOMAIN),
		'add_new_item'        => __( 'Make New Reservation', VR_BOOKING_TEXTDOMAIN),
		'add_new'             => __( 'Make New Reservation', VR_BOOKING_TEXTDOMAIN),
		'edit_item'           => __( 'Modify Reservation', VR_BOOKING_TEXTDOMAIN),
		'update_item'         => __( 'Update Reservation', VR_BOOKING_TEXTDOMAIN),
		'search_items'        => __( 'Search Reservations', VR_BOOKING_TEXTDOMAIN),
		'not_found'           => __( 'Not Found', VR_BOOKING_TEXTDOMAIN),
		'not_found_in_trash'  => __( 'Not found in Trash', VR_BOOKING_TEXTDOMAIN),
	);

// Set other options for Custom Post Type

	$args = array(
		'label'               => __( 'bookings', VR_BOOKING_TEXTDOMAIN),
		'description'         => __( 'Reservations', VR_BOOKING_TEXTDOMAIN),
		'labels'              => $labels,
		// Features this CPT supports
		'supports'            => array('title'),
		'hierarchical'        => false,
		'public'              => false,
		'show_ui'             => true,
		'show_in_menu'        => true,
        'menu_position'        => 31,
        'menu_icon'           => 'dashicons-calendar-alt',
		'show_in_nav_menus'   => false,
		'show_in_admin_bar'   => true,
		'can_export'          => true,
		'has_archive'         => false,
		'exclude_from_search' => true,
		'publicly_queryable'  => true,
		'capability_type'     => 'post',
	);

	// Registering your Custom Post Type
	register_post_type( 'booking', $args );

    // remove_post_type_support( 'booking', 'title' );
}

function create_rate_post_type(){

// Set UI labels for Custom Post Type
	$labels = array(
		'name'                => _x( 'Rates', 'Post Type General Name', VR_BOOKING_TEXTDOMAIN),
		'singular_name'       => _x( 'Rate', 'Post Type Singular Name', VR_BOOKING_TEXTDOMAIN),
		'menu_name'           => __( 'Rate plans & promotions', VR_BOOKING_TEXTDOMAIN),
		'parent_item_colon'   => __( 'Parent Rate', VR_BOOKING_TEXTDOMAIN),
		'all_items'           => __( 'All Rates', VR_BOOKING_TEXTDOMAIN),
		'view_item'           => __( 'View Rate', VR_BOOKING_TEXTDOMAIN),
		'add_new_item'        => __( 'Add Rate Plan', VR_BOOKING_TEXTDOMAIN),
		'add_new'             => __( 'Add New', VR_BOOKING_TEXTDOMAIN),
		'edit_item'           => __( 'Edit Rate', VR_BOOKING_TEXTDOMAIN),
		'update_item'         => __( 'Update Rate', VR_BOOKING_TEXTDOMAIN),
		'search_items'        => __( 'Search Rate', VR_BOOKING_TEXTDOMAIN),
		'not_found'           => __( 'Not Found', VR_BOOKING_TEXTDOMAIN),
		'not_found_in_trash'  => __( 'Not found in Trash', VR_BOOKING_TEXTDOMAIN),
	);

// Set other options for Custom Post Type

	$args = array(
		'label'               => __( 'rates'),
		'description'         => __( 'Rates'),
		'labels'              => $labels,
		// Features this CPT supports
		'supports'            => array( 'title'),
		'hierarchical'        => false,
		'public'              => false,
		'show_ui'             => true,
		'show_in_menu'        => true,
        'menu_position'        => 28,
        'menu_icon'           => 'dashicons-clipboard',
		'show_in_nav_menus'   => false,
		'show_in_admin_bar'   => true,
		'can_export'          => true,
		'has_archive'         => false,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'post',
	);

	// Registering your Custom Post Type
	register_post_type( 'rate', $args );
}

/**
 * Define the metabox and field configurations.
 */
function cmb2_metaboxes() {

    // Start with an underscore to hide fields from custom fields list
    $prefix = '_property_';

    /**
     * Initiate the metabox
     */
    $cmb = new_cmb2_box( array(
        'id'            => 'property_metabox',
        'title'         => __( 'Other Fields', VR_BOOKING_TEXTDOMAIN ),
        'object_types'  => array( 'property', ), // Post type
        'context'       => 'normal',
        'priority'      => 'high',
        'show_names'    => true, // Show field names on the left
        // 'cmb_styles' => false, // false to disable the CMB stylesheet
        // 'closed'     => true, // Keep the metabox closed by default
    ) );

    // Regular text field
    $cmb->add_field( array(
        'name'       => __( 'Address', VR_BOOKING_TEXTDOMAIN ),
        'desc'       => __( 'Street, etc.', VR_BOOKING_TEXTDOMAIN ),
        'id'         => $prefix . 'address',
        'type'       => 'text',
        'attributes'  => array(
         'required'    => 'required',
         ),
        //'show_on_cb' => 'cmb2_hide_if_no_cats', // function should return a bool value
        // 'sanitization_cb' => 'my_custom_sanitization', // custom sanitization callback parameter
        // 'escape_cb'       => 'my_custom_escaping',  // custom escaping callback parameter
        // 'on_front'        => false, // Optionally designate a field to wp-admin only
        // 'repeatable'      => true,
    ) );

    $cmb->add_field( array(
		'name'     => __( 'Location', VR_BOOKING_TEXTDOMAIN ),
	   //	'desc'     => __( 'field description (optional)', VR_BOOKING_TEXTDOMAIN ),
		'id'       => $prefix . 'taxonomy_locations',
		'type'       => 'select',
        'show_option_none' => true,
        'attributes'  => array(
         'required'    => 'required',
         ),
        'options_cb' => 'cmb2_get_locations_options',
		// 'inline'  => true, // Toggles display to inline
	) );

    // Regular text field
    $cmb->add_field( array(
        'name'       => __( 'Location Latitude', VR_BOOKING_TEXTDOMAIN ),
       // 'desc'       => __( 'Street, etc.', VR_BOOKING_TEXTDOMAIN ),
        'id'         => $prefix . 'location_latitude',
        'type'       => 'text',
    ) );

    // Regular text field
    $cmb->add_field( array(
        'name'       => __( 'Location Longitude', VR_BOOKING_TEXTDOMAIN ),
       // 'desc'       => __( 'Street, etc.', VR_BOOKING_TEXTDOMAIN ),
        'id'         => $prefix . 'location_longitude',
        'type'       => 'text',
    ) );

    // Regular text field
    $cmb->add_field( array(
        'name'       => __( 'Size', VR_BOOKING_TEXTDOMAIN ),
       // 'desc'       => __( 'Street, etc.', VR_BOOKING_TEXTDOMAIN ),
        'id'         => $prefix . 'size',
        'type'       => 'text',
        'attributes'  => array(
         'required'    => 'required',
         ),
    ) );

    $cmb->add_field( array(
        'name'       => __( '# of beds', VR_BOOKING_TEXTDOMAIN ),
       // 'desc'       => __( 'Street, etc.', VR_BOOKING_TEXTDOMAIN ),
        'id'         => $prefix . 'max_beds',
        'type'       => 'text',
        'attributes'  => array(
         'required'    => 'required',
         ),
    ) );

    $cmb->add_field( array(
        'name'       => __( '# of rooms', VR_BOOKING_TEXTDOMAIN ),
       // 'desc'       => __( 'Street, etc.', VR_BOOKING_TEXTDOMAIN ),
        'id'         => $prefix . 'n_rooms',
        'type'       => 'text',
        'attributes'  => array(
         'required'    => 'required',
         ),
    ) );

    $cmb->add_field( array(
        'name'       => __( 'Bath type', VR_BOOKING_TEXTDOMAIN ),
       // 'desc'       => __( 'Street, etc.', VR_BOOKING_TEXTDOMAIN ),
        'id'         => $prefix . 'n_bath_type',
        'type'       => 'text',
    ) );

    $cmb->add_field( array(
        'name'       => __( 'Max # of Guests', VR_BOOKING_TEXTDOMAIN ),
       // 'desc'       => __( 'Street, etc.', VR_BOOKING_TEXTDOMAIN ),
        'id'         => $prefix . 'max_guests',
        'type'       => 'text',
        'attributes'  => array(
         'required'    => 'required',
         ),
    ) );

    $cmb->add_field( array(
        'name'       => sprintf(__( 'Min. price, %s', VR_BOOKING_TEXTDOMAIN ), $this->settings['currency']),
       // 'desc'       => __( 'Street, etc.', VR_BOOKING_TEXTDOMAIN ),
        'id'         => 'general_price',
        'type'       => 'text',
        'attributes'  => array(
         'required'    => 'required',
         ),
    ) );
    
    ///////// extra bed
    
    $cmb->add_field( array(
    'name'    => __( 'Assign extra bed', VR_BOOKING_TEXTDOMAIN ),
    'id'      => $prefix . 'extra_bed',
    'type'    => 'radio',
    'show_option_none' => true,
    'options_cb' => 'cmb2_get_extra_bed_options',
) );

    $args = array(
        'post_type'   => 'extra_bed',
        'numberposts' => -1,
    );
    $posts = get_posts( $args );
    if ( $posts ) {
        foreach ( $posts as $post ) {
          $cmb->add_field( array(
            'name' => $post->post_title,
            'id'   => $prefix . 'extra_bed_'.$post->ID,
            'type' => 'hidden',
            ) );
        }
    }
    
    //////////// Getting there

    $cmb->add_field( array(
        'name'       => __( 'Getting there', VR_BOOKING_TEXTDOMAIN ),
       // 'desc'       => __( 'Street, etc.', VR_BOOKING_TEXTDOMAIN ),
        'id'         => $prefix . 'getting_there',
        'type'       => 'textarea',
    ) );

    $cmb->add_field( array(
		'name'         => __( 'Appartment images', VR_BOOKING_TEXTDOMAIN ),
		'desc'         => __( 'Upload or add multiple images.', VR_BOOKING_TEXTDOMAIN ),
		'id'           => $prefix . 'file_list',
		'type'         => 'file_list',
		'preview_size' => array( 100, 100 ), // Default: array( 50, 50 )
        'attributes'  => array(
         'required'    => 'required',
         ),
	) );

    $cmb->add_field( array(
    'name'    => __( 'House Rules', VR_BOOKING_TEXTDOMAIN ),
    'desc'    => __( 'Apply policies to this property', VR_BOOKING_TEXTDOMAIN ),
    'id'      => $prefix . 'policies',
    'type'    => 'multicheck',
    'options_cb' => 'cmb2_get_policies_options',
) );

    $cmb->add_field( array(
    'name'    => __( 'Assign additional services', VR_BOOKING_TEXTDOMAIN ),
    'id'      => $prefix . 'services',
    'type'    => 'multicheck',
    'options_cb' => 'cmb2_get_services_options',
) );

    $args = array(
        'post_type'   => 'service',
        'numberposts' => -1,
    );
    $posts = get_posts( $args );
    if ( $posts ) {
        foreach ( $posts as $post ) {
          $cmb->add_field( array(
            'name' => __( 'Service ', VR_BOOKING_TEXTDOMAIN ).$post->post_title,
            'id'   => $prefix . 'service_'.$post->ID,
            'type' => 'hidden',
            ) );
        }
    }

    // Add other metaboxes as needed

    $prefix = '_service_';

    $cmb5 = new_cmb2_box( array(
        'id'            => 'service_metabox',
        'title'         => __( 'Price:', VR_BOOKING_TEXTDOMAIN ),
        'object_types'  => array( 'service' ), // Post type
        'context'       => 'normal',
        'priority'      => 'high',
        'show_names'    => true, // Show field names on the left
        // 'cmb_styles' => false, // false to disable the CMB stylesheet
        // 'closed'     => true, // Keep the metabox closed by default
    ) );

    $cmb5->add_field( array(
        'name'       => sprintf(__( 'Price, %s', VR_BOOKING_TEXTDOMAIN ), $this->settings['currency']),
        'id'         => $prefix . 'price',
        'type'       => 'text',
        'attributes'  => array(
         'required'    => 'required',
         ),
    ) );
    
////////////////////////////////////////

    $prefix = '_extra_bed_';

    $cmb5 = new_cmb2_box( array(
        'id'            => 'extra_metabox',
        'title'         => __( 'Price:', VR_BOOKING_TEXTDOMAIN ),
        'object_types'  => array( 'extra_bed' ), // Post type
        'context'       => 'normal',
        'priority'      => 'high',
        'show_names'    => true, // Show field names on the left
        // 'cmb_styles' => false, // false to disable the CMB stylesheet
        // 'closed'     => true, // Keep the metabox closed by default
    ) );

    $cmb5->add_field( array(
        'name'       => sprintf(__( 'Price, %s', VR_BOOKING_TEXTDOMAIN ), $this->settings['currency']),
        'id'         => $prefix . 'price',
        'type'       => 'text',
        'attributes'  => array(
         'required'    => 'required',
         ),
    ) );    

    // Add other metaboxes as needed

    $prefix = '_price_';

    $cmb2 = new_cmb2_box( array(
        'id'            => 'price_metabox',
        'title'         => __( 'Rates:', VR_BOOKING_TEXTDOMAIN ),
        'object_types'  => array( 'rate', ), // Post type
        'context'       => 'normal',
        'priority'      => 'high',
        'show_names'    => true, // Show field names on the left
        // 'cmb_styles' => false, // false to disable the CMB stylesheet
        // 'closed'     => true, // Keep the metabox closed by default
    ) );

    $cmb2->add_field( array(
        'name'       => sprintf(__( 'Sun, %s', VR_BOOKING_TEXTDOMAIN ), $this->settings['currency']),
        'desc'       => __( 'digits only', VR_BOOKING_TEXTDOMAIN ),
        'id'         => $prefix . 'price_sun',
        'type'       => 'text',
    ) );

    $cmb2->add_field( array(
        'name'       => sprintf(__( 'Mon, %s', VR_BOOKING_TEXTDOMAIN ), $this->settings['currency']),
        'desc'       => __( 'digits only', VR_BOOKING_TEXTDOMAIN ),
        'id'         => $prefix . 'price_mon',
        'type'       => 'text',
    ) );

    $cmb2->add_field( array(
        'name'       => sprintf(__( 'Tue, %s', VR_BOOKING_TEXTDOMAIN ), $this->settings['currency']),
        'desc'       => __( 'digits only', VR_BOOKING_TEXTDOMAIN ),
        'id'         => $prefix . 'price_tue',
        'type'       => 'text',
    ) );

    $cmb2->add_field( array(
        'name'       => sprintf(__( 'Wed, %s', VR_BOOKING_TEXTDOMAIN ), $this->settings['currency']),
        'desc'       => __( 'digits only', VR_BOOKING_TEXTDOMAIN ),
        'id'         => $prefix . 'price_wed',
        'type'       => 'text',
    ) );

    $cmb2->add_field( array(
        'name'       => sprintf(__( 'Thu, %s', VR_BOOKING_TEXTDOMAIN ), $this->settings['currency']),
        'desc'       => __( 'digits only', VR_BOOKING_TEXTDOMAIN ),
        'id'         => $prefix . 'price_thu',
        'type'       => 'text',
    ) );

    $cmb2->add_field( array(
        'name'       => sprintf(__( 'Fri, %s', VR_BOOKING_TEXTDOMAIN ), $this->settings['currency']),
        'desc'       => __( 'digits only', VR_BOOKING_TEXTDOMAIN ),
        'id'         => $prefix . 'price_fri',
        'type'       => 'text',
    ) );

    $cmb2->add_field( array(
        'name'       => sprintf(__( 'Sat, %s', VR_BOOKING_TEXTDOMAIN ), $this->settings['currency']),
        'desc'       => __( 'digits only', VR_BOOKING_TEXTDOMAIN ),
        'id'         => $prefix . 'price_sat',
        'type'       => 'text',
    ) );

    $cmb2->add_field( array(
        'name'       => sprintf(__( 'Fri + Sat, %s', VR_BOOKING_TEXTDOMAIN ), $this->settings['currency']),
        'desc'       => __( 'digits only', VR_BOOKING_TEXTDOMAIN ),
        'id'         => $prefix . 'price_fri_sat',
        'type'       => 'text',
    ) );

    $cmb2->add_field( array(
        'name'       => sprintf(__( '7 nights, %s', VR_BOOKING_TEXTDOMAIN ), $this->settings['currency']),
        'desc'       => __( 'digits only', VR_BOOKING_TEXTDOMAIN ),
        'id'         => $prefix . 'price_7',
        'type'       => 'text',
    ) );

    $cmb2->add_field( array(
    'name'    => __( 'Color', VR_BOOKING_TEXTDOMAIN ),
    'id'      => $prefix . 'color',
    'type'    => 'colorpicker',
    'default' => '#ffff54',
    ) );

    // Add other metaboxes as needed

    $prefix = '_booking_';

    $cmb3 = new_cmb2_box( array(
        'id'            => 'booking_metabox',
        'title'         => __( '&nbsp;', VR_BOOKING_TEXTDOMAIN ),
        'object_types'  => array( 'booking', ), // Post type
        'context'       => 'normal',
        'priority'      => 'high',
        'show_names'    => true, // Show field names on the left
        // 'cmb_styles' => false, // false to disable the CMB stylesheet
        // 'closed'     => true, // Keep the metabox closed by default
    ) );

    $cmb3->add_field( array(
    'name'       => __( 'Select Property', VR_BOOKING_TEXTDOMAIN ),
   // 'desc'       => __( 'field description (optional)', VR_BOOKING_TEXTDOMAIN ),
    'id'         => $prefix . 'prop_id',
    'type'       => 'select',
    'options_cb' => 'cmb2_get_property_options',
    'show_option_none' => true,
    'attributes'  => array(
         'required'    => 'required',
         ),
    'after_field' => array($this, 'cmb2_after_row'), // callback
    ) );

    $cmb3->add_field( array(
    'name' => __( 'Date from', VR_BOOKING_TEXTDOMAIN ),
    'id'   => $prefix . 'date_from',
    'type' => 'hidden',
    ) );

    $cmb3->add_field( array(
    'name' => __( 'Date to', VR_BOOKING_TEXTDOMAIN ),
    'id'   => $prefix . 'date_to',
    'type' => 'hidden',
    ) );

    $cmb3->add_field( array(
    'name' => __( 'Date to', VR_BOOKING_TEXTDOMAIN ),
    'id'   => $prefix . 'price_tax',
    'type' => 'hidden',
    ) );

    $cmb3->add_field( array(
    'name' => __( 'Date to', VR_BOOKING_TEXTDOMAIN ),
    'id'   => $prefix . 'price_clear',
    'type' => 'hidden',
    ) );

    $cmb3->add_field( array(
    'name' => __( 'Date from', VR_BOOKING_TEXTDOMAIN ),
    'id'   => 'range_from',
    'type' => 'text',
    'attributes'  => array(
         'required'    => 'required',
         ),
    ) );

    $cmb3->add_field( array(
    'name' => __( 'Date to', VR_BOOKING_TEXTDOMAIN ),
    'id'   => 'range_to',
    'type' => 'text',
    'attributes'  => array(
         'required'    => 'required',
         ),
    ) );


    $cmb3->add_field( array(
        'name'       => sprintf(__( 'Total amount, %s', VR_BOOKING_TEXTDOMAIN ), $this->settings['currency']),
        'id'         => $prefix . 'price',
        'type'       => 'text',
        'attributes'  => array(
         'required'    => 'required',
         ),
         'after_field' => array($this, 'cmb2_after_row_amount'), // callback
    ) );

    $cmb3->add_field( array(
        'name'       => __( 'First name', VR_BOOKING_TEXTDOMAIN ),
        //'desc'       => __( 'digits only', VR_BOOKING_TEXTDOMAIN ),
        'id'         => $prefix . 'first_name',
        'type'       => 'text',
        'attributes'  => array(
         'required'    => 'required',
         ),
    ) );

    $cmb3->add_field( array(
        'name'       => __( 'Last name', VR_BOOKING_TEXTDOMAIN ),
        //'desc'       => __( 'digits only', VR_BOOKING_TEXTDOMAIN ),
        'id'         => $prefix . 'last_name',
        'type'       => 'text',
        'attributes'  => array(
         'required'    => 'required',
         ),
    ) );

    $cmb3->add_field( array(
        'name'       => __( 'No. Adults', VR_BOOKING_TEXTDOMAIN ),
        //'desc'       => __( 'digits only', VR_BOOKING_TEXTDOMAIN ),
        'id'         => $prefix . 'n_adults',
        'type'       => 'text',
        'attributes'  => array(
         'required'    => 'required',
         ),
    ) );

    $cmb3->add_field( array(
        'name'       => __( 'No. Children', VR_BOOKING_TEXTDOMAIN ),
        //'desc'       => __( 'digits only', VR_BOOKING_TEXTDOMAIN ),
        'id'         => $prefix . 'n_children',
        'type'       => 'text',
        'attributes'  => array(
         'required'    => 'required',
         ),
    ) );

    $cmb3->add_field( array(
        'name'       => __( 'E-mail', VR_BOOKING_TEXTDOMAIN ),
        //'desc'       => __( 'digits only', VR_BOOKING_TEXTDOMAIN ),
        'id'         => $prefix . 'email',
        'type'       => 'text_email',
        'attributes'  => array(
         'required'    => 'required',
         ),
    ) );

    $cmb3->add_field( array(
        'name'       => __( 'Phone', VR_BOOKING_TEXTDOMAIN ),
        //'desc'       => __( 'digits only', VR_BOOKING_TEXTDOMAIN ),
        'id'         => $prefix . 'phone',
        'type'       => 'text',
        'attributes'  => array(
         'required'    => 'required',
         ),
    ) );

    $cmb3->add_field( array(
        'name'       => __( 'Special Requests', VR_BOOKING_TEXTDOMAIN ),
        //'desc'       => __( 'digits only', VR_BOOKING_TEXTDOMAIN ),
        'id'         => $prefix . 'notes',
        'type'       => 'textarea',
    ) );
    
    $cmb3->add_field( array(
    'name'    => __( 'Assign extra bed', VR_BOOKING_TEXTDOMAIN ),
    'id'      => $prefix . 'extra_bed',
    'type'    => 'radio',
    'show_option_none' => true,
    'options_cb' => 'cmb2_get_extra_bed_book_options',
) );

    $cmb3->add_field( array(
    'name'    => __( 'Assign additional services', VR_BOOKING_TEXTDOMAIN ),
    'id'      => $prefix . 'services',
    'type'    => 'multicheck',
    'options_cb' => 'cmb2_get_services_book_options',
) );

}

/////////////////

function cmb2_after_row(){

 echo '<h4>'.__( 'Availability calendar', VR_BOOKING_TEXTDOMAIN ).'</h4>
      <div id="av_calendar"></div>';

}

function cmb2_after_row_amount(){

 echo '<div id="spin_date_from"></div>';

}
///////////////////////

function update_property_location( $post_id, $post, $update ) {

    $slug = 'property';
    // If this isn't a 'catalog_item' post, don't update it.
    if (( $slug != $post->post_type )||( ! is_admin() )) {
        return;
    }
    
    $term_id = (int)$_POST['_property_taxonomy_locations']; //(int)get_post_meta( $post_id, '_property_taxonomy_locations', true );
    
    wp_set_object_terms( $post_id, $term_id, 'locations', false );
    
}
//////////////////////
function update_booking_title( $post_id, $post, $update ) {

    $slug = 'booking';
    // If this isn't a 'catalog_item' post, don't update it.
    if (( $slug != $post->post_type )||( ! is_admin() )) {
        return;
    }

    $prop_id = get_post_meta( $post_id, '_booking_prop_id', true );
    $price_arr = $this->get_all_rates_by_ap_id($prop_id, $this->date_to_sql(get_post_meta( $post_id, 'range_from', true )), $this->date_to_sql(get_post_meta( $post_id, 'range_to', true )), get_post_meta( $post_id, '_booking_extra_bed', true ));
    update_post_meta($post_id, '_booking_price_tax', $price_arr['total_tax']);
    update_post_meta($post_id, '_booking_price_clear', $price_arr['total_clear']);
    update_post_meta($post_id, '_booking_nights_price', $price_arr['nights_total']);
    update_post_meta($post_id, '_booking_nights_price_tax', $price_arr['nights_total_tax']);
    update_post_meta($post_id, '_booking_nights_price_clear', $price_arr['nights_total_clear']);

    if (strpos( $post->post_title , '-')) {
        return;
    }

  update_post_meta($post_id, '_booking_accepted', 1);
  update_post_meta($post_id, '_booking_paid', 0);
  update_post_meta($post_id, '_booking_paid_by', 'cash');
  update_post_meta($post_id, '_booking_token', '');
  update_post_meta($post_id, '_booking_created', time());
  $user_id = $this->create_customer();

    $title = date('ymd').'-'.date('His').mt_rand(10, 99);

    $my_post = array(
      'ID'           => $post_id,
      'post_title'   => $title,
  );
  wp_update_post( $my_post );

}

/////////////////

function create_customer(){
  $output = 0;

    if ((!empty($_POST['email']))||(!empty($_POST['_booking_email']))){

         if (!empty($_POST['email'])){
         $email = $_POST['email'];
         $first_name = $_POST['first_name'];
         $last_name = $_POST['last_name'];
         $tel1 = $_POST['tel1'];
         } else {
         $email = $_POST['_booking_email'];
         $first_name = $_POST['_booking_first_name'];
         $last_name = $_POST['_booking_last_name'];
         $tel1 = $_POST['_booking_phone'];
         }

        $user_id = username_exists( $email );
        if ( !$user_id and email_exists($email) == false ) {
          $random_password = wp_generate_password( $length=12, $include_standard_special_chars=false );
          $user_id = wp_create_user( $email, $random_password, $email );
          $output = $user_id;

          wp_update_user(
          array(
          'ID'       => $user_id,
          'nickname' => $email,
          'display_name' => $first_name.' '.$last_name,
          )
          );
          $user = new WP_User( $user_id );
          $user->set_role( 'subscriber' );

          update_user_meta($user_id, 'email', $email);
          update_user_meta($user_id, '_admin_notes', __('new customer', VR_BOOKING_TEXTDOMAIN));

          } else {
            $output = $user_id;
            wp_update_user(
            array(
            'ID'       => $user_id,
            'display_name' => $first_name.' '.$last_name,
           )
          );
            }
          update_user_meta($user_id, 'first_name', $first_name);
          update_user_meta($user_id, 'last_name', $last_name);
          update_user_meta($user_id, 'tel1', $tel1);
    }
  return $output;
}

/////////////////////////////////

function create_booking_ical($prop_id, $date_start, $date_end, $ical_name, $ical_date_start, $summary){
     $post_id = 0;
     if ($this->is_free_appartment($this->date_from_sql($date_start), $this->date_from_sql($date_end), $prop_id)){
        
        $price_arr = $this->get_all_rates_by_ap_id($prop_id, $date_start, $date_end);
          $price_total = '';

          if (!empty($price_arr)){
            $price_total = $price_arr['total'];
          }

     $post_id = wp_insert_post(array (
    'post_type' => 'booking',
    'post_title' => substr($ical_date_start,2,6).'-'.date('His').mt_rand(10, 99),
    'post_content' => '',
    'post_status' => 'publish',
    'comment_status' => 'closed',
    'post_author'   => 1,
    'meta_input'   => array(
        '_booking_prop_id' => $prop_id,
        '_booking_phone' => '',
        '_booking_email' => '',
        '_booking_first_name' => 'booked from '.$ical_name,
        '_booking_last_name' => '',
        '_booking_date_from' => strtotime($date_start),
        '_booking_date_to' => strtotime($date_end),
        'range_from' => $this->date_from_sql($date_start),
        'range_to' => $this->date_from_sql($date_end),
        '_booking_price' => $price_total,
        '_booking_price_tax' => $price_arr['total_tax'],
        '_booking_price_clear' => $price_arr['total_clear'],
        '_booking_nights_price' => $price_arr['nights_total'],
        '_booking_nights_price_tax' => $price_arr['nights_total_tax'],
        '_booking_nights_price_clear' => $price_arr['nights_total_clear'],
        '_booking_notes' => $summary,
        '_booking_accepted' => 1,
        '_booking_paid' => 0,
        '_booking_paid_by' => '',
        '_booking_token' => '',
        '_booking_created' => time(),
    ),
       ));

       do_action('vrb_create_booking', $post_id);
     }
  return $post_id;
}

function create_booking(){

       $post_id = 0;
       $user_id = $this->create_customer();

       if ($user_id){
        
       $children = (!empty($_POST['children'])) ? $_POST['children'] : 0;
       $is_extra_bed = $this->is_extra_bed($_POST['id'], $children + $_POST['adults']); 

       $price_arr = $this->get_all_rates_by_ap_id($_POST['id'], $this->date_to_sql($_POST['date_from']), $this->date_to_sql($_POST['date_to']), $is_extra_bed);  

       $post_id = wp_insert_post(array (
    'post_type' => 'booking',
    'post_title' => date('ymd').'-'.date('His').mt_rand(10, 99),
    'post_content' => '',
    'post_status' => 'publish',
    'comment_status' => 'closed',
    'post_author'   => 1,
    'meta_input'   => array(
        '_booking_prop_id' => $_POST['id'],
        '_booking_phone' => $_POST['tel1'],
        '_booking_email' => $_POST['email'],
        '_booking_n_children' => $_POST['children'],
        '_booking_n_adults' => $_POST['adults'],
        '_booking_extra_bed' => $is_extra_bed,
        '_booking_first_name' => $_POST['first_name'],
        '_booking_last_name' => $_POST['last_name'],
        '_booking_date_from' => strtotime($this->date_to_sql($_POST['date_from'])),
        '_booking_date_to' => strtotime($this->date_to_sql($_POST['date_to'])),
        'range_from' => $_POST['date_from'],
        'range_to' => $_POST['date_to'],
        '_booking_price' => $price_arr['total'],
        '_booking_price_tax' => $price_arr['total_tax'],
        '_booking_price_clear' => $price_arr['total_clear'],
        '_booking_nights_price' => $price_arr['nights_total'],
        '_booking_nights_price_tax' => $price_arr['nights_total_tax'],
        '_booking_nights_price_clear' => $price_arr['nights_total_clear'],
        '_booking_notes' => $_POST['notes'],
        '_booking_accepted' => 0,
        '_booking_paid' => 0,
        '_booking_paid_by' => '',
        '_booking_token' => '',
        '_booking_created' => time(),
    ),
       ));

       do_action('vrb_create_booking', $post_id);

    if (isset($_POST['services'])){
    $services_booking = array();
    $services_arr_post = get_post_meta( $_POST['id'], '_property_services', true );
    $services_arr = array_keys($_POST['services']);

        foreach ( $services_arr as $key => $serv_id ) {
          if (in_array( $serv_id, $services_arr_post ))
          $services_booking[] = $serv_id;
        }
       update_post_meta($post_id, '_booking_services', $services_booking);
      }
     }
  return $post_id;
}

//////////// get extra bed price 

function get_extra_bed_price($extra_bed_id, $prop_id){
    $price = get_post_meta($prop_id, '_property_extra_bed_'.$extra_bed_id, true);
     if ($price == '')
      $price = get_post_meta($extra_bed_id, '_extra_bed_price', true);
    
    return $price;
}

////////  Invoice ///////////

function invoice_page_shortcode(){
   $output = '';
     if (get_query_var('reserv_code')){
      $reserv_code = get_query_var('reserv_code');
      $page = get_page_by_title( $reserv_code, OBJECT, 'booking' );

      if (!empty($page)){

      $date_from = get_post_meta( $page->ID, '_booking_date_from', true );
      $date_to = get_post_meta( $page->ID, '_booking_date_to', true );
      
      $is_extra_bed = get_post_meta( $page->ID, '_booking_extra_bed', true );  
     // $extra_acc_text = $is_extra_bed ? __(' + 1 extra bed', VR_BOOKING_TEXTDOMAIN) : '';

      //$price_arr = $this->get_all_rates_by_ap_id(get_post_meta( $page->ID, '_booking_prop_id', true ), $this->date_to_sql($date_from), $this->date_to_sql($date_to));

      $nights = date_diff(date_create(date( "Y-m-d", $date_from)), date_create(date( "Y-m-d", $date_to)))->days;

        $output .= '<div id="invoice">
        <div id="printable">
        <span id="printable1">'.__('Printable Area', VR_BOOKING_TEXTDOMAIN).'</span>
        <span id="printable2">'.__('Printable Area', VR_BOOKING_TEXTDOMAIN).'</span>
        </div>
        <img id="invoice_logo" src="'.$this->settings['invoice_logo'].'">
        <h5>'.$this->settings['invoice_title'].'</h5>
        <h2>'.__('Invoice', VR_BOOKING_TEXTDOMAIN).'</h2>
        <div id="invoice_header">
        <div>'.get_post_meta( $page->ID, '_booking_first_name', true ).' '.get_post_meta( $page->ID, '_booking_last_name', true ).'<br />
        '.get_post_meta( $page->ID, '_booking_email', true ).'<br />
        '.__('Invoice # ', VR_BOOKING_TEXTDOMAIN).$reserv_code.'<br />'.__('date: ', VR_BOOKING_TEXTDOMAIN).
        date("d/m/Y").'
        </div>
        <div>'.str_replace(array("\n"), "<br/>", $this->settings['invoice_header']).'
        </div>
        </div>
        <h4>'.__('Invoice details', VR_BOOKING_TEXTDOMAIN).'</h4>
        <table id="invoice_table">
        <tbody>
        <tr><td>'.get_post_meta( $page->ID, '_booking_first_name', true ).' '.get_post_meta( $page->ID, '_booking_last_name', true ).'<br />'.get_the_title(get_post_meta( $page->ID, '_booking_prop_id', true )).'<br />'.sprintf(__('From %s To %s %d nights<br /> %d %s/night (avg.)', VR_BOOKING_TEXTDOMAIN), date( "d/m/Y", $date_from), date( "d/m/Y", $date_to), $nights, round(floatval(get_post_meta( $page->ID, '_booking_nights_price_clear', true ))/floatval($nights), 2), $this->settings['currency']).'</td>
        <td>'.$this->format_currency(floatval(get_post_meta( $page->ID, '_booking_nights_price_clear', true ))).'</td></tr>';
        
        $prop_id = get_post_meta($page->ID, '_booking_prop_id', true);
        
        if ($is_extra_bed){
            $output .= '<tr><td>'.__(' + 1 extra bed', VR_BOOKING_TEXTDOMAIN).'</td>
            <td>'.$this->format_currency($this->get_extra_bed_price($is_extra_bed, $prop_id)*$nights).'</td></tr>';  
        }

        $services = get_post_meta($page->ID, '_booking_services', true);
         if(!empty($services)){
          foreach ( $services as $service_id ) {
          $price = '';
          $price = get_post_meta($prop_id, '_property_service_'.$service_id, true);
          if ($price == '')
          $price = get_post_meta($service_id, '_service_price', true);
          $output .= '<tr><td>'.get_the_title($service_id).'</td>
          <td>'.$this->format_currency(floatval($price)).'</td></tr>';
         }
        }

        if($this->settings['tax']){

        $output .= '<tr><td class="subtotal">'.__('Subtotal', VR_BOOKING_TEXTDOMAIN).'</td>
        <td>'.$this->format_currency(floatval(get_post_meta( $page->ID, '_booking_price_clear', true ))).'</td></tr>

        <tr><td>'.__('Taxes ', VR_BOOKING_TEXTDOMAIN).$this->settings['tax'].' %</td>
        <td>'.$this->format_currency(floatval(get_post_meta( $page->ID, '_booking_price_tax', true ))).'</td></tr>';

        }

        $output .= '<tr><td class="subtotal">'.__('Total', VR_BOOKING_TEXTDOMAIN).'</td>
        <td class="subtotal">'.$this->format_currency(floatval(get_post_meta( $page->ID, '_booking_price', true ))).'</td></tr>

        </tbody></table>

        <span>
        <input type="button" id="print" class="bc-button-color" value="'.__('PRINT', VR_BOOKING_TEXTDOMAIN).'">
        </span>

        <div id="invoice_footer">'.str_replace(array("\n"), "<br/>", $this->settings['invoice_footer']).'
        <div>
        ';

        $output .= '</div>';
      }
    }
   return $output;
}

////////// Thank You e-mail  /////

public function thanks_email($reserv_code){

     if ($reserv_code){

      $page = get_page_by_title( $reserv_code, OBJECT, 'booking' );

      if (!empty($page)){
      $post_id = get_post_meta( $page->ID, '_booking_prop_id', true );
      
      $is_extra_bed = get_post_meta( $page->ID, '_booking_extra_bed', true );  
      $extra_acc_text = $is_extra_bed ? __(' ( + 1 extra bed)', VR_BOOKING_TEXTDOMAIN) : '';

      $nights = date_diff(date_create(date( "Y-m-d", get_post_meta( $page->ID, '_booking_date_from', true ))), date_create(date( "Y-m-d", get_post_meta( $page->ID, '_booking_date_to', true ))))->days;

      $body = __($this->settings['confirm_email_header']);

      $term_list = wp_get_post_terms($post_id, 'locations', array("fields" => "all"));

      $body .= '

      '.__('Booking details', VR_BOOKING_TEXTDOMAIN).'

      '.__('Apartment: ', VR_BOOKING_TEXTDOMAIN).get_the_title($post_id).'
      '.__('Accommodates: ', VR_BOOKING_TEXTDOMAIN).get_post_meta( $post_id, '_property_max_guests', true ).$extra_acc_text.'
      '.__('rooms: ', VR_BOOKING_TEXTDOMAIN).get_post_meta( $post_id, '_property_n_rooms', true ).'
      '.__('Bathrooms: ', VR_BOOKING_TEXTDOMAIN).get_post_meta( $post_id, '_property_n_bath_type', true ).'
      '.__('Location: ', VR_BOOKING_TEXTDOMAIN).$term_list[0]->name.' '.get_post_meta( $post_id, '_property_address', true ).'
      '.__('Getting there: ', VR_BOOKING_TEXTDOMAIN).get_post_meta( $post_id, '_property_getting_there', true ).'

      '.__('Reservation number: ', VR_BOOKING_TEXTDOMAIN).$reserv_code.'
      '.__('First name: ', VR_BOOKING_TEXTDOMAIN).get_post_meta( $page->ID, '_booking_first_name', true ).'
      '.__('Last name: ', VR_BOOKING_TEXTDOMAIN).get_post_meta( $page->ID, '_booking_last_name', true ).'
      '.__('Arrival Date: ', VR_BOOKING_TEXTDOMAIN).date( "d/m/Y", get_post_meta( $page->ID, '_booking_date_from', true )).'
      '.__('Departure Date: ', VR_BOOKING_TEXTDOMAIN).date( "d/m/Y", get_post_meta( $page->ID, '_booking_date_to', true )).'
      '.__('Nights: ', VR_BOOKING_TEXTDOMAIN).$nights.'
      '.__('Adults: ', VR_BOOKING_TEXTDOMAIN).get_post_meta( $page->ID, '_booking_n_adults', true ).'
      '.__('Children: ', VR_BOOKING_TEXTDOMAIN).get_post_meta( $page->ID, '_booking_n_children', true ).'
      '.__('Special Requests: ', VR_BOOKING_TEXTDOMAIN).get_post_meta( $page->ID, '_booking_notes', true ).'
      '.__('Contacts: ', VR_BOOKING_TEXTDOMAIN).get_post_meta( $page->ID, '_booking_email', true ).' '.get_post_meta( $page->ID, '_booking_phone', true ).'
      '.__('Price per night (avg.): ', VR_BOOKING_TEXTDOMAIN).round(floatval(get_post_meta( $page->ID, '_booking_nights_price', true ))/floatval($nights), 2).' '.$this->settings['currency_code'];

      $prop_id = get_post_meta($page->ID, '_booking_prop_id', true);
        $services = get_post_meta($page->ID, '_booking_services', true);
         if(!empty($services)){
          $body_tmp = '';
          foreach ( $services as $service_id ) {
          $body_tmp .= '
          '.get_the_title($service_id).': '.$this->format_currency($this->get_service_price($prop_id, $service_id));
         }
          if ($body_tmp) $body .= '

      '.__('SERVICES: ', VR_BOOKING_TEXTDOMAIN).$body_tmp;
        }

      $body .= '

      '.__('TOTAL PRICE: ', VR_BOOKING_TEXTDOMAIN).get_post_meta( $page->ID, '_booking_price', true ).' '.$this->settings['currency_code'];

      $body .= '

      '.__('PAID: ', VR_BOOKING_TEXTDOMAIN).get_post_meta( $page->ID, '_booking_paid', true ).' '.$this->settings['currency_code'];

      $body .= '

      --
      ';

      wp_mail( get_post_meta( $page->ID, '_booking_email', true ), $this->settings['confirm_email_subject'], $body );
      wp_mail( $this->settings['confirm_email'], $this->settings['confirm_email_subject'], $body );

      }
  }

}

////////// Thank You page  /////

function thanks_page_shortcode(){

   $output = '';

   $reserv_code = '';
    if (!empty($_POST['custom'])) $reserv_code = $_POST['custom'];
    elseif (get_query_var('reserv_code')) $reserv_code = get_query_var('reserv_code');

     if ($reserv_code){

      //$reserv_code = get_query_var('reserv_code');

     // $this->thanks_email($reserv_code);

      $page = get_page_by_title( $reserv_code, OBJECT, 'booking' );

      if (!empty($page)){
      $post_id = get_post_meta( $page->ID, '_booking_prop_id', true );

      $output .= '<div id="thanks-block">';

      //<h3>'.__('Thank you for your request, one of our staff will respond shortly.', VR_BOOKING_TEXTDOMAIN).'</h3>';

      $output .= __('Reservation number: ', VR_BOOKING_TEXTDOMAIN). $reserv_code . '<br><br>
      </div>';

      $output .= '<h2 id="title1">'.__('Your reservation details', VR_BOOKING_TEXTDOMAIN).'</h2>';

      $output .= $this->apt_info($post_id, $page->ID);

      $output .= '<div id="thanks">';

      $nights = date_diff(date_create(date( "Y-m-d", get_post_meta( $page->ID, '_booking_date_from', true ))), date_create(date( "Y-m-d", get_post_meta( $page->ID, '_booking_date_to', true ))))->days;

      $output .= '<table><tbody>
          <tr><td>'.__('First name: ', VR_BOOKING_TEXTDOMAIN).'
          </td><td>'.get_post_meta( $page->ID, '_booking_first_name', true ).'
          </td></tr>
          <tr><td>'.__('Last name: ', VR_BOOKING_TEXTDOMAIN).'
          </td><td>'.get_post_meta( $page->ID, '_booking_last_name', true ).'
          </td></tr>

          <tr><td>'.__('Arrival Date: ', VR_BOOKING_TEXTDOMAIN).'
          </td><td>'.date( "d/m/Y", get_post_meta( $page->ID, '_booking_date_from', true )).'
          </td></tr>
          <tr><td>'.__('Departure Date: ', VR_BOOKING_TEXTDOMAIN).'
          </td><td>'.date( "d/m/Y", get_post_meta( $page->ID, '_booking_date_to', true )).'
          </td></tr>
          <tr><td>'.__('Nights: ', VR_BOOKING_TEXTDOMAIN).'
          </td><td>'.$nights.'
          </td></tr>
          <tr><td>'.__('Adults: ', VR_BOOKING_TEXTDOMAIN).'
          </td><td>'.get_post_meta( $page->ID, '_booking_n_adults', true ).'
          </td></tr>
          <tr><td>'.__('Children: ', VR_BOOKING_TEXTDOMAIN).'
          </td><td>'.get_post_meta( $page->ID, '_booking_n_children', true ).'
          </td></tr>
          <tr><td>'.__('Special Requests: ', VR_BOOKING_TEXTDOMAIN).'
          </td><td>'.get_post_meta( $page->ID, '_booking_notes', true ).'
          </td></tr>
          <tr><td>'.__('Contacts: ', VR_BOOKING_TEXTDOMAIN).'
          </td><td>'.get_post_meta( $page->ID, '_booking_email', true ).'<br>'.get_post_meta( $page->ID, '_booking_phone', true ).'
          </td></tr>
          <tr><td>'.__('Price per night (avg.): ', VR_BOOKING_TEXTDOMAIN).'
          </td><td>'.$this->format_currency(round(floatval(get_post_meta( $page->ID, '_booking_nights_price', true ))/floatval($nights), 2)).'
          </td></tr>
          <tr><td>'.__('TOTAL PRICE: ', VR_BOOKING_TEXTDOMAIN).'
          </td><td>'.$this->format_currency(get_post_meta( $page->ID, '_booking_price', true )).'
          </td></tr>
      </tbody></table>';

      if ($this->settings['print_button'])
      $output .= '<input type="button" class="bc-button-color" id="print" value="'.__('PRINT', VR_BOOKING_TEXTDOMAIN).'">';

      $output .= '</div>';
      }
  }

  // print_r($_POST);
   return $output;
}

///////////////////
function property_page_shortcode(){
   $output = '';
$post_id = get_the_ID();
   $term_list = wp_get_post_terms($post_id, 'locations', array("fields" => "all"));

                  $output = '<div id="book_now_ap_block">
                  <div id="book_now_modify_block">
                  <input type="button" id="modify" class="bc-button-color" name="modify" value="'.__('BOOK NOW', VR_BOOKING_TEXTDOMAIN).'">
                  </div>
                 <div id="search-form">
                 '.$this->form_get_quote_by_ap_id($post_id, $term_list[0]->term_id).'
                 </div>
                 <div id="search-ap-spin">
                 </div>
                 <div id="search-ap-res">
                 '.__('Sorry, this appartment is not available for these dates. Please try another dates.', VR_BOOKING_TEXTDOMAIN).'
                 </div>
                 </div>';

               $output .= '<div id="app-page">';

               /////////// Photos
               if ($this->settings['google_map_active']){$map_title = '<span class="doc_tab" data-x="app-map">'.__('Map', VR_BOOKING_TEXTDOMAIN).'</span>';}
               else $map_title = '';

               if ($this->settings['reviews_active']){$review_title = '<span class="doc_tab" data-x="app-reviews">'.__('Reviews', VR_BOOKING_TEXTDOMAIN).'</span>';}
               else $review_title = '';

               $output .= '<div id="tab_block"><span class="doc_tab cur_tab" data-x="app-photos">'.__('Photos', VR_BOOKING_TEXTDOMAIN).'</span><span class="doc_tab" data-x="app-desc">'.__('Description', VR_BOOKING_TEXTDOMAIN).'</span>'.$map_title.'<span class="doc_tab" data-x="app-features">'.__('Features', VR_BOOKING_TEXTDOMAIN).'</span>'.$review_title.'
              </div>';

               $output .= '<div id="app-photos" class="doc_tab_div active">';

               $output_g = '';

               $files = get_post_meta( $post_id, '_property_file_list', 1 );
               foreach ( (array) $files as $attachment_id => $attachment_url ) {
                 $image_large = wp_get_attachment_image_src($attachment_id, 'large');
                  $image_thumb = wp_get_attachment_image_src($attachment_id, 'post1-thumbnail');

                  $output_g .= '<li data-thumb="'.$image_thumb[0].'" data-src="'.$image_large[0].'">
    <img src="'.$image_large[0].'" />
  </li>';
               }

               if ($output_g) $output .= '<ul id="imageGallery">'.$output_g.'</ul>';
               $output .= '</div>';

               ////////// Decription

               $output .= '<div id="app-desc" class="doc_tab_div">';

               $output .= apply_filters( 'the_content', get_the_content());

               $output .= '</div>';

               ////////// Map

              //
               if ($this->settings['google_map_active']){

               $marker = $this->get_post_marker($post_id, 1, $this->get_price_min($post_id));
               $marker_center = 'lat: '.get_post_meta( $post_id, '_property_location_latitude', true ).', lng: '.get_post_meta( $post_id, '_property_location_longitude', true );

               $arr['markers'] = $marker;
               $arr['marker_center'] = $marker_center;

               $output .= '<div id="app-map" class="doc_tab_div">';

               $output .= '<div id="map_all"></div>';

               $output .= $this->get_map_all($arr);

               $output .= '</div>';
               }
               ////////// Features

               $output .= '<div id="app-features" class="doc_tab_div">';

               $output_g = '';

               $term_list = wp_get_post_terms($post_id, 'amenities', array("fields" => "all", 'parent' => 0));
               foreach($term_list as $term_single) {
                 //print_r($term_single);
                 $output_g .= '<li>'.$term_single->name;
                 $term_children = get_term_children($term_single->term_id, 'amenities');

                 $temp_output = '';
                 foreach($term_children as $term_child_id) {
                   $term_child = get_term_by('id', $term_child_id, 'amenities');

                   if (!is_wp_error( wp_get_post_terms($post_id, 'amenities', array("fields" => "all", 'term_id' => $term_child_id)) ))
                   $temp_output .= '<li>' . $term_child->name . '</li>';
                 }
                 if ($temp_output) $output_g .= '<ul>'.$temp_output.'</ul>'.'</li>';
                   else $output_g .= '</li>';

                }

               if ($output_g) $output .= '<ul>'.$output_g.'</ul>';

               $output .= '</div>';

               ////////// Reviews
               if ($this->settings['reviews_active']){

               $output .= '<div id="app-reviews" class="doc_tab_div">';

               $output .= do_shortcode('[RICH_REVIEWS_SHOW category="page"]');

               if ($this->settings['reviews_mode'] == 2){
                 $output .= '<h2 id="review_block_h2">'.__('Add Review', VR_BOOKING_TEXTDOMAIN).'</h2>';
                 $output .= do_shortcode('[RICH_REVIEWS_FORM]');
               }

               $output .= '</div>';
               }

               $output .= '</div>';

               if (($this->settings['reviews_active'])&&($this->settings['reviews_mode'] == 1)){
                 $output .= '<h2 id="review_block_h2">'.__('Add Review', VR_BOOKING_TEXTDOMAIN).'</h2>';
                 $output .= do_shortcode('[RICH_REVIEWS_FORM]');
               }

   return $output;
}

function get_service_price($post_id, $service_id){
  $price = '';
  $price = get_post_meta($post_id, '_property_service_'.$service_id, true);
  if ($price == '')
  $price = get_post_meta($service_id, '_service_price', true);

  if (!empty($this->settings['tax']))
  $tax_am = floatval($this->settings['tax'])/100;
  else $tax_am = 0;

  $price = $price*(1+$tax_am);

  return $price;
}

//////////// Services Page  //////////

function services_page_shortcode(){
  $output = '';

  if (!empty($_POST['id'])){
   if (!empty($_POST['children'])) $children = $_POST['children'];
  else $children = 0;

   $output .= $this->form_search_request2();

  // $output .= $this->apt_info($_POST['id']);

   $output .= '<div id="service-block">
  <form id="services" name="services" method="post" action="'.$this->home_url.'/booking">';

   $services_arr = get_post_meta( $_POST['id'], '_property_services', true );

   $args = array(
        'post_type'   => 'service',
        'numberposts' => -1,
    );
    $posts = get_posts( $args );
    if ( $posts ) {
        foreach ( $posts as $post ) {
          if (in_array( $post->ID, (array) $services_arr )){
            $output .= '<div>
            <div class="add_service_block">
            <div class="service_content_block">
            <h3>'.$post->post_title.'</h3>'.
            get_the_post_thumbnail($post->ID, 'post1-thumbnail' ).$post->post_excerpt.'</div>';
            $price = $this->get_service_price($_POST['id'], $post->ID);
            $output .= '<div class="service_price_block"><span class="service_price">'.$this->format_currency($price).'</span>';
            $output .= '<span class="include_service">
  <input id="services['.$post->ID.']" name="services['.$post->ID.']" type="checkbox" value="1">
  <label for="services['.$post->ID.']">'.__('Add', VR_BOOKING_TEXTDOMAIN).'</label>
  </span>
  </div>';
            $output .= '</div>
            </div>';
          }
        }
      }


   $output .= '<div id="proceed-button">
  <input type="submit" class="bc-button-color" name="book" id="book" value="'.__('PROCEED WITH BOOKING', VR_BOOKING_TEXTDOMAIN).'">
  </div>
  <input type="hidden" name="date_from" value="'.$_POST['date_from'].'">
  <input type="hidden" name="date_to" value="'.$_POST['date_to'].'">
  <input type="hidden" name="location" value="'.$_POST['location'].'">
  <input type="hidden" name="adults" value="'.$_POST['adults'].'">
  <input type="hidden" name="children" value="'.$children.'">
  <input type="hidden" name="rooms" value="'.$_POST['rooms'].'">
  <input type="hidden" name="id" value="'.$_POST['id'].'">
  </form>

  </div>';
  }

  return $output;
}

//////////// Booking Page  ////////

function booking_page_shortcode(){

  $output = '';

  if (!empty($_POST['id'])){
    
  if (!empty($_POST['children'])) $children = $_POST['children'];
  else $children = 0;
  
  $is_extra_bed = $this->is_extra_bed($_POST['id'], $children + $_POST['adults']);  

  $price_arr = $this->get_all_rates_by_ap_id($_POST['id'], $this->date_to_sql($_POST['date_from']), $this->date_to_sql($_POST['date_to']), $is_extra_bed);

  $amount = round($price_arr['total'], 2)*100;

  $deposit = round($price_arr['deposit'], 2)*100;

  $output .= $this->form_search_request2();

  $output .= '<h2 id="title1">'.__('Your booking', VR_BOOKING_TEXTDOMAIN).'</h2>';

   $output .= $this->apt_info($_POST['id']);

  $output .= '<div id="booking-block">
  <span>'.__('Please review your information, then fill out the booking form. All fields marked with * must be filled in.', VR_BOOKING_TEXTDOMAIN).'</span>
  <form id="booking" name="booking" method="post">
  <h3>'.__('Guest', VR_BOOKING_TEXTDOMAIN).'</h3>

  <div>
  <label for="first_name">'.__('First Name*', VR_BOOKING_TEXTDOMAIN).'</label><br>
  <input id="first_name" name="first_name" type="text" value="">
  </div>

  <div>
  <label for="last_name">'.__('Last Name*', VR_BOOKING_TEXTDOMAIN).'</label><br>
  <input id="last_name" name="last_name" type="text" value="">
  </div>

  <h3>'.__('Special Requests', VR_BOOKING_TEXTDOMAIN).'</h3>
  <div>
  <textarea id="notes" name="notes"></textarea>
  </div>

  <h3>'.__('Contacts', VR_BOOKING_TEXTDOMAIN).'</h3>
  <div>
  <label for="email">'.__('Email*', VR_BOOKING_TEXTDOMAIN).'</label><br>
  <input id="email" name="email" type="text" value="">
  </div>

  <div>
  <label for="tel1">'.__('Phone/Mobile*', VR_BOOKING_TEXTDOMAIN).'</label><br>
  <input id="tel1" name="tel1" type="text" value="">
  </div>';

  $add_fields = '';
  $add_fields = apply_filters('vrb_booking_form_fields', $add_fields);
  $output .= $add_fields;

  if ($this->settings['add_accept_term'])
  $output .= '
  <span>
  <input id="accept_term" name="accept_term" type="checkbox" value="1">
  <label for="accept_term">'.__('I have read and accept the ', VR_BOOKING_TEXTDOMAIN).'<a href="'.get_permalink(get_option('VR_BOOKING_page_terms')).'" target="_blank">'.get_the_title(get_option('VR_BOOKING_page_terms')).'.</a></label>
  </span>';

  if ($this->settings['add_accept_term_adds'])
  $output .= '
  <span>
  <input id="accept_term_adds" name="accept_term_adds" type="checkbox" value="1">
  <label for="accept_term_adds">'.__('I have read and accept the ', VR_BOOKING_TEXTDOMAIN).'<a href="'.get_permalink(get_option('VR_BOOKING_page_terms_adds')).'" target="_blank">'.get_the_title(get_option('VR_BOOKING_page_terms_adds')).'.</a></label>
  </span>
  ';
  
  $output .= '<div id="payment_methods">';

  if ($this->settings['paypal_activate'])
  $output .= '<span class="b-payment-radio">
  <input id="paypal" name="payment_method" type="radio" value="paypal" checked>
  <label for="paypal"><img class="booking_payment_img" src="https://www.paypalobjects.com/webstatic/mktg/Logo/pp-logo-200px.png" border="0" alt="PayPal Acceptance Mark"><br />'.__('Pay with PayPal', VR_BOOKING_TEXTDOMAIN).'</label>
   </span>
   ';

  if ($this->settings['stripe_activate']){
  $output .= '<span class="b-payment-radio">
  <input id="stripe" name="payment_method" type="radio" value="stripe">
  <label for="stripe"><img class="booking_payment_img" src="'.plugins_url( 'css/img/stripe_cards.png', __FILE__ ).'" border="0" alt="Stripe Acceptance Mark"><br />'.__('Pay with credit card', VR_BOOKING_TEXTDOMAIN).'</label>
   </span>
   
   <div
			id="stripe-payment-data"
			data-description="Powered by STRIPE"
			data-amount='.$amount.'
			data-name="' . esc_attr( get_bloginfo( 'name', 'display' ) ) . '"
            data-image=""
            data-locale="auto"
            data-zip-code="false"
            data-currency="'.strtolower($this->settings['currency_code']).'">
   </div>
   ';
  }

  if (($this->settings['payment_mode1'])and(($this->settings['stripe_activate'])||($this->settings['paypal_activate']))){
  $output .= '<span class="b-payment-radio">
  <input id="cash" name="payment_method" type="radio" value="cash">
  <label for="cash">'.__('Pay Cash', VR_BOOKING_TEXTDOMAIN).'</label>
   </span>
   </div>';
   } else {
      $output .= '<input id="cash" name="payment_method" type="hidden" value="cash">';
   }

  if (($this->settings['payment_mode2'])and($this->settings['payment_mode3'])){

     $output .= '<div id="select_amount" data-f="'.$amount.'" data-d="'.$deposit.'">
  <h3>'.__('Select payment amount', VR_BOOKING_TEXTDOMAIN).'</h3>
  <span>
  <input id="full_amount" name="amount" type="radio" value="full" checked>
  <label for="full_amount">'.__('Full amount: ', VR_BOOKING_TEXTDOMAIN).$this->format_currency(round($price_arr['total'], 2)).'</label>
  </span>
  <span>&nbsp;&nbsp;&nbsp;
  <input id="deposit_amount" name="amount" type="radio" value="deposit">
  <label for="deposit_amount">'.__('Deposit: ', VR_BOOKING_TEXTDOMAIN).$this->format_currency(round($price_arr['deposit'], 2)).'</label>
  </span>
  </div>';

  } elseif ($this->settings['payment_mode2']){
     $output .= '<div id="select_amount" data-f="'.$amount.'" data-d="'.$deposit.'">
  <h3>'.__('Deposit amount to pay now: ', VR_BOOKING_TEXTDOMAIN).$this->format_currency(round($price_arr['deposit'], 2)).'</h3>
  <input name="amount" type="hidden" value="deposit">
  </div>';
  } elseif ($this->settings['payment_mode3']){
     $output .= '<div id="select_amount" data-f="'.$amount.'" data-d="'.$deposit.'">
  <h3>'.__('Amount to pay now: ', VR_BOOKING_TEXTDOMAIN).$this->format_currency(round($price_arr['total'], 2)).'</h3>
  <input name="amount" type="hidden" value="full">
  </div>';
  } else {
     $output .= '<h3>'.__('Amount to pay later: ', VR_BOOKING_TEXTDOMAIN).$this->format_currency(round($price_arr['total'], 2)).'</h3>';
  $output .= '<input type="hidden" name="amount" value="full">';
  }

  $output .= '<div>
  <input type="submit" class="bc-button-color" name="book" id="book" value="BOOK NOW">
  </div>
  <input type="hidden" name="date_from" value="'.$_POST['date_from'].'">
  <input type="hidden" name="date_to" value="'.$_POST['date_to'].'">
  <input type="hidden" name="location" value="'.$_POST['location'].'">
  <input type="hidden" name="adults" value="'.$_POST['adults'].'">
  <input type="hidden" name="children" value="'.$children.'">
  <input type="hidden" name="rooms" value="'.$_POST['rooms'].'">
  <input type="hidden" name="id" value="'.$_POST['id'].'">';

  if (isset($_POST['services']))
     foreach ( $_POST['services'] as $service_id => $val )
  $output .= '
  <input type="hidden" name="services['.$service_id.']" value="'.$val.'">';

  $output .= '</form>

  </div>';
  }

  return $output;
}

///////////////

function get_prop_services_list($ap_id){
  $output = '';

  $services_arr = array();
  if (isset($_POST['services'])) $services_arr = array_keys($_POST['services']);
  elseif (get_query_var('reserv_code')) {
    $page = get_page_by_title( get_query_var('reserv_code'), OBJECT, 'booking' );
    if (!empty($page))
    $services_arr = get_post_meta($page->ID, '_booking_services', true);
  }
  if(!empty($services_arr)){

    $output .= '<ul>';
    $args = array(
        'post_type'   => 'service',
        'numberposts' => -1,
    );
    $posts = get_posts( $args );
    if ( $posts ) {
        foreach ( $posts as $post ) {
          if (in_array( $post->ID, $services_arr )){
            $price = $this->get_service_price($ap_id, $post->ID);
            $post_object = get_post( $post->ID );
            $content = apply_filters( 'the_content', $post_object->post_content);
            if ($content) $content = '<div class="list_incl">'.$content.'</div>';
            $output .= '<li><span class="link"><a href="'.esc_url( get_permalink($post->ID) ).'" target="_blank">'.$post->post_title.', '.$this->format_currency($price).'</a>'.$content.'</span></li>';
          }
        }
      }
        //'.apply_filters( 'the_content', get_the_content($post->ID)).'
    $output .= '</ul>';
  }

  return $output;
}

////////////////

function get_ap_policies($ap_id){
  $output = '';

  $policies_arr = get_post_meta( $ap_id, '_property_policies', true );

  if (!empty($policies_arr)){
  foreach ( $policies_arr as $policy_id ) {
          $post_object = get_post( $policy_id );
          $content = apply_filters( 'the_content', $post_object->post_content);
          if ($content) $content = '<div class="list_incl">'.$content.'</div>';

          $output .= '<li><span class="link"><a href="'.esc_url( get_permalink($policy_id) ).'" target="_blank">'.get_the_title($policy_id).'</a>'.$content.'</span></li>';
        }
  }
       //'.apply_filters( 'the_content', get_the_content($policy_id)).'
  if ($output) $output = '<ul>'.$output.'</ul>';

  return $output;
}

function apt_info($post_id, $confirm_page = ''){
  $output = '';

  $cancel_text = '';
  $extra_acc_text = '';
  
  if (!$confirm_page){
     $children = (!empty($_POST['children'])) ? $_POST['children'] : 0;
     $is_extra_bed = $this->is_extra_bed($post_id, $children + $_POST['adults']);
  } else {
     $is_extra_bed = get_post_meta( $confirm_page, '_booking_extra_bed', true );
  }
  
  $extra_acc_text = $is_extra_bed ? __(' (+ 1 extra bed)', VR_BOOKING_TEXTDOMAIN) : '';

  $term_list = wp_get_post_terms($post_id, 'locations', array("fields" => "all"));

  //$page = get_posts( array( 'name' => 'cancellation-policy', 'post_type' => 'page' ) );

  //if (!empty($page))
  //$cancel_text = apply_filters( 'the_content', $page[0]->post_content);

  $policies = $this->get_ap_policies($post_id);

  $services = $this->get_prop_services_list($post_id);

  $categories_output = get_property_cats($post_id);
                if ($categories_output){
                     $categories_output = '<tr><td>'.__('Categories ', VR_BOOKING_TEXTDOMAIN).'</td><td>'.$categories_output.'</td></tr>';
                }

  if (!$confirm_page) $add_slides = $this->get_slides_preview($post_id);
  else $add_slides = '';

  $output .='<div id="apt_short_info">
          '.$add_slides.'
          <table><tbody>
          <tr><td colspan="2">'.get_the_title($post_id).'</td></tr>
          '.$categories_output.'
          <tr><td>'.__('Location', VR_BOOKING_TEXTDOMAIN).'</td><td>'.$term_list[0]->name.'</td></tr>
          <tr><td>'.__('Address', VR_BOOKING_TEXTDOMAIN).'</td><td>'.get_post_meta( $post_id, '_property_address', true ).'</td></tr>
          <tr><td>'.__('Size', VR_BOOKING_TEXTDOMAIN).'</td><td>'.get_post_meta( $post_id, '_property_size', true ).'</td></tr>
          <tr><td>'.__('Accommodates', VR_BOOKING_TEXTDOMAIN).'</td><td>'.get_post_meta( $post_id, '_property_max_guests', true ).$extra_acc_text.'</td></tr>
          <tr><td>'.__('Rooms', VR_BOOKING_TEXTDOMAIN).'</td><td>'.get_post_meta( $post_id, '_property_n_rooms', true ).'</td></tr>
          <tr><td>'.__('Beds', VR_BOOKING_TEXTDOMAIN).'</td><td>'.get_post_meta( $post_id, '_property_max_beds', true ).'</td></tr>
          <tr><td>'.__('Bath type', VR_BOOKING_TEXTDOMAIN).'</td><td>'.get_post_meta( $post_id, '_property_n_bath_type', true ).'</td></tr>
          <tr><td colspan="2">'.__('Getting there', VR_BOOKING_TEXTDOMAIN).'</td></tr>
          <tr><td colspan="2" class="txtarea">'.get_post_meta( $post_id, '_property_getting_there', true ).'</td></tr>

          <tr><td colspan="2">'.__('House Rules', VR_BOOKING_TEXTDOMAIN).'</td></tr>
          <tr><td colspan="2" class="txtarea">'.$policies.'</td></tr>';

          if($services)
          $output .='
          <tr><td colspan="2">'.__('Services', VR_BOOKING_TEXTDOMAIN).'</td></tr>
          <tr><td colspan="2" class="txtarea">'.$services.'</td></tr>';

          $output .='
          </tbody></table>
          </div>';

  return $output;
}

function form_search_request2(){
  $output = '';

  $term = get_term( $_POST['location'], 'locations' );

  if (!empty($_POST['children'])) $children = $_POST['children'];
  else $children = 0;
  
  $is_extra_bed = $this->is_extra_bed($_POST['id'], $children + $_POST['adults']);
  
  $price_arr = $this->get_all_rates_by_ap_id($_POST['id'], $this->date_to_sql($_POST['date_from']), $this->date_to_sql($_POST['date_to']), $is_extra_bed);
          
          $price_total = '';

          if (!empty($price_arr)){
            $price_total = $price_arr['total'];
          }

  $output .= '<div id="search-request">
    <div id="location_r"><label>'.__('Location: ', VR_BOOKING_TEXTDOMAIN).'</label><br><span>'.$term->name.'</span></div>
    <div id="apartment_r"><label>'.__('Apartment: ', VR_BOOKING_TEXTDOMAIN).'</label><br><span>'.get_the_title($_POST['id']).'</span></div>
    <div id="date_from_r"><label>'.__('Arrival Date: ', VR_BOOKING_TEXTDOMAIN).'</label><br><span>'.$_POST['date_from'].'</span></div>
    <div id="date_to_r"><label>'.__('Departure Date: ', VR_BOOKING_TEXTDOMAIN).'</label><br><span>'.$_POST['date_to'].'</span></div>
    <div id="adults_r"><label>'.__('Adults: ', VR_BOOKING_TEXTDOMAIN).'</label><br><span>'.$_POST['adults'].'</span></div>
    <div id="children_r"><label>'.__('Children: ', VR_BOOKING_TEXTDOMAIN).'</label><br><span>'.$children.'</span></div>
    <div id="rooms_r"><label>'.__('Rooms: ', VR_BOOKING_TEXTDOMAIN).'</label><br><span>'.$_POST['rooms'].'</span></div>
    <div id="location_r"><label><strong>'.__('Total Price: ', VR_BOOKING_TEXTDOMAIN).'</strong></label><br><span>'.$this->format_currency($price_total).'</span></div>
  </div>
  ';

  return $output;
}

///////// Search Result Page  //////////////

function search_page_shortcode($atts){
  $output = '';

  if (((empty($_POST['adults']))or(empty($_POST['location']))or(empty($_POST['date_from']))or(empty($_POST['date_to'])))or(strtotime($this->date_to_sql($_POST['date_from']) . ' 00:00:00') >= strtotime($this->date_to_sql($_POST['date_to']) . ' 00:00:00'))){return $output;}

  $output .= $this->form_search_request();

/*  $args = shortcode_atts( array(
        'title' => __('Our Apartments', VR_BOOKING_TEXTDOMAIN)
	), $atts, 'booking-home' );

  $output .= '<h2 id="title1" class="center-title">'.$args['title'].'</h2>';
*/

  if (!empty($_POST['children'])) $children = $_POST['children'];
  else $children = 0;
  
  $rooms = empty($_POST['rooms']) ? 1 : $_POST['rooms'];

  $arr = $this->get_free_appartments_front($_POST['date_from'], $_POST['date_to'], intval($_POST['adults'])+intval($children), intval($rooms), $_POST['location']);

       if(!empty($arr)){
  if(!empty($arr['pages']))
  $output .= $this->get_sort_select();

  $output .= '<div id="result-row-block">'.$arr['output'].'</div>';

        if(!empty($arr['pages'])){
  $page_line = '';
  for($i=1; $i<=$arr['pages']; $i++){
     if($i == $arr['page_num']) $class = ' cur_page';
     else $class = '';
     if(get_query_var('sort_by')) $sort_by = '/sort_by/'.get_query_var('sort_by');
     else $sort_by = '';
     $page_line .= '<span class="pager_num'.$class.'" data-x="'.$i.'">'.$i.'</span>';
  }
  $output .= '<div id="pager">'.$page_line.'</div>';

      if ($this->settings['google_map_active']){
       if ($arr['markers']){
       $output .= '<h2 class="center-title">'.__('Map', VR_BOOKING_TEXTDOMAIN).'</h2>
       <div id="map_all"></div>';
       $output .= $this->get_map_all($arr);
       }
      }
     }
    }

  return $output;
}

function form_search_request(){
  $output = '';

  $term = get_term( $_POST['location'], 'locations' );
  
  $rooms = empty($_POST['rooms']) ? 1 : $_POST['rooms'];

  $output .= '<div id="search-request">
    <div id="location_r"><label>'.__('Location: ', VR_BOOKING_TEXTDOMAIN).'</label><br><span>'.$term->name.'</span></div>
    <div id="date_from_r"><label>'.__('Arrival Date: ', VR_BOOKING_TEXTDOMAIN).'</label><br><span>'.$_POST['date_from'].'</span></div>
    <div id="date_to_r"><label>'.__('Departure Date: ', VR_BOOKING_TEXTDOMAIN).'</label><br><span>'.$_POST['date_to'].'</span></div>
    <div id="adults_r"><label>'.__('Adults: ', VR_BOOKING_TEXTDOMAIN).'</label><br><span>'.$_POST['adults'].'</span></div>
    <div id="children_r"><label>'.__('Children: ', VR_BOOKING_TEXTDOMAIN).'</label><br><span>'.$_POST['children'].'</span></div>
    <div id="rooms_r"><label>'.__('Rooms: ', VR_BOOKING_TEXTDOMAIN).'</label><br><span>'.$rooms.'</span></div>
    <div id="modify_r"><input type="button" id="modify" class="bc-button-color" name="modify" value="'.__('MODIFY SEARCH', VR_BOOKING_TEXTDOMAIN).'"></div>
  </div>
  <div id="search-form">
  '.$this->form_get_quote().'
  </div>
  ';

  return $output;
}

///////////// get quote by apartment id

function form_get_quote_by_ap_id($ap_id, $location){

  $date_from = '';
  $date_to = '';

  $services_arr = get_post_meta( $ap_id, '_property_services', true );
  if (!empty($services_arr)) $form_action = '/services';
  else $form_action = '/booking';

  $output = '<form id="get_quote_ap" name="get_quote_ap" method="post" action="'.$this->home_url.$form_action.'">

  <input id="date_from" type="text" name="date_from" value="" placeholder="'.__('Arrival Date', VR_BOOKING_TEXTDOMAIN).'">
  <input id="date_to" type="text" name="date_to" value="" placeholder="'.__('Departure Date', VR_BOOKING_TEXTDOMAIN).'">

  <label for="adults">'.__('Adults:', VR_BOOKING_TEXTDOMAIN).'</label>
  <select id="adults" name="adults" size="1">';
  for ($i=1; $i<=get_post_meta( $ap_id, '_property_max_guests', true ); $i++){
  $output .= '<option value="'.$i.'"';
  if ($i == 1) $output .= ' selected';
  $output .= '>'.$i.'</option>';
  }
  $output .= '</select>

  <label for="children">'.__('Children:', VR_BOOKING_TEXTDOMAIN).'</label>
  <select id="children" name="children" size="1">';
  for ($i=0; $i<=get_post_meta( $ap_id, '_property_max_guests', true ); $i++){
  $output .= '<option value="'.$i.'"';
  if ($i == 0) $output .= ' selected';
  $output .= '>'.$i.'</option>';
  }
  $output .= '</select>

  <input id="continue" type="button" class="bc-button-color" name="continue" value="'.__('CONTINUE', VR_BOOKING_TEXTDOMAIN).'">

          <input type="hidden" name="location" value="'.$location.'">
          <input type="hidden" name="rooms" value="'.get_post_meta( $ap_id, '_property_n_rooms', true ).'">
          <input type="hidden" name="id" value="'.$ap_id.'">

  </form>';

  return $output;
}

//////////////

function form_get_quote(){

  $terms = get_terms( array(
    'taxonomy' => 'locations',
    'hide_empty' => false,
    ) );

  $args = array(
	'show_option_all'    => '',
	'show_option_none'   => '',
	'option_none_value'  => '-1',
	'orderby'            => 'name',
	'order'              => 'ASC',
	'show_count'         => 1,
	'hide_empty'         => 1,
	'child_of'           => 0,
	'exclude'            => '',
	'include'            => '',
	'echo'               => 0,
	'selected'           => 0,
	'hierarchical'       => 1,
	'name'               => 'location',
	'id'                 => 'location',
	'class'              => '',
	'depth'              => 3,
	'tab_index'          => 0,
	'taxonomy'           => 'locations',
	'hide_if_empty'      => false,
	'value_field'	     => 'term_id',
  );

  if (!empty($_POST['date_from'])) $date_from = $_POST['date_from']; else $date_from = '';
  if (!empty($_POST['date_to'])) $date_to = $_POST['date_to']; else $date_to = '';

  $output = '<form id="get_quote" name="get_quote" method="post" action="'.$this->home_url.'/search-result">

  <div>
  <label for="location">'.__('Location: ', VR_BOOKING_TEXTDOMAIN).'</label>';

  $output .= wp_dropdown_categories($args);

  /*
  '<select id="location" name="location" placeholder="'.__('Select...', VR_BOOKING_TEXTDOMAIN).'" size="1">';

  if (empty($_POST['location'])) $output .= '<option name="start" disabled selected>'.__('Select...', VR_BOOKING_TEXTDOMAIN).'</option>';
  else
  $output .= '<option name="start" disabled>'.__('Select...', VR_BOOKING_TEXTDOMAIN).'</option>';
  foreach ($terms as $term){
  $output .= '<option value="'.$term->term_id.'"';
  if ((!empty($_POST['location']))and($term->term_id == $_POST['location'])) $output .= ' selected';
  $output .= '>'.$term->name.'</option>';
  }
  $output .= '</select>  */

  $output .= '</div>

  <div>
  <input id="date_from" type="text" name="date_from" value="'.$date_from.'" placeholder="'.__('Arrival Date', VR_BOOKING_TEXTDOMAIN).'">
  </div>
  <div>
  <input id="date_to" type="text" name="date_to" value="'.$date_to.'" placeholder="'.__('Departure Date', VR_BOOKING_TEXTDOMAIN).'">
  </div>

  <div>
  <select id="adults" name="adults" size="1">';
  if (empty($_POST['adults'])) $output .= '<option name="start" disabled selected>'.__('Adults', VR_BOOKING_TEXTDOMAIN).'</option>';
  else
  $output .= '<option name="start" disabled>'.__('Adults', VR_BOOKING_TEXTDOMAIN).'</option>';
  for ($i=1; $i<=$this->settings['max_select_adults']; $i++){
  $output .= '<option value="'.$i.'"';
  if ((!empty($_POST['adults']))and($i == $_POST['adults'])) $output .= ' selected';
  $output .= '>'.$i.'</option>';
  }
  $output .= '</select>
  </div>

  <div>
  <select id="children" name="children" size="1">';
  if (!isset($_POST['children'])) $output .= '<option name="start" disabled selected>'.__('Children', VR_BOOKING_TEXTDOMAIN).'</option>';
  else
  $output .= '<option name="start" disabled>'.__('Children', VR_BOOKING_TEXTDOMAIN).'</option>';
  for ($i=0; $i<=$this->settings['max_select_children']; $i++){
  $output .= '<option value="'.$i.'"';
  if ((isset($_POST['children']))and($i == $_POST['children'])) $output .= ' selected';
  $output .= '>'.$i.'</option>';
  }
  $output .= '</select>
  </div>

  <div>
  <select id="rooms" name="rooms" size="1">';
  if (empty($_POST['rooms'])) $output .= '<option name="start" disabled selected>'.__('Rooms', VR_BOOKING_TEXTDOMAIN).'</option>';
  else
  $output .= '<option name="start" disabled>'.__('Rooms', VR_BOOKING_TEXTDOMAIN).'</option>';
  for ($i=1; $i<=$this->settings['max_select_rooms']; $i++){
  $output .= '<option value="'.$i.'"';
  if ((!empty($_POST['rooms']))and($i == $_POST['rooms'])) $output .= ' selected';
  $output .= '>'.$i.'</option>';
  }
  $output .= '</select>
  </div>

  <div>
  <input id="guote" type="submit" class="bc-button-color" name="guote" value="'.__('SEARCH', VR_BOOKING_TEXTDOMAIN).'">
  </div>
  </form>';

  return $output;
}

function get_sort_select($class = ''){

  $output = '<div id="sort_by_block">
  <label for="sort_by">'.__('Sort by: ', VR_BOOKING_TEXTDOMAIN).'</label>
  <select id="sort_by" class="'.$class.'" name="sort_by" size="1">';
   if (!get_query_var('sort_by')) $output .= '<option name="start" disabled selected>'.__('Select...', VR_BOOKING_TEXTDOMAIN).'</option>';
  else
  $output .= '<option name="start" disabled>'.__('Select...', VR_BOOKING_TEXTDOMAIN).'</option>';

  $output .= '<option value="1"';
  if ((get_query_var('sort_by'))and('1' == get_query_var('sort_by'))) $output .= ' selected';
  $output .= '>'.__('Price Low to High', VR_BOOKING_TEXTDOMAIN).'</option>';

  $output .= '<option value="2"';
  if ((get_query_var('sort_by'))and('2' == get_query_var('sort_by'))) $output .= ' selected';
  $output .= '>'.__('Price High to Low', VR_BOOKING_TEXTDOMAIN).'</option>';

  if ($this->settings['reviews_active']){
  $output .= '<option value="3"';
  if ((get_query_var('sort_by'))and('3' == get_query_var('sort_by'))) $output .= ' selected';
  $output .= '>'.__('Rating High to Low', VR_BOOKING_TEXTDOMAIN).'</option>';
  }

  $output .= '</select></div>';

  return $output;
}

function sort_by_callback(){

   $arr = $this->get_all_appartments_front();

   echo $arr['post_blocks'];
   wp_die();
}

///////////// Home page ////////////////

function home_page_shortcode($atts){
  $output = '';

  $args = shortcode_atts( array(
        'title' => __('Our Apartments', VR_BOOKING_TEXTDOMAIN),
        'map_title' => __('Map', VR_BOOKING_TEXTDOMAIN),
        'search' => "true"
	), $atts, 'booking-home' );
  
  $args['search'] = ($args['search'] == "false") ? false : true;

  $arr = $this->get_all_appartments_front();

  if(!empty($arr)){
    
  $output .= '<h2 class="center-title">'.$args['title'].'</h2>
  ';
  
  if ($args['search']){
    $output .= $this->form_get_quote().$this->get_sort_select('start');
  }

  $output .= '<div id="home_results">'.$arr['post_blocks'].'</div>';
  $page_line = '';
  for($i=1; $i<=$arr['pages']; $i++){
     if($i == $arr['page_num']) $class = ' cur_page';
     else $class = '';
     if(get_query_var('sort_by')) $sort_by = '&sort_by='.get_query_var('sort_by');
     else $sort_by = '';
     $page_line .= '<a href="?page_num='.$i.$sort_by.'"><span class="pager_num'.$class.'" data-x="'.$i.'">'.$i.'</span></a>';
  }
  $output .= '<div id="pager">'.$page_line.'</div>';

  if ($this->settings['google_map_active']){
  $output .= '<h2 class="center-title">'.$args['map_title'].'</h2>
       <div id="map_all"></div>';
  $output .= $this->get_map_all($arr);
   }
  }

  return $output;
}

//////// get_map_all

public function get_map_all($markers){
  $output = '';
           //plugins_url( "css/img/es-map-marker.png", VR_BOOKING_PLUGIN )
  $output .= '    <script>

function initMap() {
  var map = new google.maps.Map(document.getElementById(\'map_all\'), {
    zoom: '.$this->settings['map_zoom'].',
    center: {'.$markers['marker_center'].'},
    scrollwheel: false,
  });
  
  google.maps.event.addDomListener(window, "resize", function() {
    var center = map.getCenter();
    google.maps.event.trigger(map, "resize");
    map.setCenter(center); 
  });

  setMarkers(map);
}

var beaches = [
  '.$markers['markers'].'
];

function setMarkers(map) {
  var image = {
    url: \''.plugins_url( $this->markers_urls[$this->settings['map_marker']], VR_BOOKING_PLUGIN ).'\',
    size: new google.maps.Size(30, 56),
    origin: new google.maps.Point(0, 0),
    anchor: new google.maps.Point(22, 55)
  };
  // Shapes define the clickable region of the icon. The type defines an HTML
  // <area> element \'poly\' which traces out a polygon as a series of X,Y points.
  // The final coordinate closes the poly by connecting to the first coordinate.
  var shape = {
    coords: [1, 1, 1, 45, 35, 45, 35, 1],
    type: \'poly\'
  };
  for (var i = 0; i < beaches.length; i++) {
    var beach = beaches[i];
    var marker = new google.maps.Marker({
      position: {lat: beach[1], lng: beach[2]},
      map: map,
      icon: image,
      shape: shape,
      title: beach[0],
      zIndex: beach[3],
      mycontent: beach[4]
    });

    var infowindow = new google.maps.InfoWindow();
    var content = beach[4];

    google.maps.event.addListener(marker,\'click\', function(){
           infowindow.setContent(this.mycontent);
           infowindow.open(map,this);
    });
   }
}

    </script>

<script async defer
        src="https://maps.googleapis.com/maps/api/js?key='.$this->settings['google_api'].'&signed_in=true&callback=initMap"></script>
';

  return $output;
}

/////////////

function date_to_sql($date){
  if ($date){
  $date_arr = explode('/', $date);
  if (sizeof($date_arr) == 3)
  return $date_arr[2].'-'.$date_arr[1].'-'.$date_arr[0];
  }
  else return '';
}

function date_from_sql($date){

  if ($date!=="0000-00-00"){
  $date_arr = explode('-', $date);
  if (sizeof($date_arr) == 3)
  return $date_arr[2].'/'.$date_arr[1].'/'.$date_arr[0];
  }
  else return '';
}

function get_post_marker($post_id, $i, $price){
   $output = '';

   if(('' != get_post_meta( $post_id, '_property_location_latitude', true ))&&('' != get_post_meta( $post_id, '_property_location_longitude', true ))){

   if ($this->settings['reviews_active']) $add_str = '<div class="map_info_line"><span>'.__('Rating', VR_BOOKING_TEXTDOMAIN).'</span><span>'.do_shortcode('[RICH_REVIEWS_SNIPPET id="'.$post_id.'" stars_only="true"]').'</span></div>';
   else $add_str = '';

   $str = '<div class="map_info"><a href="'.get_permalink($post_id).'"><h4>'.get_the_title($post_id).'</h4></a><div class="from_price">'.__('from', VR_BOOKING_TEXTDOMAIN).' <span class="from_price_amount">'.$this->format_currency($price).'</span>'.__(' per night', VR_BOOKING_TEXTDOMAIN).'</div><table><tbody><tr><td>'.get_the_post_thumbnail($post_id, 'post1-thumbnail' ).'</td><td><div class="map_info_line"><span>'.__('Rooms', VR_BOOKING_TEXTDOMAIN).'</span><span>'.get_post_meta( $post_id, '_property_n_rooms', true ).'</span></div><div class="map_info_line"><span>'.__('Beds', VR_BOOKING_TEXTDOMAIN).'</span><span>'.get_post_meta( $post_id, '_property_max_beds', true ).'</span></div><div class="map_info_line"><span>'.__('Guests', VR_BOOKING_TEXTDOMAIN).'</span><span>'.get_post_meta( $post_id, '_property_max_guests', true ).'</span></div>'.$add_str.'<a href="'.get_permalink($post_id).'"><input type="button" class="bc-button-color" value="'.__('View details', VR_BOOKING_TEXTDOMAIN).'"></a></td></tr></tbody></table></div>';

          $str = str_replace(array("\n\r", "\n", "\r"), '', $str);

          $output = '[\''.get_the_title($post_id).'\', '.get_post_meta( $post_id, '_property_location_latitude', true ).', '.get_post_meta( $post_id, '_property_location_longitude', true ).', '.$i.', \''.$str.'\'],';
    }
    return $output;
}

/// get average rating
	function get_average_rating($post_id) {
        global $wpdb;

        $whereStatement = "WHERE (review_status=\"1\" and post_id=\"$post_id\")";

		$approvedReviewsCount = $wpdb->get_var("SELECT COUNT(*) FROM wp_richreviews " . $whereStatement);
		$averageRating = 0;
		if ($approvedReviewsCount != 0) {
			$averageRating = $wpdb->get_var("SELECT AVG(review_rating) FROM wp_richreviews " . $whereStatement);
			$averageRating = floor(10*floatval($averageRating))/10;
		}
		return $averageRating;
	}

function get_all_appartments_front(){
   $output = '';
   $marker_center = '';
   $markers = '';
   $res_arr = array();

    $args = array(
               'post_type' => 'property',
               'posts_per_page'=> -1,
                  );

    $the_query = new WP_Query( $args );
      $i = 1;
      $set_center = false;
     while ( $the_query->have_posts() ) : $the_query->the_post();

          $post_id = get_the_ID();

          $price = $this->get_price_min($post_id);

          if ($price) {

          if ((!$set_center)&&('' != get_post_meta( $post_id, '_property_location_latitude', true ))&&('' != get_post_meta( $post_id, '_property_location_longitude', true ))){
             $marker_center = 'lat: '.get_post_meta( $post_id, '_property_location_latitude', true ).', lng: '.get_post_meta( $post_id, '_property_location_longitude', true );
               $set_center = true;
             }

          $markers .= $this->get_post_marker($post_id, $i, $price);

          if ($this->settings['reviews_active'])
          $res_arr[$i]['rating'] = $this->get_average_rating($post_id);

          $res_arr[$i]['price'] = $price;
          $res_arr[$i]['res'] ='<div class="result-block">
          <a href="'.get_permalink($post_id).'"><h4>'.get_the_title($post_id).'</h4></a>
          '.$this->get_slides_preview($post_id).'
          <div class="from_price">'.__('from', VR_BOOKING_TEXTDOMAIN).' <span class="from_price_amount">'.$this->format_currency($price).'</span>'.__(' per night', VR_BOOKING_TEXTDOMAIN).'</div>
          <a href="'.get_permalink($post_id).'"><input type="button" class="bc-button-color" value="'.__('View Listing', VR_BOOKING_TEXTDOMAIN).'" name="view" id="view-'.$post_id.'"></a>
          </div>';
            $i++;
            }
     endwhile;
/* Restore original Post Data */
wp_reset_postdata();

   if (!empty($res_arr)){
      $tmp = Array();
      if ((get_query_var('sort_by'))and(get_query_var('sort_by') == 3)and($this->settings['reviews_active'])){
      foreach($res_arr as &$ma) $tmp[] = &$ma["rating"];
      }
      else{
      foreach($res_arr as &$ma) $tmp[] = &$ma["price"];
      }

      if ((get_query_var('sort_by'))and(get_query_var('sort_by') == 1)) $sort_order = SORT_ASC;
        else $sort_order = SORT_DESC;

      array_multisort($tmp, $sort_order, $res_arr);
      //array_splice ($res_arr, 10);  // sort and save top 10 matches
      if (get_query_var('page_num')){
        $page_num = get_query_var('page_num');
        $offset = (get_query_var('page_num')-1)*$this->settings['search_res_front'];
        }
        else {
        $page_num = 1;
        $offset=0;
        }
        array_splice ($res_arr, 0, $offset);
        array_splice ($res_arr, $this->settings['search_res_front']);
        foreach($res_arr as $res_arr_item) $output .= $res_arr_item['res'];
   }

   if ($output) {
      $arr['markers'] = $markers;
      $arr['marker_center'] = $marker_center;
      $arr['post_blocks'] = $output;
      $arr['total_rec'] = $i-1;
      $arr['pages'] = intval(ceil(($i-1)/$this->settings['search_res_front']));
      $arr['page_num'] = $page_num;
      return $arr;
  }

   return array();
}

function get_slides_preview($post_id){
  $output = '';

  // Get the list of files
    $files = get_post_meta( $post_id, '_property_file_list', 1 );
    $attachment_id_arr = array_keys((array)$files);

    if(!empty($files)){
    $output .= '<div class="cycle-slideshow"
    data-cycle-manual-fx=scrollHorz
    data-cycle-manual-speed="700"
    data-cycle-swipe=true
    data-cycle-swipe-fx=scrollHorz
    data-cycle-timeout=0
    data-cycle-slides="img"
    data-cycle-prev=".r-left-'.$post_id.'"
    data-cycle-next=".r-right-'.$post_id.'"
    data-cycle-loader=true
    data-cycle-progressive="#images-'.$post_id.'"
    data-cycle-overlay-template="<span class=slide_caption>{{slideNum}} / '.(sizeof($files)).'</span>"
    >

    <!-- only one image declared in markup -->
    ';

    $src_arr = wp_get_attachment_image_src( $attachment_id_arr[0], 'post1-thumbnail' );
    $output .= '<img src="'.$src_arr[0].'">';
    unset($attachment_id_arr[0]);

    $output .= '
    <script id="images-'.$post_id.'" type="text/cycle">';

    // Loop through them and output an image
    foreach ( $attachment_id_arr as $key => $attachment_id ) {
        $src_arr = wp_get_attachment_image_src( $attachment_id, 'post1-thumbnail' );
        $output .= '<img src="'.$src_arr[0].'">'."\n";
    }

    $output .= '</script>
    <!-- empty element for overlay -->
    <div class="cycle-overlay custom"></div>
     <!-- prev/next links -->
    <i class="fa fa-caret-left r-left-'.$post_id.'" aria-hidden="true"></i>
    <i class="fa fa-caret-right r-right-'.$post_id.'" aria-hidden="true"></i>
    </div>';
    }
    else $output = '';

  return $output;
}

function get_prop_icons($post_id){
   $output = '';

   $term_list = wp_get_post_terms($post_id, 'amenities', array("fields" => "all"));
   //print_r($term_list);
   foreach($term_list as $term_single) {
       switch ($term_single->slug){
        case 'non-smoking':
          $output .= '<li class="icon-prop-non"></li>';
          break;
        case 'air-conditioning':
          $output .= '<li class="icon-prop-air"></li>';
          break;
        case 'television':
          $output .= '<li class="icon-prop-tv"></li>';
          break;
        case 'internet-access':
          $output .= '<li class="icon-prop-wifi"></li>';
          break;
        case 'heating':
          $output .= '<li class="icon-prop-heating"></li>';
          break;
       }
   }

   if ($output) $output = '<ul>'.$output.'</ul>';

   return $output;
}

///// check free apartment by id

function is_free_appartment($from_date, $to_date, $ap_id){
   $output = true;

   $to = strtotime($this->date_to_sql($to_date));
   $from = strtotime($this->date_to_sql($from_date));

   $args = array(
               'post_type' => 'booking',
               'posts_per_page'=> -1,
               'meta_query' => array(
                    'key'     => '_booking_prop_id',
                    'value'   => $ap_id,
                    'compare' => '=',
                    ),
                   );

    $the_query = new WP_Query( $args );
     while ( $the_query->have_posts() ) : $the_query->the_post();
          $booking_id = get_the_ID();
          $booking_date_from = get_post_meta( $booking_id, '_booking_date_from', true );
          $booking_date_to = get_post_meta( $booking_id, '_booking_date_to', true );
          if ((($booking_date_from < $to)&&(($booking_date_to >= $to)))||(($booking_date_from <= $from)&&(($booking_date_to > $from)))){
          $output = false;
          break;
          }
     endwhile;
wp_reset_postdata();

   return $output;
}

//////// is extra bed ?

function is_extra_bed($post_id, $guests){
    
    $output = ''; 
    $extra_bed_id = get_post_meta($post_id, '_property_extra_bed', true);
    
    if ($extra_bed_id){
      $max_guests = get_post_meta($post_id, '_property_max_guests', true);
      $free_beds = $max_guests + 1 - $guests;
      $output = (!$free_beds) ? $extra_bed_id : 0;
    }
 return $output;   
}

///////////////////

function get_free_appartments_front($from_date, $to_date, $guests, $rooms, $location_id){
   $output = '';
   $marker_center = '';
   $markers = '';

   $args = array(
               'post_type' => 'booking',
               'posts_per_page'=> -1,
               'meta_query' => array(
                  'relation' => 'OR',
                  array(
                  'relation' => 'AND',
                  'date_from' => array(
                    'key'     => '_booking_date_from',
                    'value'   => strtotime($this->date_to_sql($to_date)),
                    'compare' => '<',
                    'type'    => 'NUMERIC',
                    ),
                  'date_to' => array(
                    'key'     => '_booking_date_to',
                    'value'   => strtotime($this->date_to_sql($to_date)),
                    'compare' => '>=',
                    'type'    => 'NUMERIC',
                    ),
                   ),
                  array(
                  'relation' => 'AND',
                  'date_from2' => array(
                    'key'     => '_booking_date_from',
                    'value'   => strtotime($this->date_to_sql($from_date)),
                    'compare' => '<=',
                    'type'    => 'NUMERIC',
                    ),
                  'date_to2' => array(
                    'key'     => '_booking_date_to',
                    'value'   => strtotime($this->date_to_sql($from_date)),
                    'compare' => '>',
                    'type'    => 'NUMERIC',
                    ),
                    ),
                   ),
                  );
    $id_exclude = array();
    $the_query = new WP_Query( $args );

     while ( $the_query->have_posts() ) : $the_query->the_post();

          $id_exclude[] = get_post_meta( get_the_ID(), '_booking_prop_id', true );

     endwhile;
/* Restore original Post Data */
wp_reset_postdata();

      // print_r($id_exclude);
    $args = array(
               'post_type' => 'property',
               'post__not_in' => $id_exclude,
               'posts_per_page'=> -1,
               'tax_query' => array(
                  array(
                   'taxonomy' => 'locations',
                   'field'    => 'term_id',
                   'terms'    => $location_id,
                   ),
               ),
               'meta_query' => array(
                  'relation' => 'AND',
                  'n_rooms' => array(
                    'key'     => '_property_n_rooms',
                    'value'    => $rooms,
                    'compare' => '>=',
                    ),
                  'max_guests' => array(
                    'key'     => '_property_max_guests',
                    'value'   => ($guests - 1),
                    'compare' => '>=',
                    ),
                  ),
                  );

     $res_arr = array();

    $the_query = new WP_Query( $args );
       $i=1;
     $set_center = false;
    // $page2 = get_posts( array( 'name' => 'price-includes', 'post_type' => 'page' ) );
    // $price_includes = apply_filters( 'the_content', $page2[0]->post_content);

     while ( $the_query->have_posts() ) : $the_query->the_post();

          $post_id = get_the_ID();
          
          $extra_bed_id = get_post_meta($post_id, '_property_extra_bed', true);
          $max_guests = get_post_meta( $post_id, '_property_max_guests', true );
          $free_beds = $max_guests + 1 - $guests;
          
          $is_extra_bed = $this->is_extra_bed($post_id, $guests);
          
          if ( ($is_extra_bed)||($max_guests >= $guests) ){
            
          $extra_acc_text = ($is_extra_bed) ? __(' (+ 1 extra bed)', VR_BOOKING_TEXTDOMAIN) : '';
        //  $extra_bed_flag = ($is_extra_bed) ? 1 : 0;  

          $services_arr = get_post_meta( $post_id, '_property_services', true );
          if (!empty($services_arr)) $form_action = '/services';
          else $form_action = '/booking';

          $price_arr = $this->get_all_rates_by_ap_id($post_id, $this->date_to_sql($from_date), $this->date_to_sql($to_date), $is_extra_bed);
          $price_total = '';
          $price = '';

          if (!empty($price_arr)){
            $price_total = $price_arr['total'];
            $price = $price_arr['min'];
          }

          if ($price) {

          if ((!$set_center)&&('' != get_post_meta( $post_id, '_property_location_latitude', true ))&&('' != get_post_meta( $post_id, '_property_location_longitude', true ))){
             $marker_center = 'lat: '.get_post_meta( $post_id, '_property_location_latitude', true ).', lng: '.get_post_meta( $post_id, '_property_location_longitude', true );
               $set_center = true;
             }

          $markers .= $this->get_post_marker($post_id, $i, $price);

          if ($this->settings['reviews_active'])
          $res_arr[$i]['rating'] = $this->get_average_rating($post_id);

          $res_arr[$i]['price'] = $price;
          $res_arr[$i]['res'] = '<div class="result-row">
          <div class="app-details">
          <a href="'.get_permalink($post_id).'"><h4>'.get_the_title($post_id).'</h4></a>
          <table><tbody>
          <tr><td>'.__('Accommodates', VR_BOOKING_TEXTDOMAIN).'</td><td>'.$max_guests.$extra_acc_text.'</td></tr>
          <tr><td>'.__('Rooms', VR_BOOKING_TEXTDOMAIN).'</td><td>'.get_post_meta( $post_id, '_property_n_rooms', true ).'</td></tr>
          <tr><td>'.__('Size', VR_BOOKING_TEXTDOMAIN).'</td><td>'.get_post_meta( $post_id, '_property_size', true ).'</td></tr>
          </tbody></table>
          </div>
          <div class="app-img">'.$this->get_slides_preview($post_id).'</div>
          <div class="app-price">
          <span class="total">'.__('Total: ', VR_BOOKING_TEXTDOMAIN).$this->format_currency($price_total).'
          </span>';

    /*      <span class="day">'.__('from ', VR_BOOKING_TEXTDOMAIN).$this->format_currency($price).' '.__('per night', VR_BOOKING_TEXTDOMAIN).'
          </span>
          <span class="link"><a href="#">'.__('* Price includes', VR_BOOKING_TEXTDOMAIN).'</a>
          <div class="list_incl">'.$price_includes.'</div>
          </span>
    */
          $res_arr[$i]['res'] .= '</div>
          <div class="app-reserv">
          <form id="booking_'.$post_id.'" name="booking_'.$post_id.'" action="'.$this->home_url.$form_action.'" method="post">
          <input type="hidden" name="date_from" value="'.$_POST['date_from'].'">
          <input type="hidden" name="date_to" value="'.$_POST['date_to'].'">
          <input type="hidden" name="location" value="'.$_POST['location'].'">
          <input type="hidden" name="adults" value="'.$_POST['adults'].'">
          <input type="hidden" name="children" value="'.$_POST['children'].'">
          <input type="hidden" name="rooms" value="'.$rooms.'">
          <input type="hidden" name="id" value="'.$post_id.'">
          <input type="submit" class="bc-button-color" value="'.__('BOOK NOW', VR_BOOKING_TEXTDOMAIN).'" name="reserve" id="reserve">
          </form>
          </div>
          </div>';
            $i++;
            }
           // end if extra_bed  
          }  
     endwhile;
/* Restore original Post Data */
wp_reset_postdata();

   if (!empty($res_arr)){
      $tmp = Array();
      if ((get_query_var('sort_by'))and(get_query_var('sort_by') == 3)and($this->settings['reviews_active'])){
      foreach($res_arr as &$ma) $tmp[] = &$ma["rating"];
      }
      else{
      foreach($res_arr as &$ma) $tmp[] = &$ma["price"];
      }

      if ((get_query_var('sort_by'))and(get_query_var('sort_by') == 1)) $sort_order = SORT_ASC;
        else $sort_order = SORT_DESC;

      array_multisort($tmp, $sort_order, $res_arr);
      //array_splice ($res_arr, 10);  // sort and save top 10 matches
      if (get_query_var('page_num')){
        $page_num = get_query_var('page_num');
        $offset = (get_query_var('page_num')-1)*$this->settings['search_res_other'];
        }
        else {
        $page_num = 1;
        $offset=0;
        }
        array_splice ($res_arr, 0, $offset);
        array_splice ($res_arr, $this->settings['search_res_other']);
        foreach($res_arr as $res_arr_item) $output .= $res_arr_item['res'];
   }
    if ($output){
      $arr['markers'] = $markers;
      $arr['marker_center'] = $marker_center;
      $arr['output'] = $output;
      $arr['total_rec'] = $i-1;
      $arr['pages'] = intval(ceil(($i-1)/$this->settings['search_res_other']));
      $arr['page_num'] = $page_num;
   }
  else {
      $arr['output'] = '<div id="sorry_search">'.__('Sorry, we do not have available appartments for these dates. Please try another dates.', VR_BOOKING_TEXTDOMAIN).'</div>';
      $arr['markers']='';
   }
   return $arr;
}

public function get_price_min($post_id){

   $res = $this->get_all_rates_by_ap_id($post_id, date("Y-m-d"), date("Y-m-d", time() + 365*24*60*60));

   return $res['min'];
}

}

Global $VRBooking_var;

$VRBooking_var = new VRBooking();


