
jQuery(function($){
            $('#assign-rates-button').click(function(event){
    		event.preventDefault();
		    $('#overlay').fadeIn(400, function(){
				$('#assign_rates')
					.css('display', 'block')
					.animate({opacity: 1}, 200);
		    });
      });
           $('#modal_close, #overlay').click( function(){
	     	$('#assign_rates').animate({opacity: 0}, 200,	function(){
					$(this).css('display', 'none');
					$('#overlay').fadeOut(400);
				}
			 );
            });
});

jQuery(function($){
    ////////////////////////////////////////////////
            $('.set-price-button').click(function(event){
            $('#assign_price h3').html($(this).attr('data-s'));
		    $('#overlay').fadeIn(400, function(){
				$('#assign_price')
					.css('display', 'block')
					.animate({opacity: 1}, 200);
		    });
            var service_id = $(this).attr('data-a');

            $('#service-price-prop-button').click(function(event){
            //  $('#ap_res').html('<span class="spin_f"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i></span>');

              var price = $('#service_price_prop').val();
              $('#_property_service_'+service_id).val(price);
              $('#service_price_'+service_id).html(price);
              $('#assign_price').animate({opacity: 0}, 200,	function(){
					$(this).css('display', 'none');
					$('#overlay').fadeOut(400);
				}
			 );
            });

      });
      
      ////////////////////////////////
      
      $('.set-price-button2').click(function(event){
            $('#assign_price h3').html($(this).attr('data-s'));
		    $('#overlay').fadeIn(400, function(){
				$('#assign_price')
					.css('display', 'block')
					.animate({opacity: 1}, 200);
		    });
            var extra_bed_id = $(this).attr('data-a');

            $('#service-price-prop-button').click(function(event){
            //  $('#ap_res').html('<span class="spin_f"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i></span>');

              var price = $('#service_price_prop').val();
              $('#_property_extra_bed_'+extra_bed_id).val(price);
              $('#extra_bed_price_'+extra_bed_id).html(price);
              $('#assign_price').animate({opacity: 0}, 200,	function(){
					$(this).css('display', 'none');
					$('#overlay').fadeOut(400);
				}
			 );
            });

      });
      
      /////////////////////////////////////////////    
      
           $('#modal_close, #overlay').click( function(){
	     	$('#assign_price').animate({opacity: 0}, 200,	function(){
					$(this).css('display', 'none');
					$('#overlay').fadeOut(400);
				}
			 );
            });
});

jQuery(function($){
           $('#choose_rate').on('change','select', function(){
             event.preventDefault();
             var i = $(this).val();
             $('#choose_rate tr').removeClass('active');
             $('#choose_rate tr[data-i="'+i+'"]').addClass('active');
           });
});

function show_calendar_by_ap_id() {
    jQuery("#range_rates_from").on('focus', function(){
      jQuery(this).blur();
    });
    jQuery("#range_rates_to").on('focus', function(){
      jQuery(this).blur();
    });
    jQuery( "#ap_res_calendar" ).datepicker({
    //  numberOfMonths: 3,
	  numberOfMonths: [2,2],
      beforeShow: function() {
                addCustomInformation(false);
			},
    //---^----------- if closed by default (when you're using <input>)
      onChangeMonthYear: function() {
                addCustomInformation(false);
			},
	  beforeShowDay: function(date) {
				var date1 = jQuery.datepicker.parseDate("dd/mm/yy", jQuery("#range_rates_from").val());
				var date2 = jQuery.datepicker.parseDate("dd/mm/yy", jQuery("#range_rates_to").val());
				return [true, date1 && ((date.getTime() == date1.getTime()) || (date2 && date >= date1 && date <= date2)) ? "dp-highlight" : ""];
			},
	  onSelect: function(dateText, inst) {
                    //   dateText = $('#ap_res_calendar').datepicker({ dateFormat: 'dd/mm/yy' }).val();
				var date1 = jQuery.datepicker.parseDate("dd/mm/yy", jQuery("#range_rates_from").val());
				var date2 = jQuery.datepicker.parseDate("dd/mm/yy", jQuery("#range_rates_to").val());
                var selectedDate = jQuery.datepicker.parseDate(jQuery.datepicker._defaults.dateFormat, dateText);


                if (!date1 || date2) {
					jQuery("#range_rates_from").val(mm_dd_to_dd_mm(dateText));
					jQuery("#range_rates_to").val("");
                    jQuery(this).datepicker();
                } else if( selectedDate < date1 ) {
                    jQuery("#range_rates_to").val( jQuery("#range_rates_from").val() );
                    jQuery("#range_rates_from").val( mm_dd_to_dd_mm(dateText) );
                    jQuery(this).datepicker();
                } else {
					jQuery("#range_rates_to").val(mm_dd_to_dd_mm(dateText));
                    jQuery(this).datepicker();
				}
                addCustomInformation();
			}
    });
         addCustomInformation();// if open by default (when you're using <div>)
}

function mm_dd_to_dd_mm(date_in){
  var d = date_in.split("/");
  var date_out = d[1]+'/'+d[0]+'/'+d[2];
  return date_out;
}

function addCustomInformation(av) {
  setTimeout(function() {

    jQuery(".ui-datepicker-calendar td").filter(function() {
      var date = jQuery(this).text();
      return /\d/.test(date);
    }).find("a").each(function(){

       var m = parseInt(jQuery(this).parent().attr('data-month'))+1;
       var y = parseInt(jQuery(this).parent().attr('data-year'));
       var d = parseInt(jQuery(this).text());
       m = (m<10)?"0"+m:m;
       d = (d<10)?"0"+d:d;
       var cur_date = y+'-'+m+'-'+d;
      // var new_date = new Date(y, m, d);
       var date_ind = Date.parse(cur_date); // new_date.getTime();
       date_ind = date_ind/1000;
       var rate = 0;
       var av_check = 0;

       if (typeof rate_calendar[date_ind]!=='undefined'){
         rate = rate_calendar[date_ind];
    //     $(this).attr('data-custom', rate);
         jQuery(this).attr('style', 'background-color:'+color_calendar[date_ind]+';');
       } else {
         rate = rate_general_price;
       }

       if (av) {
           av_check = av_calendar[date_ind];
           if ( av_check == 3){
           //jQuery(this).attr('style', 'background-color: #e44; color: #fee;');
               jQuery(this).parent().addClass('ui-datepicker-unselectable ui-state-disabled not_available');
            }
           if ( av_check == 2){
               jQuery(this).parent().addClass('not_available1');
            }
           if ( av_check == 1){
               jQuery(this).parent().addClass('not_available2');
            }
           if ( av_check == 4){
               jQuery(this).parent().addClass('ui-datepicker-unselectable ui-state-disabled not_available4');
            }
           }

       jQuery(this).attr('data-custom', rate);

       //color_calendar = response.color_arr;

    }); // Add custom data here
  }, 0)
}

/**
 * Date.parse with progressive enhancement for ISO 8601 <https://github.com/csnover/js-iso8601>
 * � 2011 Colin Snover <http://zetafleet.com>
 * Released under MIT license.
 */
(function (Date, undefined) {
    var origParse = Date.parse, numericKeys = [ 1, 4, 5, 6, 7, 10, 11 ];
    Date.parse = function (date) {
        var timestamp, struct, minutesOffset = 0;

        // ES5 �15.9.4.2 states that the string should attempt to be parsed as a Date Time String Format string
        // before falling back to any implementation-specific date parsing, so that�s what we do, even if native
        // implementations could be faster
        //              1 YYYY                2 MM       3 DD           4 HH    5 mm       6 ss        7 msec        8 Z 9 �    10 tzHH    11 tzmm
        if ((struct = /^(\d{4}|[+\-]\d{6})(?:-(\d{2})(?:-(\d{2}))?)?(?:T(\d{2}):(\d{2})(?::(\d{2})(?:\.(\d{3}))?)?(?:(Z)|([+\-])(\d{2})(?::(\d{2}))?)?)?$/.exec(date))) {
            // avoid NaN timestamps caused by �undefined� values being passed to Date.UTC
            for (var i = 0, k; (k = numericKeys[i]); ++i) {
                struct[k] = +struct[k] || 0;
            }

            // allow undefined days and months
            struct[2] = (+struct[2] || 1) - 1;
            struct[3] = +struct[3] || 1;

            if (struct[8] !== 'Z' && struct[9] !== undefined) {
                minutesOffset = struct[10] * 60 + struct[11];

                if (struct[9] === '+') {
                    minutesOffset = 0 - minutesOffset;
                }
            }

            timestamp = Date.UTC(struct[1], struct[2], struct[3], struct[4], struct[5] + minutesOffset, struct[6], struct[7]);
        }
        else {
            timestamp = origParse ? origParse(date) : NaN;
        }

        return timestamp;
    };
}(Date));

jQuery(function($){
      $('#assign-button').on('click', function () {
        event.preventDefault();
        $( "#ap_res_calendar" ).datepicker("destroy");
      $('#ap_res_calendar').html('<span class="spin_f"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i></span>');
      var range_rates_from = $('#range_rates_from').val(),
          range_rates_to = $('#range_rates_to').val(),
          select_rate = $('#select_rate').val(),
          select_ap = $('#select_ap').val();

        if ((range_rates_from != '')&&(range_rates_to != '')){

        $.ajax({
		url : lst.ajax_url,
		type : 'POST',
		data : {
			action : 'assign_rate',
            range_rates_from : range_rates_from,
            range_rates_to : range_rates_to,
            select_rate : select_rate,
            select_ap : select_ap,
            // check
	        nonce : lst.nonce
		},
		success : function( msg ) {
		   $('#general_price').val('');
		   $('#ap_res_calendar').html('');
           try {
			var response = JSON.parse( msg );
		    } catch ( e ) {
			  return false;
		    }
            rate_calendar = new Object();
            color_calendar = new Object();
            rate_general_price = 0;
            if ((response.total)||(response.general_price)){
            rate_calendar = response.price_arr;
            color_calendar = response.color_arr;
            rate_general_price = response.general_price;
            $('#general_price').val(response.general_price);
            }
            $('#range_rates_from').val('');
            $('#range_rates_to').val('');
            show_calendar_by_ap_id();
		  }
        });
        }
      });
});

var rate_calendar = new Object();
var color_calendar = new Object();
var av_calendar = new Object();
var rate_general_price = 0;

jQuery(function($){
      $('#select-ap-button').on('click', function () {
        event.preventDefault();
        $('#ap_res').css("display","none");
        $( "#ap_res_calendar" ).datepicker("destroy");
      $('#ap_res_calendar').html('<span class="spin_f"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i></span>');
      var select_ap = $('#select_ap').val();

        $.ajax({
		url : lst.ajax_url,
		type : 'POST',
		data : {
			action : 'select_ap',
            select_ap : select_ap,
            // check
	        nonce : lst.nonce
		},
		success : function( msg ) {
		   $('#general_price').val('');
		   $('#range_rates_from').val('');
           $('#range_rates_to').val('');
		   $('#ap_res').css("display","block");
           $('#assign-button').css("display","block");
		   $('#ap_res_calendar').html('');
           try {
			var response = JSON.parse( msg );
		    } catch ( e ) {
			  return false;
		    }
            rate_calendar = new Object();
            color_calendar = new Object();
            rate_general_price = 0;
            if ((response.total)||(response.general_price)){
              rate_calendar = response.price_arr;
              color_calendar = response.color_arr;
              rate_general_price = response.general_price;
              $('#general_price').val(response.general_price);
            }
            show_calendar_by_ap_id();
		  }
        });

      });
});

jQuery(function($){
      $('#update-general-button').on('click', function () {
        event.preventDefault();
        $( "#ap_res_calendar" ).datepicker("destroy");
        $('#ap_res_calendar').html('');
      $('#ap_title').html('<span class="spin_f"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i></span>');
      var select_ap = $('#select_ap').val(),
          general_price = $('#general_price').val();

        $.ajax({
		url : lst.ajax_url,
		type : 'POST',
		data : {
			action : 'update_general_p',
            select_ap : select_ap,
            general_price : general_price,
            // check
	        nonce : lst.nonce
		},
		success : function( msg ) {
           $('#ap_title').html('');
           try {
			var response = JSON.parse( msg );
		    } catch ( e ) {
			  return false;
		    }
            rate_calendar = new Object();
            color_calendar = new Object();
            rate_general_price = 0;
            if ((response.total)||(response.general_price)){
            rate_calendar = response.price_arr;
            color_calendar = response.color_arr;
            rate_general_price = response.general_price;
            $('#general_price').val(response.general_price);
            }
            $('#range_rates_from').val('');
            $('#range_rates_to').val('');
            show_calendar_by_ap_id();
		}
        });

      });
});

function get_booking_calendar(){
            var id = jQuery("#_booking_prop_id").val();
            jQuery( "#av_calendar" ).datepicker("destroy");
            jQuery('#av_calendar').html('<span class="spin_f"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i></span>');
                if (id != ''){
                 jQuery.ajax({
		url : lst.ajax_url,
		type : 'POST',
		data : {
			action : 'select_ap2',
            select_ap : id,
            // check
	        nonce : lst.nonce
		},
		success : function( msg ) {
		   jQuery('#av_calendar').html('');
           try {
			var response = JSON.parse( msg );
		    } catch ( e ) {
			  return false;
		    }
            rate_calendar = new Object();
            color_calendar = new Object();
            av_calendar = new Object();
            rate_general_price = 0;
            if ((response.total)||(response.general_price)){
              rate_calendar = response.price_arr;
              color_calendar = response.color_arr;
              av_calendar = response.av_arr;
              rate_general_price = response.general_price;
            }
            show_calendar_by_ap_id2();
		  }
        });
                } else
                jQuery('#av_calendar').html('');
}

jQuery(function($){
            setTimeout(function () {
            var start_id2 = $("#select_ap3").val();
            if((start_id2 != '')&&(start_id2 != undefined)) get_booking_calendar2();
            }, 200);

            $('#select-ap-button3').on('click', function(event){
                get_booking_calendar2();
                jQuery("#sync_spin").html('');
            });
            
            $('#sync_icals').on('click', function(event){
            var prop_id = $("#select_ap3").val();    
            jQuery('#sync_spin').html('<span class="spin_f"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i></span>');      
           jQuery.ajax({
		   url : lst.ajax_url,
		   type : 'POST',
		   data : {
			action : 'sync_icals',
            icals_prop_id : prop_id,
            // check
	        nonce : lst.nonce
		   },
		   success : function( msg ) {
             jQuery("#sync_spin").html(' Syncronized');
             get_booking_calendar2();
		   }
          });   
            });
});

jQuery(function($){
            var start_id = $("#_booking_prop_id").val();
            if((start_id != '')&&(start_id != undefined))
            {
              get_booking_calendar();
             // get_amount_booking();
            }

            $('#_booking_prop_id').on('change', function(event){
              //  jQuery("#range_from").val('');
              //  jQuery("#range_to").val('');
                event.preventDefault();
                get_booking_calendar();
                get_amount_booking();
           });

           $('[id^="_booking_services"], [id^="_booking_extra_bed"]').on('change', function(event){
                get_amount_booking();
           });
});

////////////////////////////////////

jQuery(function($){     
           $('#ical-import-list').on('click', '.ical_edit', function(event){
              var p_li = $(this).parent();
				p_li.find('label, input').css('display', 'block')
					.animate({opacity: 1}, 400);
                p_li.find('input[type="button"]').css('display', 'inline-block');
                
                $(this).addClass('ical_edit_opened');
                $(this).removeClass('ical_edit');
                
                $('#ical-import-list').on('click', '.ical_edit_opened', function(event){
                   $(this).addClass('ical_edit');
                   $(this).removeClass('ical_edit_opened');
                   var ppp_li = $(this).parent();
				    ppp_li.find('label, input').animate({opacity: 0}, 400).css('display', 'none'); 
                });    
                
                $('#ical-import-list').on('click', 'input[type="button"]', function(event){ 
                   var prop_id = $(this).attr('data-p'),
                       pp_li = $(this).parent(),
                       old_ical_name = pp_li.attr('data-o'),
                       add_ical_name = pp_li.find('[id^="add_ical_name"]').val(),
                       add_ical_url = pp_li.find('[id^="add_ical_url"]').val();
              jQuery('#ical-import-list').html('<span class="spin_f"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i></span>');      
        jQuery.ajax({
		url : lst.ajax_url,
		type : 'POST',
		data : {
			action : 'update_ical',
            icals_prop_id : prop_id,
            old_ical_name : old_ical_name,
            add_ical_name : add_ical_name,
            add_ical_url : add_ical_url,
            // check
	        nonce : lst.nonce
		},
		success : function( msg ) {
            jQuery("#ical-import-list").html(msg);
		  }
        });
                });    
           });

       $('#ical-import-list').on('click', '.ical_del', function(event){  
          var icals_prop_id = jQuery("#select_ap3").val();
          var ical_name = $(this).attr('data-n');
              
           // event.preventDefault();
		    $('#overlay').fadeIn(400, function(){
				$('#confirm_del_ical')
					.css('display', 'block')
					.animate({opacity: 1}, 200);
                    
                 $('#confirm_del_ical').on('click', '#delete', function(event){
                 //////////////   
                    jQuery('#ical-import-list').html('<span class="spin_f"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i></span>');      
        jQuery.ajax({
		url : lst.ajax_url,
		type : 'POST',
		data : {
			action : 'del_ical',
            icals_prop_id : icals_prop_id,
            ical_name : ical_name,
            // check
	        nonce : lst.nonce
		},
		success : function( msg ) {
            jQuery("#ical-import-list").html(msg);
            ///////////////    
                    $('#confirm_del_ical').animate({opacity: 0}, 200,	function(){
					$(this).css('display', 'none');
					$('#overlay').fadeOut(400);
				});
             location.reload(true);
		  }
        });
             // location.reload(true);
                 });   
		      });
            });
           
           
           $('#modal_close, #overlay, #confirm_del_ical #cancel').click( function(){
	     	$('#confirm_del_ical').animate({opacity: 0}, 200,	function(){
					$(this).css('display', 'none');
					$('#overlay').fadeOut(400);
				}
			 );
            });
});

function get_booking_calendar2(){
            var export_url = jQuery("#ical-export-url").attr('data-u');
            var id = jQuery("#select_ap3").val();
            jQuery( "#av_calendar3" ).datepicker("destroy");
            jQuery('#av_calendar3').html('<span class="spin_f"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i></span>');
                if (id != ''){
                 jQuery.ajax({
		url : lst.ajax_url,
		type : 'POST',
		data : {
			action : 'select_ap2',
            select_ap : id,
            // check
	        nonce : lst.nonce
		},
		success : function( msg ) {
		   jQuery('#av_calendar3').html('');
           try {
			var response = JSON.parse( msg );
		    } catch ( e ) {
			  return false;
		    }
            rate_calendar = new Object();
            color_calendar = new Object();
            av_calendar = new Object();
            rate_general_price = 0;
           // if ((response.total)||(response.general_price)){
              
              rate_calendar = response.price_arr;
              color_calendar = response.color_arr;
              av_calendar = response.av_arr;
              rate_general_price = response.general_price;
              jQuery("#ical-export-url").val(export_url+id);
              jQuery("#add_ical_prop_id").val(id);
              update_icals_list(id);
              
             // alertObj(av_calendar);
           // }
            show_calendar_by_ap_id3();
		  }
        });
                } else
                jQuery('#av_calendar3').html('');
}

function alertObj(obj) { 
    var str = ""; 
    for(k in obj) { 
        str += k+": "+ obj[k]+"\r\n"; 
    } 
    alert(str); 
}

function update_icals_list(id){
    jQuery.ajax({
		url : lst.ajax_url,
		type : 'POST',
		data : {
			action : 'get_icals',
            icals_prop_id : id,
            // check
	        nonce : lst.nonce
		},
		success : function( msg ) {
		    try {
			var response = JSON.parse( msg );
		    } catch ( e ) {
			  return false;
		    }
            if (response.count >=3 ) jQuery("#add_ical").remove();
            jQuery("#ical-import-list").html(response.list);
		  }
        });    
}

/////////////////////////////////////////////

function show_calendar_by_ap_id3() {
    var first = true;
    jQuery( "#av_calendar3" ).datepicker({
    //  numberOfMonths: 3,
	  numberOfMonths: 2,
      beforeShow: function() {
                addCustomInformation(true);
			},
      onChangeMonthYear: function() {
                addCustomInformation(true);
			},
	  onSelect: function(dateText, inst) {
                var selectedDate = jQuery.datepicker.parseDate(jQuery.datepicker._defaults.dateFormat, dateText);
                    selectedDate.setHours(0);
                    selectedDate.setMinutes(0);
                    selectedDate.setSeconds(0);
                    selectedDate.setMilliseconds(0);
                var timestamp_new = Math.round(selectedDate.getTime()/1000) + 24*60*60;

                addCustomInformation(true);

                if (first == true) {
                  jQuery( "#av_calendar3" ).datepicker('option', 'minDate', dateText);
                  var date_ind = selectedDate.getTime();
                  date_ind = date_ind/1000;
                  for (var key in av_calendar) {
                    var val = parseInt(av_calendar[key]);
                    if ((key > date_ind)&&(val > 1)){
                      var dd = new Date(key*1000);
                      var m = dd.getMonth()+1;
                      var y = dd.getFullYear();
                      var d = dd.getDate();
                      m = (m<10)?"0"+m:m;
                      d = (d<10)?"0"+d:d;
                      var next_date = m+'/'+d+'/'+y;
                      jQuery( "#av_calendar3" ).datepicker('option', 'maxDate', next_date);
                      break;
                      }
                    }
                  } else {
                  jQuery( "#av_calendar3" ).datepicker('option', 'minDate', '-1 year');
                  jQuery( "#av_calendar3" ).datepicker('option', 'maxDate', '+2 year');
                  }

                if (first == true) first = false; else first = true;
			}
    });
         addCustomInformation(true);// if open by default (when you're using <div>)
}

///////////////////////////////////////

function show_calendar_by_ap_id2() {
    jQuery("#_booking_price").on('focus', function(){
      jQuery(this).blur();
    });
    jQuery("#range_from").on('focus', function(){
      jQuery(this).blur();
    });
    jQuery("#range_to").on('focus', function(){
      jQuery(this).blur();
    });
    var first = true;

    jQuery( "#av_calendar" ).datepicker({
    //  numberOfMonths: 3,
	  numberOfMonths: 2,
      beforeShow: function() {
                addCustomInformation(true);
			},
    //---^----------- if closed by default (when you're using <input>)
      onChangeMonthYear: function() {
                addCustomInformation(true);
			},
      beforeShowDay: function(date) {
				var date1 = jQuery.datepicker.parseDate("dd/mm/yy", jQuery("#range_from").val());
				var date2 = jQuery.datepicker.parseDate("dd/mm/yy", jQuery("#range_to").val());
				return [true, date1 && ((date.getTime() == date1.getTime()) || (date2 && date >= date1 && date <= date2)) ? "dp-highlight" : ""];
			},
	  onSelect: function(dateText, inst) {
                    //   dateText = $('#ap_res_calendar').datepicker({ dateFormat: 'dd/mm/yy' }).val();
				var date1 = jQuery.datepicker.parseDate("dd/mm/yy", jQuery("#range_from").val());
				var date2 = jQuery.datepicker.parseDate("dd/mm/yy", jQuery("#range_to").val());
                var selectedDate = jQuery.datepicker.parseDate(jQuery.datepicker._defaults.dateFormat, dateText);
                    selectedDate.setHours(0);
                    selectedDate.setMinutes(0);
                    selectedDate.setSeconds(0);
                    selectedDate.setMilliseconds(0);
                var timestamp_new = Math.round(selectedDate.getTime()/1000) + 24*60*60;

                if (!date1 || date2) {
					jQuery("#range_from").val(mm_dd_to_dd_mm(dateText));
					jQuery("#range_to").val("");
                    jQuery("#_booking_date_from").val(timestamp_new);
					jQuery("#_booking_date_to").val("");
                    jQuery(this).datepicker();
                } else if( selectedDate < date1 ) {
                    jQuery("#range_to").val( jQuery("#range_from").val() );
                    jQuery("#range_from").val( mm_dd_to_dd_mm(dateText) );
                    jQuery("#_booking_date_to").val( jQuery("#_booking_date_from").val() );
                    jQuery("#_booking_date_from").val(timestamp_new);
                    jQuery(this).datepicker();
                } else {
					jQuery("#range_to").val(mm_dd_to_dd_mm(dateText));
                    jQuery("#_booking_date_to").val(timestamp_new);
                    jQuery(this).datepicker();
				}
                addCustomInformation(true);
                get_amount_booking();

                if (first == true) {
                  first = false;
                  jQuery( "#av_calendar" ).datepicker('option', 'minDate', dateText);
                  var date_ind = selectedDate.getTime();
                  date_ind = date_ind/1000;
                  for (var key in av_calendar) {
                    var val = parseInt(av_calendar[key]);
                    if ((key > date_ind)&&(val > 1)){
                      var dd = new Date(key*1000);
                      var m = dd.getMonth()+1;
                      var y = dd.getFullYear();
                      var d = dd.getDate();
                      m = (m<10)?"0"+m:m;
                      d = (d<10)?"0"+d:d;
                      var next_date = m+'/'+d+'/'+y;
                      jQuery( "#av_calendar" ).datepicker('option', 'maxDate', next_date);
                      break;
                      }
                    }
                  } else {
                  jQuery( "#av_calendar" ).datepicker('option', 'minDate', '-1 year');
                  jQuery( "#av_calendar" ).datepicker('option', 'maxDate', '+2 year');
                  }
			}
    });
         addCustomInformation(true);// if open by default (when you're using <div>)
     var date_sart = jQuery("#range_from").val();
    if (date_sart != '') {
       date_sart = mm_dd_to_dd_mm(date_sart);
       jQuery( "#av_calendar" ).datepicker("setDate", date_sart);
    }    
}

function get_amount_booking(){

                var id = jQuery("#_booking_prop_id").val();
              //  event.preventDefault();
                var date_from = jQuery("#range_from").val();
                var date_to = jQuery("#range_to").val();
                var extra_bed = jQuery("input[name='_booking_extra_bed']:checked").val();
                var services = new Object();
                var services2 = jQuery("input[name='_booking_services[]']:checked").map(function(){
                     var key = jQuery(this).val();
                     services[key] = 1;
                     return key;
                  }).get();

              jQuery('#spin_date_from').html('<span class="spin_f"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i></span>');
                if (id != ''){
                 jQuery.ajax({
		url : lst.ajax_url,
		type : 'POST',
		data : {
			action : 'get_amount_booking',
            id : id,
            date_from : date_from,
            date_to : date_to,
            services : services,
            extra_bed : extra_bed,
            // check
	        nonce : lst.nonce
		},
		success : function( msg ) {
		   jQuery('#spin_date_from').html('');
           try {
			var response = JSON.parse( msg );
		    } catch ( e ) {
			  return false;
		    }
           jQuery("#_booking_price").val(response.total);
           jQuery("#_booking_price_tax").val(response.total_tax);
           jQuery("#_booking_price_clear").val(response.total_clear);
		  }
        });
                } else
                jQuery('#spin_date_from').html('');

}


////////////////////////

jQuery(function($){
            $('.invoice-button').click(function(event){
    		//event.preventDefault();
            //var id = '';
            var url = $(this).attr('data-u');
		   // var id = $("#the-list th input:checked").val();
           // if (id != undefined){
           //   var title = $("#inline_"+id+" .post_title").html();
           //   var redirectWindow = window.open(url+title, '_blank');
                 var redirectWindow = window.open(url, '_blank');
                 redirectWindow.location;
           //  }
           });
   });

////////////////////////

jQuery(function($){

            $('.booking_page_customers table.customers').on('click', '[id^="edit-notes-button"]', function(event){
            var user_id = $(this).attr('data-u');
            var td = $(this).parent();
            $(this).detach();
            var text = td.text();
            td.html('<textarea id="'+user_id+'" cols=17 rows=5>'+text+'</textarea><button id="save-notes-button'+user_id+'" data-u="'+user_id+'" type="button" title="Save">Save notes</button>');
           });

           $('.booking_page_customers table.customers').on('click', '[id^="save-notes-button"]', function(event){
              var user_id = $(this).attr('data-u');
              var text = $('textarea#'+user_id).val();
              var td = $(this).parent();
            //  $(this).detach();
              td.html('<span class="spin_f"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i></span>');

              jQuery.ajax({
		url : lst.ajax_url,
		type : 'POST',
		data : {
			action : 'save_admin_notes',
            user_id : user_id,
            text : text,
            // check
	        nonce : lst.nonce
		},
		success : function( msg ) {
		   td.html(msg+'<button id="edit-notes-button'+user_id+'" data-u="'+user_id+'" type="button" title="Edit">Edit notes</button>');
		  }
        });

           });
   });
