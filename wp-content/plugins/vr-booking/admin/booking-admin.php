<?php

//  admin class
//


if ( ! defined( 'ABSPATH' ) )
	exit;

require_once VR_BOOKING_PLUGIN_DIR . '/classes/customers-list.php';
//require_once VR_BOOKING_PLUGIN_DIR . '/classes/list-table-example.php';

///// admin class for manage options

class VRBooking_admin
{
    /**
     * Holds the values to be used in the fields callbacks
     */
    private $options;

    public function __construct()
    {
        add_action( 'admin_menu', array( $this, 'add_plugin_page' ) );
        add_action( 'admin_init', array( $this, 'page_init' ) );
        add_action( 'admin_enqueue_scripts', array( $this, 'enqueued_assets' ) );
        add_filter('submenu_file', array( $this, 'submenu_current' ));
        add_filter( 'custom_menu_order', array( $this, 'pms_submenu_order'));

        add_filter( 'posts_where', array( $this, 'search_where' ));
        add_filter('posts_join', array( $this, 'search_join' ));
        add_filter( 'posts_groupby', array( $this, 'search_group_by' ));

        add_action('wp_ajax_save_admin_notes', array( $this, 'save_admin_notes_callback'));

        add_action('current_screen', array( $this, 'current_screen_callback'));
        add_filter('post_updated_messages', array( $this, 'booking_updated_messages'));
        add_action( 'admin_init', array( $this, 'update_screen'));
        
        add_action('wp_ajax_get_icals', array( $this, 'get_icals_callback'));
        add_action('wp_ajax_del_ical', array( $this, 'del_ical_callback'));
        add_action('wp_ajax_update_ical', array( $this, 'update_ical_callback'));
        add_action('wp_ajax_sync_icals', array( $this, 'sync_icals_callback'));

        //////////// $markers_urls ///////
       $this->markers_urls[1] = 'css/img/mm-blue.png';
       $this->markers_urls[2] = 'css/img/mm-green.png';
       $this->markers_urls[3] = 'css/img/mm-l-blue.png';
       $this->markers_urls[4] = 'css/img/mm-orange.png';
       $this->markers_urls[5] = 'css/img/mm-purple.png';
       $this->markers_urls[6] = 'css/img/mm-red.png';
       $this->markers_urls[7] = 'css/img/mm-turquoise.png';
       $this->markers_urls[8] = 'css/img/mm-violet.png';
       $this->markers_urls[9] = 'css/img/mm-yellow.png'; 
       $this->c_element = 3;
    }

function enqueued_assets() {
         wp_enqueue_style( 'wp-color-picker');
         wp_enqueue_script( 'wp-color-picker');
        if(function_exists( 'wp_enqueue_media' )){
         wp_enqueue_media();
         } else {
           wp_enqueue_style('thickbox');
           wp_enqueue_script('media-upload');
           wp_enqueue_script('thickbox');
           }
           
            wp_enqueue_script( 'jquery-ui-custom-js', plugins_url( "js/jquery-ui.js", VR_BOOKING_PLUGIN ), array('jquery'), '1.12.1', true );

    // wp_enqueue_script( 'jquery-ui-custom-js', 'https://code.jquery.com/ui/1.12.1/jquery-ui.js', array('jquery'), '1.12.1', true );

     wp_enqueue_script( 'my-admin-custom-js', plugins_url( "admin/js/booking-admin.js", VR_BOOKING_PLUGIN ), array('jquery'), '1.0', true );
     wp_localize_script( 'my-admin-custom-js', 'lst', array(
            'ajax_url' => admin_url( 'admin-ajax.php' ),
            'nonce' => wp_create_nonce('lst-nonce')
         )
        );

      wp_enqueue_style('fontawesome2', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css', '', '4.6.3', 'all');
     wp_enqueue_style('jquery-ui-admin-style', '//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css', '', '1.12.1', 'all');
     wp_enqueue_style( 'booking-admin-style', plugins_url( "admin/css/booking-admin.css", VR_BOOKING_PLUGIN ));
     
     global $pagenow;
     
     if(isset($_GET['post_type']) && $_GET['post_type'] == 'property' && $pagenow == 'edit.php'){
       $cc = 4;
       $count = count(get_posts(array('post_type'   => 'property', 'numberposts' => -1 )));
       if ($count >= apply_filters('vr_booking_c_properties', $this->c_element*$cc)){
        $custom_css = ".wrap .page-title-action{display:none;}";
        wp_add_inline_style( 'booking-admin-style', $custom_css );
        }
     }         
}

    /**
     * Add options page
     */
    function add_plugin_page()
    {
        // This page will be under "Settings"
          add_menu_page(
            __('VR Booking Plugin Settings', VR_BOOKING_TEXTDOMAIN),
            __('VR Booking plugin', VR_BOOKING_TEXTDOMAIN),
            'manage_options',
            'vr-booking-settings',
            array( $this, 'create_admin_page' ),
            '',
            26
        );

        add_submenu_page( 'edit.php?post_type=booking', __('Central Reservation Office (CRO)', VR_BOOKING_TEXTDOMAIN), __('Central Reservation Office (CRO)', VR_BOOKING_TEXTDOMAIN), 'manage_options', 'cro', array( $this, 'create_cro_page' ));

        add_submenu_page( 'edit.php?post_type=booking', __('My Customers', VR_BOOKING_TEXTDOMAIN), __('My Customers', VR_BOOKING_TEXTDOMAIN), 'manage_options', 'customers', array( $this, 'create_customers_page' ));

        add_submenu_page( 'edit.php?post_type=booking', __('My Invoices', VR_BOOKING_TEXTDOMAIN), __('My Invoices', VR_BOOKING_TEXTDOMAIN), 'manage_options', 'edit.php?post_type=booking&subpage=invoices');

        add_submenu_page( 'vr-booking-settings', __('General settings', VR_BOOKING_TEXTDOMAIN), __('General settings', VR_BOOKING_TEXTDOMAIN), 'manage_options', 'vr-booking-settings');
        add_submenu_page( 'vr-booking-settings', __('Other settings', VR_BOOKING_TEXTDOMAIN), __('Other settings', VR_BOOKING_TEXTDOMAIN), 'manage_options', 'admin.php?page=vr-booking-settings&subpage=other#other_settings');

    }
////////////////////////

function update_screen(){  
    global $pagenow;
    
    if(isset($_GET['post_type']) && $_GET['post_type'] == 'property'){
       $cc = 4;
       $count = count(get_posts(array('post_type' => 'property', 'numberposts' => -1 ))); 
       if ($count >= apply_filters('vr_booking_c_properties', $this->c_element*$cc)){
          if($pagenow == 'post-new.php'){ 
        wp_redirect(admin_url('edit.php?post_type=property'));
         exit;
        }
        if($pagenow == 'edit.php'){
          $page = remove_submenu_page( 'edit.php?post_type=property', 'post-new.php?post_type=property' );
        }     
       }
    }
    return;
}    

///////////////////////////
function pms_submenu_order( $menu_ord )
{
    global $submenu;

    // Enable the next line to inspect the $submenu values
    // echo '<pre>'.print_r($submenu,true).'</pre>';

    $arr = array();
    $arr[] = $submenu['edit.php?post_type=booking'][11];
    $arr[] = $submenu['edit.php?post_type=booking'][10];
    $arr[] = $submenu['edit.php?post_type=booking'][5];
    $arr[] = $submenu['edit.php?post_type=booking'][12];
    $arr[] = $submenu['edit.php?post_type=booking'][13];
    $submenu['edit.php?post_type=booking'] = $arr;

    return $menu_ord;
}

function submenu_current($submenu_file){
    global $current_screen;
    if(( 'booking' == $current_screen->post_type )&&( isset( $_GET['subpage'] ))) {
         if ( $_GET['subpage'] == 'invoices')
         return 'edit.php?post_type=booking&subpage=invoices';

       //  if ( $_GET['subpage'] == 'customers')
       //  return 'edit.php?post_type=booking&subpage=customers';
    }
    if(( 'toplevel_page_vr-booking-settings' == $current_screen->base )&&( isset( $_GET['subpage'] ))) {
         if ( $_GET['subpage'] == 'other')
         return 'admin.php?page=vr-booking-settings&subpage=payment#other_settings';
    }
    return $submenu_file;
}

function search_join ($join){
    global $pagenow, $wpdb;
    if ( isset( $_GET['s'] )){
    if ( is_admin() && $pagenow=='edit.php' && $_GET['post_type']=='booking' && $_GET['s'] != '') {
        $join .='LEFT JOIN '.$wpdb->postmeta. ' ON '. $wpdb->posts . '.ID = ' . $wpdb->postmeta . '.post_id ';
     }
    }
    return $join;
}

function search_where( $where ){
    global $pagenow, $wpdb;
    if ( isset( $_GET['s'] )){
    if ( is_admin() && $pagenow=='edit.php' && $_GET['post_type']=='booking' && $_GET['s'] != '') {
       // $start_date = $_GET['s'];
        $where = preg_replace(
       "/\(\s*".$wpdb->posts.".post_title\s+LIKE\s*(\'[^\']+\')\s*\)/",
       "(".$wpdb->posts.".post_title LIKE $1) OR ((".$wpdb->postmeta.".meta_key = 'range_from') AND (".$wpdb->postmeta.".meta_value LIKE $1))", $where );
     }
    }
    return $where;
}


function search_group_by($groupby) {
    global $pagenow, $wpdb;
    if ( isset( $_GET['s'] )){
      if ( is_admin() && $pagenow == 'edit.php' && $_GET['post_type']=='booking' && $_GET['s'] != '' ) {
        $groupby = "$wpdb->posts.ID";
      }
    }  
    return $groupby;
}

////////////////////////////

function booking_updated_messages($messages){
   global $post, $post_ID;

  $messages['booking'] = array(
    0 => '', // Unused. Messages start at index 1.
    1 => __('Reservation updated.'),
    2 => __('Custom field updated.'),
    3 => __('Custom field deleted.'),
    4 => __('Reservation updated.'),
    /* translators: %s: date and time of the revision */
    5 => isset($_GET['revision']) ? sprintf( __('Reservation restored to revision from %s'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
    6 => __('Reservation saved.'),
    7 => __('Reservation saved.'),
    8 => __('Reservation submitted.'),
    9 => sprintf( __('Reservation scheduled for: <strong>%1$s</strong>.'),
      // translators: Publish box date format, see http://php.net/date
      date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) )),
    10 => __('Reservation draft updated.'),
  );
  return $messages;
}

/////////////////////////
function current_screen_callback($screen) {
    if( is_object($screen) && $screen->post_type == 'booking' ) {
        add_filter('gettext', array( $this, 'wps_translation'), 10, 3);
    }
}

function wps_translation($translation, $text, $domain) {
        $translations = get_translations_for_domain( $domain);
        if ( $text == 'Published on: <b>%1$s</b>') {
            return $translations->translate( 'Created on: <b>%1$s</b>' );
        }
        if ( $text == 'Publish <b>immediately</b>') {
            return $translations->translate( 'Save <b>immediately</b>' );
        }
         if ( $text == 'Publish') {
            return $translations->translate( 'Save' );
        }
    return $translation;
}


//////////////// create_customers_page

function create_customers_page(){
     $this->options = get_option( 'booking_settings' );
     $this->customers_obj = new Customers_List();
     ?>
        <div class="wrap">
            <h2><?php echo __('My Customers', VR_BOOKING_TEXTDOMAIN); ?></h2>

        <div id="poststuff">
			<div id="post-body" class="metabox-holder columns-2">
				<div id="post-body-content">
					<div class="meta-box-sortables ui-sortable">
						<form method="post">
							<?php
						 	$this->customers_obj->prepare_items();
						 	$this->customers_obj->display(); ?>
						</form>
					</div>
				</div>
			</div>
			<br class="clear">
		</div>

        </div>
        <?php

}

function save_admin_notes_callback(){
   $user_id = isset($_POST['user_id']) ? $_POST['user_id'] : 0;
   $text = isset($_POST['text']) ? $_POST['text'] : 0;
   $output = '';

   if (($user_id)&&($text)){
      update_user_meta($user_id, '_admin_notes', $text);
      $output = $text;
   }
   echo $output;
   wp_die();
}

//////////////////////

function create_cro_page(){
        // Set class property
        $this->options = get_option( 'booking_settings' );
        
        $this->add_ical_post();
        
        ?>
        <div class="wrap">
            <h2><?php echo __('Central Reservation Office (CRO)', VR_BOOKING_TEXTDOMAIN); ?></h2>
        </div>
        <?php
        $apartments_arr = cmb2_get_property_options();
        $tmp = '';
        foreach($apartments_arr as $ap_id => $ap_title){
          $tmp .= '<option value="'.$ap_id.'">'.__($ap_title).'</option>';
        }
        if ($tmp) $tmp = '<label for="select_ap3">Select property: </label><select size="1" id="select_ap3" name="select_ap3">'.$tmp.'</select>';

        echo '<div id="cro_select">'.$tmp.'
        <button id="select-ap-button3" type="button" title="Select">Select</button>
        </div>

        <div id="av_calendar3"></div>

        <div id="cro_footer">
        <a href="'.admin_url( 'post-new.php?post_type=booking' ).'">'.__('Add new reservation', VR_BOOKING_TEXTDOMAIN).'</a>
        <a href="'.admin_url( 'edit.php?post_type=booking' ).'">'.__('View all reservations', VR_BOOKING_TEXTDOMAIN).'</a>
        <div id="cro_footer_right"><span class="available"></span>'.__('Available', VR_BOOKING_TEXTDOMAIN).'<span class="booked"></span>'.__('Booked', VR_BOOKING_TEXTDOMAIN).'</div>
        </div>';
        
        echo '<div id="ical-block">';
        
        // Export calendar URL for current property
        echo '<div id="ical-block-export">
        
        <h2>'.__('iCal import/export', VR_BOOKING_TEXTDOMAIN).'</h2>';
        
        echo '<span class="ical-title">'.__('Export iCalendar URL for this property: ', VR_BOOKING_TEXTDOMAIN).'</span><input type="text" id="ical-export-url" data-u="'.get_home_url().'/index.php?ical_prop=" value="">';        
        
        echo '</div>';
        // Export calendar .ics file for current property
        
        // Import calensar .ics file
        
        // Sinhronize calendars: add, edit, dell; list - for current property;
        echo '<div id="ical-block-import">';
        
        echo '<span class="ical-title">'.__('Import iCalendars', VR_BOOKING_TEXTDOMAIN).'</span><br />';
        
        echo '<span class="ical-list">'.__('Add import from new iCalendar', VR_BOOKING_TEXTDOMAIN).'</span>
        <form name="add_ical" id="add_ical" method="post">
        <label for="add_ical_name">'.__('Calendar name: ', VR_BOOKING_TEXTDOMAIN).'</label>
        <input type="text" name="add_ical_name" id="add_ical_name" value="">
        <label for="add_ical_url">'.__('Calendar URL: ', VR_BOOKING_TEXTDOMAIN).'</label>
        <input type="text" name="add_ical_url" id="add_ical_url" value="">
        <input type="submit" name="add_ical_submit" id="add_ical_submit" value="Add">
        <input type="hidden" name="add_ical_prop_id" id="add_ical_prop_id" value="">
        </form>';
        
        echo '<div><input type="button" name="sync_icals" id="sync_icals" value="Syncronize iCalendars"><div id="sync_spin"></div></div>';
        
        echo '<span class="ical-list">'.__('The list of the iCalendars which are imported to VR Booking', VR_BOOKING_TEXTDOMAIN).'</span><ol id="ical-import-list"></ol>';
        
        echo '</div>';
        
        echo '</div>';
        
        echo '<div id="confirm_del_ical">
  <span id="modal_close"><i class="fa fa-remove"></i></span>
  <h1>'.__('Delete selected iCalendar?', VR_BOOKING_TEXTDOMAIN).'</h1>
  <input type="button" name="cancel" id="cancel" value="Cancel">
  <input type="button" name="delete" id="delete" value="Delete">
  </div>
  <div id="overlay"></div>';
    }
    
///////////// iCal block   //////////////

function add_ical_post(){
   if (isset($_POST['add_ical_submit'])){
      $ical_name = $_POST['add_ical_name'];
      $ical_url = $_POST['add_ical_url'];
      $ical_prop = $_POST['add_ical_prop_id'];
      if ($this->get_icals_count($ical_prop)<3)
      $this->add_ical($ical_prop, $ical_name, $ical_url);
   } 
  return;  
}

function get_icals_count($prop_id){
   $icals_list = $this->get_icals($prop_id); 
   return sizeof($icals_list); 
}

function add_ical($ical_prop, $ical_name, $ical_url){
      $new_ical_arr[$ical_name] = $ical_url;
      $icals_list = $this->get_icals($ical_prop);
      if (!empty($icals_list)) $new_ical_arr = array_merge($icals_list, $new_ical_arr);
      update_post_meta( $ical_prop, "_icals_import_list", maybe_serialize($new_ical_arr) );
  return;  
}

function update_ical_callback(){
   $output = ''; 
   
   if (isset($_POST['icals_prop_id'])){
     $prop_id = $_POST['icals_prop_id'];
     $old_ical_name = $_POST['old_ical_name'];
     $add_ical_name = $_POST['add_ical_name'];
     $add_ical_url = $_POST['add_ical_url'];
     $this->del_ical($prop_id, $old_ical_name);
     $this->add_ical($prop_id, $add_ical_name, $add_ical_url);
     $output = $this->get_icals_li($prop_id);
   } 
   
   echo $output;
   wp_die(); 
}

function get_icals($prop_id){
   $icals_list = maybe_unserialize(get_post_meta( $prop_id, '_icals_import_list', true)); 
   return $icals_list; 
}

function get_icals_li($prop_id){
   $output = ''; 
    
   $icals_list = $this->get_icals($prop_id);
      foreach ($icals_list as $ical_name => $ical_url)
       $output .= '<li data-o="'.$ical_name.'">'.$ical_name.'
       <label for="add_ical_name_'.$prop_id.'">'.__('Calendar name: ', VR_BOOKING_TEXTDOMAIN).'</label>
       <input type="text" name="add_ical_name_'.$prop_id.'" id="add_ical_name_'.$prop_id.'" value="'.$ical_name.'">
       <label for="add_ical_url_'.$prop_id.'">'.__('Calendar URL: ', VR_BOOKING_TEXTDOMAIN).'</label>
       <input type="text" name="add_ical_url_'.$prop_id.'" id="add_ical_url_'.$prop_id.'" value="'.$ical_url.'">
       <input type="button" name="add_ical_update_'.$prop_id.'" id="add_ical_update_'.$prop_id.'" value="Update" data-p="'.$prop_id.'">
       
       <span class="ical_del" data-n="'.$ical_name.'"><i class="fa fa-trash fa-lg"></i></span><span class="ical_edit" data-n="'.$ical_name.'"><i class="fa fa-pencil fa-lg"></i></span></li>'; 
   return $output; 
}

function get_icals_callback(){
   $output = array(); 
   
   if (isset($_POST['icals_prop_id'])){
      $prop_id = $_POST['icals_prop_id'];    
      $output['list'] = $this->get_icals_li($prop_id);
      $output['count'] = $this->get_icals_count($prop_id);
   }  
   echo json_encode($output);
   wp_die(); 
}

function sync_icals_callback(){
   Global $VRBooking_var; 
   $output = ''; 
   
   if (isset($_POST['icals_prop_id'])){
      $prop_id = $_POST['icals_prop_id'];    
      $VRBooking_var->import_icals($prop_id);    
      $output = 1;
   }  
   echo $output;
   wp_die();
}

function del_ical($prop_id, $ical_name){
   $icals_list = $this->get_icals($prop_id);
   if((!empty($icals_list))&&(isset($icals_list[$ical_name]))) {
      unset($icals_list[$ical_name]);
      update_post_meta( $prop_id, "_icals_import_list", maybe_serialize($icals_list) );
      return true;
   }
   return false; 
}

function del_ical_callback(){
   $output = ''; 
   
   if (isset($_POST['icals_prop_id'])){
      $prop_id = $_POST['icals_prop_id'];
      $ical_name = $_POST['ical_name'];
      $this->del_ical($prop_id, $ical_name);    
      $output = $this->get_icals_li($prop_id);
   }  
   echo $output;
   wp_die();    
}   

///////////////////////////////////////    

    /**
     * Options page callback
     */
    function create_admin_page()
    {
        // Set class property
        $this->options = get_option( 'booking_settings' );
        ?>
        <div class="wrap">
            <h2><?php echo __('VR Booking Plugin Settings', VR_BOOKING_TEXTDOMAIN); ?></h2>
            <form method="post" action="options.php">
            <?php
                // This prints out all hidden setting fields
                settings_fields( 'vr-booking-settings' );
                do_settings_sections( 'vr-booking-settings' );
                submit_button();
            ?>
            </form>
        </div>

        <script>
(function( $ ) {
	// Add Color Picker to all inputs that have 'color-field' class
	$(function() {
	$('.color-field').wpColorPicker();
	});
})( jQuery );
       </script>

        <?php
    }
////////////////////////////////////////////////
///////////////////////////////////////////////
////////////////////////////////////////////////
    /**
     * Register and add settings
     */
    function page_init()
    {
        register_setting(
            'vr-booking-settings', // Option group
            'booking_settings', // Option name
            array( $this, 'sanitize' ) // Sanitize
        );

        ///////// General

        add_settings_section(
            'setting_section_5', // ID
            __('General',VR_BOOKING_TEXTDOMAIN), // Title
            array( $this, 'print_section_info5' ), // Callback
            'vr-booking-settings' // Page
        );

        add_settings_field(
            'max_select_adults', // ID
            __('Maximum # of adults',VR_BOOKING_TEXTDOMAIN), // Title
            array( $this, 'max_select_adults_callback' ), // Callback
            'vr-booking-settings', // Page
            'setting_section_5' // Section
        );

        add_settings_field(
            'max_select_children', // ID
            __('Maximum # of children',VR_BOOKING_TEXTDOMAIN), // Title
            array( $this, 'max_select_children_callback' ), // Callback
            'vr-booking-settings', // Page
            'setting_section_5' // Section
        );

        add_settings_field(
            'max_select_rooms', // ID
            __('Maximum # of rooms',VR_BOOKING_TEXTDOMAIN), // Title
            array( $this, 'max_select_rooms_callback' ), // Callback
            'vr-booking-settings', // Page
            'setting_section_5' // Section
        );

        add_settings_field(
            'search_res_front', // ID
            __('Display per page (front page)',VR_BOOKING_TEXTDOMAIN), // Title
            array( $this, 'search_res_front_callback' ), // Callback
            'vr-booking-settings', // Page
            'setting_section_5' // Section
        );

        add_settings_field(
            'search_res_other', // ID
            __('Search results per page',VR_BOOKING_TEXTDOMAIN), // Title
            array( $this, 'search_res_other_callback' ), // Callback
            'vr-booking-settings', // Page
            'setting_section_5' // Section
        );

        add_settings_field(
            'color_button', // ID
            __('Adjust colors',VR_BOOKING_TEXTDOMAIN), // Title
            array( $this, 'color_button_callback' ), // Callback
            'vr-booking-settings', // Page
            'setting_section_5' // Section
        );

        ///////// Payment

        add_settings_section(
            'setting_section_3', // ID
            __('Payment settings',VR_BOOKING_TEXTDOMAIN), // Title
            array( $this, 'print_section_info3' ), // Callback
            'vr-booking-settings' // Page
        );

        add_settings_field(
            'tax', // ID
            __('Taxes, %',VR_BOOKING_TEXTDOMAIN), // Title
            array( $this, 'tax_callback' ), // Callback
            'vr-booking-settings', // Page
            'setting_section_3' // Section
        );

        add_settings_field(
            'currency', // ID
            __('Currency symbol ($, &euro; etc.)',VR_BOOKING_TEXTDOMAIN), // Title
            array( $this, 'currency_callback' ), // Callback
            'vr-booking-settings', // Page
            'setting_section_3' // Section
        );

        add_settings_field(
            'currency_code', // ID
            __('Currency 3 letters code (ISO 4217: USD, EUR, etc.)',VR_BOOKING_TEXTDOMAIN), // Title
            array( $this, 'currency_code_callback' ), // Callback
            'vr-booking-settings', // Page
            'setting_section_3' // Section
        );

        add_settings_field(
            'currency_place', // ID
            __('Place currency symbol',VR_BOOKING_TEXTDOMAIN), // Title
            array( $this, 'currency_place_callback' ), // Callback
            'vr-booking-settings', // Page
            'setting_section_3' // Section
        );

        ///////// Booking Modes

        add_settings_section(
            'setting_section_8', // ID
            __('Booking Modes',VR_BOOKING_TEXTDOMAIN), // Title
            array( $this, 'print_section_info8' ), // Callback
            'vr-booking-settings' // Page
        );

        add_settings_field(
            'payment_mode1', // ID
            __('Book now. Pay Later: Customers do not have to pay online payment to make reservation.',VR_BOOKING_TEXTDOMAIN), // Title
            array( $this, 'payment_mode1_callback' ), // Callback
            'vr-booking-settings', // Page
            'setting_section_8' // Section
        );

        add_settings_field(
            'payment_mode2', // ID
            __('Deposit to Guarantee the Reservation: Customers pay online deposit to guarantee the reservation.',VR_BOOKING_TEXTDOMAIN), // Title
            array( $this, 'payment_mode2_callback' ), // Callback
            'vr-booking-settings', // Page
            'setting_section_8' // Section
        );

        add_settings_field(
            'payment_mode3', // ID
            __('Pre-Paid Reservation: Customers pay online full amount of reservation.',VR_BOOKING_TEXTDOMAIN), // Title
            array( $this, 'payment_mode3_callback' ), // Callback
            'vr-booking-settings', // Page
            'setting_section_8' // Section
        );

        ///////// Payment Gateway

        add_settings_section(
            'setting_section_7', // ID
            __('Activate Payment Gateways',VR_BOOKING_TEXTDOMAIN), // Title
            array( $this, 'print_section_info3' ), // Callback
            'vr-booking-settings' // Page
        );

        add_settings_field(
            'paypal_activate', // ID
            __('Activate PayPal',VR_BOOKING_TEXTDOMAIN), // Title
            array( $this, 'paypal_activate_callback' ), // Callback
            'vr-booking-settings', // Page
            'setting_section_7' // Section
        );

        add_settings_field(
            'stripe_activate', // ID
            __('Activate Stripe',VR_BOOKING_TEXTDOMAIN), // Title
            array( $this, 'stripe_activate_callback' ), // Callback
            'vr-booking-settings', // Page
            'setting_section_7' // Section
        );

        add_settings_field(
            'deposit_amount', // ID
            __('Deposit amount (% from total)',VR_BOOKING_TEXTDOMAIN), // Title
            array( $this, 'deposit_amount_callback' ), // Callback
            'vr-booking-settings', // Page
            'setting_section_8' // Section
        );

        ////////////// PayPal //////////

        add_settings_section(
            'setting_section_1', // ID
            __('PayPal settings',VR_BOOKING_TEXTDOMAIN), // Title
            array( $this, 'print_section_info' ), // Callback
            'vr-booking-settings' // Page
        );

        add_settings_field(
            'paypal_sandbox', // ID
            __('Use PayPal sandbox mode?',VR_BOOKING_TEXTDOMAIN), // Title
            array( $this, 'paypal_sandbox_callback' ), // Callback
            'vr-booking-settings', // Page
            'setting_section_1' // Section
        );

        add_settings_field(
            'paypal_email', // ID
            __('E-mail for PayPal account',VR_BOOKING_TEXTDOMAIN), // Title
            array( $this, 'paypal_email_callback' ), // Callback
            'vr-booking-settings', // Page
            'setting_section_1' // Section
        );

     //////////// Stripe

        add_settings_section(
            'setting_section_2', // ID
            __('Stripe settings',VR_BOOKING_TEXTDOMAIN), // Title
            array( $this, 'print_section_info2' ), // Callback
            'vr-booking-settings' // Page
        );

        add_settings_field(
            'stripe_sandbox', // ID
            __('Use Stripe test mode?',VR_BOOKING_TEXTDOMAIN), // Title
            array( $this, 'stripe_sandbox_callback' ), // Callback
            'vr-booking-settings', // Page
            'setting_section_2' // Section
        );

        add_settings_field(
            'stripe_test_secret_key', // ID
            __('Stripe Test Secret Key',VR_BOOKING_TEXTDOMAIN), // Title
            array( $this, 'stripe_test_secret_key_callback' ), // Callback
            'vr-booking-settings', // Page
            'setting_section_2' // Section
        );

        add_settings_field(
            'stripe_test_publishable_key', // ID
            __('Stripe Test Publishable Key',VR_BOOKING_TEXTDOMAIN), // Title
            array( $this, 'stripe_test_publishable_key_callback' ), // Callback
            'vr-booking-settings', // Page
            'setting_section_2' // Section
        );

        add_settings_field(
            'stripe_live_secret_key', // ID
            __('Stripe Live Secret Key',VR_BOOKING_TEXTDOMAIN), // Title
            array( $this, 'stripe_live_secret_key_callback' ), // Callback
            'vr-booking-settings', // Page
            'setting_section_2' // Section
        );

        add_settings_field(
            'stripe_live_publishable_key', // ID
            __('Stripe Live Publishable Key',VR_BOOKING_TEXTDOMAIN), // Title
            array( $this, 'stripe_live_publishable_key_callback' ), // Callback
            'vr-booking-settings', // Page
            'setting_section_2' // Section
        );

        ///////// Booking settings

        add_settings_section(
            'setting_section_9', // ID
            __('Booking settings',VR_BOOKING_TEXTDOMAIN), // Title
            array( $this, 'print_section_info3' ), // Callback
            'vr-booking-settings' // Page
        );

        add_settings_field(
            'add_accept_term', // ID
            __('Add "I have read and accept the Terms and Conditions" required field to booking form?',VR_BOOKING_TEXTDOMAIN), // Title
            array( $this, 'add_accept_term_callback' ), // Callback
            'vr-booking-settings', // Page
            'setting_section_9' // Section
        );

        add_settings_field(
            'add_accept_term_adds', // ID
            __('Add new required field to booking form?',VR_BOOKING_TEXTDOMAIN), // Title
            array( $this, 'add_accept_term_adds_callback' ), // Callback
            'vr-booking-settings', // Page
            'setting_section_9' // Section
        );

        add_settings_field(
            'print_button', // ID
            __('Add Print button to Confirmation page',VR_BOOKING_TEXTDOMAIN), // Title
            array( $this, 'print_button_callback' ), // Callback
            'vr-booking-settings', // Page
            'setting_section_9' // Section
        );

        /////////////////////
        /////////////////////
        add_settings_section(
            'setting_section_12', // ID
            __('Other Settings',VR_BOOKING_TEXTDOMAIN), // Title
            array( $this, 'print_section_info4' ), // Callback
            'vr-booking-settings' // Page
        );
        /////// Olark chat

        add_settings_section(
            'setting_section_10', // ID
            __('Olark Live Chat',VR_BOOKING_TEXTDOMAIN), // Title
            array( $this, 'print_section_info4' ), // Callback
            'vr-booking-settings' // Page
        );

        add_settings_field(
            'chat_active', // ID
            __('Activate Olark live chat?',VR_BOOKING_TEXTDOMAIN), // Title
            array( $this, 'chat_active_callback' ), // Callback
            'vr-booking-settings', // Page
            'setting_section_10' // Section
        );

        add_settings_field(
            'chat_id', // ID
            __('Olark site id (from your Olark account):',VR_BOOKING_TEXTDOMAIN), // Title
            array( $this, 'chat_id_callback' ), // Callback
            'vr-booking-settings', // Page
            'setting_section_10' // Section
        );

        ///////// Google

        add_settings_section(
            'setting_section_4', // ID
            __('Google map',VR_BOOKING_TEXTDOMAIN), // Title
            array( $this, 'print_section_info4' ), // Callback
            'vr-booking-settings' // Page
        );

        add_settings_field(
            'google_map_active', // ID
            __('Show properties on google map?',VR_BOOKING_TEXTDOMAIN), // Title
            array( $this, 'google_map_active_callback' ), // Callback
            'vr-booking-settings', // Page
            'setting_section_4' // Section
        );

        add_settings_field(
            'google_api', // ID
            __('Google API key',VR_BOOKING_TEXTDOMAIN), // Title
            array( $this, 'google_api_callback' ), // Callback
            'vr-booking-settings', // Page
            'setting_section_4' // Section
        );

        add_settings_field(
            'map_zoom', // ID
            __('Map zoom',VR_BOOKING_TEXTDOMAIN), // Title
            array( $this, 'map_zoom_callback' ), // Callback
            'vr-booking-settings', // Page
            'setting_section_4' // Section
        );

        add_settings_field(
            'map_marker', // ID
            __('Select map marker',VR_BOOKING_TEXTDOMAIN), // Title
            array( $this, 'map_marker_callback' ), // Callback
            'vr-booking-settings', // Page
            'setting_section_4' // Section
        );

        /////// Reviews

        add_settings_section(
            'setting_section_11', // ID
            __('Reviews',VR_BOOKING_TEXTDOMAIN), // Title
            array( $this, 'print_section_info4' ), // Callback
            'vr-booking-settings' // Page
        );

        add_settings_field(
            'reviews_active', // ID
            __('Activate Rich Reviews?',VR_BOOKING_TEXTDOMAIN), // Title
            array( $this, 'reviews_active_callback' ), // Callback
            'vr-booking-settings', // Page
            'setting_section_11' // Section
        );

        add_settings_field(
            'reviews_mode', // ID
            __('How to show "add review" form on Property page:',VR_BOOKING_TEXTDOMAIN), // Title
            array( $this, 'reviews_mode_callback' ), // Callback
            'vr-booking-settings', // Page
            'setting_section_11' // Section
        );

        ////////////////////// Invoices

        add_settings_section(
            'setting_section_6', // ID
            __('Invoices',VR_BOOKING_TEXTDOMAIN), // Title
            array( $this, 'print_section_info2' ), // Callback
            'vr-booking-settings' // Page
        );

        add_settings_field(
            'invoice_logo', // ID
            __('Logo image',VR_BOOKING_TEXTDOMAIN), // Title
            array( $this, 'invoice_logo_callback' ), // Callback
            'vr-booking-settings', // Page
            'setting_section_6' // Section
        );

        add_settings_field(
            'invoice_title', // ID
            __('Invoice title text (below logo)',VR_BOOKING_TEXTDOMAIN), // Title
            array( $this, 'invoice_title_callback' ), // Callback
            'vr-booking-settings', // Page
            'setting_section_6' // Section
        );

        add_settings_field(
            'invoice_header', // ID
            __('Invoice header text (your company details)',VR_BOOKING_TEXTDOMAIN), // Title
            array( $this, 'invoice_header_callback' ), // Callback
            'vr-booking-settings', // Page
            'setting_section_6' // Section
        );

        add_settings_field(
            'invoice_footer', // ID
            __('Invoice footer text',VR_BOOKING_TEXTDOMAIN), // Title
            array( $this, 'invoice_footer_callback' ), // Callback
            'vr-booking-settings', // Page
            'setting_section_6' // Section
        );
        
        ////////// E-mails
        add_settings_section(
            'setting_section_13', // ID
            __('E-mails',VR_BOOKING_TEXTDOMAIN), // Title
            array( $this, 'print_section_info2' ), // Callback
            'vr-booking-settings' // Page
        );
        
        add_settings_field(
            'confirm_email', // ID
            __('Site email for booking confirmation',VR_BOOKING_TEXTDOMAIN), // Title
            array( $this, 'confirm_email_callback' ), // Callback
            'vr-booking-settings', // Page
            'setting_section_13' // Section
        );
        
        add_settings_field(
            'confirm_email_subject', // ID
            __('Confirmation email subject',VR_BOOKING_TEXTDOMAIN), // Title
            array( $this, 'confirm_email_subject_callback' ), // Callback
            'vr-booking-settings', // Page
            'setting_section_13' // Section
        );

        add_settings_field(
            'confirm_email_header', // ID
            __('Confirmation email body',VR_BOOKING_TEXTDOMAIN), // Title
            array( $this, 'confirm_email_header_callback' ), // Callback
            'vr-booking-settings', // Page
            'setting_section_13' // Section
        );

    }

    /**
     * Sanitize each setting field as needed
     *
     * @param array $input Contains all settings fields as array keys
     */
    function sanitize( $input )
    {
        $new_input = array();

        $new_input['chat_active'] = $input['chat_active'];
        $new_input['chat_id'] = $input['chat_id'];

        $new_input['reviews_active'] = $input['reviews_active'];
        $new_input['reviews_mode'] = $input['reviews_mode'];

        $new_input['paypal_sandbox'] = $input['paypal_sandbox'];
        $new_input['stripe_sandbox'] = $input['stripe_sandbox'];
        $new_input['paypal_activate'] = $input['paypal_activate'];
        $new_input['stripe_activate'] = $input['stripe_activate'];

        if (isset($input['payment_mode1']))
        $new_input['payment_mode1'] = $input['payment_mode1'];
        else
        $new_input['payment_mode1'] = '';

        if (isset($input['payment_mode2']))
        $new_input['payment_mode2'] = $input['payment_mode2'];
        else
        $new_input['payment_mode2'] = '';

        if (isset($input['payment_mode3']))
        $new_input['payment_mode3'] = $input['payment_mode3'];
        else
        $new_input['payment_mode3'] = '';

        if ((!$new_input['payment_mode1'])and(!$new_input['payment_mode2'])and(!$new_input['payment_mode3']))
        $new_input['payment_mode1'] = 1;

        if ((!$new_input['paypal_activate'])and(!$new_input['stripe_activate'])){
           $new_input['payment_mode2'] = '';
           $new_input['payment_mode3'] = '';
           $new_input['payment_mode1'] = 1;
        }

        $new_input['add_accept_term'] = $input['add_accept_term'];
        $new_input['add_accept_term_adds'] = $input['add_accept_term_adds'];
        $new_input['print_button'] = $input['print_button'];

        $new_input['confirm_email_subject'] = $input['confirm_email_subject'];
        $new_input['confirm_email_header'] = $input['confirm_email_header'];
        $new_input['confirm_email'] = $input['confirm_email'];

        $new_input['tax'] = floatval($input['tax']);
        $new_input['currency'] = $input['currency'];
        $new_input['currency_code'] = $input['currency_code'];

        $new_input['currency_place'] = intval($input['currency_place']);
        $new_input['invoice_header'] = $input['invoice_header'];
        $new_input['invoice_footer'] = $input['invoice_footer'];
        $new_input['invoice_title'] = $input['invoice_title'];
        $new_input['invoice_logo'] = $input['invoice_logo'];

        $new_input['google_api'] = $input['google_api'];
        $new_input['map_zoom'] = intval($input['map_zoom']);
        $new_input['google_map_active'] = $input['google_map_active'];
        $new_input['map_marker'] = intval($input['map_marker']);

       // $new_input['deposit_active'] = $input['deposit_active'];
        $new_input['deposit_amount'] = $input['deposit_amount'];

        $new_input['max_select_adults'] = intval($input['max_select_adults']);
        $new_input['max_select_children'] = intval($input['max_select_children']);
        $new_input['max_select_rooms'] = intval($input['max_select_rooms']);
        $new_input['search_res_front'] = intval($input['search_res_front']);
        $new_input['search_res_other'] = intval($input['search_res_other']);
        $new_input['color_button'] = $input['color_button'];

        $new_input['paypal_email'] = sanitize_text_field($input['paypal_email']);
        $new_input['stripe_test_secret_key'] = sanitize_text_field($input['stripe_test_secret_key']);
        $new_input['stripe_test_publishable_key'] = sanitize_text_field($input['stripe_test_publishable_key']);
        $new_input['stripe_live_secret_key'] = sanitize_text_field($input['stripe_live_secret_key']);
        $new_input['stripe_live_publishable_key'] = sanitize_text_field($input['stripe_live_publishable_key']);

        return $new_input;
    }

    /**
     * Print the Section text
     */
    function print_section_info()
    {
        // echo __('Enter your settings below:', VR_BOOKING_TEXTDOMAIN);
    }

    function print_section_info2()
    {
        // echo __('Enter your settings below:', VR_BOOKING_TEXTDOMAIN);
    }

    function print_section_info3()
    {
        // echo __('Enter your settings below:', VR_BOOKING_TEXTDOMAIN);
    }

    function print_section_info4()
    {
        // echo __('Enter your settings below:', VR_BOOKING_TEXTDOMAIN);
    }

    function print_section_info5()
    {
        // echo __('Enter your settings below:', VR_BOOKING_TEXTDOMAIN);
    }

    function print_section_info8()
    {
       echo __('Select Payment Modes:', VR_BOOKING_TEXTDOMAIN);
    }

    function max_select_adults_callback(){
        printf(
            '<input type="text" id="max_select_adults" name="booking_settings[max_select_adults]" value="%s" />',
            isset( $this->options['max_select_adults'] ) ? esc_attr( $this->options['max_select_adults']) : '7'
        );
    }

    function max_select_children_callback(){
        printf(
            '<input type="text" id="max_select_children" name="booking_settings[max_select_children]" value="%s" />',
            isset( $this->options['max_select_children'] ) ? esc_attr( $this->options['max_select_children']) : '7'
        );
    }

    function max_select_rooms_callback(){
        printf(
            '<input type="text" id="max_select_rooms" name="booking_settings[max_select_rooms]" value="%s" />',
            isset( $this->options['max_select_rooms'] ) ? esc_attr( $this->options['max_select_rooms']) : '7'
        );
    }

    function search_res_front_callback(){
        printf(
            '<input type="text" id="search_res_front" name="booking_settings[search_res_front]" value="%s" />',
            isset( $this->options['search_res_front'] ) ? esc_attr( $this->options['search_res_front']) : '6'
        );
    }

    function search_res_other_callback(){
        printf(
            '<input type="text" id="search_res_other" name="booking_settings[search_res_other]" value="%s" />',
            isset( $this->options['search_res_other'] ) ? esc_attr( $this->options['search_res_other']) : '10'
        );
    }

    function color_button_callback(){
        printf(
            '<input type="text" id="color_button" class="color-field" name="booking_settings[color_button]" value="%s" />',
            isset( $this->options['color_button'] ) ? esc_attr( $this->options['color_button']) : '#81d742'
        );
    }

    function google_api_callback(){
        printf(
            '<input type="text" id="google_api" name="booking_settings[google_api]" value="%s" />',
            isset( $this->options['google_api'] ) ? esc_attr( $this->options['google_api']) : ''
        );
    }

    function map_zoom_callback(){
        printf(
            '<input type="text" id="map_zoom" name="booking_settings[map_zoom]" value="%s" />',
            isset( $this->options['map_zoom'] ) ? esc_attr( $this->options['map_zoom']) : '13'
        );
    }

    function tax_callback(){
        printf(
            '<input type="text" id="tax" name="booking_settings[tax]" value="%s" />',
            isset( $this->options['tax'] ) ? esc_attr( $this->options['tax']) : ''
        );
    }

    function currency_callback(){
        printf(
            '<input type="text" id="currency" name="booking_settings[currency]" value="%s" />',
            isset( $this->options['currency'] ) ? esc_attr( $this->options['currency']) : '$'
        );
    }

    function currency_code_callback(){
        printf(
            '<input type="text" id="currency" name="booking_settings[currency_code]" value="%s" />',
            isset( $this->options['currency_code'] ) ? esc_attr( $this->options['currency_code']) : 'USD'
        );
    }

    function deposit_amount_callback(){
        printf(
            '<input type="text" id="deposit_amount" name="booking_settings[deposit_amount]" value="%s" />',
            isset( $this->options['deposit_amount'] ) ? esc_attr( $this->options['deposit_amount']) : ''
        );
    }

    ////////////////// reviews_active
    function reviews_active_callback(){

        $check = isset($this->options['reviews_active']) ?  $this->options['reviews_active'] : 0;
        $checked1 = '';
        $checked2 = '';
        if ($check==1) $checked1 = 'checked';
           else $checked2 = 'checked';

        echo '<p><input id="booking_settings[reviews_active]1" name="booking_settings[reviews_active]" type="radio" value="1" '.$checked1.'/><label id="booking_settings_reviews_active1" for="booking_settings[reviews_active]1">'.__('Yes', VR_BOOKING_TEXTDOMAIN).'</label></p>';
       echo '<p><input id="booking_settings[reviews_active]2" name="booking_settings[reviews_active]" type="radio" value="0" '.$checked2.'/><label id="booking_settings_reviews_active2" for="booking_settings[reviews_active]2">'.__('No', VR_BOOKING_TEXTDOMAIN).'</label></p>';
    }

    function reviews_mode_callback(){

        $check = isset($this->options['reviews_mode']) ?  $this->options['reviews_mode'] : 2;
        $checked1 = '';
        $checked2 = '';
        if ($check==1) $checked1 = 'checked';
           else $checked2 = 'checked';

        echo '<p><input id="booking_settings[reviews_mode]1" name="booking_settings[reviews_mode]" type="radio" value="1" '.$checked1.'/><label id="booking_settings_reviews_mode1" for="booking_settings[reviews_mode]1">'.__('Show in all properties sections', VR_BOOKING_TEXTDOMAIN).'</label></p>';
       echo '<p><input id="booking_settings[reviews_mode]2" name="booking_settings[reviews_mode]" type="radio" value="2" '.$checked2.'/><label id="booking_settings_reviews_mode2" for="booking_settings[reviews_mode]2">'.__('Show in REVIEW section only', VR_BOOKING_TEXTDOMAIN).'</label></p>';
    }


    //////////// chat_id
    function chat_id_callback(){
        printf(
            '<input type="text" id="chat_id" name="booking_settings[chat_id]" value="%s" />',
            isset( $this->options['chat_id'] ) ? esc_attr( $this->options['chat_id']) : ''
        );
    }
    function chat_active_callback(){

        $check = isset($this->options['chat_active']) ?  $this->options['chat_active'] : 0;
        $checked1 = '';
        $checked2 = '';
        if ($check==1) $checked1 = 'checked';
           else $checked2 = 'checked';

        echo '<p><input id="booking_settings[chat_active]1" name="booking_settings[chat_active]" type="radio" value="1" '.$checked1.'/><label id="booking_settings_chat_active1" for="booking_settings[chat_active]1">'.__('Yes', VR_BOOKING_TEXTDOMAIN).'</label></p>';
       echo '<p><input id="booking_settings[chat_active]2" name="booking_settings[chat_active]" type="radio" value="0" '.$checked2.'/><label id="booking_settings_chat_active2" for="booking_settings[chat_active]2">'.__('No', VR_BOOKING_TEXTDOMAIN).'</label></p>';
    }

    ////////////// map_marker

    function map_marker_callback(){

        $check = isset($this->options['map_marker']) ?  $this->options['map_marker'] : 1;

        foreach ($this->markers_urls as $key => $url){
           if ($key == $check) $checked = ' checked';
           else $checked = '';

           echo '<input id="booking_settings[map_marker]'.$key.'" name="booking_settings[map_marker]" type="radio" value="'.$key.'"'.$checked.'/><label id="booking_settings_map_marker'.$key.'" for="booking_settings[map_marker]'.$key.'"><img src="'.plugins_url( $url, VR_BOOKING_PLUGIN ).'"></label>';
        }
    }

    function google_map_active_callback(){

        $check = isset($this->options['google_map_active']) ?  $this->options['google_map_active'] : 0;

        $checked1 = '';
        $checked2 = '';
        if ($check==1) $checked1 = 'checked';
           else $checked2 = 'checked';

        echo '<p><input id="booking_settings[google_map_active]1" name="booking_settings[google_map_active]" type="radio" value="1" '.$checked1.'/><label id="booking_settings_google_map_active1" for="booking_settings[google_map_active]1">'.__('Yes', VR_BOOKING_TEXTDOMAIN).'</label></p>';
       echo '<p><input id="booking_settings[google_map_active]2" name="booking_settings[google_map_active]" type="radio" value="0" '.$checked2.'/><label id="booking_settings_google_map_active2" for="booking_settings[google_map_active]2">'.__('No', VR_BOOKING_TEXTDOMAIN).'</label></p>';

    }

    //////////////////////payment_mode

    function payment_mode1_callback(){
        $check = isset($this->options['payment_mode1']) ?  $this->options['payment_mode1'] : 0;

        if ($check==0) $checked1 = '';
        else $checked1 = ' checked';

        echo '<p><input id="booking_settings[payment_mode1]" name="booking_settings[payment_mode1]" type="checkbox" value="1"'.$checked1.'/><label id="booking_settings_payment_mode1" for="booking_settings[payment_mode1]">'.__('Activate', VR_BOOKING_TEXTDOMAIN).'</label></p>';
    }

    function payment_mode2_callback(){
        $check = isset($this->options['payment_mode2']) ?  $this->options['payment_mode2'] : 0;

        if ($check==0) $checked1 = '';
        else $checked1 = ' checked';

        echo '<p><input id="booking_settings[payment_mode2]" name="booking_settings[payment_mode2]" type="checkbox" value="1"'.$checked1.'/><label id="booking_settings_payment_mode2" for="booking_settings[payment_mode2]">'.__('Activate', VR_BOOKING_TEXTDOMAIN).'</label></p>';
    }

    function payment_mode3_callback(){
        $check = isset($this->options['payment_mode3']) ?  $this->options['payment_mode3'] : 0;

        if ($check==0) $checked1 = '';
        else $checked1 = ' checked';

        echo '<p><input id="booking_settings[payment_mode3]" name="booking_settings[payment_mode3]" type="checkbox" value="1"'.$checked1.'/><label id="booking_settings_payment_mode3" for="booking_settings[payment_mode3]">'.__('Activate', VR_BOOKING_TEXTDOMAIN).'</label></p>';
    }
    //////////////////////////////

    function paypal_activate_callback(){
    $check = isset($this->options['paypal_activate']) ?  $this->options['paypal_activate'] : 0;
        $checked1 = '';
        $checked2 = '';
        if ($check==1) $checked1 = 'checked';
           else $checked2 = 'checked';

        echo '<p><input id="booking_settings[paypal_activate]1" name="booking_settings[paypal_activate]" type="radio" value="1" '.$checked1.'/><label id="booking_settings_paypal_activate1" for="booking_settings[paypal_activate]1">'.__('Yes', VR_BOOKING_TEXTDOMAIN).'</label></p>';
       echo '<p><input id="booking_settings[paypal_activate]2" name="booking_settings[paypal_activate]" type="radio" value="0" '.$checked2.'/><label id="booking_settings_paypal_activate2" for="booking_settings[paypal_activate]2">'.__('No', VR_BOOKING_TEXTDOMAIN).'</label></p>';
    }

    function stripe_activate_callback(){
    $check = isset($this->options['stripe_activate']) ?  $this->options['stripe_activate'] : 0;
        $checked1 = '';
        $checked2 = '';
        if ($check==1) $checked1 = 'checked';
           else $checked2 = 'checked';

        echo '<p><input id="booking_settings[stripe_activate]1" name="booking_settings[stripe_activate]" type="radio" value="1" '.$checked1.'/><label id="booking_settings_stripe_activate1" for="booking_settings[stripe_activate]1">'.__('Yes', VR_BOOKING_TEXTDOMAIN).'</label></p>';
       echo '<p><input id="booking_settings[stripe_activate]2" name="booking_settings[stripe_activate]" type="radio" value="0" '.$checked2.'/><label id="booking_settings_stripe_activate2" for="booking_settings[stripe_activate]2">'.__('No', VR_BOOKING_TEXTDOMAIN).'</label></p>';
    }

    //////////////////////////////

    function deposit_active_callback(){

        $check = isset($this->options['deposit_active']) ?  $this->options['deposit_active'] : 0;

        $checked1 = '';
        $checked2 = '';
        if ($check==1) $checked2 = 'checked';
           else $checked1 = 'checked';

        echo '<p><input id="booking_settings[deposit_active]1" name="booking_settings[deposit_active]" type="radio" value="0" '.$checked1.'/><label id="booking_settings_deposit_active1" for="booking_settings[deposit_active]1">'.__('No', VR_BOOKING_TEXTDOMAIN).'</label></p>';
       echo '<p><input id="booking_settings[deposit_active]2" name="booking_settings[deposit_active]" type="radio" value="1" '.$checked2.'/><label id="booking_settings_deposit_active2" for="booking_settings[deposit_active]2">'.__('Yes', VR_BOOKING_TEXTDOMAIN).'</label></p>';

    }

    function currency_place_callback(){

        $check = isset($this->options['currency_place']) ?  $this->options['currency_place'] : 1;

        $checked1 = '';
        $checked2 = '';
        if ($check==1) $checked1 = 'checked';
           else $checked2 = 'checked';

        echo '<p><input id="booking_settings[currency_place]1" name="booking_settings[currency_place]" type="radio" value="1" '.$checked1.'/><label id="booking_settings_currency_place1" for="booking_settings[currency_place]1">'.__('Before amount', VR_BOOKING_TEXTDOMAIN).'</label></p>';
       echo '<p><input id="booking_settings[currency_place]2" name="booking_settings[currency_place]" type="radio" value="2" '.$checked2.'/><label id="booking_settings_currency_place2" for="booking_settings[currency_place]2">'.__('After amount', VR_BOOKING_TEXTDOMAIN).'</label></p>';

    }

    ///////////////////

    function add_accept_term_callback(){
    $check = isset($this->options['add_accept_term']) ?  $this->options['add_accept_term'] : 0;
        $checked1 = '';
        $checked2 = '';
        if ($check==1) $checked1 = 'checked';
           else $checked2 = 'checked';

        echo '<p><input id="booking_settings[add_accept_term]1" name="booking_settings[add_accept_term]" type="radio" value="1" '.$checked1.'/><label id="booking_settings_add_accept_term1" for="booking_settings[add_accept_term]1">'.__('Yes', VR_BOOKING_TEXTDOMAIN).'</label></p>';
       echo '<p><input id="booking_settings[add_accept_term]2" name="booking_settings[add_accept_term]" type="radio" value="0" '.$checked2.'/><label id="booking_settings_add_accept_term2" for="booking_settings[add_accept_term]2">'.__('No', VR_BOOKING_TEXTDOMAIN).'</label></p>';
    }

    function add_accept_term_adds_callback(){
    $check = isset($this->options['add_accept_term_adds']) ?  $this->options['add_accept_term_adds'] : 0;
        $checked1 = '';
        $checked2 = '';
        if ($check==1) $checked1 = 'checked';
           else $checked2 = 'checked';

        echo '<p><input id="booking_settings[add_accept_term_adds]1" name="booking_settings[add_accept_term_adds]" type="radio" value="1" '.$checked1.'/><label id="booking_settings_add_accept_term_adds1" for="booking_settings[add_accept_term_adds]1">'.__('Yes', VR_BOOKING_TEXTDOMAIN).'</label></p>';
       echo '<p><input id="booking_settings[add_accept_term_adds]2" name="booking_settings[add_accept_term_adds]" type="radio" value="0" '.$checked2.'/><label id="booking_settings_add_accept_term_adds2" for="booking_settings[add_accept_term_adds]2">'.__('No', VR_BOOKING_TEXTDOMAIN).'</label></p>';
    }

    function print_button_callback(){
    $check = isset($this->options['print_button']) ?  $this->options['print_button'] : 0;
        $checked1 = '';
        $checked2 = '';
        if ($check==1) $checked1 = 'checked';
           else $checked2 = 'checked';

        echo '<p><input id="booking_settings[print_button]1" name="booking_settings[print_button]" type="radio" value="1" '.$checked1.'/><label id="booking_settings_print_button1" for="booking_settings[print_button]1">'.__('Yes', VR_BOOKING_TEXTDOMAIN).'</label></p>';
       echo '<p><input id="booking_settings[print_button]2" name="booking_settings[print_button]" type="radio" value="0" '.$checked2.'/><label id="other_settings" for="booking_settings[print_button]2">'.__('No', VR_BOOKING_TEXTDOMAIN).'</label></p>';
    }
    
    function confirm_email_callback(){
        printf(
            '<input type="text" id="confirm_email" name="booking_settings[confirm_email]" value="%s" />',
            isset( $this->options['confirm_email'] ) ? esc_attr( $this->options['confirm_email']) : get_bloginfo('admin_email')
        );
    }

    function confirm_email_subject_callback(){
        printf(
            '<input type="text" id="confirm_email_subject" name="booking_settings[confirm_email_subject]" value="%s" />',
            isset( $this->options['confirm_email_subject'] ) ? esc_attr( $this->options['confirm_email_subject']) : ''
        );
    }

    function confirm_email_header_callback(){
        printf(
            '<textarea id="confirm_email_header" name="booking_settings[confirm_email_header]" rows=5>%s</textarea>',
            isset( $this->options['confirm_email_header'] ) ? esc_attr( $this->options['confirm_email_header']) : ''
        );
    }

    function invoice_header_callback(){
        printf(
            '<textarea id="currency" name="booking_settings[invoice_header]" rows=5>%s</textarea>',
            isset( $this->options['invoice_header'] ) ? esc_attr( $this->options['invoice_header']) : ''
        );
    }

    function invoice_footer_callback(){
        printf(
            '<textarea id="currency" name="booking_settings[invoice_footer]" rows=5>%s</textarea>',
            isset( $this->options['invoice_footer'] ) ? esc_attr( $this->options['invoice_footer']) : ''
        );
    }

    function invoice_title_callback(){
        printf(
            '<input type="text" id="invoice_title" name="booking_settings[invoice_title]" value="%s" />',
            isset( $this->options['invoice_title'] ) ? esc_attr( $this->options['invoice_title']) : ''
        );
    }

    function invoice_logo_callback(){

        $logo_src = isset( $this->options['invoice_logo'] ) ? $this->options['invoice_logo'] : '';

        echo '<div id="invoice_logo_upload_block"><img id="invoice_logo_preview" src="'.$logo_src.'" width="100px"/>';
        echo '<input type="text" id="invoice_logo" name="booking_settings[invoice_logo]" value="'.$logo_src.'" /><input type="button" class="invoice_logo_upload" value="'.__('Upload', VR_BOOKING_TEXTDOMAIN).'"></div>';

        echo '<script>
    jQuery(document).ready(function($) {
        $(\'.invoice_logo_upload\').click(function(e) {
            e.preventDefault();

            var custom_uploader = wp.media({
                title: \'Custom Image\',
                button: {
                    text: \'Upload Image\'
                },
                multiple: false  // Set this to true to allow multiple files to be selected
            })
            .on(\'select\', function() {
                var attachment = custom_uploader.state().get(\'selection\').first().toJSON();
                $(\'#invoice_logo_preview\').attr(\'src\', attachment.url);
                $(\'#invoice_logo\').val(attachment.url);

            })
            .open();
        });
    });
</script>';
    }

    ////////////////////////
    function paypal_email_callback(){
        printf(
            '<input type="text" id="paypal_email" name="booking_settings[paypal_email]" value="%s" />',
            isset( $this->options['paypal_email'] ) ? esc_attr( $this->options['paypal_email']) : ''
        );
    }

    function stripe_test_secret_key_callback(){
        printf(
            '<input type="text" id="stripe_test_secret_key" name="booking_settings[stripe_test_secret_key]" value="%s" />',
            isset( $this->options['stripe_test_secret_key'] ) ? esc_attr( $this->options['stripe_test_secret_key']) : ''
        );
    }

    function stripe_test_publishable_key_callback(){
        printf(
            '<input type="text" id="stripe_test_publishable_key" name="booking_settings[stripe_test_publishable_key]" value="%s" />',
            isset( $this->options['stripe_test_publishable_key'] ) ? esc_attr( $this->options['stripe_test_publishable_key']) : ''
        );
    }

    function stripe_live_secret_key_callback(){
        printf(
            '<input type="text" id="stripe_live_secret_key" name="booking_settings[stripe_live_secret_key]" value="%s" />',
            isset( $this->options['stripe_live_secret_key'] ) ? esc_attr( $this->options['stripe_live_secret_key']) : ''
        );
    }

    function stripe_live_publishable_key_callback(){
        printf(
            '<input type="text" id="stripe_live_publishable_key" name="booking_settings[stripe_live_publishable_key]" value="%s" />',
            isset( $this->options['stripe_live_publishable_key'] ) ? esc_attr( $this->options['stripe_live_publishable_key']) : ''
        );
    }

    function paypal_sandbox_callback(){

        $check = isset($this->options['paypal_sandbox']) ?  $this->options['paypal_sandbox'] : 1;

        $checked1 = '';
        $checked2 = '';
        if ($check==1) $checked1 = 'checked';
           else $checked2 = 'checked';

        echo '<p><input id="booking_settings[paypal_sandbox]1" name="booking_settings[paypal_sandbox]" type="radio" value="1" '.$checked1.'/><label id="booking_settings_paypal_sandbox1" for="booking_settings[paypal_sandbox]1">'.__('Sandbox', VR_BOOKING_TEXTDOMAIN).'</label></p>';
       echo '<p><input id="booking_settings[paypal_sandbox]2" name="booking_settings[paypal_sandbox]" type="radio" value="0" '.$checked2.'/><label id="booking_settings_paypal_sandbox2" for="booking_settings[paypal_sandbox]2">'.__('Live', VR_BOOKING_TEXTDOMAIN).'</label></p>';

    }

   function stripe_sandbox_callback(){

        $check = isset($this->options['stripe_sandbox']) ?  $this->options['stripe_sandbox'] : 1;

        $checked1 = '';
        $checked2 = '';
        if ($check==1) $checked1 = 'checked';
           else $checked2 = 'checked';

        echo '<p><input id="booking_settings[stripe_sandbox]1" name="booking_settings[stripe_sandbox]" type="radio" value="1" '.$checked1.'/><label id="booking_settings_stripe_sandbox1" for="booking_settings[stripe_sandbox]1">'.__('Test', VR_BOOKING_TEXTDOMAIN).'</label></p>';
       echo '<p><input id="booking_settings[stripe_sandbox]2" name="booking_settings[stripe_sandbox]" type="radio" value="0" '.$checked2.'/><label id="booking_settings_stripe_sandbox2" for="booking_settings[stripe_sandbox]2">'.__('Live', VR_BOOKING_TEXTDOMAIN).'</label></p>';

    }

}

	$VRBooking_admin = new VRBooking_admin();


