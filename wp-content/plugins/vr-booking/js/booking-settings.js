//jQuery.datetimepicker.setLocale('en');


jQuery(function(){
jQuery( "#booking" ).validate({
  rules: {
    first_name: {
      required: true
    },
    last_name: {
      required: true
    },
    email: {
      required: true,
      email: true
    },
    tel1: {
      required: true
    },
    address: {
      required: true
    },
    number: {
      required: true
    },
    town: {
      required: true
    },
    state: {
      required: true
    },
    country: {
      required: true
    },
    passport: {
      required: true
    },
    dob: {
      required: true
    },
    accept_term: {
      required: true
    },
    accept_term_adds: {
      required: true
    },
    accept_cancel: {
      required: true
    }
  }
});

jQuery( "#get_quote" ).validate({
  rules: {
    location: {
      required: true
    },
    date_from: {
      required: true
    },
    date_to: {
      required: true
    },
    adults: {
      required: true
    },
    children: {
      required: true
    }
  }
});
});

jQuery('#date_from').datetimepicker({
  format:'d/m/Y',
  timepicker:false,
  scrollInput:false,
  minDate: 0,
  lang:'en',
  onSelectDate:function(ct,$i){
    var d = jQuery('#date_from').datetimepicker('getValue');
    d.setTime(d.getTime() + 86400000);
    var next_day_year = d.getFullYear();
    var next_day_month = d.getMonth()+1;
    next_day_month = (parseInt(next_day_month)<10)?"0"+next_day_month:next_day_month;
    var next_day_day = d.getDate();
    next_day_day = (parseInt(next_day_day)<10)?"0"+next_day_day:next_day_day;
    //alert(d);
    //$('#booking_date_in').val(d.getDate() + "/" + d.getMonth() + "/" + d.getFullYear());
    jQuery('#date_to').datetimepicker('setOptions', {minDate: d});
    jQuery('#date_to').val(next_day_day + "/" + next_day_month + "/" + next_day_year);
  },
}).on('focus',function()
    {
        jQuery(this).blur();
    });

jQuery('#date_to').datetimepicker({
  format:'d/m/Y',
  timepicker:false,
  scrollInput:false,
  minDate: '+1970/01/02',
  lang:'en',
 // onShow:function( ct ){
 //  this.setOptions({
 //   minDate:jQuery('#booking_date_out').val()?jQuery('#booking_date_out').val():false
  // })
 // },
}).on('focus',function()
    {
        jQuery(this).blur();
    });

jQuery(function($){
            $('#print').click(function(){
    		window.print();
         });
});

jQuery(function($){
            $('#modify').click(function(){
    		$('#search-form').toggleClass('active');
         });
});

function success_contact_popup(){
   jQuery('#overlay').fadeIn(400, function(){
				jQuery('#success_contact')
					.css('display', 'block')
					.animate({opacity: 1}, 200);
		    });
}

jQuery(function($){
            $('#cookies_popup').click(function(event){
    		event.preventDefault();
		    $('#overlay').fadeIn(400, function(){
				$('#cookies_text')
					.css('display', 'block')
					.animate({opacity: 1}, 200);
		    });
      });
           $('li.quick a').click(function(event){
            var isMobile = window.matchMedia("only screen and (max-width: 760px)");
            if (!isMobile.matches) {
             //Conditional script here
    		event.preventDefault();
		    $('#overlay').fadeIn(400, function(){
				$('#tel_text')
					.css('display', 'block')
					.animate({opacity: 1}, 200);
		     });
            }
      });
           $('#modal_close, #overlay').click( function(){
	     	$('#cookies_text').animate({opacity: 0}, 200,	function(){
					$(this).css('display', 'none');
					$('#overlay').fadeOut(400);
				}
			 );
             $('#tel_text').animate({opacity: 0}, 200,	function(){
					$(this).css('display', 'none');
					$('#overlay').fadeOut(400);
				}
			 );
             $('#success_contact').animate({opacity: 0}, 200,	function(){
					$(this).css('display', 'none');
					$('#overlay').fadeOut(400);
				}
			 );
            });
});

jQuery(function($){
      $('#tab_block .doc_tab').click(function () {
        $('#tab_block .doc_tab.cur_tab').removeClass('cur_tab');
        $(this).addClass('cur_tab');
        var div_tab = $(this).attr('data-x');
        $('#app-page .doc_tab_div.active').removeClass('active');
        $('#'+div_tab).addClass('active');
        if (div_tab == 'app-map') {
          if (typeof initMap == 'function') initMap();
        }
});
});
//////////////////////////////////////

function sort_ajax_fn(){
      var sort_by = jQuery('#sort_by').val();
      var page_num = jQuery('#pager .cur_page').attr('data-x');

	  jQuery.ajax({
		url : lst.ajax_url,
		type : 'POST',
		data : {
		    action : 'sort_by',
			sort_by : sort_by,
            page_num : page_num,
            // check
	        nonce : lst.nonce
		},
		success : function( msg ) {
           jQuery('#home_results').html(msg);
		}
	});
}


jQuery(function($){
      $('#sort_by.start').on('change' , function () {
      var sort_by = $('#sort_by').val();
      var page_num = $('#pager .cur_page').attr('data-x');
      var start = window.location.pathname;
      document.location.href = start + '?page_num='+page_num+'&sort_by='+sort_by;
   });
});

jQuery(function($){
      $('.search-result #sort_by').on('change' , function () {
      var sort_by = $('#sort_by').val();
      var page_num = $('#pager .cur_page').attr('data-x');
      var page_num_add = '';
      if (page_num != undefined) page_num_add = '/page_num/'+page_num;
      var form = $('<form id="new_form_search" action="/search-result'+page_num_add+'/sort_by/'+sort_by + '" method="post"><input type="hidden" name="date_from" value="'+$('#date_from').val()+'"><input type="hidden" name="date_to" value="'+$('#date_to').val()+'"><input type="hidden" name="location" value="'+$('#location').val()+'"><input type="hidden" name="adults" value="'+$('#adults').val()+'"><input type="hidden" name="children" value="'+$('#children').val()+'"><input type="hidden" name="bedrooms" value="'+$('#bedrooms').val()+'"></form>');
      $('body').append(form);
      form.submit();
});
});

jQuery(function($){
      $('.search-result .pager_num').on('click' , function () {
      var sort_by = $('#sort_by').val();
      var sort_by_add = '';
      if (sort_by != undefined) sort_by_add = '/sort_by/'+sort_by;
      var page_num = $(this).attr('data-x');
      var page_num_add = '/page_num/'+page_num;
      var form = $('<form id="new_form_search" action="/search-result' +page_num_add + sort_by_add + '" method="post"><input type="hidden" name="date_from" value="'+$('#date_from').val()+'"><input type="hidden" name="date_to" value="'+$('#date_to').val()+'"><input type="hidden" name="location" value="'+$('#location').val()+'"><input type="hidden" name="adults" value="'+$('#adults').val()+'"><input type="hidden" name="children" value="'+$('#children').val()+'"><input type="hidden" name="bedrooms" value="'+$('#bedrooms').val()+'"></form>');
      $('body').append(form);
      form.submit();
});
});


jQuery(function($){
      $('#get_quote_ap').on('click', '#continue', function () {
      //  event.preventDefault();
      $('#search-ap-res').css("display", "none");
      $('#search-ap-spin').html('<span class="spin_f"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i></span>');
      var date_from = $('#date_from').val(),
          date_to = $('#date_to').val(),
          id = $('#get_quote_ap input[name="id"]').val();
        if ((date_from != '')&&(date_to != '')){
        $.ajax({
		url : lst.ajax_url,
		type : 'POST',
		data : {
			action : 'check_ap',
            date_from : date_from,
            date_to : date_to,
            id : id,
            // check
	        nonce : lst.nonce
		},
		success : function( msg ) {
           if(msg == 1){
              $("#get_quote_ap").submit();
		    }
            else{
              $('#search-ap-spin').html('');
              $('#search-ap-res').css("display", "block");
            }
		  }
        });
        } else{
              $('#search-ap-spin').html('Please, select dates.');
            }
      });
});

//////////////////////////////////////

jQuery(document).ready(function($) {
    $('#imageGallery').lightSlider({
        gallery:true,
        adaptiveHeight:true,
        item:1,
        loop:true,
        thumbItem:5,
        slideMargin:0,
        enableDrag: false,

        currentPagerPosition:'left',
        onSliderLoad: function(el) {
            el.lightGallery({
                selector: '#imageGallery .lslide'
            });
        }
    });
  });
