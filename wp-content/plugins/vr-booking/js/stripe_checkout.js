jQuery( function( $ ) {
    var am = $('#stripe-payment-data').attr( 'data-amount');
	/**
	 * Object to handle Stripe payment forms.
	 */
	var wc_stripe_form = {

		/**
		 * Initialize e handlers and UI state.
		 */
		init: function( form ) {
			this.form          = form;
			this.stripe_submit = false;

			$( this.form )
				// We need to bind directly to the click (and not checkout_place_order_stripe) to avoid popup blockers
				// especially on mobile devices (like on Chrome for iOS) from blocking StripeCheckout.open from opening a tab
				.on( 'click', '#book', this.onSubmit);
		},

		isStripeChosen: function() {
			return $( '#stripe' ).is( ':checked' );
		},

		isStripeModalNeeded: function( e ) {
			var token = wc_stripe_form.form.find( 'input.stripe_token' );

			// If this is a stripe submission (after modal) and token exists, allow submit.
			if ( wc_stripe_form.stripe_submit && token ) {
				return false;
			}

			// Don't affect submission if modal is not needed.
			if ( ! wc_stripe_form.isStripeChosen() ) {
				return false;
			}

            wc_stripe_form.form.validate();
            if (!wc_stripe_form.form.valid()){
				return false;
			}

			return true;
		},

		onSubmit: function( e ) {
			if ( wc_stripe_form.isStripeModalNeeded() ) {
				e.preventDefault();

				// Capture submittal and open stripecheckout
				var $form = wc_stripe_form.form,
					$data = $( '#stripe-payment-data' ),
					token = $form.find( 'input.stripe_token' );

				token.val( '' );

				var token_action = function( res ) {
					$form.find( 'input.stripe_token' ).remove();
					$form.append( '<input type="hidden" class="stripe_token" name="stripe_token" value="' + res.id + '"/>' );
					wc_stripe_form.stripe_submit = true;
					$form.submit();
				};

				StripeCheckout.open({
					key               : wc_stripe_params.key,
					amount            : parseInt(am),
					name              : $data.data( 'name' ),
					description       : $data.data( 'description' ),
					currency          : $data.data( 'currency' ),
					image             : $data.data( 'image' ),
					locale            : $data.data( 'locale' ),
					email             : $( '#email' ).val(),
					token             : token_action,
				});

                $(window).on('popstate', function() {
                  StripeCheckout.close();
                  });

				return false;
			}

			return true;
		}
	};

	wc_stripe_form.init( $( "form#booking" ) );

    $(function(){
      $('form#booking').on( 'click', 'input[name="amount"]', function () {
        var amount = 0;
        if ($( '#full_amount' ).is( ':checked' )){
           amount = $("#select_amount").attr('data-f');
        }
        if ($( '#deposit_amount' ).is( ':checked' )){
           amount = $("#select_amount").attr('data-d');
        }
        $('#stripe-payment-data').attr( 'data-amount', amount);
        am = amount;
      });
});

} );
