<?php
class HbDataBaseCreation {
    
	private $hbdb;
	private $utils;
	private $charset_collate;
       
	public function __construct( $hbdb, $utils ) {
        global $wpdb;
		$this->hbdb = $hbdb;
		$this->utils = $utils;
        $this->charset_collate = '';
		if ( ! empty( $wpdb->charset ) ) {
			$this->charset_collate = "DEFAULT CHARACTER SET {$wpdb->charset}";
		}
		if ( ! empty( $wpdb->collate ) ) {
			$this->charset_collate .= " COLLATE {$wpdb->collate}";
		}
	}
	    
	public function create_plugin_tables() {
		
        global $wpdb;
		
		$table_name = $this->hbdb->resa_table;
		$query = "CREATE TABLE IF NOT EXISTS $table_name (
			id bigint(20) NOT NULL AUTO_INCREMENT,
			check_in DATE NOT NULL,
			check_out DATE NOT NULL,
			accom_id bigint(20) NOT NULL,
			accom_num bigint(20) NOT NULL,	
			adults SMALLINT(10) NOT NULL,
			children SMALLINT(10) NOT NULL,
			price decimal(14,2) NOT NULL,
			deposit decimal(14,2) NOT NULL,
			paid decimal(14,2) NOT NULL,
            payment_gateway varchar(20) NOT NULL,
			currency varchar(3) NOT NULL,
			customer_id bigint(20) NOT NULL,
			status varchar(20) NOT NULL,
			options text NOT NULL,
			additional_info text NOT NULL,
			admin_comment text NOT NULL,
            lang varchar(20) NOT NULL,
			payment_token varchar(40) NOT NULL,
			payment_status varchar(40) NOT NULL,
			payment_status_reason varchar(40) NOT NULL,
            amount_to_pay decimal(14,2) NOT NULL,
			received_on DATETIME NOT NULL,
			updated_on DATETIME NOT NULL,
			uid varchar(256) NOT NULL,
			origin varchar(40) NOT NULL,
			synchro_id varchar(128) NOT NULL,
			booking_form_num tinyint(4) NOT NULL,
			PRIMARY KEY (id)
		) $this->charset_collate;";
		
		$wpdb->query( $query );
		
		$table_name = $this->hbdb->customers_table;
		$query = "CREATE TABLE IF NOT EXISTS $table_name (
			id bigint(20) NOT NULL AUTO_INCREMENT,
			email varchar(127) NOT NULL,
			info text NOT NULL,
			payment_id varchar(40) NOT NULL,
			PRIMARY KEY (id)
		) $this->charset_collate;";
		
		$wpdb->query( $query );
		
		$table_name = $this->hbdb->rates_table;
		$query = "CREATE TABLE IF NOT EXISTS $table_name (
			id bigint(20) NOT NULL AUTO_INCREMENT,
			type varchar(15) NOT NULL,
			all_accom tinyint(1) NOT NULL,
			all_seasons tinyint(1) NOT NULL,
			amount decimal(14,2) NOT NULL,
			nights smallint(6) NOT NULL,
			PRIMARY KEY (id)
		) $this->charset_collate;";
		
		$wpdb->query( $query );
		
		$table_name = $this->hbdb->rates_accom_table;
		$query = 
			"CREATE TABLE IF NOT EXISTS $table_name  (
			rate_id bigint(20) NOT NULL,
			accom_id bigint(20) NOT NULL
			) $this->charset_collate;";

		$wpdb->query( $query );
		
		$table_name = $this->hbdb->rates_seasons_table;
		$query = 
			"CREATE TABLE IF NOT EXISTS $table_name  (
			rate_id bigint(20) NOT NULL,
			season_id bigint(20) NOT NULL
			) $this->charset_collate;";

		$wpdb->query( $query );
		
		$table_name = $this->hbdb->rates_rules_table;
		$query = 
			"CREATE TABLE IF NOT EXISTS $table_name  (
			rate_id bigint(20) NOT NULL,
			rule_id bigint(20) NOT NULL
			) $this->charset_collate;";

		$wpdb->query( $query );
		
		$table_name = $this->hbdb->discounts_table;
		$query = "CREATE TABLE IF NOT EXISTS $table_name  (
			id bigint(20) NOT NULL AUTO_INCREMENT,
			amount decimal(14,2) NOT NULL,
			amount_type varchar(10) NOT NULL,
			all_accom tinyint(1) NOT NULL,
			all_seasons tinyint(1) NOT NULL,
			PRIMARY KEY (id)
		) $this->charset_collate;";
		
		$wpdb->query( $query );
		
		$table_name = $this->hbdb->discounts_accom_table;
		$query = "CREATE TABLE IF NOT EXISTS $table_name  (
			discount_id bigint(20) NOT NULL,
			accom_id bigint(20) NOT NULL
		) $this->charset_collate;";
		
		$wpdb->query( $query );
		
		$table_name = $this->hbdb->discounts_rules_table;
		$query = 
			"CREATE TABLE IF NOT EXISTS $table_name  (
			discount_id bigint(20) NOT NULL,
			rule_id bigint(20) NOT NULL
			) $this->charset_collate;";

		$wpdb->query( $query );
		
		$table_name = $this->hbdb->seasons_table;
		$query = "CREATE TABLE IF NOT EXISTS $table_name  (
			id bigint(20) NOT NULL AUTO_INCREMENT,
			name varchar(200) NOT NULL,
			PRIMARY KEY (id)
		) $this->charset_collate;";

		$wpdb->query( $query );
		
		$table_name = $this->hbdb->seasons_dates_table;
		$query = "CREATE TABLE IF NOT EXISTS $table_name  (
		  id bigint(20) NOT NULL AUTO_INCREMENT,
		  season_id bigint(20) NOT NULL,
		  start_date date NOT NULL,
		  end_date date NOT NULL,
		  days varchar(13) NOT NULL,
		  PRIMARY KEY (id)
		) $this->charset_collate;";
		
		$wpdb->query( $query );
		
		$table_name = $this->hbdb->fields_table;
		$query = "CREATE TABLE IF NOT EXISTS $table_name  (
			id varchar(50) NOT NULL,
			name varchar(200) NOT NULL,
			standard tinyint(1) NOT NULL,
			displayed tinyint(1) NOT NULL,
			required tinyint(1) NOT NULL,
			type varchar(20) NOT NULL,
			has_choices tinyint(1) NOT NULL,
			order_num bigint(20) NOT NULL,
			form_name varchar(15) NOT NULL,
			data_about varchar(15) NOT NULL,
			PRIMARY KEY (id)
		) $this->charset_collate;";
		
		$wpdb->query( $query );		
		
		$query = "SELECT COUNT(*) FROM $table_name";
		$nb_fields = $wpdb->get_var( $query );
		if ( $nb_fields == 0 ) {
			$query = "
				INSERT INTO $table_name
				(id, name, standard, displayed, required, type, order_num, form_name, data_about) 
				VALUES 
				('details_form_title', 'Form title', 1, 1, 0, 'title', 1, 'booking', 'customer'),
				('first_name', 'First name', 1, 1, 1, 'text', 2, 'booking', 'customer'),
				('last_name', 'Last name', 1, 1, 1, 'text', 3, 'booking', 'customer'),
				('email', 'Email', 1, 1, 1, 'email', 4, 'booking', 'customer'),
				('phone', 'Phone', 0, 1, 0, 'text', 5, 'booking', 'customer'),
				('address_1', 'Address line 1', 0, 1, 0, 'text', 6, 'booking', 'customer'),
				('address_2', 'Address line 2', 0, 1, 0, 'text', 7, 'booking', 'customer'),
				('city', 'City', 0, 1, 0, 'text', 8, 'booking', 'customer'),
				('state_province', 'State / province', 0, 1, 0, 'text', 9, 'booking', 'customer'),
				('zip_code', 'Zip code', 0, 1, 0, 'text', 10, 'booking', 'customer'),
				('country', 'Country', 0, 1, 0, 'text', 11, 'booking', 'customer'),
				
				('contact_name', 'Name', 0, 1, 1, 'text', 1, 'contact', ''),
				('contact_email', 'Email', 0, 1, 1, 'email', 2, 'contact', ''),
				('contact_message', 'Message', 0, 1, 1, 'textarea', 3, 'contact', '')
				";
			$wpdb->query( $query );
		}
		
		$table_name = $this->hbdb->fields_choices_table;
		$query = "CREATE TABLE IF NOT EXISTS $table_name  (
			id varchar(50) NOT NULL,
			field_id varchar(50) NOT NULL,
			name varchar(200) NOT NULL
		) $this->charset_collate;";
		
		$wpdb->query( $query );

		$table_name = $this->hbdb->strings_table;
		$query = 
			"
			CREATE TABLE IF NOT EXISTS $table_name (
			id varchar(60) NOT NULL,
			locale varchar(10) NOT NULL,
			value varchar(500) NOT NULL,
			PRIMARY KEY (id, locale)
			) $this->charset_collate;
			";
		
		$wpdb->query( $query );
		
		$query = "SELECT COUNT(*) FROM $table_name";
		$nb_strings = $wpdb->get_var( $query );
		if ( $nb_strings == 0 ) {
			$query = 
				"
				INSERT INTO $table_name
				(id, locale, value) 
				VALUES 
				('accom_available_at_chosen_dates', 'en_US', 'The %accom_name is available at the chosen dates.'),
				('accom_book_now_button', 'en_US', 'Book now!'),
				('accom_can_not_suit_nb_people', 'en_US', 'Unfortunately the %accom_name can not suit %persons_nb persons.'),
				('accom_deposit_amount', 'en_US', 'Deposit amount:'),
				('accom_not_available_at_chosen_dates', 'en_US', 'The %accom_name is not available at the chosen dates.'),
				('accom_no_longer_available', 'en_US', 'Unfortunately the accommodation you have selected is no longer available. Please select another accommodation or start searching again.'),
				('accom_page_form_title', 'en_US', 'Check price and availability for the %accom_name'),
				('accom_starting_price', 'en_US', 'price starting at %price' ),
				('accom_starting_price_duration_unit', 'en_US', 'per night' ),
				('adults', 'en_US', 'Adults'),
				('book_now_button', 'en_US', 'Book now!'),
				('change_search_button', 'en_US', 'Change search'),
				('check_in', 'en_US', 'Check-in'),
				('check_in_date_before_date', 'en_US', 'The check-in date can not be before the %date.'),
				('check_in_date_past', 'en_US', 'The check-in date can not be in the past.'),
				('check_out', 'en_US', 'Check-out'),
				('check_out_before_check_in', 'en_US', 'The check-out date must be after the check-in date.'),
				('check_out_date_after_date', 'en_US', 'The check-out date can not be after the %date.'),
				('children', 'en_US', 'Children'),
				('chosen_adults', 'en_US', 'Adults:'),
				('chosen_check_in', 'en_US', 'Check-in:'),
				('chosen_check_out', 'en_US', 'Check-out:'),
				('chosen_children', 'en_US', 'Children:'),
				('connection_error', 'en_US', 'There was a connection error. Please try again.'),
				('default_form_title', 'en_US', 'Search'),
				('details_form_title', 'en_US', 'Enter your details'),
				('hide_price_breakdown', 'en_US', 'Hide price breakdown'),
				('invalid_check_in_date', 'en_US', 'The check-in date is not valid.'),
				('invalid_check_in_out_date', 'en_US', 'The check-in date and the check-out date are not valid.'),
				('invalid_check_out_date', 'en_US', 'The check-out date is not valid.'),
				('invalid_email', 'en_US', 'Invalid email.'),
				('invalid_number', 'en_US', 'This field can only contain numbers.'),
				('legend_occupied', 'en_US', 'occupied'),
				('no_accom_at_chosen_dates', 'en_US', 'Unfortunately we could not find any accommodation for the dates you entered.'),
				('no_accom_can_suit_nb_people', 'en_US', 'Unfortunately we could not find any accommodation that would suit %persons_nb persons.'),
				('no_check_in_date', 'en_US', 'Please enter a check-in date.'),
				('no_check_in_out_date', 'en_US', 'Please enter a check-in date and a check-out date.'),
				('no_check_out_date', 'en_US', 'Please enter a check-out date.'),
				('number_of_nights', 'en_US', 'Number of nights:'),
				('one_type_of_accommodation_found', 'en_US', 'We have found 1 type of accommodation that suit your needs.'),
				('external_payment_txt_deposit', 'en_US', ' (deposit)'),
				('external_payment_txt_desc', 'en_US', '%accom_name%deposit_txt - %nights_txt (from %check_in_date to %check_out_date) - %adults_txt%children_txt'),
				('external_payment_txt_one_adult', 'en_US', '1 adult'),
				('external_payment_txt_one_child', 'en_US', ' - 1 child'),
				('external_payment_txt_one_night', 'en_US', '1 night'),
				('external_payment_txt_several_adults', 'en_US', '%nb_adults adults'),
				('external_payment_txt_several_children', 'en_US', ' - %nb_children children'),
				('external_payment_txt_several_nights', 'en_US', '%nb_nights nights'),
				('price_breakdown_accom_price', 'en_US', 'Accommodation price:'),
				('price_breakdown_adults_several', 'en_US', 'Adults (%nb_adults):'),
				('price_breakdown_adult_one', 'en_US', 'Adult:'),
				('price_breakdown_children_several', 'en_US', 'Children (%nb_children):'),
				('price_breakdown_child_one', 'en_US', 'Child:'),
				('price_breakdown_dates', 'en_US', 'From %from_date to %to_date:'),
				('price_breakdown_extra_adults_several', 'en_US', 'Extra adults (%nb_adults):'),
				('price_breakdown_extra_adult_one', 'en_US', 'Extra adult:'),
				('price_breakdown_extra_children_several', 'en_US', 'Extra children (%nb_children):'),
				('price_breakdown_extra_child_one', 'en_US', 'Extra child:'),
				('price_for_1_night', 'en_US', 'price for 1 night'),
				('price_for_several_nights', 'en_US', 'price for %nb_nights nights'),
				('required_field', 'en_US', 'Required field.'),
				('searching', 'en_US', 'Searching...'),
				('search_button', 'en_US', 'Search'),
				('selected_accom', 'en_US', 'You have selected the %accom_name.'),
				('select_accom_button', 'en_US', 'Select this accommodation'),
				('select_accom_title', 'en_US', 'Select your accommodation'),
				('several_types_of_accommodation_found', 'en_US', 'We have found %nb_types types of accommodation that suit your needs.'),
				('summary_accommodation', 'en_US', 'Accommodation:'),
				('summary_change', 'en_US', 'Change'),
				('summary_deposit', 'en_US', 'Deposit amount:'),
				('summary_price', 'en_US', 'Total price:'),
				('summary_title', 'en_US', 'Reservation summary'),
				('table_rates_from', 'en_US', 'From'),
				('table_rates_nights', 'en_US', 'Nights'),
				('table_rates_price', 'en_US', 'Price'),
				('table_rates_to', 'en_US', 'To'),
				('thanks_message_1', 'en_US', 'Thanks for your reservation! We have just sent you a confirmation email at %customer_email with the following details:'),
				('thanks_message_2', 'en_US', 'See you soon!'),
				('timeout_error', 'en_US', 'Your session has timed out. Please start a new booking request.'),
				('view_accom_at_chosen_date', 'en_US', 'View all available accommodation at the chosen dates.'),
				('view_accom_button', 'en_US', 'View this accommodation'),
				('view_accom_for_persons', 'en_US', 'View all available accommodation for %persons_nb persons.'),
				('view_price_breakdown', 'en_US', 'View price breakdown'),
				
				('check_in_day_not_allowed', 'en_US', 'Check-in is allowed only on specific days (%check_in_days).'),
				('check_out_day_not_allowed', 'en_US', 'Check-out is allowed only on specific days (%check_out_days).'),
				('minimum_stay', 'en_US', 'A %nb_nights-nights minimum stay policy applies. Please modify your dates to extend your stay.'),
				('maximum_stay', 'en_US', 'A %nb_nights-nights maximum stay policy applies. Please modify your dates to shorten your stay.'),
				('check_out_day_not_allowed_for_check_in_day', 'en_US', 'Guests check-in on %check_in_day must check-out on a specific day (%check_out_days).'),
				('minimum_stay_for_check_in_day', 'en_US', 'A %nb_nights-night minimum stay is required for booking starting on %check_in_day.'),
				('maximum_stay_for_check_in_day', 'en_US', 'A %nb_nights-night maximum stay applies for booking starting on %check_in_day.'),
				('table_rates_per_night', 'en_US', 'per night'),
				('table_rates_for_night_stay', 'en_US', 'for a %nb_nights-night stay'),
				
				('contact_send_button', 'en_US', 'Send'),
				('contact_message_sent', 'en_US', 'Thanks for your message! We will reply as soon as possible.'),
				('contact_message_not_sent', 'en_US', 'An error occured. Your message has not been sent.'),
				('contact_already_sent', 'en_US', 'Your message has already been sent.'),
				
				('price_breakdown_discount', 'en_US', 'Discount:'),
				('price_breakdown_before_discount', 'en_US', 'Price before discount:')
				";
			$wpdb->query( $query );
			
			$this->insert_hb_text_v_1_3();
            $this->insert_hb_text_v_1_4();
            $this->insert_hb_text_v_1_5();
            $this->insert_hb_text_v_1_5_1();
            $this->insert_hb_text_v_1_5_3();
            $this->insert_hb_text_v_1_6();
            $this->insert_hb_text_v_1_6_2();
            $this->insert_hb_text_v_1_6_3();
            $this->insert_hb_text_v_1_7();
		}

		$table_name = $this->hbdb->booking_rules_table;
		$query = 
			"
			CREATE TABLE IF NOT EXISTS $table_name (
			id bigint(20) NOT NULL AUTO_INCREMENT,
			type varchar(20) NOT NULL,
			name varchar(50) NOT NULL,
			check_in_days varchar(13) NOT NULL,
			check_out_days varchar(13) NOT NULL,
			minimum_stay smallint(6) NOT NULL,
			maximum_stay smallint(6) NOT NULL,
			all_accom tinyint(1) NOT NULL,
			all_seasons tinyint(1) NOT NULL,
			conditional_type varchar(20) NOT NULL,
			PRIMARY KEY (id)
			) $this->charset_collate;
			";
		  
		$wpdb->query( $query );
		
		$table_name = $this->hbdb->booking_rules_accom_table;
		$query = 
			"CREATE TABLE IF NOT EXISTS $table_name  (
			rule_id bigint(20) NOT NULL,
			accom_id bigint(20) NOT NULL
			) $this->charset_collate;";

		$wpdb->query( $query );
			
		$this->create_v_1_3_hb_tables();
        $this->create_v_1_5_hb_tables();
        $this->insert_email_templates();
        $this->create_v_1_5_4_hb_tables();
        $this->create_v_1_6_hb_tables();
        $this->create_v_1_6_2_hb_tables();
	}
    
    public function insert_email_templates() {
        global $wpdb;
		$table_name = $this->hbdb->email_templates_table;
        
        $email_templates = array(
            array(
                'name' => __( 'Admin notification', 'hbook-admin' ),
                'to_address' => '',
                'from_address' => '[customer_first_name] [customer_last_name] <[customer_email]>',
                'subject' => __( 'New reservation', 'hbook-admin' ),
                'message' => 
                    __( 'New reservation:', 'hbook-admin' ) . "\n\n" .
                    __( '- Customer first name:', 'hbook-admin' ) . ' [customer_first_name]' . "\n" .
                    __( '- Customer last name:', 'hbook-admin' ) . ' [customer_last_name]' . "\n" .
                    __( '- Customer email:', 'hbook-admin' ) . ' [customer_email]' . "\n" .
                    __( '- Check-in date:', 'hbook-admin' ) . ' [resa_check_in]' . "\n" .
                    __( '- Check-out date:', 'hbook-admin' ) . ' [resa_check_out]' . "\n" .
                    __( '- Number of adults:', 'hbook-admin' ) . ' [resa_adults]' . "\n" .
                    __( '- Number of children:', 'hbook-admin' ) . ' [resa_children]' . "\n" .
                    __( '- Accommodation:', 'hbook-admin' ) . ' [resa_accommodation]' . "\n" .
                    __( '- Price:', 'hbook-admin' ) . ' [resa_price]',
                'format' => 'TEXT',
                'lang' => 'all',
                'action' => 'new_resa'
            ),
            array(
                'name' => __( 'Customer notification', 'hbook-admin' ),
                'to_address' => '[customer_email]',
                'from_address' => '',
                'subject' => __( 'Your reservation', 'hbook-admin' ),
                'message' => 
                    __( 'Hello', 'hbook-admin' ) . ' [customer_first_name],' . "\n\n" .
                    __( 'Thank you for choosing to stay with us! We are pleased to confirm your reservation as follows:', 'hbook-admin' ) . "\n\n" .
                    __( 'Check-in date:', 'hbook-admin' ) . ' [resa_check_in]' . "\n" .
                    __( 'Check-out date:', 'hbook-admin' ) . ' [resa_check_out]' . "\n" .
                    __( 'Number of adults:', 'hbook-admin' ) . ' [resa_adults]' . "\n" .
                    __( 'Number of children:', 'hbook-admin' ) . ' [resa_children]' . "\n" .
                    __( 'Accommodation:', 'hbook-admin' ) . ' [resa_accommodation]' . "\n" .
                    __( 'Price:', 'hbook-admin' ) . ' [resa_price]' . "\n\n" .
                    __( 'See you soon!', 'hbook-admin' ),
                'format' => 'TEXT',
                'lang' => 'all',
                'action' => 'new_resa'
            ),
            array(
                'name' => __( 'Reservation confirmation', 'hbook-admin' ),
                'to_address' => '[customer_email]',
                'from_address' => '',
                'subject' => __( 'Your reservation', 'hbook-admin' ),
                'message' => 
                    __( 'Hello', 'hbook-admin' ) . ' [customer_first_name],' . "\n\n" .
                    __( 'Thank you for choosing to stay with us! We are pleased to confirm your reservation as follows:', 'hbook-admin' ) . "\n\n" .
                    __( 'Check-in date:', 'hbook-admin' ) . ' [resa_check_in]' . "\n" .
                    __( 'Check-out date:', 'hbook-admin' ) . ' [resa_check_out]' . "\n" .
                    __( 'Number of adults:', 'hbook-admin' ) . ' [resa_adults]' . "\n" .
                    __( 'Number of children:', 'hbook-admin' ) . ' [resa_children]' . "\n" .
                    __( 'Accommodation:', 'hbook-admin' ) . ' [resa_accommodation]' . "\n" .
                    __( 'Price:', 'hbook-admin' ) . ' [resa_price]' . "\n\n" .
                    __( 'See you soon!', 'hbook-admin' ),
                'format' => 'TEXT',
                'lang' => 'all',
                'action' => 'confirmation_resa'
            )
        );
        
        foreach ( $email_templates as $email_tmpl ) {
            $values = "'" . implode( "', '", $email_tmpl ) . "'";
            $query = 
                "
                INSERT INTO $table_name
                (name, to_address, from_address, subject, message, format, lang, action)
                VALUES 
                ( $values )
                ";
            $wpdb->query( $query );
        }
    }
	
	public function alter_plugin_tables( $installed_version ) {
		global $wpdb;
		if ( version_compare( '1.1', $installed_version ) > 0 ) {
			$table_name = $this->hbdb->resa_table;
			$query = 
				"
				ALTER TABLE $table_name
				ADD admin_comment text NOT NULL AFTER status;
				";
			$wpdb->query( $query );
		}
		if ( version_compare( '1.2', $installed_version ) > 0 ) {
			$table_name = $this->hbdb->strings_table;
			$query = 
				"
				INSERT INTO $table_name
				(id, locale, value) 
				VALUES 
				
				('check_in_day_not_allowed', 'en_US', 'Check-in is allowed only on specific days (%check_in_days).'),
				('check_out_day_not_allowed', 'en_US', 'Check-out is allowed only on specific days (%check_out_days).'),
				('minimum_stay', 'en_US', 'A %nb_nights-nights minimum stay policy applies. Please modify your dates.'),
				('maximum_stay', 'en_US', 'A %nb_nights-nights maximum stay policy applies. Please modify your dates.'),
				('check_out_day_not_allowed_for_check_in_day', 'en_US', 'Guests check-in on %check_in_day must check-out on a specific day (%check_out_days).'),
				('minimum_stay_for_check_in_day', 'en_US', 'A %nb_nights-night minimum stay is required for booking starting on %check_in_day.'),
				('maximum_stay_for_check_in_day', 'en_US', 'A %nb_nights-night maximum stay applies for booking starting on %check_in_day.'),
				('table_rates_per_night', 'en_US', 'per night'),
				('table_rates_for_night_stay', 'en_US', 'for a %nb_nights-night stay'),
				
				('price_breakdown_discount', 'en_US', 'Discount:'),
				('price_breakdown_before_discount', 'en_US', 'Price before discount:')
				";
				
			$wpdb->query( $query );
			
			$table_name = $this->hbdb->fields_table;
			$query =
				"
				ALTER TABLE $table_name
				ADD form_name varchar(15) NOT NULL
				";
			$wpdb->query( $query );
			$query =
				"
				UPDATE $table_name
				SET form_name='booking'
				";
			$wpdb->query( $query );
			
			$table_name = $this->hbdb->rates_table;
			$new_table_name = $this->hbdb->rates_table . '_old';
			$query =
				"
				RENAME TABLE $table_name TO $new_table_name
				";
			$wpdb->query( $query );
			
			$table_name = $this->hbdb->rates_table;
			$query = "CREATE TABLE IF NOT EXISTS $table_name  (
				id bigint(20) NOT NULL AUTO_INCREMENT,
				type varchar(15) NOT NULL,
				all_accom tinyint(1) NOT NULL,
				all_seasons tinyint(1) NOT NULL,
				amount decimal(14,2) NOT NULL,
				nights smallint(6) NOT NULL,
				PRIMARY KEY (id)
			) $this->charset_collate;";
			
			$wpdb->query( $query );
			
			$this->from_rates_1_1_to_rates_1_2();

		}
		if ( version_compare( '1.2.1', $installed_version ) > 0 ) {
			$table_name = $this->hbdb->resa_table;
			$query = 
				"
				ALTER TABLE $table_name
				ADD deposit decimal(14,2) NOT NULL AFTER price
				";
			$wpdb->query( $query );
		}
		if ( version_compare( '1.3', $installed_version ) > 0 ) {
			$this->create_v_1_3_hb_tables();
			$this->update_v_1_3_hb_tables();
			$this->insert_hb_text_v_1_3();
		}
        if ( version_compare( '1.4', $installed_version ) > 0 ) {
			$this->update_v_1_4_hb_tables();
			$this->insert_hb_text_v_1_4();
            $this->update_deposit_option();
		}
        if ( version_compare( '1.5', $installed_version ) > 0 ) {
            $this->create_v_1_5_hb_tables();
            $this->update_v_1_5_hb_tables();
            $this->insert_hb_text_v_1_5();
            $this->insert_accom_num_name_v_1_5();
            $this->from_email_settings_to_email_templates_v_1_5();
        }
        if ( version_compare( '1.5.1', $installed_version ) > 0 ) {
            $this->update_resa_min_date_option();
            $this->insert_hb_text_v_1_5_1();
        }
        if ( version_compare( '1.5.3', $installed_version ) > 0 ) {
            $this->insert_hb_text_v_1_5_3();
        }
        if ( version_compare( '1.5.4', $installed_version ) > 0 ) {
            $this->create_v_1_5_4_hb_tables();
        }
        if ( version_compare( '1.6', $installed_version ) > 0 ) {
            $this->insert_hb_text_v_1_6();
            $this->update_v_1_6_hb_tables();
			$this->create_v_1_6_hb_tables();
            $this->update_paypal_options();
			if ( get_option( 'hb_purchase_code' ) ) {
				$this->utils->verify_purchase_code( get_option( 'hb_purchase_code' ) );
			}
        }
		if ( version_compare( '1.6.2', $installed_version ) > 0 ) {
			$this->create_v_1_6_2_hb_tables();
			$this->update_v_1_6_2_hb_tables();
			$this->insert_hb_text_v_1_6_2();
		}
		if ( version_compare( '1.6.3', $installed_version ) > 0 ) {
			$this->update_v_1_6_3_hb_tables();
			$this->insert_hb_text_v_1_6_3();
		}
		if ( version_compare( '1.6.4', $installed_version ) > 0 ) {
			$this->update_v_1_6_4_hb_tables();
		}	
		if ( version_compare( '1.7', $installed_version ) > 0 ) {
			$this->update_v_1_7_hb_tables();
			$this->insert_hb_text_v_1_7();
			$this->update_options_v_1_7();
		}
	}
	
	private function from_rates_1_1_to_rates_1_2() {
		global $wpdb;
		$old_rate_table = $this->hbdb->rates_table . '_old';
		$new_rate_table = $this->hbdb->rates_table;
		$rates = $wpdb->get_results( 
			"
			SELECT *
			FROM $old_rate_table
			"
			, ARRAY_A 
		);
		foreach ( $rates as $old_rate ) {
			$new_rate = array(
				'all_accom' => 0,
				'all_seasons' => 0,
				'amount' => $old_rate['rate'],
				'nights' => 1
			);
			switch ( $old_rate['type'] ) {
				case 'normal' : $new_rate['type'] = 'accom'; break;
				case 'adult' : $new_rate['type'] = 'extra_adults'; break;
				case 'child' : $new_rate['type'] = 'extra_children'; break;
			}
			$wpdb->insert( $new_rate_table, $new_rate );
			$rate_id = $wpdb->insert_id;
			$wpdb->insert( $this->hbdb->rates_accom_table, array( 'rate_id' => $rate_id, 'accom_id' => $old_rate['accom_id'] ) );
			$wpdb->insert( $this->hbdb->rates_seasons_table, array( 'rate_id' => $rate_id, 'season_id' => $old_rate['season_id'] ) );
		}
	}
	
	private function insert_hb_text_v_1_3() {
        global $wpdb;
		$table_name = $this->hbdb->strings_table;
		$query = 
			"
			INSERT INTO $table_name
			(id, locale, value) 
			VALUES 
			
			('price_breakdown_fees', 'en_US', 'Fees:'),
            ('price_breakdown_nights_several', 'en_US', 'nights'),
			('price_breakdown_night_one', 'en_US', 'night'),
			('price_breakdown_multiple_nights', 'en_US', 'x %nb_nights-night'),
            ('fee_details_adults_several', 'en_US', 'adults'),
			('fee_details_adult_one', 'en_US', 'adult'),
			('fee_details_children_several', 'en_US', 'children'),
			('fee_details_child_one', 'en_US', 'child'),
			('no_adults', 'en_US', 'Please select the number of adults.'),
			('no_adults_children', 'en_US', 'Please select the number of adults and the number of children.'),
			('no_children', 'en_US', 'Please select the number of children.'),
            ('select_options_title', 'en_US', 'Select your options'),
            ('free_option', 'en_US', '(free)'),
            ('each_option', 'en_US', ' each'), 
            ('summary_accom_price', 'en_US', 'Accommodation price:'),
            ('total_options_price', 'en_US', 'Total options price:'),
            ('summary_options_price', 'en_US', 'Options price:')
			";
			
		$result = $wpdb->query( $query );
		
		if ( ! $result ) {
			$query = 
			"
			INSERT INTO $table_name
			(id, locale, value) 
			VALUES 
			
			('price_breakdown_fees', 'en_US', 'Fees:'),
            ('price_breakdown_nights_several', 'en_US', 'nights'),
			('price_breakdown_night_one', 'en_US', 'night'),
			('price_breakdown_multiple_nights', 'en_US', 'x %nb_nights-night'),
            ('fee_details_adults_several', 'en_US', 'adults'),
			('fee_details_adult_one', 'en_US', 'adult'),
			('fee_details_children_several', 'en_US', 'children'),
			('fee_details_child_one', 'en_US', 'child'),
            ('select_options_title', 'en_US', 'Select your options'),
            ('free_option', 'en_US', '(free)'),
            ('each_option', 'en_US', 'each'), 
            ('summary_accom_price', 'en_US', 'Accommodation price:'),
            ('total_options_price', 'en_US', 'Total options price:'),
            ('summary_options_price', 'en_US', 'Options price:')
			";
			
			$wpdb->query( $query );
		}
	}
	  
	private function create_v_1_3_hb_tables() {
	
        global $wpdb;
        
		$table_name = $this->hbdb->options_table;
		$query = "CREATE TABLE IF NOT EXISTS $table_name  (
			id bigint(20) NOT NULL AUTO_INCREMENT,
			name varchar(200) NOT NULL,
			amount decimal(14,2) NOT NULL,
			amount_children decimal(14,2) NOT NULL,
			choice_type varchar(8) NOT NULL,
			apply_to_type varchar(25) DEFAULT NULL,
			quantity_max_option varchar(25) NOT NULL,
			quantity_max int(11) NOT NULL,
			quantity_max_child int(11) NOT NULL,
			all_accom tinyint(1) NOT NULL,
			PRIMARY KEY (id)
		) $this->charset_collate;";
		
		$wpdb->query( $query );
		
		$table_name = $this->hbdb->options_choices_table;
		$query = "CREATE TABLE IF NOT EXISTS $table_name  (
			id bigint(20) NOT NULL AUTO_INCREMENT,
			option_id bigint(20) NOT NULL,
			name varchar(200) NOT NULL,
			amount decimal(14,2) NOT NULL,
			amount_children decimal(14,2) NOT NULL,
			PRIMARY KEY (id)
		) $this->charset_collate;";
		
		$wpdb->query( $query );
		
		$table_name = $this->hbdb->options_accom_table;
		$query = "CREATE TABLE IF NOT EXISTS $table_name  (
			option_id bigint(20) NOT NULL,
			accom_id bigint(20) NOT NULL
		) $this->charset_collate;";

		$wpdb->query( $query );
		
		$table_name = $this->hbdb->fees_table;
		$query = "CREATE TABLE IF NOT EXISTS $table_name  (
			id bigint(20) NOT NULL AUTO_INCREMENT,
			name varchar(200) NOT NULL,
			amount decimal(14,2) NOT NULL,
			amount_children decimal(14,2) NOT NULL,
			apply_to_type varchar(25) DEFAULT NULL,
			all_accom tinyint(1) NOT NULL,
			global tinyint(1) NOT NULL,
			PRIMARY KEY (id)
		) $this->charset_collate;";

		$wpdb->query( $query );

		$table_name = $this->hbdb->fees_accom_table;
		$query = "CREATE TABLE IF NOT EXISTS $table_name  (
			fee_id bigint(20) NOT NULL,
			accom_id bigint(20) NOT NULL,
			PRIMARY KEY (fee_id, accom_id)
		) $this->charset_collate;";

		$wpdb->query( $query );
	}
    
    private function update_v_1_3_hb_tables() {
        global $wpdb;
        $table_name = $this->hbdb->resa_table;
        $query = 
            "
            ALTER TABLE $table_name
            ADD options text NOT NULL AFTER status
            ";
        $wpdb->query( $query );
    }  
    
    private function update_v_1_4_hb_tables() {
        global $wpdb;
        $table_name = $this->hbdb->resa_table;
        $query = 
            "
            ALTER TABLE $table_name
            ADD paid decimal(14,2) NOT NULL AFTER deposit
            ";
        $wpdb->query( $query );
        
        $query = 
            "
            UPDATE $table_name SET paid = -1
            ";
        $wpdb->query( $query );
    }
    
    private function insert_hb_text_v_1_4() {
        global $wpdb;
		$table_name = $this->hbdb->strings_table;
		$query = 
			"
			INSERT INTO $table_name
			(id, locale, value) 
			VALUES 
			
			('payment_type', 'en_US', 'Select your payment type:'),
            ('payment_type_offline', 'en_US', 'On arrival or offline payment'),
            ('payment_type_deposit', 'en_US', 'Pay deposit now'),
            ('payment_type_full', 'en_US', 'Pay total price now')
            ";
        $wpdb->query( $query );
    }
	
    private function update_deposit_option() {
        if ( get_option( 'hb_resa_payment' ) == 'none' ) {
            update_option( 'hb_deposit_type', 'none' );
        }
    }
    
    private function create_v_1_5_hb_tables() {
        global $wpdb;
        
        $table_name = $this->hbdb->accom_num_name_table;
        $query = "CREATE TABLE IF NOT EXISTS $table_name  (
			accom_id bigint(20) NOT NULL,
			accom_num bigint(20) NOT NULL,
			num_name varchar(50) DEFAULT NULL,
			PRIMARY KEY (accom_id,accom_num)
		) $this->charset_collate;";
		$wpdb->query( $query );
        
		$table_name = $this->hbdb->email_templates_table;
		$query = "CREATE TABLE IF NOT EXISTS $table_name  (
		  id bigint(20) NOT NULL AUTO_INCREMENT,
		  name varchar(200) NOT NULL,
		  to_address varchar(200) NOT NULL,
		  from_address varchar(200) NOT NULL,
		  subject varchar(200) NOT NULL,
		  message longtext NOT NULL,
		  format varchar(10) NOT NULL,
		  lang varchar(20) NOT NULL,
		  action varchar(20) NOT NULL,
		  PRIMARY KEY (id)
		) $this->charset_collate;";

		$wpdb->query( $query );
    }
            
    private function insert_accom_num_name_v_1_5() {
        global $wpdb;
        $accom_ids = $this->hbdb->get_all_accom_ids();
        foreach ( $accom_ids as $accom_id ) {
            $accom_quantity = get_post_meta( $accom_id, 'accom_quantity', true );
            if ( $accom_quantity == '' ) {
                $accom_quantity = 1;
            }
            $accom_num_name = array();
            for ( $i = 1; $i <= $accom_quantity; $i++ ) {
                $wpdb->insert( 
                    $this->hbdb->accom_num_name_table, 
                    array(
                        'accom_id' => $accom_id,
                        'accom_num' => $i,
                        'num_name' => $i
                    )
                );
            }
            update_post_meta( $accom_id, 'accom_num_name_index', $i - 1 );
        }
    }
    
    private function update_v_1_5_hb_tables() {
        global $wpdb;
        $table_name = $this->hbdb->resa_table;
        $query = 
            "
            ALTER TABLE $table_name
            ADD lang varchar(20) NOT NULL AFTER admin_comment
            ";
        $wpdb->query( $query );
    }
    
    public function insert_hb_text_v_1_5() {
        global $wpdb;
		$table_name = $this->hbdb->strings_table;
		$query = 
			"
			INSERT INTO $table_name
			(id, locale, value) 
			VALUES 
			('txt_before_book_now_button', 'en_US', '')
            ";
        $wpdb->query( $query );
    }
    
    public function from_email_settings_to_email_templates_v_1_5() {
        global $wpdb;
		$table_name = $this->hbdb->email_templates_table;
        
        $email_templates = array();
        
        $email_templates['admin']['name'] = __( 'Admin notification', 'hbook-admin' );
        $email_templates['ack']['name'] = __( 'Customer notification', 'hbook-admin' );
        $email_templates['confirm']['name'] = __( 'Reservation confirmation', 'hbook-admin' );
        
        $email_templates['admin']['to_address'] = get_option( 'hb_admin_email', '' );
        $email_templates['ack']['to_address'] = '[customer_email]';
        $email_templates['confirm']['to_address'] = '[customer_email]';
        
        $settings = array( 
            'email_from' => 'from_address', 
            'email_subject' => 'subject', 
            'email_message' => 'message', 
            'message_type' => 'format', 
        );
        $email_types = array( 'admin', 'ack', 'confirm' );
        
        foreach ( $email_types as $email_type ) {
            foreach ( $settings as $old_setting => $new_setting ) {
                $email_templates[ $email_type ][ $new_setting ] = get_option( 'hb_' . $email_type . '_' . $old_setting );
            }
            
            $email_templates[ $email_type ]['lang'] = 'all';
            $email_templates[ $email_type ]['format'] = strtoupper( $email_templates[ $email_type ]['format'] );
        }
        
        if ( get_option( 'hb_notify_admin' ) == 'yes' ) {
            $email_templates['admin']['action'] = 'new_resa';
        } else {
            $email_templates['admin']['action'] = 'not_automatic';
        }
        if ( get_option( 'hb_ack_email' ) == 'yes' ) {
            $email_templates['ack']['action'] = 'new_resa';
        } else {
            $email_templates['ack']['action'] = 'not_automatic';
        }
        if ( get_option( 'hb_confirm_email' ) == 'yes' ) {
            $email_templates['confirm']['action'] = 'confirmation_resa';
        } else {
            $email_templates['confirm']['action'] = 'not_automatic';
        }
        
        foreach ( $email_templates as $email_tmpl ) {
            $values = "'" . implode( "', '", $email_tmpl ) . "'";
            $query = 
                "
                INSERT INTO $table_name
                (name, to_address, from_address, subject, message, format, lang, action)
                VALUES 
                ( $values )
                ";
            $wpdb->query( $query );
        }
    }
    
    private function update_resa_min_date_option() {
        if ( get_option( 'hb_min_date' ) ) {
            update_option( 'hb_min_date_fixed', get_option( 'hb_min_date' ) );
        }
    }
    
    private function insert_hb_text_v_1_5_1() {
        global $wpdb;
		$table_name = $this->hbdb->strings_table;
		$query = 
			"
			INSERT INTO $table_name
			(id, locale, value) 
			VALUES 
			('chosen_options', 'en_US', 'Extra services:'),
			('price_option', 'en_US', '(%price%each%max)')
            ";
        $wpdb->query( $query );
    }
    
    public function insert_hb_text_v_1_5_3() {
        global $wpdb;
		$table_name = $this->hbdb->strings_table;
		$query =
            "
            INSERT INTO $table_name
			(id, locale, value) 
			VALUES
            ('legend_past', 'en_US', 'Past' ),
            ('legend_closed', 'en_US', 'Closed' ),
            ('legend_available', 'en_US', 'Available' ),
            ('legend_before_check_in', 'en_US', 'Before check-in day' ),
            ('legend_no_check_in', 'en_US', 'Not available for check-in' ),
            ('legend_no_check_out', 'en_US', 'Not available for check-out' ),
            ('legend_check_in_only', 'en_US', 'Available for check-in only' ),
            ('legend_check_out_only', 'en_US', 'Available for check-out only' ),
            ('legend_no_check_out_min_stay', 'en_US', 'Not available for check-out (due to the %nb_nights-night minimum-stay requirement)' ),
            ('legend_no_check_out_max_stay', 'en_US', 'Not available for check-out (due to the %nb_nights-night maximum-stay requirement)' ),
            ('legend_check_in', 'en_US', 'Chosen check-in day' ),
            ('legend_check_out', 'en_US', 'Chosen check-out day' ),
            ('legend_select_check_in', 'en_US', 'Select a check-in date.'),
            ('legend_select_check_out', 'en_US', 'Select a check-out date.')
            ";
        $wpdb->query( $query );
    }
    
    private function create_v_1_5_4_hb_tables() {
        global $wpdb;
        
        $table_name = $this->hbdb->accom_blocked_table;
        $query = "CREATE TABLE IF NOT EXISTS $table_name  (
            id bigint(20) NOT NULL AUTO_INCREMENT,
			accom_id bigint(20) NOT NULL,
			accom_all_ids tinyint(1) NOT NULL,
			accom_num bigint(20) NOT NULL,
            accom_all_num tinyint(1) NOT NULL,
            from_date DATE NOT NULL,
			to_date DATE NOT NULL,
			comment text NOT NULL,
			uid varchar(256) NOT NULL,
			PRIMARY KEY (id)
		) $this->charset_collate;";
		$wpdb->query( $query );
    }
    
    private function insert_hb_text_v_1_6() {
        global $wpdb;
		$table_name = $this->hbdb->strings_table;
		$query =
            "
            INSERT INTO $table_name
			(id, locale, value) 
			VALUES
			('processing', 'en_US', 'Processing...'),
            ('payment_method', 'en_US', 'Select your payment method:' ),
            ('stripe_card_number', 'en_US', 'Card number:' ),
            ('stripe_expiration', 'en_US', 'Expiration date:' ),
            ('stripe_cvc', 'en_US', 'CVC:' ),
            ('stripe_invalid_card', 'en_US', 'Could not process the payment. The card is not valid.' ),
            ('stripe_invalid_card_number', 'en_US', 'The card number is not valid.' ),
            ('stripe_invalid_expiration', 'en_US', 'The expiration date is not valid.' ),
            ('stripe_payment_method_label', 'en_US', 'Credit or debit card' ),
            ('paypal_payment_method_label', 'en_US', 'PayPal' ),
            ('paypal_text_before_form', 'en_US', 'Click on Book Now to confirm your reservation. You will be redirected to PayPal to complete your payment.' ),
			('terms_and_cond_title', 'en_US', 'Terms and conditions' ),
			('terms_and_cond_text', 'en_US', 'I have read and I accept the terms and conditions for this booking.' ),
			('terms_and_cond_error', 'en_US', 'Please confirm you have read and you accept the terms and conditions by checking the box.' )
            ";
        $wpdb->query( $query );
    }
    
    private function update_v_1_6_hb_tables() {
        global $wpdb;
		
        $table_name = $this->hbdb->resa_table;
        $query = 
            "
            ALTER TABLE $table_name
            ADD payment_gateway varchar(20) NOT NULL AFTER paid,
            ADD amount_to_pay decimal(14,2) NOT NULL AFTER payment_status_reason
            ";
        $wpdb->query( $query );
        
		$query = 
			"
			ALTER TABLE $table_name
			ADD updated_on DATETIME NOT NULL,
			ADD uid varchar(256) NOT NULL,
			ADD origin varchar(40) NOT NULL
			";
		$wpdb->query( $query );
		
		$gmt_offset = get_option( 'gmt_offset' );
		if ( ! $gmt_offset ) {
			$gmt_offset = 0;
		}
		$query = 
		 	"
			UPDATE $table_name
			SET received_on = received_on - INTERVAL $gmt_offset HOUR
			";
		$wpdb->query( $query );
		
		$site_url = site_url();
		$query = 
		 	"
			UPDATE $table_name
			SET updated_on = received_on, origin = 'website', uid = CONCAT( 'D', SUBSTRING( received_on, 1 ,10 ), 'T', SUBSTRING( received_on, 11 ,19 ), 'R', FLOOR( 10000 + ( RAND() * 89999 ) ), '@$site_url' )
			";
		$wpdb->query( $query );
		
        $query = 
            "
            ALTER TABLE $table_name
            CHANGE paypal_token payment_token varchar(40) NOT NULL
            ";
        $wpdb->query( $query );
			
		$table_name = $this->hbdb->accom_blocked_table;
        $query = 
            "
            ALTER TABLE $table_name
            ADD accom_all_ids tinyint(1) NOT NULL AFTER accom_id,
			ADD uid varchar(256) NOT NULL AFTER to_date
            ";
        $wpdb->query( $query );
		
		$all_accom_blocked = $wpdb->get_results( 
			"
			SELECT * 
			FROM $table_name
			", ARRAY_A 
		);
		foreach ( $all_accom_blocked as $blocked ) {
			$wpdb->update(
				$table_name,
				array(
					'uid' => $this->hbdb->get_uid(),
				),
				array(
					'id' => $blocked['id']
				)
			);
		}
		
		$all_accom_all_num_blocked = $wpdb->get_results( 
			"
			SELECT * 
			FROM $table_name
			WHERE accom_all_num = 1 AND accom_num = 0
			", ARRAY_A 
		);
		foreach ( $all_accom_all_num_blocked as $blocked ) {
			$accom_num_name = $this->hbdb->get_accom_num_name( $blocked['accom_id'] );
			foreach ( $accom_num_name as $accom_num => $accom_name ) {
				$wpdb->insert( 
					$table_name, 
					array( 
						'accom_id' => $blocked['accom_id'],
						'accom_all_ids' => 0,
						'accom_num' => $accom_num,
						'accom_all_num' => 1,
						'from_date' => $blocked['from_date'],
						'to_date' => $blocked['to_date'],
						'uid' => $this->hbdb->get_uid(),
					) 
				);
			}
			$wpdb->delete( $table_name, array( 'id' => $blocked['id'] ) );
		}
		
		$table_name = $this->hbdb->customers_table;
        $query = 
            "
            ALTER TABLE $table_name
            ADD info text NOT NULL
            ";
        $wpdb->query( $query );
		
		$customer_info = array( 'first_name', 'last_name', 'email', 'address_1', 'address_2', 'city', 'state_province', 'zip_code', 'country', 'phone' );
		$customers = $this->hbdb->get_all( 'customers' );
		foreach ( $customers as $customer ) {
			$customer_data = array();
			foreach ( $customer_info as $info ) {
				if ( $customer[ $info ] != '' ) {
					$customer_data[ $info ] = $customer[ $info ];
				}
			}
			$wpdb->update( 
				$table_name, 
				array( 
					'info' => json_encode( $customer_data ),
				),
				array(
					'id' => $customer['id']
				)
			);
		}
    }
    
    private function update_paypal_options() {
        if ( get_option( 'hb_paypal_sandbox' ) == 'yes' ) {
            update_option( 'hb_paypal_mode', 'sandbox' );
            update_option( 'hb_paypal_api_sandbox_user', get_option( 'hb_paypal_api_user' ) );
			update_option( 'hb_paypal_api_sandbox_psw', get_option( 'hb_paypal_api_psw' ) );
			update_option( 'hb_paypal_api_sandbox_signature', get_option( 'hb_paypal_api_signature' ) );
        } else {
            update_option( 'hb_paypal_mode', 'live' );
            update_option( 'hb_paypal_api_live_user', get_option( 'hb_paypal_api_user' ) );
			update_option( 'hb_paypal_api_live_psw', get_option( 'hb_paypal_api_psw' ) );
			update_option( 'hb_paypal_api_live_signature', get_option( 'hb_paypal_api_signature' ) );
        }
    }
	
	private function create_v_1_6_hb_tables() {
		global $wpdb;
		
		$table_name = $this->hbdb->ical_table;
		$query = "CREATE TABLE IF NOT EXISTS $table_name (
			synchro_id varchar(128) NOT NULL,
			synchro_url varchar(512) NOT NULL,
			accom_id bigint(20) NOT NULL,
			accom_num bigint(20) NOT NULL,
			calendar_id varchar(256) NOT NULL,
			calendar_name varchar(50) NOT NULL,
			PRIMARY KEY (synchro_id)
		) $this->charset_collate;";
		$wpdb->query( $query );
		
		$table_name = $this->hbdb->sync_errors_table;
		$query = "CREATE TABLE IF NOT EXISTS $table_name (
			error_type varchar(50) NOT NULL,
			synchro_url varchar(512) NOT NULL,
			uid varchar(256) NOT NULL,
			calendar_name varchar(50) NOT NULL,
			accom_id bigint(20) NOT NULL,
			accom_num bigint(20) NOT NULL,
			check_in date NOT NULL,
			check_out date NOT NULL
		) $this->charset_collate;";
		$wpdb->query( $query );
    }

	private function create_v_1_6_2_hb_tables() {
		global $wpdb;
		
		$table_name = $this->hbdb->booking_rules_seasons_table;
		$query = 
			"CREATE TABLE IF NOT EXISTS $table_name  (
			rule_id bigint(20) NOT NULL,
			season_id bigint(20) NOT NULL
			) $this->charset_collate;";

		$wpdb->query( $query );
		
		$table_name = $this->hbdb->discounts_seasons_table;
		$query = 
			"CREATE TABLE IF NOT EXISTS $table_name  (
			discount_id bigint(20) NOT NULL,
			season_id bigint(20) NOT NULL
			) $this->charset_collate;";

		$wpdb->query( $query );
	}
	
	private function update_v_1_6_2_hb_tables() {
		global $wpdb;
		
		$table_name = $this->hbdb->strings_table;		
		$query = "ALTER TABLE $table_name MODIFY id VARCHAR(60)";
		$wpdb->query( $query );
		
		$table_name = $this->hbdb->booking_rules_table;
		$query = 
			"
			ALTER TABLE $table_name
			ADD all_seasons tinyint(1) NOT NULL AFTER all_accom
			";
		$wpdb->query( $query );
		
		$query =
			"
			UPDATE $table_name
			SET all_seasons = 1
			";
		$wpdb->query( $query );
		
		$table_name = $this->hbdb->discounts_table;
		$query = 
			"
			ALTER TABLE $table_name
			ADD all_seasons tinyint(1) NOT NULL AFTER all_accom
			";
		$wpdb->query( $query );
		
		$query =
			"
			UPDATE $table_name
			SET all_seasons = 1
			";
		$wpdb->query( $query );
		
		$table_name = $this->hbdb->booking_rules_seasons_table;
		$seasons = $this->hbdb->get_all( 'seasons' );
		$rules = $this->hbdb->get_all( 'booking_rules' );
		$values = array();
		foreach ( $seasons as $season ) {
			foreach ( $rules as $rule ) {
				$values[] = '(' . intval( $rule['id'] ) . ',' . intval( $season['id'] ) . ')';
			}
		}
		if ( $values ) {
			$values = implode( ',', $values );
			$query =
				"
				INSERT INTO $table_name (rule_id, season_id)
				VALUES $values
				";
			$wpdb->query( $query );
		}
		
		$table_name = $this->hbdb->discounts_seasons_table;
		$seasons = $this->hbdb->get_all( 'seasons' );
		$discounts = $this->hbdb->get_all( 'discounts' );
		$values = array();
		foreach ( $seasons as $season ) {
			foreach ( $discounts as $discount ) {
				$values[] = '(' . intval( $discount['id'] ) . ',' . intval( $season['id'] ) . ')';
			}
		}
		if ( $values ) {
			$values = implode( ',', $values );
			$query =
				"
				INSERT INTO $table_name (discount_id, season_id)
				VALUES $values
				";
			$wpdb->query( $query );
		}
	}
	
	private function insert_hb_text_v_1_6_2() {
        global $wpdb;
		$table_name = $this->hbdb->strings_table;
		$query =
            "
            INSERT INTO $table_name
			(id, locale, value) 
			VALUES
			('check_in_day_not_allowed_seasonal', 'en_US', 'Check-in is allowed only on certain days. Please change your check-in date.'),
			('check_out_day_not_allowed_seasonal', 'en_US', 'Check-out is allowed only on certain days. Please change your check-out date.'),
			('minimum_stay_seasonal', 'en_US', 'A minimum stay policy applies. Please modify your dates to extend your stay.'),
			('maximum_stay_seasonal', 'en_US', 'A maximum stay policy applies. Please modify your dates to shorten your stay.'),
			('check_out_day_not_allowed_for_check_in_day_seasonal', 'en_US', 'With this check-in date check-out is allowed only on certain days. Please change your check-in or check-out dates.'),
			('minimum_stay_for_check_in_day_seasonal', 'en_US', 'With this check-in date a minimum stay policy applies.'),
			('maximum_stay_for_check_in_day_seasonal', 'en_US', 'With this check-in date a maximum stay policy applies.')
			";
		$wpdb->query( $query );
	}
	
	private function update_v_1_6_3_hb_tables() {
		global $wpdb;
		$table_name = $this->hbdb->resa_table;
		$query = 
            "
            ALTER TABLE $table_name
            ADD synchro_id varchar(128) NOT NULL AFTER origin
            ";
        $wpdb->query( $query );
		
		$table_name = $this->hbdb->ical_table;
		$query = 
            "
            ALTER TABLE $table_name
            ADD synchro_id varchar(128) NOT NULL AFTER synchro_url
            ";
        $wpdb->query( $query );
		$all_synced_calendar = $this->hbdb->get_ical_sync();
		foreach ( $all_synced_calendar as $synced_calendar ) {
			$synchro_id = uniqid( '', true );
			$synchro_url = $synced_calendar['synchro_url'];
			$wpdb->update(
				$table_name,
				array(
					'synchro_id' => $synchro_id,
				),
				array(
					'synchro_url' => $synchro_url,
				)
			);
		}
		$query = 
            "
            ALTER TABLE $table_name
            ADD PRIMARY KEY(synchro_id);
            ";
        $wpdb->query( $query );
	}		
	
	private function insert_hb_text_v_1_6_3() {
        global $wpdb;
		$table_name = $this->hbdb->strings_table;
		$query =
            "
            INSERT INTO $table_name
			(id, locale, value) 
			VALUES
			('payment_section_title', 'en_US', 'Payment')
			";
		$wpdb->query( $query );
	}
	
	private function update_v_1_6_4_hb_tables() {
		global $wpdb;
		
		$table_name = $this->hbdb->fields_table;
		$query = 
            "
            ALTER TABLE $table_name
            ADD data_about varchar(15) NOT NULL
            ";
        $wpdb->query( $query );
		
		$query =
			"
			UPDATE $table_name
			SET data_about = 'customer'
			";
		$wpdb->query( $query );
		
		$table_name = $this->hbdb->resa_table;
		$query = 
            "
            ALTER TABLE $table_name
            ADD additional_info text NOT NULL
            ";
        $wpdb->query( $query );
	}
	
	private function update_v_1_7_hb_tables() {
		global $wpdb;
		
		$table_name = $this->hbdb->resa_table;
		$query = 
            "
            ALTER TABLE $table_name
            ADD booking_form_num tinyint(4) NOT NULL
            ";
        $wpdb->query( $query );
		
		$table_name = $this->hbdb->accom_blocked_table;
		$query = 
            "
            ALTER TABLE $table_name
            ADD comment text NOT NULL
            ";
        $wpdb->query( $query );
		
		$table_name = $this->hbdb->customers_table;
		$query = 
            "
            ALTER TABLE $table_name
            ADD payment_id varchar(40) NOT NULL
            ";
        $wpdb->query( $query );
		
		$table_name = $this->hbdb->options_table;
		$query = 
            "
			ALTER TABLE $table_name
			ADD quantity_max_option varchar(25) NOT NULL,
			ADD quantity_max int(11) NOT NULL,
			ADD quantity_max_child int(11) NOT NULL
			";
		$wpdb->query( $query );
		
		$query =
			"
			UPDATE $table_name
			SET quantity_max_option = 'no'
			";
		$wpdb->query( $query );
		
		$paypal_desc_vars = array( 
			'txt_deposit',
			'txt_desc',
			'txt_one_adult',
			'txt_one_child',
			'txt_one_night',
			'txt_several_adults',
			'txt_several_children',
			'txt_several_nights',
		);
		$table_name = $this->hbdb->strings_table;
		foreach ( $paypal_desc_vars as $paypal_desc_var ) {
			$old_id = 'paypal_' . $paypal_desc_var;
			$new_id = 'external_payment_' . $paypal_desc_var;
			$query =
			"
			UPDATE $table_name
			SET id = '$new_id'
			WHERE id = '$old_id'
			";
			$wpdb->query( $query );
		}
	}
	
	private function insert_hb_text_v_1_7() {
        global $wpdb;
		$table_name = $this->hbdb->strings_table;
		$query =
            "
            INSERT INTO $table_name
			(id, locale, value) 
			VALUES
			('max_option', 'en_US', ' - Maximum: %max_value'),
			('thanks_message_payment_done_1', 'en_US', ''),
			('thanks_message_payment_done_2', 'en_US', ''),
			('error_season_not_defined', 'en_US', 'Error: season is not defined for %night.'),
			('error_rate_not_defined', 'en_US', 'Error: please add a rate for accommodation named %accom_name and season named %season_name.'),
			('payment_type_store_credit_card', 'en_US', 'Leave credit card details (the card will not be charged now)'),
			('stripe_processing_error', 'en_US', 'Could not process the payment (%error_msg).' )
			";
			
		$wpdb->query( $query );
	}
	
	private function update_options_v_1_7() {
		if ( get_option( 'hb_form_style' ) == 'plugin' ) {
			update_option( 'hb_buttons_style', 'custom' );
			update_option( 'hb_inputs_selects_style', 'custom' );
		}
		if ( get_option( 'hb_resa_payment' ) == 'none' ) {
			update_option( 'hb_resa_payment', 'offline' );
		}
	} 

}