<?php
class HbAdminPageLicence extends HbAdminPage {
	
	public function __construct( $page_id, $hbdb, $utils, $options_utils ) {
		$this->data = array(
			'hb_text' => array(
				'remove_purchase_code' => __( 'Remove purchase code for this website?', 'hbook_admin' ),
			)
		);
		parent::__construct( $page_id, $hbdb, $utils, $options_utils );
	}
	
	public function display() {
	?>

	<div class="wrap">
		
		<h2><?php _e( 'Licence', 'hbook-admin' ); ?></h2>
		
		<hr/>

		<?php if ( $this->utils->is_htw_theme_active() ) : ?>

		<p><?php printf( __( 'HBook is bundled with %1$s. Please use %1$s purchase code to activate HBook.', 'hbook-admin' ), wp_get_theme() ); ?></p>

		<?php else : ?>
			
		<p><a href="https://hotelwp.com/documentation/hbook/#faq-purchase-code"><?php _e( 'Where can I find my purchase code?', 'hbook-admin' ); ?></a></p>

		<?php endif; ?>
		
		<?php $this->display_right_menu(); ?>
		
		<form id="hb-verify-licence" method="post">
			
			<?php 
			$valid_purchase_code = get_option( 'hb_valid_purchase_code' );
			
			if ( $valid_purchase_code == 'error' ) :
				if ( get_option( 'hb_purchase_code_error' ) == 'no-online-validation' ) {
					$error_text = __( 'An error occurred when trying to validate your licence. Please go to HotelWP to obtain a validation code.', 'hbook-admin' );
					if ( strpos( $error_text, 'HotelWP' ) ) {
						$error_text = str_replace( 'HotelWP', '<a target="_blank" href="https://hotelwp.com/hbook-licence-validation/">HotelWP</a>', $error_text );
					} else {
						$error_text .= ' (<a target="_blank" href="https://hotelwp.com/hbook-licence/">HotelWP</a>)';
					}
				} else if ( get_option( 'hb_purchase_code_error', 'wrong-validation-code' ) ) { 
					$error_text = __( 'The code you entered is not valid. Please try again. You can also contact %s if you need further help.', 'hbook-admin' );
					$hwp_support_string = __( 'HotelWP support', 'hbook-admin' );
					if ( strpos( $error_text, '%s' ) ) {
						$error_text = str_replace( '%s', '<a target="_blank" href="https://hotelwp.com/contact-support/">' . $hwp_support_string . '</a>', $error_text );
					} else {
						$error_text .= ' (<a target="_blank" href="https://hotelwp.com/contact-support/">HotelWP support</a>)';
					}
				}
				?>
				
				<p><?php echo( $error_text ); ?></p>
				
				<p>
					<label><?php _e( 'Your website url:', 'hbook-admin' ); ?></label><br/>
					<input type="text" onclick="this.select()" onfocus="this.select()" readonly="readonly" value="<?php echo( site_url() ); ?>" />
				</p>
				<p>
					<label><?php _e( 'Enter validation code', 'hbook-admin' ); ?></label><br/>
					<input type="text" name="hb-licence-validation-code" />
				</p>
				<p style="display:none">
					<input type="text" name="hb-forced-licence-validation" />
					<?php echo( get_option( 'hb_purchase_code_error_text' ) ); ?>
				</p>
				
				<?php
				delete_option( 'hb_purchase_code_error' );
				delete_option( 'hb_purchase_code_error_text' );
				delete_option( 'hb_purchase_code_error_text' );
				
			endif;
			?>
			
			<p>
				<label><?php _e( 'Enter your purchase code', 'hbook-admin' ); ?></label><br/>
				<input type="text" name="hb-purchase-code" size="50" value="<?php echo( get_option( 'hb_purchase_code' ) ); ?>" />
				<?php if ( $valid_purchase_code == 'error' ) : ?>
				<br/><br/>
				<?php endif; ?>
				<input type="submit" value="<?php _e( 'Verify purchase code', 'hbook-admin' ) ?>" class="button-primary" />
			</p>
			
			<?php if ( in_array( $valid_purchase_code, array( 'yes', 'no', 'already', 'removed', 'pending' ) ) ) : ?>
				
			<p <?php if ( $valid_purchase_code == 'no' ) { echo( 'class="hb-purchase-code-not-valid-msg"'); } ?>>
				<?php
				if ( $valid_purchase_code == 'yes' ) {
					_e( 'Your purchase code is valid.', 'hbook-admin' );
				} else if ( $valid_purchase_code == 'no' ) {
					_e( 'Your purchase code is not valid.', 'hbook-admin' );
				} else if ( $valid_purchase_code == 'already' ) {
					_e( 'This purchase code is already in use.', 'hbook-admin' );
				} else if ( $valid_purchase_code == 'removed' ) {
					_e( 'The purchase code has been removed.', 'hbook-admin' );
					delete_option( 'hb_valid_purchase_code' );
				} else if ( $valid_purchase_code == 'pending' ) {
					_e( 'This purchase code has not been verify.', 'hbook-admin' );
				}
				?>
			</p>
			
			<?php 
			endif;
			
			if ( $valid_purchase_code == 'error' ) {
				update_option( 'hb_valid_purchase_code', 'pending' );
			}
			
			if ( $valid_purchase_code == 'yes' ) : ?>
			<p>
				<a class="hb-remove-purchase-code" href="#"><?php _e( 'Remove purchase code for this website.', 'hbook-admin' ); ?></a>
			</p>
			<?php endif; ?>
	
		</form>
	</div><!-- end .wrap -->
	
	<?php
	}
	
}