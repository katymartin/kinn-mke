<?php
class HbAdminPageReservationsDisplayHelper {
	
	private $accom_list;
	
	public function __construct( $accom_list ) {
		$this->accom_list = $accom_list;
	}
	
	public function display_resa_calendar() {
	?>
	
	<div class="hb-resa-section">
	
		<h3><?php _e( 'Calendar', 'hbook-admin' ); ?></h3>
		<div>
			<select id="hb-resa-cal-accommodation">
				<option value="all"><?php _e( 'All accommodations', 'hbook-admin' ); ?></option>
				<?php
				foreach( $this->accom_list as $accom_id => $accom_name ) { ?>
					<option value="<?php echo( $accom_id ); ?>"><?php echo( $accom_name ); ?></option>
				<?php
				}
				?>
			</select>
		</div><br/>
		
		<div id="hb-resa-cal-wrapper">
			<div id="hb-resa-cal-scroller">
				<table id="hb-resa-accom-table" class="hb-resa-cal-table"></table>
				<table id="hb-resa-cal-table" class="hb-resa-cal-table"></table>
			</div>
		</div>
	
	</div><!-- end .hb-resa-section -->
	
	<hr/>
	
	<?php
	}
	
	public function display_resa_details() {
	?>
	
	<div class="hb-resa-section">
	
		<h3><?php _e( 'Reservation details', 'hbook-admin' ); ?></h3>
		
		<!-- ko if: selected_resa() == 0 -->
		<p><?php _e( 'Click on a number in the calendar to view the reservation details', 'hbook-admin' ); ?></p>
		<!-- /ko -->
		
		<!-- ko if: selected_resa() != 0 -->
		<p>
			<a href="#" data-bind="click: hide_selected_resa"><?php _e( 'Hide', 'hbook-admin' ); ?></a>
		</p>
		
		<table class="wp-list-table widefat hb-resa-table">
		
			<?php $this->display_resa_thead(); ?>
						
			<tbody data-bind="foreach { data: resa_detailed }">
			<?php $this->display_resa_tr(); ?>
			</tbody>
		
		</table>
		<!-- /ko -->
		
	</div>
	
	<hr/>
	
	<?php
	}
	
	public function display_resa_list() {
	?>
	
	<div class="hb-resa-section">
	
		<h3><?php _e( 'Reservation list', 'hbook-admin' ); ?></h3>
		
		<!-- ko if: resa().length == 0 -->
		<?php _e( 'No reservations yet.', 'hbook-admin' ); ?>
		<!-- /ko -->
		
		<!-- ko if: resa().length != 0 -->
		
		<?php $this->display_resa_pagination(); ?>
		
		<table class="wp-list-table widefat hb-resa-table">
		
			<?php $this->display_resa_thead(); ?>
		
			<tbody data-bind="foreach { data: resa_paginated }">
			<?php $this->display_resa_tr(); //, beforeRemove: hide_resa, afterAdd: show_resa?>
			</tbody>
			
		</table>

		<?php $this->display_resa_pagination(); ?>
		
		<!-- /ko -->
		
	</div>
	
	<?php	
	}
	
	private function display_resa_thead() {
	?>
	
		<thead>
			<tr>
				<td class="hb-resa-num-column"><?php _e( 'Num', 'hbook-admin' ); ?></td>
				<td class="hb-resa-status-column"><?php _e( 'Status', 'hbook-admin' ); ?></td>
				<td class="hb-resa-check-in-out-column"><?php _e( 'Check-in / Check-out', 'hbook-admin' ); ?></td>
				<td class="hb-resa-accom-column">
					<?php _e( 'Accom. type', 'hbook-admin' ); ?>
					<small><?php _e( '(number)', 'hbook-admin' ); ?></small>
				</td>
				<td class="hb-resa-info-column"><?php _e( 'Information', 'hbook-admin' ); ?></td>
				<td class="hb-resa-comment-column"><?php _e( 'Comment', 'hbook-admin' ); ?></td>
				<td class="hb-resa-customer-column"><?php _e( 'Customer', 'hbook-admin' ); ?></td>
				<td class="hb-resa-price-column"><?php _e( 'Price / Payment', 'hbook-admin' ); ?></td>
				<td class="hb-resa-received-column"><?php _e( 'Received on', 'hbook-admin' ); ?></td>
				<td class="hb-resa-actions-column"><?php if ( $this->user_can_edit() ) { _e( 'Actions', 'hbook-admin' ); } ?></td>
			</tr>
		</thead>
		
	<?php
	}
	
	private function display_resa_tr() {
	?>
	
		<tr data-bind="attr: { 'data-resa-num': id, class: anim_class }">
			<td class="hb-resa-num-column" data-bind="text: id"></td>
			<td data-bind="html: status_markup"></td>
			<td>
				<span data-bind="text: check_in"></span><br/>
				<span data-bind="text: check_out"></span><br/>
				<span data-bind="text: nb_nights"></span>
				<span data-bind="visible: nb_nights() == 1"><?php _e( 'night', 'hbook-admin' ); ?></span>
				<span data-bind="visible: nb_nights() != 1"><?php _e( 'nights', 'hbook-admin' ); ?></span>
			</td>
			<td>
				<div data-bind="visible: editing_accom()"><b><?php _e( 'Current accom.:', 'hbook-admin' ); ?></b></div>
				<div data-bind="html: accom"></div>
				<!-- ko if: status() != 'processing' -->
				<?php if ( $this->user_can_edit() ) : ?>
				<a data-bind="visible: accom_num() != 0 && ! editing_accom() && ! fetching_accom() && ! saving_accom(), click: $root.edit_accom" href="#"><?php _e( 'Edit', 'hbook-admin' ); ?></a>
				<?php endif; ?>
				<div data-bind="visible: saving_accom()"><?php _e( 'Updating...', 'hbook-admin' ); ?></div>
				<div data-bind="visible: fetching_accom()"><?php _e( 'Fetching available accom...', 'hbook-admin' ); ?></div>
				<div data-bind="visible: editing_accom()">
					<br/>
					<div><b><?php _e( 'Select new accom.:', 'hbook-admin' ); ?></b></div>
					<div class="hb-accom-editor" data-bind="html: accom_editor"></div>
					<a data-bind="click: $root.save_accom" href="#" class="button-primary"><?php _e( 'Save', 'hbook-admin' ); ?></a>
					<a data-bind="click: $root.cancel_edit_accom" href="#" class="button"><?php _e( 'Cancel', 'hbook-admin' ); ?></a>
				</div>
				<!-- /ko -->
			</td>
			<td>
				<!-- ko if: status() != 'processing' -->
				<div data-bind="visible: ! editing_resa_info()">
					<div data-bind="html: resa_info_html"></div>
					<?php if ( $this->user_can_edit() ) : ?>
					<div><a data-bind="click: $root.edit_resa_info" href="#"><?php _e( 'Edit', 'hbook-admin' ); ?></a></div>
					<?php endif; ?>
				</div>
				<div data-bind="visible: editing_resa_info()">
					<?php _e( 'Adults:', 'hbook-admin' ); ?>
					<br/>
					<input class="hb-input-edit-resa" data-bind="value: adults_tmp" type="text" />
					<?php _e( 'Children:', 'hbook-admin' ); ?>
					<br/>
					<input class="hb-input-edit-resa" data-bind="value: children_tmp" type="text" />
					<div data-bind="html: additional_info_editing_markup"></div>
					<a data-bind="click: $root.save_resa_info, visible: ! saving_resa_info()" href="#" class="button-primary"><?php _e( 'Save', 'hbook-admin' ); ?></a>
					<input type="button" disabled data-bind="visible: saving_resa_info()" href="#" class="button-primary" value="<?php _e( 'Saving', 'hbook-admin' ); ?>" />
					<a data-bind="click: $root.cancel_edit_resa_info" href="#" class="button"><?php _e( 'Cancel', 'hbook-admin' ); ?></a>
				</div>
				<!-- /ko -->
			</td>
			<td>
				<!-- ko if: status() != 'processing' -->
				<div data-bind="visible: ! editing_comment()">
					<div data-bind="html: admin_comment_html()"></div>
					<?php if ( $this->user_can_edit() ) : ?>
					<div><a data-bind="visible: admin_comment() == '', click: $root.edit_comment" href="#"><?php _e( 'Add a comment', 'hbook-admin' ); ?></a></div>
					<div><a data-bind="visible: admin_comment() != '', click: $root.edit_comment" href="#"><?php _e( 'Edit', 'hbook-admin' ); ?></a></div>
					<?php endif; ?>
				</div>
				<div data-bind="visible: editing_comment()">
					<textarea data-bind="value: admin_comment_tmp" rows="6" class="widefat"></textarea><br/>
					<a data-bind="click: $root.save_comment, visible: ! saving_comment()" href="#" class="button-primary"><?php _e( 'Save', 'hbook-admin' ); ?></a>
					<input type="button" disabled data-bind="visible: saving_comment()" href="#" class="button-primary" value="<?php _e( 'Saving', 'hbook-admin' ); ?>" />
					<a data-bind="click: $root.cancel_edit_comment" href="#" class="button"><?php _e( 'Cancel', 'hbook-admin' ); ?></a>
				</div>
				<!-- /ko -->
			</td>
			<td>
				<!-- ko if: status() != 'processing' && customer_id() != 0 -->
				<div data-bind="visible: ! editing_customer(), html: customer_info_markup"></div>
				<?php if ( $this->user_can_edit() ) : ?>
				<a data-bind="visible: ! editing_customer(), click: $root.edit_customer" href="#"><?php _e( 'Edit', 'hbook-admin' ); ?></a>
				<?php endif; ?>
				<div data-bind="visible: editing_customer()">
					<div class="hb-customer-edit-wrapper">
						<h4><?php _e( 'Customer details', 'hbook-admin' ) ?></h4>
						<a data-bind="click: $root.save_customer, visible: ! saving_customer()" href="#" class="button-primary"><?php _e( 'Save', 'hbook-admin' ); ?></a>
						<input type="button" disabled data-bind="visible: saving_customer()" href="#" class="button-primary" value="<?php _e( 'Saving', 'hbook-admin' ); ?>" />
						<a data-bind="click: $root.cancel_edit_customer" href="#" class="button"><?php _e( 'Cancel', 'hbook-admin' ); ?></a>
						<hr/>
						<div data-bind="html: customer_info_editing_markup"></div>
						<hr/>
						<a data-bind="click: $root.save_customer, visible: ! saving_customer()" href="#" class="button-primary"><?php _e( 'Save', 'hbook-admin' ); ?></a>
						<input type="button" disabled data-bind="visible: saving_customer()" href="#" class="button-primary" value="<?php _e( 'Saving', 'hbook-admin' ); ?>" />
						<a data-bind="click: $root.cancel_edit_customer" href="#" class="button"><?php _e( 'Cancel', 'hbook-admin' ); ?></a>
					</div>
				</div>
				<!-- /ko -->
				<!-- ko if: status() != 'processing' && customer_id() == 0 -->
				<a data-bind="click: $root.create_customer, visible: ! creating_customer()" href="#"><?php _e( 'Create customer', 'hbook-admin' ); ?></a>
				<div data-bind="visible: creating_customer()"><?php _e( 'Creating customer...', 'hbook-admin' ); ?></div>
				<!-- /ko -->
			</td>
			<td>
				
				<div data-bind="html: price_markup"></div>
				
				<!-- ko if: status() != 'processing' -->
				
				<div data-bind="html: price_status"></div>
				
				<!-- ko if: paid() != -1 -->
				<?php if ( $this->user_can_edit() ) : ?>
				<a data-bind="click: $root.mark_paid, visible: paid() != price() && ! marking_paid()" href="#"><?php _e( 'Mark as paid', 'hbook-admin' ); ?></a>
				<b data-bind="visible: marking_paid()"><?php _e( 'Marking as paid...', 'hbook-admin' ); ?></b>
				<?php endif; ?>
				
				<div class="hb-to-be-paid-details">
					<div data-bind="visible: ! editing_paid()">
						<div data-bind="visible: paid() != 0 && paid() != price(), html: price_details"></div>
						<?php if ( $this->user_can_edit() ) : ?>
						<a data-bind="click: $root.edit_paid" href="#"><?php _e( 'Edit payment', 'hbook-admin' ); ?></a>
						<?php endif; ?>
					</div>
					<div data-bind="visible: editing_paid()">
						<?php _e( 'Price:', 'hbook-admin' ); ?>
						<br/>
						<input class="hb-input-edit-resa" data-bind="value: price_tmp" type="text" />
						<?php _e( 'Paid:', 'hbook-admin' ); ?>
						<br/>
						<input class="hb-input-edit-resa" data-bind="value: paid_tmp" type="text" />
						<a data-bind="click: $root.save_paid, visible: ! saving_paid()" href="#" class="button-primary"><?php _e( 'Save', 'hbook-admin' ); ?></a>
						<input type="button" disabled data-bind="visible: saving_paid()" href="#" class="button-primary" value="<?php _e( 'Saving', 'hbook-admin' ); ?>" />
						<a data-bind="click: $root.cancel_edit_paid" href="#" class="button"><?php _e( 'Cancel', 'hbook-admin' ); ?></a>
					</div>
				</div>
				<div class="hb-charge-details">
					<div data-bind="visible: charge_action_visible"><a data-bind="click: $root.edit_charge" href="#"><?php _e( 'Charge', 'hbook-admin' ); ?></a></div>
					<div data-bind="visible: editing_charge()">
						<?php _e( 'Charge amount:', 'hbook-admin' ); ?>
						<br/>
						<input class="hb-input-edit-resa" data-bind="value: charge_amount" type="text" />
						<a data-bind="click: $root.charge, visible: ! charging()" href="#" class="button-primary"><?php _e( 'Charge', 'hbook-admin' ); ?></a>
						<input type="button" disabled data-bind="visible: charging()" href="#" class="button-primary" value="<?php _e( 'Charging', 'hbook-admin' ); ?>" />
						<a data-bind="click: $root.cancel_edit_charge" href="#" class="button"><?php _e( 'Cancel', 'hbook-admin' ); ?></a>
					</div>
				</div>
				<!-- /ko -->
				
				<!-- /ko -->
				
			</td>
			<td data-bind="text: received_on"></td>
			<td>
				<?php if ( $this->user_can_edit() ) : ?>
				<!-- ko if: status() != 'processing' -->
				<a href="#" title="<?php _e( 'Confirm', 'hbook-admin' ); ?>" class="dashicons dashicons-yes" data-bind="click: $root.mark_read_resa, visible: ! action_processing() && status() == 'new'"></a>
				<a href="#" title="<?php _e( 'Confirm', 'hbook-admin' ); ?>" class="dashicons dashicons-yes" data-bind="click: $root.confirm_resa, visible: ! action_processing() && status() == 'pending'"></a>
				<a href="#" title="<?php _e( 'Cancel', 'hbook-admin' ); ?>" class="dashicons dashicons-no" data-bind="click: $root.cancel_resa, visible: ! action_processing() && status() != 'cancelled'"></a>
				<a href="#" title="<?php _e( 'Delete', 'hbook-admin' ); ?>" class="dashicons dashicons-trash" data-bind="click: $root.delete_resa, visible: ! action_processing()"></a>
				<span data-bind="visible: updating" class="hb-ajaxing hb-resa-updating">
					<span class="spinner"></span>
					<span><?php _e( 'Processing...', 'hbook-admin' ); ?></span>
				</span>
				<span data-bind="visible: deleting" class="hb-ajaxing hb-resa-updating">
					<span class="spinner"></span>
					<span><?php _e( 'Deleting...', 'hbook-admin' ); ?></span>
				</span>
				<!-- /ko -->
				<?php endif ?>
			</td>
		</tr>
	
	<?php
	}

	private function display_resa_pagination() {
	?>
	
	<!-- ko if: total_pages() > 1 -->
	<p>
		<a href="#" class="button" data-bind="click: first_page">&laquo;</a>
		<a href="#" class="button" data-bind="click: previous_page">&lsaquo;</a>
		&nbsp;&nbsp;
		<?php printf( __( 'Viewing page %s of %s', 'hbook-admin' ), '<span data-bind="text: current_page_number"></span>', '<span data-bind="text: total_pages"></span>' ); ?>
		&nbsp;&nbsp;
		<a href="#" class="button" data-bind="click: next_page">&rsaquo;</a>
		<a href="#" class="button" data-bind="click: last_page">&raquo;</a>
	</p>
	<!-- /ko -->
	
	<?php
	}
	
	private function user_can_edit() {
		if ( current_user_can( 'manage_resa' ) || current_user_can( 'manage_options' ) ) {
			return true;
		} else {
			return false;
		}
	}
	
}