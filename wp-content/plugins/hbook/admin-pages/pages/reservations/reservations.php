<?php
class HbAdminPageReservations extends HbAdminPage {
	
	private $accom_list;
	private $blocked_accom_displayer;
	private $resa_exporter_displayer;
	private $sync_errors_displayer;
	private $add_resa_displayer;
	private $resa_display_helper;
	private $customers_displayer;
	
	public function __construct( $page_id, $hbdb, $utils, $options_utils ) {
		$hb_text = array(
			'new' => __( 'New', 'hbook-admin' ),
			'pending' => __( 'Pending', 'hbook-admin' ),
			'confirmed' => __( 'Confirmed', 'hbook-admin' ),
			'cancelled' => __( 'Cancelled', 'hbook-admin' ),
			'processing' => __( 'Processing', 'hbook-admin' ),
            'not_allocated' => __( '(not allocated)', 'hbook-admin' ),
            'paid' => __( 'Paid', 'hbook-admin' ),
            'unpaid' => __( 'Unpaid', 'hbook-admin' ),
            'not_fully_paid' => __( 'Not fully paid', 'hbook-admin' ),
            'paid_details' => __( 'Paid:', 'hbook-admin' ),
            'to_be_paid_details' => __( 'To be paid:', 'hbook-admin' ),
			'confirm_delete_resa' => __( 'Delete reservation?', 'hbook-admin' ),
			'select_accom_num' => __( 'Select accommodation:', 'hbook-admin' ),
			'accom_not_selected' => __( 'Please select an accommodation.', 'hbook-admin' ),
			'customer_not_selected' => __( 'Please enter a customer id or customer details.', 'hbook-admin' ),
			'customer_id_not_valid' => __( 'Please enter the id of an existing customer.', 'hbook-admin' ),
			'select_accom_none' => __( 'No accommodation available.', 'hbook-admin' ),
			'info_adults' => __( 'Adults:', 'hbook-admin' ),
			'info_children' => __( 'Children:', 'hbook-admin' ),
			'invalid_price' => __( 'Invalid price.', 'hbook-admin' ),
			'customer_id' => __( 'Customer id:', 'hbook-admin' ),
			'more_info' => __( 'More information', 'hbook-admin' ),
			'less_info' => __( 'Less information', 'hbook-admin' ),
			'admin_comment' => __( 'Comment:', 'hbook-admin' ),
            'error' => __( 'Error:', 'hbook-admin' ),
            'no_accom_available_on_confirmed' => __( 'The reservation could not be confirmed because there is no accommodation available for the reservation date.', 'hbook-admin' ),
            'no_export_data_selected' => __( 'Please select the data you want to export.', 'hbook-admin' ),
			'confirm_delete_blocked_accom' => __( 'Remove blocked dates?', 'hbook-admin' ),
			'all' => __( 'All', 'hbook-admin' ),
			'confirm_delete_sync_errors' => __( 'Delete synchronization errors messages?', 'hbook-admin' ),
			'charge_amount_too_high' => __( 'The charge amount can not be above %amount', 'hbook-admin' )
		);

		$resa = $hbdb->get_all_resa_by_date();
		foreach ( $resa as $key => $resa_data ) {
            $resa[ $key ]['old_currency'] = '';
			if ( $resa[ $key ]['currency'] != get_option( 'hb_currency' ) ) {
                $resa[ $key ]['old_currency'] = '(' . $resa[ $key ]['currency'] . ')';
            }
			if ( $resa[ $key ]['payment_status'] != '' ) {
				$resa[ $key ]['payment_status'] = '<div class="hb-payment-status" title="' . $resa[ $key ]['payment_status'] . '">' . $resa[ $key ]['payment_status'] . '</div>';
			}
			if ( $resa[ $key ]['payment_status_reason'] != '' ) {
				$resa[ $key ]['payment_status'] .= '<div class="hb-payment-status" title="' . ucfirst( $resa[ $key ]['payment_status_reason'] ) . '">' . ucfirst( $resa[ $key ]['payment_status_reason'] ) . '</div>';
			}
			
            $resa[ $key ]['non_editable_info'] = $utils->resa_non_editable_info_markup( $resa_data );
			
			$resa[ $key ]['received_on'] = $utils->get_blog_datetime( $resa[ $key ]['received_on'] );
		}
		
		$this->accom_list = $hbdb->get_all_accom();
		$accom_tmp = array();
		foreach ( $this->accom_list as $accom_id => $accom_name ) {
            $accom_num_name = $hbdb->get_accom_num_name( $accom_id );
			$accom_tmp[ $accom_id ] = array(
				'name' => $accom_name,
				'number' => get_post_meta( $accom_id, 'accom_quantity', true ),
                'num_name' => $accom_num_name
			);
		}
		$accom_info = $accom_tmp;

		$month_short_name = __( 'Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec', 'hbook-admin' );
		$month_short_name = explode( ',', $month_short_name );
		$days_short_name = __( 'Sun,Mon,Tue,Wed,Thu,Fri,Sat', 'hbook-admin' );
		$days_short_name = explode( ',', $days_short_name );
		
		$customer_fields = $hbdb->get_customer_form_fields();
		$customer_fields_tmp = array();
		foreach ( $customer_fields as $field ) {
			$customer_fields_tmp[ $field['id'] ] = $field['name'];
		}
		$customer_fields = $customer_fields_tmp;
		
		$additional_info_fields = $hbdb->get_additional_booking_info_form_fields();
		$additional_info_fields_tmp = array();
		foreach ( $additional_info_fields as $field ) {
			$additional_info_fields_tmp[ $field['id'] ] = $field['name'];
		}
		$additional_info_fields = $additional_info_fields_tmp;
		
		$new_resa_status = 'confirmed';
		if ( get_option( 'hb_resa_admin_status' ) == 'new' ) {
			$new_resa_status = 'new';
		}
		
		$this->data = array(
			'resa' => $resa,
			'accoms' => $accom_info,
			'hb_text' => $hb_text,
			'month_short_name' => $month_short_name,
			'days_short_name' => $days_short_name,
            'hb_price_precision' => get_option( 'hb_price_precision' ),
            'hb_customer_ids' => $hbdb->get_customer_ids(),
            'hb_blocked_accom' => $hbdb->get_all_blocked_accom(),
			'hb_customer_fields' => $customer_fields,
			'hb_additional_info_fields' => $additional_info_fields,
			'hb_customers' => $hbdb->get_all( 'customers' ),
			'hb_new_resa_status' => $new_resa_status,
			'hb_stripe_active' => get_option( 'hb_stripe_active' ),
		);
		parent::__construct( $page_id, $hbdb, $utils, $options_utils );
		
		require_once plugin_dir_path( __FILE__ ) . 'blocked-accom-display.php';
		$this->blocked_accom_displayer = new HbAdminPageReservationsBlockedAccom( $this->accom_list );
		require_once plugin_dir_path( __FILE__ ) . 'resa-exporter-display.php';
		$this->resa_exporter_displayer = new HbAdminPageReservationsExport( $this->utils );
		require_once plugin_dir_path( __FILE__ ) . 'resa-sync-errors-display.php';
		$this->sync_errors_displayer = new HbAdminPageReservationsSyncErrors( $this->hbdb );
		require_once plugin_dir_path( __FILE__ ) . 'add-resa.php';
		$this->add_resa_displayer = new HbAdminPageReservationsAddResa( $this->hbdb, $this->utils );
		require_once plugin_dir_path( __FILE__ ) . 'resa-display.php';
		$this->resa_display_helper = new HbAdminPageReservationsDisplayHelper( $this->accom_list );
		require_once plugin_dir_path( __FILE__ ) . 'customers-display.php';
		$this->customers_displayer = new HbAdminPageReservationsCustomers();
	}
	
	public function display() {
	?>

	<div class="wrap">

		<h1><?php _e( 'Reservations', 'hbook-admin' ); ?></h1><hr/>
		
		<?php 
		if ( current_user_can( 'manage_resa' ) || current_user_can( 'manage_options' ) ) { 
			$this->sync_errors_displayer->display();
		}
		$this->resa_display_helper->display_resa_details();
		$this->resa_display_helper->display_resa_calendar();
		if ( current_user_can( 'manage_resa' ) || current_user_can( 'manage_options' ) ) { 
			$this->blocked_accom_displayer->display();
			$this->add_resa_displayer->display();
		}
		$this->resa_display_helper->display_resa_list();
		if ( current_user_can( 'manage_resa' ) || current_user_can( 'manage_options' ) ) { 
			$this->resa_exporter_displayer->display();
		}
		//$this->customers_displayer->display();
		?>
		
	</div><!-- end .wrap -->

	<?php
	}
	
}