<?php 
class HbAdminPageReservationsCustomers {
	
	public function display() {
	?>
		
		<div class="hb-resa-section">
			
			<h3 id="hb-customers-toggle" class="hb-resa-section-toggle">
				<?php _e( 'Customers', 'hbook-admin' ); ?>
				<span class="dashicons dashicons-arrow-up"></span>
				<span class="dashicons dashicons-arrow-down"></span>
			</h3>
						
			<div id="hb-customers">
				
				<!-- ko if: customers_list().length == 0 -->
				<?php _e( 'There is not any customers yet.', 'hbook-admin' ); ?>
				<!-- /ko -->
				
				<!-- ko if: customers_list().length > 0 -->
				<div class="hb-table hb-customers-table">
				
					<div class="hb-table-head hb-clearfix">
						<div class="hb-table-head-data hb-table-head-data-customer-id"><?php _e( 'Id', 'hbook-admin' ); ?></div>
						<div class="hb-table-head-data"><?php _e( 'First name', 'hbook-admin' ); ?></div>
						<div class="hb-table-head-data"><?php _e( 'Last name', 'hbook-admin' ); ?></div>
						<div class="hb-table-head-data"><?php _e( 'Email', 'hbook-admin' ); ?></div>
						<div class="hb-table-head-data"><?php _e( 'Other informations', 'hbook-admin' ); ?></div>
						<div class="hb-table-head-data hb-table-head-data-action"><?php _e( 'Actions', 'hbook-admin' ); ?></div>
					</div>
					
					<!-- ko foreach: customers_list -->
					<div class="hb-table-row hb-clearfix">
						<div data-bind="attr: { class: anim_class }">
							<div class="hb-table-data hb-table-head-data-customer-id" data-bind="text: id"></div>
							<div class="hb-table-data" data-bind="text: first_name"></div>
							<div class="hb-table-data" data-bind="text: last_name"></div>
							<div class="hb-table-data" data-bind="text: email"></div>
							<div class="hb-table-data" data-bind="html: other_info"></div>
							<div class="hb-table-data hb-table-data-action">
								<a href="#" title="<?php _e( 'Delete', 'hbook-admin' ); ?>" class="dashicons dashicons-trash" data-bind="visible: ! deleting(), click: $root.delete_blocked_accom"></a>
								<span data-bind="visible: deleting" class="hb-ajaxing hb-resa-updating">
									<span class="spinner"></span>
									<span><?php _e( 'Deleting...', 'hbook-admin' ); ?></span>
								</span>
							</div>
						</div>
					</div>
					<!-- /ko -->
					
				</div>
				<!-- /ko -->
				
			</div>
			
		</div>
		
		<hr/>
		
	<?php
	}
	
}