function hb_date_to_str( date ) {
    var year = date.getFullYear(),
        month = date.getMonth() + 1,
        day = date.getDate();
    day = day + '';
    if ( day.length == 1 ) {
        day = '0' + day;
    }
    month = month + '';
    if ( month.length == 1 ) {
        month = '0' + month;
    }
    var str = year + '-' + month + '-' + day;
    return str;
}

function hb_date_str_to_obj( str_date ) {
    var array_date = str_date.split( '-' );
    return new Date( array_date[0], array_date[1] - 1, array_date[2] );
}

function get_day_tds( type, the_day, accom_id, accom_num ) {

    var tds = '',
        today = hb_date_to_str( new Date() );
    
    var yesterday = new Date();
    yesterday.setDate( yesterday.getDate() -1 );
    
    var current_day = hb_date_str_to_obj( the_day );
    current_day.setDate( current_day.getDate() - 7 );
    current_day.setTime( current_day.getTime() + 1000*3600*2 );
    
    var first_day = hb_date_str_to_obj ( the_day );
    first_day.setDate( first_day.getDate() - 7 );
    
    var last_day = hb_date_str_to_obj( the_day );
    last_day.setDate( last_day.getDate() + 28 );

    while ( current_day <= last_day ) {

        switch ( type ) {

            case 'days-table':
                var day_label_class = '';
                if ( hb_date_to_str( current_day ) == today ) {
                    day_label_class = 'hb-resa-day-today';
                } else if ( hb_date_to_str( current_day ) == hb_date_to_str( yesterday ) ) {
                    day_label_class = 'hb-resa-day-yesterday';
                }
                tds += '<td colspan="2" class="hb-resa-day-label ' + day_label_class + '">' +
                        '<span><b>' + days_short_name[ current_day.getDay() ] + '</b>' +
                            '<span>' + current_day.getDate() + '<br/>' + month_short_name[ current_day.getMonth() ] + '<br/>' + '</span>' +
                        '</span>' +
                    '</td>';
            break;

            case 'days-accom-availability':
                tds += '<td colspan="2" class="hb-resa-day-availability hb-resa-cal-day-' + hb_date_to_str( current_day ) + '"></td>';
            break;

            case 'days-cal':
                var data = 'data-accom-id="' + accom_id + '" data-accom-num="' + accom_num + '" data-day="' + hb_date_to_str( current_day ) + '"';
                var resa_day_class = '';
                if ( hb_date_to_str( current_day ) == today ) {
                    resa_day_class = 'hb-resa-day-today-line';
                } else if ( hb_date_to_str( current_day ) == hb_date_to_str( yesterday ) ) {
                    resa_day_class = 'hb-resa-day-today-line';
                }
                tds += '<td ' + data + ' class="hb-resa-checkinout-day"><div></div><span></span></td>' +
                        '<td ' + data + ' class="hb-resa-day ' + resa_day_class + '"></td>';
            break;

        }

        current_day.setDate( current_day.getDate() + 1 );
    }
    return tds;
}

function hb_create_resa_cal_tables( day, displayed_accoms ) {
    var accom_trs = '',
        day_tds = '',
        cal_trs = '',
        go_to_previous_month = '',
        go_to_next_month = '',
        c_day = hb_date_str_to_obj( day ),
        two_weeks_before_the_day = hb_date_str_to_obj( day ),
        two_weeks_after_the_day = hb_date_str_to_obj( day );

    jQuery.each( displayed_accoms, function( accom_id, accom ) {
        jQuery.each( accom.num_name, function( accom_num, num_name ) {
            accom_trs += '<tr><td> ';
            accom_trs += accom.name;
            if ( num_name ) {
                accom_trs += ' (' + num_name + ')';
            }
            accom_trs += '</td></tr>';
            cal_trs += '<tr>' + get_day_tds( 'days-cal', day, accom_id, accom_num ) + '</tr>';
        });
    });

    two_weeks_after_the_day.setDate( c_day.getDate() + 14 );
    two_weeks_before_the_day.setDate( c_day.getDate() - 14 );

    var first_day = hb_date_str_to_obj ( day );
    first_day.setDate( first_day.getDate() - 7 );
    
    var last_day = hb_date_str_to_obj( day );
    last_day.setDate( last_day.getDate() + 27 );
    
    var calendar_range = first_day.getDate() + ' ' + month_short_name[ first_day.getMonth() ] + ' ' + first_day.getFullYear() + ' - ' + last_day.getDate() + ' ' + month_short_name[ last_day.getMonth() ] + ' ' + last_day.getFullYear();
    var calendar_controls = '<a href="#" class="hb-go-to-previous-two-weeks button" data-day="' + hb_date_to_str( two_weeks_before_the_day ) + '">&lsaquo;</a><a href="#" class="hb-display-calendar button"><span class="dashicons dashicons-calendar-alt"></span></a><a href="#" class="hb-go-to-next-two-weeks button" data-day="' + hb_date_to_str( two_weeks_after_the_day ) + '">&rsaquo;</a>';
    jQuery( '#hb-resa-accom-table' ).html( 
        '<tr>' +
            '<td id="hb-commands-top" class="hb-resa-cal-commands">' +
                '<span class="calendar-range" data-last-day="' + hb_date_to_str( last_day ) + '">' + 
                    calendar_range  +
                '</span>' + 
                '<div class="hb-calendar-controls">' + 
                    calendar_controls + 
                '</div>' + 
            '</td>' +
        '</tr>' +
        accom_trs +
        '<tr>' + 
            '<td id="hb-commands-bottom" class="hb-resa-cal-commands">' +
                '<span class="calendar-range">' + 
                    calendar_range +
                '</span>' + 
                '<div class="hb-calendar-controls bottom">' + 
                    calendar_controls + 
                '</div>' + 
            '</td>' + 
        '</tr>'
    );
    jQuery( '#hb-resa-cal-table' ).html( '<tr>' + get_day_tds( 'days-table', day ) + '</tr>' + cal_trs + '<tr id="hb-cal-days-bottom">' + get_day_tds( 'days-table', day ) + '</tr>' );
    jQuery( '#hb-resa-cal-table' ).data( 'first-day', day );
}

function hb_set_resa_cal( all_resa, all_blocked_accom, all_customers ) {
    if ( ! jQuery( '.calendar-range' ).length ) {
        return;
    }
    
    var resa_day_today_lines = jQuery( '.hb-resa-day-today-line' );
    jQuery( '.hb-resa-checkinout-day' ).removeClass().addClass( 'hb-resa-checkinout-day' );
    jQuery( '.hb-resa-day' ).removeClass().addClass( 'hb-resa-day' );
    resa_day_today_lines.addClass( 'hb-resa-day-today-line' );
    jQuery( '.hb-resa-day a' ).remove();

	var td_width = jQuery( '.hb-resa-day' ).outerWidth();

    for ( var i = 0; i < all_blocked_accom.length; i++ ) {
        var data_accom_id_num = '',
			from_date,
			to_date,
			blocked_nb_days,
			blocked_accom_comment;
		
		from_date = all_blocked_accom[i].from_date;
		to_date = all_blocked_accom[i].to_date;
		if ( from_date == '0000-00-00' ) {
			from_date = '2016-01-01';
		}		
		if ( to_date == '0000-00-00' ) {
			to_date = '2029-12-31';
		}
		if ( ! all_blocked_accom[i].accom_all_ids ) {
	        data_accom_id_num = '[data-accom-id="' + all_blocked_accom[i].accom_id + '"]';
	        if ( ! all_blocked_accom[i].accom_all_num ) {
	            data_accom_id_num += '[data-accom-num="' + all_blocked_accom[i].accom_num + '"]';
	        }
		}
		
		blocked_nb_days = hb_nb_days( from_date, to_date )
		blocked_accom_comment = '<div class="hb-resa-day-blocked-comment"';
		var blocked_accom_comment_width = td_width * 2 * blocked_nb_days - Math.round( td_width / 2 );
		blocked_accom_comment += '" style="width:' + blocked_accom_comment_width + 'px"';
		blocked_accom_comment += ' title="' + all_blocked_accom[i].comment + '"">';
		if ( blocked_nb_days > 1 ) {
			blocked_accom_comment += '<span>' + all_blocked_accom[i].comment + '</span>';
		} else {
			blocked_accom_comment += '&nbsp;';
		}
		blocked_accom_comment += '</div>';
		
		jQuery( data_accom_id_num + '[data-day="' + from_date + '"].hb-resa-checkinout-day' ).addClass( 'hb-resa-day-taken-checkin hb-resa-day-checkin-blocked' );
		jQuery( data_accom_id_num + '[data-day="' + from_date + '"].hb-resa-day' ).addClass( 'hb-resa-day-taken hb-resa-day-blocked' ).html( blocked_accom_comment );
		if ( jQuery( data_accom_id_num + '[data-day="' + from_date + '"].hb-resa-day span' ).width() < jQuery( data_accom_id_num + '[data-day="' + from_date + '"].hb-resa-day div' ).width() ) {
			jQuery( data_accom_id_num + '[data-day="' + from_date + '"].hb-resa-day div' ).css( 'text-align', 'center' );
		}
        
        var current_day = hb_date_str_to_obj( from_date ),
			last_day = hb_date_str_to_obj( to_date );
            last_day_calendar = hb_date_str_to_obj( jQuery( '.calendar-range' ).data( 'last-day' ) );
            
		last_day.setDate( last_day.getDate() - 1 );

        if ( last_day > last_day_calendar ) {
            last_day = last_day_calendar;
        }
        
		while ( current_day < last_day ) {
			current_day.setDate( current_day.getDate() + 1 );
			jQuery( data_accom_id_num + '[data-day="' + hb_date_to_str( current_day ) + '"].hb-resa-checkinout-day' ).addClass( 'hb-resa-day-taken hb-resa-day-blocked' );
			jQuery( data_accom_id_num + '[data-day="' + hb_date_to_str( current_day ) + '"].hb-resa-day' ).addClass( 'hb-resa-day-taken hb-resa-day-blocked' );
		}

		jQuery( data_accom_id_num + '[data-day="' + to_date + '"].hb-resa-checkinout-day' ).addClass( 'hb-resa-day-taken-checkout hb-resa-day-checkout-blocked' );
	}
    
	for ( var i = 0; i < all_resa.length; i++ ) {
		if ( all_resa[i].status() != 'cancelled' && all_resa[i].status() != 'pending' ) {
            var data_accom_id_num = '[data-accom-id="' + all_resa[i].accom_id() + '"][data-accom-num="' + all_resa[i].accom_num() + '"]',
				customer = ko.utils.arrayFirst( all_customers, function( customer ) {
					return all_resa[i].customer_id() == customer.id;
				}),
				resa_nb_days = hb_nb_days( all_resa[i].check_in, all_resa[i].check_out ),
				customer_name = '',
            	resa_call_details_link;
            
			if ( customer ) {
				if ( customer.first_name() ) {
					customer_name = customer.first_name();
				}
				if ( customer.last_name() ) {
					if ( customer_name ) {
						customer_name += ' ';
					}
					customer_name += customer.last_name();
				}
			}
			
			resa_call_details_link = '<a href="#" class="hb-resa-day-call-details';
			if ( resa_nb_days == 1 && all_resa[i].id >= 1000 ) {
				resa_call_details_link += ' hb-resa-day-long-id'
			}
			if ( resa_nb_days > 1 ) {
				resa_call_details_link += ' hb-resa-multiple-nights'
				var resa_call_details_link_width = td_width * 2 * resa_nb_days - Math.round( td_width / 2 );
				resa_call_details_link += '" style="width:' + resa_call_details_link_width + 'px"';
			} else {
				resa_call_details_link += '"';
			}
			resa_call_details_link += ' title="' + all_resa[i].id + '.' + customer_name + '"';
			resa_call_details_link += ' data-resa-id="' + all_resa[i].id + '">';
			resa_call_details_link += all_resa[i].id;
			if ( resa_nb_days > 1 ) {
				resa_call_details_link += '<span>.' + customer_name + '</span>';
			}
			resa_call_details_link += '</a>';
				
			jQuery( data_accom_id_num + '[data-day="' + all_resa[i].check_in + '"].hb-resa-checkinout-day' ).addClass( 'hb-resa-day-taken-checkin hb-resa-day-checkin-' + all_resa[i].status() );
			jQuery( data_accom_id_num + '[data-day="' + all_resa[i].check_in + '"].hb-resa-day' ).addClass( 'hb-resa-day-taken hb-resa-day-' + all_resa[i].status() ).html( resa_call_details_link );
			
			if ( jQuery( data_accom_id_num + '[data-day="' + all_resa[i].check_in + '"].hb-resa-day span' ).width() < jQuery( data_accom_id_num + '[data-day="' + all_resa[i].check_in + '"].hb-resa-day a' ).width() ) {
				jQuery( data_accom_id_num + '[data-day="' + all_resa[i].check_in + '"].hb-resa-day a' ).css( 'text-align', 'center' );
			}

			var current_day = hb_date_str_to_obj( all_resa[i].check_in ),
				last_day = hb_date_str_to_obj( all_resa[i].check_out );

			last_day.setDate( last_day.getDate() - 1 );

			while ( current_day < last_day ) {
				current_day.setDate( current_day.getDate() + 1 );
				jQuery( data_accom_id_num + '[data-day="' + hb_date_to_str( current_day ) + '"].hb-resa-checkinout-day' ).addClass( 'hb-resa-day-taken hb-resa-day-' + all_resa[i].status() );
				jQuery( data_accom_id_num + '[data-day="' + hb_date_to_str( current_day ) + '"].hb-resa-day' ).addClass( 'hb-resa-day-taken hb-resa-day-' + all_resa[i].status() );
			}

			jQuery( data_accom_id_num + '[data-day="' + all_resa[i].check_out + '"].hb-resa-checkinout-day' ).addClass( 'hb-resa-day-taken-checkout hb-resa-day-checkout-' + all_resa[i].status() );
		}
	}
}

jQuery( document ).ready( function( $ ) {

	$( '#hb-resa-cal-scroller').scroll( function() {
		$( '#hb-resa-accom-table' ).css( 'left', $( this ).scrollLeft() );
		$( '#hb-resa-cal-commands' ).css( { 'left': $( this ).scrollLeft(), 'top': $( this ).scrollTop() } );
	});

    function set_month_picker( year ) {
        var months = '',
            year_before = new Date( year - 1, 0, 1 ),
            year_after = new Date( year + 1, 0, 1),
            current_day = new Date( year, 0, 1 );
        
        $( '.hb-go-to-previous-year' ).data( 'year', year - 1 );
		$( '.hb-go-to-next-year' ).data( 'year', year + 1 );
        for ( var i=1; i <= 12 ; i++ ) {
            var data = 'data-day="' + hb_date_to_str( current_day ) + '"';
            months += '<a href="#" class="hb-month button"' + data + '>' + month_short_name[ current_day.getMonth() ] + ' ' + current_day.getFullYear() + '</a>';
            current_day.setMonth( current_day.getMonth() + 1 );
            current_day.setDate( 1 );
        }
        $( '.hb-months' ).html( months );
    }

	var month_picker_controls = '<a href="#" class="hb-go-to-previous-year button">&lsaquo;</a>';
    month_picker_controls += '<a href="#" class="hb-go-to-next-year button">&rsaquo;</a>';
    var month_picker = '<div class="hb-month-picker"><div class="hb-month-picker-controls">';
    month_picker += month_picker_controls;
    month_picker += '</div><div class="hb-months"></div></div>';
	$( 'body' ).append( month_picker );
    
    var today = new Date(),
        today_year = today.getFullYear();
    set_month_picker( today_year );

	$( '#hb-resa-cal-wrapper' ).on( 'click', '.hb-display-calendar', function() {
        if ( $( '.hb-month-picker').is( ':visible' ) ) {
            $( '.hb-month-picker').slideUp();
        } else {
            var coordinate = $( this ).offset();
            if ( $( this ).parent().hasClass( 'bottom' ) ) {
                $( '.hb-month-picker').css( 'top', coordinate[ 'top' ] - 210 );
            } else {
                $( '.hb-month-picker').css( 'top', coordinate[ 'top' ] + 37 );
            }
            $( '.hb-month-picker').css( 'left', coordinate[ 'left' ] - 100 );
            $( '.hb-month-picker').slideDown();
        }
	});
    
	$( 'body' ).on( 'click', '.hb-go-to-previous-year, .hb-go-to-next-year', function() {
        set_month_picker( $( this ).data( 'year' ) );
		return false;
	});

});