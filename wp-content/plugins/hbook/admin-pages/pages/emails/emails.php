<?php
class HbAdminPageEmails extends HbAdminPage {
    
    public function __construct( $page_id, $hbdb, $utils, $options_utils ) {
        $langs = $utils->get_langs();
        $hb_email_langs = array();
        foreach ( $langs as $locale => $lang_name ) {
            $email_lang = array(
                'lang_value' => $locale,
                'lang_name' => $lang_name
            );
            $hb_email_langs[] = $email_lang;
        }
        $hb_email_langs[] = array(
            'lang_value' => 'all',
            'lang_name' => __( 'All', 'hbook-admin' )
        );
        $this->data = array(
            'hb_text' => array(
                'new_email_tmpl' => __( 'New email template', 'hbook-admin' ),
				'invalid_email_address' => __( 'This e-mail address does not seem valid.', 'hbook-admin' ),
				'invalid_multiple_to_address' => __( 'Please seperate multiple e-mail addresses with commas.', 'hbook-admin' ),
				'invalid_from_address' => __( 'Please use a complete e-mail address eg. Your Name <email@domain.com>', 'hbook-admin' )
            ),
            'email_tmpls' => $hbdb->get_all( 'email_templates' ),
            'hb_email_langs' => $hb_email_langs,
            'hb_email_actions' => array(
                array(
                    'action_value' => 'new_resa',
                    'action_text' => __( 'New reservation', 'hbook-admin' ),
                ),
                array(
                    'action_value' => 'confirmation_resa',
                    'action_text' => __( 'Reservation confirmation', 'hbook-admin' ),
                ),
                array(
                    'action_value' => 'cancellation_resa',
                    'action_text' => __( 'Reservation cancellation', 'hbook-admin' ),
                ),
				array(
                    'action_value' => 'new_resa_admin',
                    'action_text' => __( 'New reservation (from admin)', 'hbook-admin' ),
                ),
                array(
                    'action_value' => 'not_automatic',
                    //'action_text' => __( 'Not automatically sent', 'hbook-admin' ),
                    'action_text' => __( 'Inactive', 'hbook-admin' )
                )
            ),
        );
		parent::__construct( $page_id, $hbdb, $utils, $options_utils );
	}
	
	public function display() {
    ?>
    
    <div class="wrap">
    
        <h2>
			<?php _e( 'Email templates', 'hbook-admin' ); ?>
			<a href="#" class="add-new-h2" data-bind="click: create_email_tmpl"><?php _e( 'Add new email template', 'hbook-admin' ); ?></a>
			<span class="hb-add-new spinner"></span>
		</h2>
       
        <?php $this->display_right_menu(); ?>
    
        <br/>
		
		<!-- ko if: email_tmpls().length == 0 -->
		<?php _e( 'No email templates set yet.', 'hbook-admin' ); ?>
		<!-- /ko -->
		
		<!-- ko if: email_tmpls().length > 0 -->
		<?php
        $table_class = 'hb-table hb-email-tmpls-table';
        if ( $this->utils->is_site_multi_lang() ) {
            $table_class .= ' hb-email-multiple-lang';
        }
        ?>
        
		<div class="<?php echo( $table_class ); ?>">
		
			<div class="hb-table-head hb-clearfix">
				<div class="hb-table-head-data"><?php _e( 'Name', 'hbook-admin' ); ?></div>
				<div class="hb-table-head-data hb-data-addresses"><?php _e( 'To address', 'hbook-admin' ); ?></div>
				<div class="hb-table-head-data hb-data-addresses"><?php _e( 'From address', 'hbook-admin' ); ?></div>
				<div class="hb-table-head-data"><?php _e( 'Subject', 'hbook-admin' ); ?></div>
				<div class="hb-table-head-data hb-data-message"><?php _e( 'Message', 'hbook-admin' ); ?></div>
				<div class="hb-table-head-data"><?php _e( 'Format', 'hbook-admin' ); ?></div>
				<div class="hb-table-head-data"><?php _e( 'Send on', 'hbook-admin' ); ?></div>
				<div class="hb-table-head-data hb-data-email-lang"><?php _e( 'For language', 'hbook-admin' ); ?></div>
				<div class="hb-table-head-data hb-table-head-data-action"><?php _e( 'Actions', 'hbook-admin' ); ?></div>
			</div>
			<div data-bind="template: { name: template_to_use, foreach: email_tmpls, beforeRemove: hide_setting }"></div>

			<script id="text_tmpl" type="text/html">
				<div class="hb-table-row hb-clearfix">
					<div class="hb-table-data" data-bind="text: name"></div>
					<div class="hb-table-data hb-data-addresses" data-bind="html: to_address_html"></div>
					<div class="hb-table-data hb-data-addresses" data-bind="html: from_address_html"></div>
					<div class="hb-table-data" data-bind="text: subject"></div>
					<div class="hb-table-data hb-data-message" data-bind="html: message_html"></div>
					<div class="hb-table-data" data-bind="text: format"></div>
					<div class="hb-table-data" data-bind="text: action_text"></div>
					<div class="hb-table-data hb-data-email-lang" data-bind="text: lang_text"></div>
					<div class="hb-table-data hb-table-data-action"><?php $this->display_admin_action(); ?></div>
				</div>
			</script>

			<script id="edit_tmpl" type="text/html">
				<div class="hb-table-row hb-clearfix">
					<div class="hb-table-data"><input data-bind="value: name" type="text" /></div>
					<div class="hb-table-data hb-data-addresses"><input data-bind="value: to_address" type="text" /></div>
					<div class="hb-table-data hb-data-addresses"><input data-bind="value: from_address" type="text" /></div>
					<div class="hb-table-data"><input data-bind="value: subject" type="text" /></div>
					<div class="hb-table-data hb-data-message"><textarea class="hb-template-email-message" data-bind="value: message" /></textarea></div>
                    <div class="hb-table-data">
                        <input data-bind="checked: format" name="format" id="format_text" type="radio" value="TEXT" />
                        <label for="format_text"><?php _e( 'TEXT', 'hbook-admin' ); ?></label><br/>
                        <input data-bind="checked: format" name="format" id="format_html" type="radio" value="HTML" />
                        <label for="format_html"><?php _e( 'HTML', 'hbook-admin' ); ?></label>
                    </div>
                    <div class="hb-table-data">
                        <select data-bind="options: hb_email_actions, optionsValue: 'action_value', optionsText: 'action_text', value: action">
                        </select>
                    </div>
                    <div class="hb-table-data hb-data-email-lang">
                        <select data-bind="options: hb_email_langs, optionsValue: 'lang_value', optionsText: 'lang_name', value: lang">
                        </select>
                    </div>
					<div class="hb-table-data hb-table-data-action"><?php $this->display_admin_on_edit_action(); ?></div>
				</div>
			</script>
			
        </div>
        
		<h4><?php _e( '"To address" fields:', 'hbook-admin' ); ?></h4>
        <p><?php _e( 'Separate multiple e-mail addresses with commas eg. <b>email-1@domain.com,email-2@domain.com</b>', 'hbook-admin' ); ?></p>
        <h4><?php _e( '"From address" fields:', 'hbook-admin' ); ?></h4>
        <p><?php _e( 'Insert a complete e-mail address (a name followed by an email address wrapped between <b style="font-weight:900; font-size: 15px">&lt;</b> and <b style="font-weight:900; font-size: 15px">&gt;</b>) eg. <b>Your Name &lt;your.email@domain.com&gt;</b>', 'hbook-admin' ); ?></p>
        <h4><?php _e( '"To address" and "From address" fields:', 'hbook-admin' ); ?></h4>
        <p><?php _e( 'If the field is blank the email address of the WordPress administrator will be use.', 'hbook-admin' ); ?></p>
        <h4><?php _e( '"To address", "From address", "Subject" and "Message" fields:', 'hbook-admin' ); ?></h4>
        <p>
        
        <?php _e( 'You can use the following variables:', 'hbook-admin' ); ?><br/>
        
        <?php
        $available_var = array(
			'[resa_id]', '[resa_check_in]', '[resa_check_out]', '[resa_number_of_nights]', '[resa_accommodation]', 
			'[resa_accommodation_num]', '[resa_adults]', '[resa_children]', '[resa_admin_comment]', '[resa_extras]','[resa_price]', 
			'[resa_deposit]', '[resa_paid]', '[resa_remaining_balance]', '[resa_received_on]'
		);

		$resa_additional_fields = $this->hbdb->get_additional_booking_info_form_fields();
		foreach ( $resa_additional_fields as $field ) {
			$available_var[] = '[resa_' . $field['id'] . ']';
		}
		
		$available_var[] = '[customer_id]';
		$customer_fields = $this->hbdb->get_customer_form_fields();
		foreach ( $customer_fields as $field ) {
			$available_var[] = '[customer_' . $field['id'] . ']';
		}
		
		$available_var = implode( ' &nbsp;-&nbsp; ', $available_var );
        echo( $available_var );
        ?>
        
        </p>
        
    	<!-- /ko -->
    	
    </div>
    
    <?php
    }
    
}