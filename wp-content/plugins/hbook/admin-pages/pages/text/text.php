<?php
class HbAdminPageText extends HbAdminPage {
	
	private $sections;
	private $strings;
	private $langs;
	private $variables;
	
	public function __construct( $page_id, $hbdb, $utils, $options_utils ) {
		$this->data = array(
			'hb_text' => array(
				'form_saved' => __( 'All text has been saved.', 'hbook-admin' ),
			)
		);
		$this->sections = array(
			'search-form-txt' => array(
				'title' => __( 'Search form text', 'hbook-admin' ),
				'strings' => $utils->get_search_form_txt()
			),
			'search-form' => array(
				'title' => __( 'Search form messages', 'hbook-admin' ),
				'strings' => $utils->get_search_form_msg()
			),
			'accom-select' => array(
				'title' => __( 'Accommodation selection', 'hbook-admin' ),
				'strings' => array_merge( $utils->get_accom_selection_txt(), $hbdb->get_fee_names() ),
			),
            'options-select' => array(
				'title' => __( 'Extra services selection', 'hbook-admin' ),
				'strings' => array_merge( $utils->get_options_selection_txt(), $hbdb->get_option_names() ),
			),
			'details-form-txt' => array(
				'title' => __( 'Booking details form text', 'hbook-admin' ),
				'strings' => $hbdb->get_form_labels( 'booking' ),
			),
			'details-form-msg' => array(
				'title' => __( 'Booking details form messages', 'hbook-admin' ),
				'strings' => $utils->get_details_form_msg()
			),
			'summary' => array(
				'title' => __( 'Summary', 'hbook-admin' ),
				'strings' => $utils->get_summary_txt()
			),
            'payment-choice' => array(
				'title' => __( 'Payment choice', 'hbook-admin' ),
				'strings' => $utils->get_payment_type_choice()
			),
			'stripe' => array(
				'title' => __( 'Stripe payment', 'hbook-admin' ),
				'strings' => $utils->get_stripe_txt()
			),
			'paypal' => array(
				'title' => __( 'Paypal payment', 'hbook-admin' ),
				'strings' => $utils->get_paypal_txt()
			),
			'external-payment-desc' => array(
				'title' => __( 'Paypal payment description', 'hbook-admin' ),
				'strings' => $utils->get_external_payment_desc_txt()
			),
			'book-now-area' => array(
                'title' => __( 'Book now area', 'hbook-admin' ),
                'strings' => $utils->get_book_now_area_txt(), 
            ),
			'error-msg' => array(
				'title' => __( 'Error messages', 'hbook-admin' ),
				'strings' => $utils->get_error_form_msg()
			),
			'cal-legend' => array(
				'title' => __( 'Calendars legend', 'hbook-admin' ),
				'strings' => $utils->get_cal_legend_txt()
			),
			'rates-table' => array(
				'title' => __( 'Rates table', 'hbook-admin' ),
				'strings' => $utils->get_rates_table_txt()
			),
			'accom-list' => array(
				'title' => __( 'Accommodation list', 'hbook-admin' ),
				'strings' => $utils->get_accom_list_txt()
			)
		);
		if ( $utils->is_htw_theme_active() ) {
			$this->sections['contact-form'] = array(
				'title' => __( 'Contact form', 'hbook-admin' ),
				'strings' => array_merge( $hbdb->get_form_labels( 'contact' ), $utils->get_contact_form_txt() )
			);
		}
		$this->strings = $hbdb->get_all_strings();
		$this->langs = $utils->get_langs();
		$this->variables = $utils->get_txt_variables();
		parent::__construct( $page_id, $hbdb, $utils, $options_utils );
	}
	
	public function display() {
	?>

	<div class="wrap">

		<form id="hb-admin-form">

			<input id="hb-nonce" type="hidden" name="nonce" value="" />
			<input id="hb-action" type="hidden" name="action" value="" />
			
			<div class="hb-clearfix">
				<h1><?php _e( 'HBook text', 'hbook-admin' ); ?></h1>
				<?php $this->display_right_menu(); ?>
				<div class="hb-options-save-beside-title">
					<div>
						<a href="#" class="hb-options-save button-primary"><?php _e( 'Save changes', 'hbook-admin' ); ?></a>
					</div>
					<div class="hb-ajaxing">
						<span class="spinner"></span>
						<span><?php _e( 'Saving...', 'hbook-admin' ); ?></span>
					</div>
					<div class="hb-saved"></div>
				</div>
			</div>

			<hr/>
			
			<?php
			foreach ( $this->sections as $section_id => $section ) {
			?>
			
			<h3 id="hb-text-section-<?php echo( $section_id ); ?>"><?php echo( $section['title'] ); ?></h3>
			
			<?php if ( $section_id == 'details-form-txt' ) { ?>
			<p><i><?php _e( 'Leave the following fields blank to use their default name.', 'hbook-admin' ); ?></i></p>
			<?php } ?>
			
				<?php
				foreach ( $section['strings'] as $string_id => $string_name ) {
				?>
				
				<h4><?php echo( $string_name ); ?></h4>
				<?php
				if ( isset( $this->variables[ $string_id ] ) ) {
				?>
					<small class="hb-variable-desc">
					<?php 
					if ( count( $this->variables[ $string_id ] ) > 1 ) {
						_e( 'You can use these variables:', 'hbook-admin' );
					} else {
						_e( 'You can use this variable:', 'hbook-admin' );
					}
					echo( ' ' );
					echo( implode( $this->variables[ $string_id ], ', ' ) );
					?>
					</small>
				<?php
				}
				?>
				<p>
				<?php
					foreach ( $this->langs as $locale => $lang_name ) {
						$translation = '';
						if ( isset( $this->strings[ $string_id ][ $locale ] ) ) {
							$translation = $this->strings[ $string_id ][ $locale ];
						}
						if ( count( $this->langs ) > 1 ) {
				?>
					<label class="hb-string-lang"><?php echo( $lang_name ); ?><span> (<?php echo( $locale );?>)</span></label><br/>
				<?php
						}
				?>
					<input type="text" name="string-id-<?php echo( $string_id ); ?>-in-<?php echo( $locale ); ?>" value="<?php echo( esc_attr( $translation ) ); ?>" />
				</p>
				<?php 
					}
				}
				?>
				
				<br class="hb-before-save-button" />
				<?php $this->options_utils->display_save_options_section(); ?>
				
			<?php
			}	
			?>
			
		</form>
		
	</div><!-- end .wrap -->

	<?php
	}
}