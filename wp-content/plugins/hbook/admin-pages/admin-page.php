<?php
class HbAdminPage {

	private $page_id;
	private $common_text;
	protected $data;
	protected $hbdb;
	protected $utils;
	protected $options_utils;
	protected $form_name;
	
	public function __construct( $page_id, $hbdb, $utils, $options_utils ) {
		$this->page_id = $page_id;
		$this->hbdb = $hbdb;
		$this->utils = $utils;
		$this->options_utils = $options_utils;
		$this->common_text = array(
			'all' => __( 'All', 'hbook-admin' ),
			'save' => __( 'Save', 'hbook-admin' ),
			'saving' => __( 'Saving...', 'hbook-admin' ),
			'confirm_delete' => __( 'Delete \'%setting_name\'?', 'hbook-admin' ),
			'confirm_delete_default' => __( 'Confirm deletion?', 'hbook-admin' ),
			'unsaved_warning' => __( 'It seems some changes have not been saved.', 'hbook-admin' ),
			'no_accom_selected' => __( 'No accommodation selected', 'hbook-admin' ),
		);
		$this->data['hb_text'] = array_merge( $this->common_text, $this->data['hb_text'] );
		$this->data['all_accom_ids'] = $this->hbdb->get_all_accom_ids();
        $ajax_timeout = intval( get_option( 'hb_ajax_timeout' ) );
        if ( ! $ajax_timeout ) {
            $ajax_timeout = 20000;
        }
        $this->data['hb_ajax_settings'] = array( 'timeout' => $ajax_timeout );
		foreach ( $this->data as $key => $value ) { 
			wp_localize_script( 'hb-' . $this->page_id . '-script', $key, $value );
		}
		wp_nonce_field( 'hb_nonce_update_db', 'hb_nonce_update_db' );
	}
	
	protected function display_admin_action( $setting_type = '' ) {
	?>
		<?php if ( $setting_type == 'season' ) { ?>
		<a href="#" title="<?php _e( 'Add season dates', 'hbook-admin' ); ?>" class="dashicons dashicons-plus" data-bind="click: $root.create_season_dates, visible: ! deleting() && ! adding_child()"></a>
		<?php }	?>
		<?php if ( $setting_type == 'option' ) { ?>
		<a href="#" title="<?php _e( 'Add option choice', 'hbook-admin' ); ?>" class="dashicons dashicons-plus" data-bind="click: $root.create_option_choice, visible: option.choice_type() == 'multiple' && ! deleting() && ! adding_child()"></a>
		<?php }	?>
		<?php if ( ( $setting_type == 'season' ) || ( $setting_type == 'option' ) ) { ?>
		<span data-bind="visible: adding_child()" class="hb-ajaxing hb-adding-child">
			<span class="spinner"></span>
		</span>
		<?php }	?>
		<a href="#" title="<?php _e( 'Edit', 'hbook-admin' ); ?>" class="dashicons dashicons-edit" data-bind="click: $root.edit_setting, visible: ! deleting()"></a>
		<?php if ( $setting_type == 'season_dates' ) { ?>
		<a href="#" title="<?php _e( 'Delete', 'hbook-admin' ); ?>" class="dashicons dashicons-trash" data-bind="click: function( data, event ) { $root.remove( data, event, season ) }, visible: ! deleting()"></a>
		<?php } else if ( $setting_type == 'option_choice' ) { ?>
		<a href="#" title="<?php _e( 'Delete', 'hbook-admin' ); ?>" class="dashicons dashicons-trash" data-bind="click: function( data, event ) { $root.remove( data, event, option ) }, visible: ! deleting()"></a>
		<?php } else { ?>
		<a href="#" title="<?php _e( 'Delete', 'hbook-admin' ); ?>" class="dashicons dashicons-trash" data-bind="click: $root.remove, visible: ! deleting()"></a>
		<?php } ?>
		<span data-bind="visible: deleting" class="hb-ajaxing hb-deleting">
			<span class="spinner"></span>
			<span><?php _e( 'Deleting...', 'hbook-admin' ); ?></span>
		</span>
	<?php
	}

	protected function display_admin_on_edit_action( $setting_type = '' ) {
	?>
		<input type="button" class="button-primary" data-bind="click: $root.save_setting, disable: saving, value: save_text" />
		<a href="#" class="button" data-bind="click: $root.cancel_edit_setting, visible: ! brand_new"><?php _e( 'Cancel', 'hbook-admin' ); ?></a>
		<?php if ( $setting_type == 'season_dates' ) { ?>
		<a href="#" class="button" data-bind="click: function( data, event ) { $root.remove( data, event, season ) }, visible: brand_new"><?php _e( 'Cancel', 'hbook-admin' ); ?></a>
		<?php } else if ( $setting_type == 'option_choice' ) { ?>
		<a href="#" class="button" data-bind="click: function( data, event ) { $root.remove( data, event, option ) }, visible: brand_new"><?php _e( 'Cancel', 'hbook-admin' ); ?></a>
		<?php } else { ?>
		<a href="#" class="button" data-bind="click: $root.remove, visible: brand_new"><?php _e( 'Cancel', 'hbook-admin' ); ?></a>
		<?php } ?>
	<?php
	}

	protected function display_select_days( $id ) {
		$days = $this->utils->days_full_name();
		foreach( $days as $i => $day ) { 
		?>
			<input id="hb-<?php echo( $id . '-' . $day ); ?>"  data-bind="checked: <?php echo( $id ); ?>" type="checkbox" value="<?php echo( $i ); ?>" />
			<label for="hb-<?php echo( $id . '-' . $day ); ?>"><?php echo( $day ); ?></label><br/>
		<?php 
		}
		?>
		<a data-bind="click: select_all_<?php echo( $id ); ?>" href="#"><?php _e( 'Select all', 'hbook-admin' ); ?></a> -
		<a data-bind="click: unselect_all_<?php echo( $id ); ?>" href="#"><?php _e( 'Unselect all', 'hbook-admin' ); ?></a>
		<?php
	}
	
	protected function display_checkbox_list( $data, $data_type, $display_check_all_box = true, $display_select_all_link = false ) {
		if ( $display_check_all_box ) {
		?>
		<input data-bind="checked: all_<?php echo( $data_type ); ?>" type="checkbox" id="hb-checkbox-<?php echo( $data_type ); ?>-all" />
		<label for="hb-checkbox-<?php echo( $data_type ); ?>-all"><?php _e( 'All', 'hbook-admin' ); ?></label><br/>
		<?php
		}
		foreach ( $data as $id => $name ) {
		?>
			<input id="hb-checkbox-<?php echo( $data_type ); ?>-<?php echo( $id ); ?>" data-bind="checked: <?php echo( $data_type ); ?>, disable: all_<?php echo( $data_type ); ?>" type="checkbox" value="<?php echo( $id ); ?>" />
			<label for="hb-checkbox-<?php echo( $data_type ); ?>-<?php echo( $id ); ?>"><?php echo( $name ); ?></label><br/>
		<?php
		}
		if ( $display_select_all_link ) {
		?>
		<a data-bind="click: select_all_<?php echo( $data_type ); ?>" href="#"><?php _e( 'Select all', 'hbook-admin' ); ?></a> - 
		<?php
		}
		?>
		<a data-bind="click: unselect_all_<?php echo( $data_type ); ?>" href="#"><?php _e( 'Unselect all', 'hbook-admin' ); ?></a>
		<?php
	}
	
	protected function display_edit_amount_fixed_percent() {
		?>
		<input data-bind="value: amount" type="text" class="hb-rate-amount" /><br/>
		<p class="amount-type"><?php _e( 'Amount type:', 'hbook-admin' ); ?></p>
		<input data-bind="checked: amount_type" id="hb-amount-type-fixed" type="radio" value="fixed" /><label for="hb-amount-type-fixed"><?php _e( 'Fixed', 'hbook-admin' ); ?> (<?php echo( $this->utils->get_currency_symbol() ); ?>)</label><br/>
		<input data-bind="checked: amount_type" id="hb-amount-type-percent" type="radio" value="percent" /><label for="hb-amount-type-percent"><?php _e( 'Percentage', 'hbook-admin' ); ?></label>
		<?php
	}

	protected function display_form_builder() {
	?>
		<div class="hb-form-fields-container" data-bind="sortable: { data: fields, connectClass: 'hb-form-fields-container' }">

			<div class="hb-form-field" data-bind="css: { 'hb-standard-field': standard == 'yes' }, attr: { id: id }, visible: form_name == '<?php echo( $this->form_name ); ?>'">
				<a class="hb-form-field-delete dashicons dashicons-no" href="#" data-bind="click: function( data, event ) { $root.remove_field( data, event ) }" title="<?php _e( 'Remove field', 'hbook-admin' ); ?>"></a>
				<p class="hb-form-field-name">
					<span data-bind="visible: ! editing_name(), text: name"></span>
					<input data-bind="visible: editing_name, value: name" type="text" class="hb-input-field-name" />
					<a data-bind="visible: ! editing_name(), click: $root.edit_field_name" class="dashicons dashicons-edit hb-form-field-edit-name" title="<?php _e( 'Edit field name', 'hbook-admin' ); ?>" href="#"></a>
					<a data-bind="visible: editing_name, click: $root.stop_edit_field_name" class="button" href="#"><?php _e( 'OK', 'hbook-admin' ); ?></a>
				</p>
				<?php
				$field_types = array(
					'text' => __( 'Text', 'hbook-admin' ),
					'email' => __( 'Email', 'hbook-admin' ),
					'number' => __( 'Number', 'hbook-admin' ),
					'textarea' => __( 'Text area', 'hbook-admin' ),
					'select' => __( 'Select', 'hbook-admin' ),
					'radio' => __( 'Radio buttons', 'hbook-admin' ),
					'checkbox' => __( 'Check boxes', 'hbook-admin' ),
					'title' => __( 'Title', 'hbook-admin' ),
					'explanation' => __( 'Explanation', 'hbook-admin' ),
				);
				$fields_options = '';
				foreach ( $field_types as $ft_id => $ft_label ) {
					$fields_options .= '<option value="' . $ft_id . '">' . $ft_label . '</option>';
				}
				?>
				<p>
					<span class="hb-form-field-displayed"><?php _e( 'Displayed?', 'hbook-admin' ); ?></span>
					<input data-bind="checked: displayed, attr: { id: displayed_yes_input_id }" type="radio" value="yes" />
					<label data-bind="attr: { 'for': displayed_yes_input_id }"><?php _e( 'Yes', 'hbook-admin' ); ?></label>
					&nbsp;&nbsp;
					<input data-bind="checked: displayed, attr: { id: displayed_no_input_id }" type="radio" value="no" />
					<label data-bind="attr: { 'for': displayed_no_input_id }"><?php _e( 'No', 'hbook-admin' ); ?></label>
				</p>
				<div data-bind="slideVisible: displayed">
					<p data-bind="visible: type() != 'title' && type() != 'explanation'">
						<span class="hb-form-field-required"><?php _e( 'Required?', 'hbook-admin' ); ?></span>
						<input data-bind="checked: required, attr: { id: required_yes_input_id }" type="radio" value="yes" />
						<label data-bind="attr: { 'for': required_yes_input_id }"><?php _e( 'Yes', 'hbook-admin' ); ?></label>
						&nbsp;&nbsp;
						<input data-bind="checked: required, attr: { id: required_no_input_id }" type="radio" value="no" />
						<label data-bind="attr: { 'for': required_no_input_id }"><?php _e( 'No', 'hbook-admin' ); ?></label>
					</p>
					<div class="hb-form-additional-field-settings">
						<p>
							<?php _e( 'Field type:', 'hbook-admin' ); ?>
							<select class="hb-form-field-select" data-bind="value: type">
							<?php echo( $fields_options ); ?>
							</select>
						</p>
						<div class="hb-form-field-choices" data-bind="visible: ( type() == 'checkbox' ) || ( type() == 'radio' ) || ( type() == 'select' )">
							<?php _e( 'Choices:', 'hbook-admin' ); ?>
							<a data-bind="click: add_choice" class="dashicons dashicons-plus" title="<?php _e( 'Add a choice', 'hbook-admin' ); ?>" href="#"></a>
							<ul class="hb-form-fields-choices-ul" data-bind="sortable: { data: choices, connectClass: 'hb-form-fields-choices-ul' }">
								<li>
									<span data-bind="visible: ! editing_choice(), text: name"></span>
									<input data-bind="visible: editing_choice, value: name" type="text" class="hb-input-choice-name" />
									<a data-bind="visible: ! editing_choice(), click: $parent.edit_choice_name" class="dashicons dashicons-edit hb-form-field-edit-choice" title="<?php _e( 'Edit choice', 'hbook-admin' ); ?>" href="#"></a>
									<a data-bind="visible: editing_choice, click: $parent.stop_edit_choice_name" class="button" href="#"><?php _e( 'OK', 'hbook-admin' ); ?></a>
									<a data-bind="click: $parent.remove_choice" class="dashicons dashicons-no hb-form-field-remove-choice" title="<?php _e( 'Remove choice', 'hbook-admin' ); ?>" href="#"></a>
								</li>
							</ul>
						</div>
					</div>
					<p data-bind="visible: type() != 'title' && type() != 'explanation' && form_name == 'booking'">
						<?php _e( 'Data about?', 'hbook-admin' ); ?><br/>
						<input data-bind="checked: data_about, attr: { id: data_about_customer_input_id }" type="radio" value="customer" />
						<label data-bind="attr: { 'for': data_about_customer_input_id }"><?php _e( 'Customer', 'hbook-admin' ); ?></label>
						&nbsp;&nbsp;
						<input data-bind="checked: data_about, attr: { id: data_about_booking_input_id }" type="radio" value="booking" />
						<label data-bind="attr: { 'for': data_about_booking_input_id }"><?php _e( 'Booking', 'hbook-admin' ); ?></label>
					</p>
				</div>
			</div>
			
		</div>
	<?php
	}
	
    protected function display_right_menu() {
        $hbook_pages = $this->utils->get_hbook_pages();
        ?>
        
        <a id="hb-admin-settings-link" href="<?php echo( admin_url( 'admin.php?page=hb_menu' ) ); ?>"><?php _e( 'HBook settings', 'hbook-admin' ); ?> <span class="dashicons dashicons-arrow-down-alt2"></span></a>
        <ul id="hb-admin-right-menu">
        
        <?php foreach ( $hbook_pages as $page ) : ?>
        
        <li>
            <a <?php if ( $_GET['page'] == $page['id'] ) : ?>class="hb-admin-right-menu-current-item"<?php endif; ?> href="<?php echo( admin_url( 'admin.php?page=' . $page['id'] ) ); ?>">
                <?php echo( $page['name'] ); ?>
            </a>
        </li>
        
        <?php endforeach; ?>
        
        </ul>
        
        <?php
    }
    
	protected function get_apply_to_types( $type = 'fee' ) {
		$types = array(
			array(
				'option_value' => 'per-person',
				'option_text' => __( 'Per person', 'hbook-admin' )
			),
            array(
				'option_value' => 'per-person-per-day',
				'option_text' => __( 'Per person / per day', 'hbook-admin' )
			),
			array(
				'option_value' => 'per-accom',
				'option_text' => __( 'Per accommodation', 'hbook-admin' )
			),
			array(
				'option_value' => 'per-accom-per-day',
				'option_text' => __( 'Per accommodation / per day', 'hbook-admin' )
			)
		);	
        if ( $type == 'option' ) {
            $types[] = array( 'option_value' => 'quantity', 'option_text' => __( 'Quantity', 'hbook-admin' ) );
            $types[] = array( 'option_value' => 'quantity-per-day', 'option_text' => __( 'Quantity per day', 'hbook-admin' ) );
        }
        return $types;
	}
	
}	