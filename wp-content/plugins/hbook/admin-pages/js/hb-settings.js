jQuery( document ).ready( function( $ ) {
    var close_right_menu_timer;
    
    $( '#hb-admin-settings-link, #hb-admin-right-menu' ).mouseenter( function() {
        clearTimeout( close_right_menu_timer );
        $( '#hb-admin-right-menu' ).fadeIn();
    });
    
    $( '#hb-admin-settings-link, #hb-admin-right-menu' ).mouseleave( function() {
        close_right_menu_timer = setTimeout( function() { $( '#hb-admin-right-menu' ).fadeOut() }, 500 );
    });
});