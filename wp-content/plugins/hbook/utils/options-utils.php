<?php
class HbOptionsUtils {

	public $currencies;
	public $payment_settings;
	public $appearance_settings;
	public $misc_settings;
	public $search_form_options;
	public $accom_selection_options;
	public $booking_rules_options;
	public $contact_form_options;
    
	public function __construct( $utils = false ) {
		
		if ( $utils ) {
			$this->currencies = $utils->currencies_code_name();
		} else {
			$this->currencies = array();
		}
        
        $this->payment_settings = array(
            
            'payment_settings' => array(
                'label' => __( 'Booking payment settings', 'hbook-admin' ),
                'options' => array(
                    
                    'hb_resa_payment_multiple_choice' => array(
                        'label' => __( 'Customers can choose between different payment options:', 'hbook-admin' ),
                        'type' => 'radio',
                        'choice' => array(
							'yes' => __( 'Yes', 'hbook-admin' ),
							'no' => __( 'No', 'hbook-admin' ),
						),
						'default' => 'no'
                    ),
                    
                    'hb_resa_payment' => array(
						'label' => __( 'Booking payment:', 'hbook-admin' ),
						'type' => 'radio',
						'choice' => array(
							'offline' => __( 'Customers do not have to pay online to book an accommodation (payment on arrival or offline - e.g. bank wire)', 'hbook-admin' ),
							'store_credit_card' => __( 'Customers have to leave their credit card details (this option is available only with Stripe)', 'hbook-admin' ),
							'deposit' => __( 'Customers have to pay a deposit online to book an accommodation', 'hbook-admin' ),
							'full' => __( 'Customers have to pay the full stay price online to book an accommodation', 'hbook-admin' )
						),
						'default' => 'none',
                        'wrapper-class' => 'hb-resa-payment-choice-multiple'
					),
                                        
                    'hb_resa_payment_offline' => array(
                        'label' => __( 'Customers can pay on arrival or offline - e.g.bank wire:', 'hbook-admin' ),
                        'type' => 'radio',
                        'choice' => array(
							'yes' => __( 'Yes', 'hbook-admin' ),
							'no' => __( 'No', 'hbook-admin' ),
						),
						'default' => 'yes',
                        'wrapper-class' => 'hb-resa-payment-choice-single'
                    ),
                    
                    'hb_resa_payment_store_credit_card' => array(
                        'label' => __( 'Customers can leave their credit card details for a later charge (this option is available only with Stripe):', 'hbook-admin' ),
                        'type' => 'radio',
                        'choice' => array(
							'yes' => __( 'Yes', 'hbook-admin' ),
							'no' => __( 'No', 'hbook-admin' ),
						),
						'default' => 'no',
                        'wrapper-class' => 'hb-resa-payment-choice-single'
                    ),
					
					'hb_resa_payment_deposit' => array(
                        'label' => __( 'Customers can pay an online deposit:', 'hbook-admin' ),
                        'type' => 'radio',
                        'choice' => array(
							'yes' => __( 'Yes', 'hbook-admin' ),
							'no' => __( 'No', 'hbook-admin' ),
						),
						'default' => 'no',
                        'wrapper-class' => 'hb-resa-payment-choice-single'
                    ),
                    
                    'hb_resa_payment_full' => array(
                        'label' => __( 'Customers can pay the full amount online:', 'hbook-admin' ),
                        'type' => 'radio',
                        'choice' => array(
							'yes' => __( 'Yes', 'hbook-admin' ),
							'no' => __( 'No', 'hbook-admin' ),
						),
						'default' => 'no',
                        'wrapper-class' => 'hb-resa-payment-choice-single'
                    ),
                    
					'hb_deposit_type' => array(
						'label' => __( 'Deposit type:', 'hbook-admin' ),
						'type' => 'radio',
						'choice' => array(
							'none' => __( 'None', 'hbook-admin' ),
							'percentage' => __( 'Percentage', 'hbook-admin' ),
							'fixed' => __( 'Fixed', 'hbook-admin' ),
						),
						'default' => 'none'
					),
                    
					'hb_deposit_amount' => array(
						'label' => __( 'Deposit amount:', 'hbook-admin' ),
						'type' => 'text',
						'class' => 'hb-small-field',
					),	
				
				)
			),
			
            'price_settings' => array(
				'label' => __( 'Price settings', 'hbook-admin' ),
				'options' => array(

					'hb_currency' => array(
						'label' => __( 'Payment currency:', 'hbook-admin' ),
						'type' => 'select',
						'choice' => $this->currencies,
						'default' => 'USD'
					),
					'hb_currency_position' => array(
						'label' => __( 'Currency symbol position:', 'hbook-admin' ),
						'type' => 'radio',
						'choice' => array(
							'before' => __( 'Before price', 'hbook-admin' ),
							'after' => __( 'After price', 'hbook-admin' ),
						),
						'default' => 'before'
					),
					'hb_price_precision' => array(
						'label' => __( 'Price precision:', 'hbook-admin' ),
						'type' => 'radio',
						'choice' => array(
                            'two_decimals' => __( 'Two decimals' ,'hbook-admin' ),
							'no_decimals' => __( 'No decimals' ,'hbook-admin' ),
						),
						'default' => 'two_decimals'
					),
					
                )
            )
            
        );
        
        $this->appearance_settings = array(
        
            'general_appearance_settings' => array(
				'label' => __( 'General settings', 'hbook-admin' ),
                'options' => array(
                    'hb_horizontal_form_min_width' => array(
                        'label' => __( 'Minimum width required for displaying a horizontal form:', 'hbook-admin' ),
                        'caption' => __( 'If the available space is less than the minimum width the form will be displayed vertically.', 'hbook-admin' ),
                        'type' => 'text',
						'class' => 'hb-small-field',
                        'default' => '500',
                    ),
                    'hb_page_padding_top' => array(
						'label' => __( 'Page padding top:', 'hbook-admin' ),
						'type' => 'text',
						'class' => 'hb-small-field',
                        'default' => '10',
					),

                )
            ),
			
			'buttons_appearance' => array(
				'label' => __( 'Buttons appearance', 'hbook-admin' ),
                'options' => array(
					'hb_buttons_style' => array(
						'label' => __( 'Buttons style:', 'hbook-admin' ),
						'type' => 'radio',
						'choice' => array(
							'theme' => __( 'Use theme styles', 'hbook-admin' ),
							'custom' => __( 'Custom', 'hbook-admin' ),
						),
						'default' => 'theme'
					),
					'hb_buttons_css_options' => array(),
				)
			),
			
			'inputs_selects_appearance' => array(
				'label' => __( 'Inputs and selects appearance', 'hbook-admin' ),
                'options' => array(
					'hb_inputs_selects_style' => array(
						'label' => __( 'Inputs and selects style:', 'hbook-admin' ),
						'type' => 'radio',
						'choice' => array(
							'theme' => __( 'Use theme styles', 'hbook-admin' ),
							'custom' => __( 'Custom', 'hbook-admin' ),
						),
						'default' => 'theme'
					),
					'hb_inputs_selects_css_options' => array(),
				)
			),
			
			'calendar_colors' => array(
				'label' => __( 'Calendars appearance', 'hbook-admin' ),
                'options' => array(
                    'hb_calendar_colors' => array(),
					'hb_calendar_shadows' => array(
						'label' => __( 'Add a shadow to calendars:', 'hbook-admin' ),
						'type' => 'radio',
						'choice' => array(
							'yes' => __( 'Yes', 'hbook-admin' ),
							'no' => __( 'No', 'hbook-admin' ),
						),
						'default' => 'yes'
					),           
                )
            ),
			
			'custom_css_appearance_settings' => array(
				'label' => __( 'Custom CSS', 'hbook-admin' ),
                'options' => array(
					
					'hb_custom_css_frontend' => array(
						'label' => __( 'Custom CSS for the front-end pages:', 'hbook-admin' ),
						'type' => 'textarea',
					),
                    'hb_custom_css_backend' => array(
						'label' => __( 'Custom CSS for the admin pages:', 'hbook-admin' ),
						'type' => 'textarea',
					),
					
				)
			)
            
        );
        
		$this->misc_settings = array(
			
            'confirmation_settings' => array(
                'label' => __( 'Confirmation settings', 'hbook-admin' ),
                'options' => array(
                
                    'hb_resa_unpaid_has_confirmation' => array(
                        'label' => __( 'Unpaid reservations have to be confirmed before dates are blocked out:', 'hbook-admin' ),
                        'type' => 'radio',
						'choice' => array(
							'yes' => __( 'Yes', 'hbook-admin' ),
							'no' => __( 'No', 'hbook-admin' ),
						),
						'default' => 'no'
                    ),
                    'hb_resa_paid_has_confirmation' => array(
                        'label' => __( 'Paid reservations have to be confirmed before dates are blocked out:', 'hbook-admin' ),
                        'type' => 'radio',
						'choice' => array(
							'yes' => __( 'Yes', 'hbook-admin' ),
							'no' => __( 'No', 'hbook-admin' ),
						),
						'default' => 'no'
                    ),
                )
            ),            

			'admin_resa' => array(
                'label' => __( 'Settings for reservations created from admin', 'hbook-admin' ),
                'options' => array(
                    'hb_resa_admin_status' => array(
                        'label' => __( 'Status of reservation', 'hbook-admin' ),
                        'type' => 'radio',
						'choice' => array(
							'confirmed' => __( 'Confirmed', 'hbook-admin' ),
							'new' => __( 'New', 'hbook-admin' ),
						),
						'default' => 'confirmed'
                    ),
                )
            ),
			
			'opening_dates' => array(
				'label' => __( 'Opening dates', 'hbook-admin' ),
				'options' => array(
                    
                    'hb_min_date_days' => array(
                        'label' => __( 'Minimum selectable date for a reservation:', 'hbook-admin' ),
                        'caption' => __( 'Enter a number of <u>days</u> from current date...', 'hbook-admin' ),
                        'type' => 'text',
						'class' => 'hb-small-field',
                    ),
					'hb_min_date_fixed' => array(
						'caption' => __( '...or enter a fixed date (yyyy-mm-dd format)', 'hbook-admin' ),
						'type' => 'text',
						'class' => 'hb-small-field',
					),						
					'hb_max_date_months' => array(
						'label' => __( 'Maximum selectable date for a reservation:', 'hbook-admin' ),
						'caption' => __( 'Enter the maximum number of <u>months</u> ahead...', 'hbook-admin' ),
						'type' => 'text',
						'class' => 'hb-small-field',
						'default' => '12'
					),		
					'hb_max_date_fixed' => array(
						'caption' => __( '...or enter a fixed date (yyyy-mm-dd format)', 'hbook-admin' ),
						'type' => 'text',
						'class' => 'hb-small-field',
					),
				),
            ),
                
            'date_settings' => array(
				'label' => __( 'Date settings', 'hbook-admin' ),
                'options' => array(
                    'hb_front_end_date_settings' => array()               
                )
            ),
            
			'terms' => array(
				'label' => __( 'Terms and conditions', 'hbook-admin' ),
				'options' => array(
                    
					'hb_display_terms_and_cond' => array(
						'label' => __( 'Display a terms and conditions checkbox:', 'hbook-admin' ),
						'caption' => sprintf( __( 'You can change the text of the terms and conditions checkbox on the %s Text page%s.', 'hbook-admin' ), '<a href="' . admin_url( 'admin.php?page=hb_text#hb-text-section-book-now-area' ) . '">', '</a>' ), 
						'type' => 'radio',
						'choice' => array(
							'yes' => __( 'Yes', 'hbook-admin' ),
							'no' => __( 'No', 'hbook-admin' ),
						),
						'default' => 'no'
					),
					
				),
			),
					
			'misc' => array(
				'label' => __( 'Misc', 'hbook-admin' ),
				'options' => array(
                    
                    'hb_accommodation_slug' => array(
						'label' => __( 'Accommodation url slug:', 'hbook-admin' ),
                        'caption' => __( 'The url slug can not be blank. If you leave this field empty the slug will be set to "hb_accommodation".', 'hbook-admin' ),
						'type' => 'text',
                        'default' => 'hb_accommodation',
					),
					'hb_uninstall_delete_all' => array(
						'label' => __( 'Delete all stored information on uninstall:', 'hbook-admin' ),
						'type' => 'radio',
						'choice' => array(
							'yes' => __( 'Yes', 'hbook-admin' ),
							'no' => __( 'No', 'hbook-admin' ),
						),
						'default' => 'no'
					),
                    'hb_ajax_timeout' => array(
						'label' => __( 'Delay before a timeout error occurs (in ms) :', 'hbook-admin' ),
						'type' => 'text',
                        'default' => '20000',
                        'class' => 'hb-small-field',
					),	
				)
			)
			
		);
		
		$this->search_form_options = array(
			'search_form_options' => array(
				'options' => array(
				
					'hb_maximum_adults' => array(
						'label' => __( 'Maximum adults number', 'hbook-admin' ),
						'type' => 'text',
						'class' => 'hb-small-field',
						'default' => '5'
					),						
					'hb_maximum_children' => array(
						'label' => __( 'Maximum children number', 'hbook-admin' ),
						'type' => 'text',
						'class' => 'hb-small-field',
						'default' => '5'
					),	
					'hb_display_adults_field' => array(
						'label' => __( 'Display an Adults field?', 'hbook-admin' ),
						'type' => 'radio',
						'choice' => array(
							'yes' => __( 'Yes', 'hbook-admin' ),
							'no' => __( 'No', 'hbook-admin' ),
						),
						'default' => 'yes'
					),	
					'hb_display_children_field' => array(
						'label' => __( 'Display a Children field?', 'hbook-admin' ),
						'type' => 'radio',
						'choice' => array(
							'yes' => __( 'Yes', 'hbook-admin' ),
							'no' => __( 'No', 'hbook-admin' ),
						),
						'default' => 'yes'
					),	
					'hb_search_form_placeholder' => array(
						'label' => __( 'Display placeholders instead of labels?', 'hbook-admin' ),
						'type' => 'radio',
						'choice' => array(
							'yes' => __( 'Yes', 'hbook-admin' ),
							'no' => __( 'No', 'hbook-admin' ),
						),
						'default' => 'no'
					),	
					
				)
			)
		);		
		
		$this->accom_selection_options = array(
			'accom_selection_options' => array(
				'options' => array(
				
					'hb_title_accom_link' => array(
						'label' => __( 'Link title towards accommodation pages?', 'hbook-admin' ),
						'type' => 'radio',
						'choice' => array(
							'yes' => __( 'Yes', 'hbook-admin' ),
							'no' => __( 'No', 'hbook-admin' ),
						),
						'default' => 'no'
					),
					
					'hb_thumb_display' => array(
						'label' => __( 'Display an accommodation thumbnail?', 'hbook-admin' ),
						'type' => 'radio',
						'choice' => array(
							'yes' => __( 'Yes', 'hbook-admin' ),
							'no' => __( 'No', 'hbook-admin' ),
						),
						'default' => 'yes'
					),
					
					'hb_thumb_accom_link' => array(
						'label' => __( 'Link thumbnail towards accommodation pages?', 'hbook-admin' ),
						'type' => 'radio',
						'choice' => array(
							'yes' => __( 'Yes', 'hbook-admin' ),
							'no' => __( 'No', 'hbook-admin' ),
						),
						'default' => 'no'
					),
                    
                    'hb_search_accom_thumb_width' => array(
						'label' => __( 'Thumbnail width (in px)', 'hbook-admin' ),
						'type' => 'text',
						'default' => '100',
                        'class' => 'hb-small-field',
					),
                    
                    'hb_search_accom_thumb_height' => array(
						'label' => __( 'Thumbnail height (in px)', 'hbook-admin' ),
						'type' => 'text',
						'default' => '100',
                        'class' => 'hb-small-field',
					),
                    
					'hb_button_accom_link' => array(
						'label' => __( 'Display a button that links towards accommodation pages?', 'hbook-admin' ),
						'type' => 'radio',
						'choice' => array(
							'yes' => __( 'Yes', 'hbook-admin' ),
							'no' => __( 'No', 'hbook-admin' ),
						),
						'default' => 'no'
					),
                    
					'hb_display_price_breakdown' => array(
						'label' => __( 'Display price breakdown?', 'hbook-admin' ),
						'type' => 'radio',
						'choice' => array(
							'yes' => __( 'Yes', 'hbook-admin' ),
							'no' => __( 'No', 'hbook-admin' ),
						),
						'default' => 'yes'
					),
					
				)
			)
		);
		
		$this->contact_form_options = array(
			'contact_form_options' => array(
				'options' => array(
				
					'hb_contact_email' => array(
						'label' => __( 'Email address:', 'hbook-admin' ) . ' ' . __( '(leave blank to use WordPress admin email)', 'hbook-admin' ),
						'type' => 'text',
					),
					
					'hb_contact_message_type' => array(
						'label' => __( 'Message type:', 'hbook-admin' ),
						'type' => 'radio',
						'choice' => array(
							'text' => __( 'Text', 'hbook-admin' ),
							'html' => __( 'HTML', 'hbook-admin' ),
						),
						'default' => 'text'
					),
					
					'hb_contact_from' => array(
						'label' => __( 'From:', 'hbook-admin' ),
						'type' => 'text',
						'default' => '[contact_name] <[contact_email]>',
					),
					
					'hb_contact_subject' => array(
						'label' => __( 'Subject:', 'hbook-admin' ),
						'type' => 'text',
						'default' => __( 'New message sent from your website', 'hbook-admin' )
					),
					
					'hb_contact_message' => array(
						'label' => __( 'Message:', 'hbook-admin' ),
						'type' => 'textarea',
						'default' => 
						__( 'Here is a message sent from your website:', 'hbook-admin' ) . "\n\n" . 
						__( 'Name:', 'hbook-admin' ) . ' [contact_name]'  . "\n" .
						__( 'Email:', 'hbook-admin' ) . ' [contact_email]' . "\n\n" .
						__( 'Message:', 'hbook-admin' ) . "\n\n" .
						'[contact_message]',
						'caption' => ''
					)
					
				)
			)
		);
		
	}
	
	public function init_options() {
		$options = array_merge( $this->misc_settings, $this->payment_settings, $this->appearance_settings, $this->search_form_options, $this->accom_selection_options );
		if ( function_exists( 'htw_is_active' ) ) {
			$options = array_merge( $options, $this->contact_form_options );
		}
		foreach ( $options as $section ) {
			foreach ( $section['options'] as $id => $option ) {
				if ( ( get_option( $id ) === false ) && ( isset( $option['default'] ) ) ) {
					add_option( $id, $option['default'] );
				}
			}
		}
		if ( ( get_option( 'hb_stripe_mode' ) === false ) ) {
			update_option( 'hb_stripe_mode', 'test' );
		}
		if ( ( get_option( 'hb_paypal_mode' ) === false ) ) {
			update_option( 'hb_paypal_mode', 'sandbox' );
		}
		if ( ( get_option( 'hb_curl_set_timeout' ) === false ) ) {
			update_option( 'hb_curl_set_timeout', 'no' );
		}
		if ( ( get_option( 'hb_curl_set_ssl_version' ) === false ) ) {
			update_option( 'hb_curl_set_ssl_version', 'no' );
		}
		if ( get_option( 'hb_store_credit_card' ) === false ) {
			update_option( 'hb_store_credit_card', 'no' );
		}
	}
	
	public function delete_options() {
		$options = array_merge( $this->misc_settings, $this->payment_settings, $this->appearance_settings, $this->search_form_options, $this->accom_selection_options );
		foreach ( $options as $section ) {
			foreach ( $section['options'] as $id => $option ) {
				delete_option( $id );
			}
		}
		
        $old_options = array(
            'hb_notify_admin',
            'hb_admin_email_subject',
            'hb_admin_message_type',
            'hb_admin_email_message',
            'hb_ack_email',
            'hb_ack_email_subject',
            'hb_ack_message_type',
            'hb_ack_email_message',
            'hb_confirm_email',
            'hb_confirm_email_subject',
            'hb_confirm_message_type',
            'hb_confirm_email_message',
            'hb_admin_email',
            'hb_ack_email_from',
            'hb_confirm_email_from',
            'hb_admin_email_from',
            'hb_flush_rewrite',
			'hb_paypal_sandbox',
			'hb_paypal_api_user',
			'hb_paypal_api_psw',
			'hb_paypal_api_signature',

			'hb_stripe_active',
			'hb_paypal_active',
			'hb_stripe_test_secret_key',
			'hb_stripe_test_publishable_key',
			'hb_stripe_live_secret_key',
			'hb_stripe_live_publishable_key',
			'hb_paypal_api_sandbox_user',
			'hb_paypal_api_sandbox_psw',
			'hb_paypal_api_sandbox_signature',
			'hb_paypal_api_live_user',
			'hb_paypal_api_live_psw',
			'hb_paypal_api_live_signature',
			'hb_paypal_mode',
			'hb_stripe_mode',
			
			'hb_valid_purchase_code',
			'hb_purchase_code_error',
			'hb_purchase_code',
			'hb_last_synced',
			
			'hb_form_style',
			
			'hb_store_credit_card'
		);
		
        foreach ( $old_options as $option_id ) {
            delete_option( $option_id );
        }
	}
	
	public function get_options_list( $options_name ) {
        $options_to_get = $this->$options_name;
		$options_list = array();
		foreach ( $options_to_get as $section ) {
			foreach ( $section['options'] as $id => $option ) {
				$options_list[] = $id;
			}
		}
		return $options_list;
	}
	
    public function display_section_title( $title ) {
    ?>
    
        <hr/>
        <h3><?php echo( $title ); ?></h3>    
        
    <?php
    }
    
	public function display_text_option( $id, $option ) {
		$class = '';
		$caption_class = '';
		$wrapper_class = '';
		if ( isset( $option['class'] ) ) {
			$class = $option['class'];
		}
        if ( isset( $option['wrapper-class'] ) ) {
			$wrapper_class = $option['wrapper-class'];
		}
        ?>
        
		<p class="<?php echo( $wrapper_class ); ?>">
			<?php
			if ( isset( $option['label'] ) ) {
			?>
			<label for="<?php echo( $id ); ?>"><?php echo( $option['label'] ); ?></label><br/>
			<?php
			} else {
				$caption_class = "hb-no-label";
			}
			?>
			<?php
			if ( isset( $option['caption'] ) ) {
			?>
			<small class="<?php echo( $caption_class ); ?>"><?php echo( $option['caption'] ); ?></small><br/>
			<?php
			}
			?>
			<input type="text" id="<?php echo( $id ); ?>" name="<?php echo( $id ); ?>" class="<?php echo( $class ); ?>" size="50" value="<?php echo( esc_attr( get_option( $id ) ) ); ?>" />
		</p>
	<?php	
	}
	
	public function display_textarea_option( $id, $option ) {
        $wrapper_class = '';
        if ( isset( $option['wrapper-class'] ) ) {
			$wrapper_class = $option['wrapper-class'];
		}
        ?>
        
		<p class="<?php echo( $wrapper_class ); ?>">
			<label for="<?php echo( $id ); ?>"><?php echo( $option['label'] ); ?></label><br/>
			<textarea id="<?php echo( $id ); ?>" name="<?php echo( $id ); ?>" rows="8" class="widefat"><?php echo( esc_textarea( get_option( $id ) ) ); ?></textarea><br/>
			<?php if ( isset( $option['caption'] ) && $option['caption'] != '' ) { ?>
			<small class="hb-textarea-caption"><?php echo( $option['caption'] ); ?></small>
			<?php } ?>
		</p>
	<?php	
	}

	public function display_radio_option( $id, $option ) {
        $wrapper_class = '';
		if ( isset( $option['wrapper-class'] ) ) {
			$wrapper_class = $option['wrapper-class'];
		}
        ?>
        
		<p class="<?php echo( $wrapper_class ); ?>">
			<label><?php echo( $option['label'] ); ?></label><br/>
			<?php
			if ( isset( $option['caption'] ) ) {
			?>
			<small><?php echo( $option['caption'] ); ?></small><br/>
			<?php
			}
			foreach ( $option['choice'] as $choice_id => $choice_label ) {
			?>
			<input type="radio" id="<?php echo( $id . '_' . $choice_id ); ?>" name="<?php echo( $id ); ?>" value="<?php echo( $choice_id ); ?>" <?php echo( get_option( $id ) == $choice_id ? 'checked' : '' ); ?> />
			<label for="<?php echo( $id . '_' . $choice_id ); ?>"><?php echo( $choice_label ); ?></label>&nbsp;&nbsp;
				<?php
				if ( count ( $option['choice'] ) > 2 ) {
					echo( '<br/>' );
				}
			}
			?>
		</p>
	<?php
	}

	public function display_select_option( $id, $option ) {
        $wrapper_class = '';
        if ( isset( $option['wrapper-class'] ) ) {
			$wrapper_class = $option['wrapper-class'];
		}
        ?>
        
		<p class="<?php echo( $wrapper_class ); ?>">
			<label for="<?php echo( $id ); ?>"><?php echo( $option['label'] ); ?></label><br/>
			<?php $current_choice = get_option( $id ); ?>
			<select id="<?php echo( $id ); ?>" name="<?php echo( $id ); ?>">
				<?php foreach ( $option['choice'] as $choice_id => $choice_label ) { ?>
					<option value="<?php echo( $choice_id ); ?>" <?php if ( $choice_id == $current_choice ) { echo( 'selected' ); } ?>><?php echo( $choice_label ); ?></option>
				<?php } ?>
			</select>
		</p>
	<?php
	}
	
	public function display_save_options_section() {
	?>
		<div class="hb-options-save-wrapper">

			<span class="hb-ajaxing">
				<span class="spinner"></span>
				<span><?php _e( 'Saving...', 'hbook-admin' ); ?></span>
			</span>
		
			<span class="hb-saved"></span>
			
			<p>
				<a href="#" class="hb-options-save button-primary"><?php _e( 'Save changes', 'hbook-admin' ); ?></a>
			</p>
			
		</div>
	<?php
	}
	
}