<?php
class HbAccommodation {

	private $hbdb;
	
	public function __construct( $hbdb ) {
		$this->hbdb = $hbdb;
	}
	
	public function create_accommodation_post_type() {
		register_post_type( 'hb_accommodation',
			array(
				'labels' => array(
					'name' => __( 'Accommodation', 'hbook-admin' ),
                    'all_items' => __( 'All Accommodation', 'hbook-admin' ),
					'add_new_item' => __( 'Add New Accommodation', 'hbook-admin' ),
					'edit_item' => __( 'Edit Accommodation', 'hbook-admin' ),
					'new_item' => __( 'New Accommodation', 'hbook-admin' ),
					'view_item' => __( 'View Accommodation post', 'hbook-admin' ),
					'search_items' => __( 'Search Accommodation', 'hbook-admin' ),
					'not_found' => __( 'No accommodation found.', 'hbook-admin' ),
					'not_found_in_trash' => __( 'No accommodation post found in trash.', 'hbook-admin' ),
				),
				'public' => true,
				'has_archive' => true,
				'supports' => apply_filters( 'hb_accommodation_supports', array( 'title', 'editor', 'thumbnail' ) ),
				'menu_icon' => 'dashicons-admin-home',
                'rewrite' => array( 'slug' => get_option( 'hb_accommodation_slug', 'hb_accommodation' ) ),
                'taxonomies' => apply_filters( 'hb_accommodation_taxonomies', array() ),
			)
		);
        if ( get_option( 'hb_flush_rewrite' ) != 'no_flush' ) {
            flush_rewrite_rules();
            update_option( 'hb_flush_rewrite', 'no_flush' );
        }
	}

	public function accommodation_meta_box() {
		add_meta_box( 'accommodation_meta_box', __( 'Accommodation settings', 'hbook-admin' ), array( $this, 'accommodation_meta_box_display' ), 'hb_accommodation', 'normal' );
        add_meta_box( 'accommodation_side_meta_box', __( 'Accommodation display', 'hbook-admin' ), array( $this, 'accommodation_side_meta_box_display' ), 'hb_accommodation', 'side' );
	}

    public function accommodation_side_meta_box_display( $post ) {
    	if ( ! function_exists( 'htw_is_active' ) ) :
		?>
    
        <p>
            <?php 
            $accom_default_page = get_post_meta( $post->ID, 'accom_default_page', true );
            if ( ! $accom_default_page ) {
                $accom_default_page = 'yes';
            }
            ?>
            <input 
                type="radio" id="hb-accom-default-page-yes" name="hb-accom-default-page" value="yes" 
                <?php if ( $accom_default_page == 'yes' ) { echo( 'checked' ); } ?>
            />
            <label  for="hb-accom-default-page-yes">
            <?php _e( 'Use this post to display the accommodation', 'hbook-admin' ); ?>
            </label>
            <br/>
            <input 
                type="radio" id="hb-accom-default-page-no" name="hb-accom-default-page" value="no" 
                <?php if ( $accom_default_page == 'no' ) { echo( 'checked' ); } ?>
            />
            <label for="hb-accom-default-page-no">
            <?php _e( 'Use another post or page to display the accommodation', 'hbook-admin' ); ?>
            </label>
        </p>
		
		<p class="hb-accom-select-linked-page">
			<label for="hb-accom-linked-page"  class="hb-accom-settings-label"><?php _e( 'Enter the ID of the page used for displaying the accommodation:', 'hbook-admin' ); ?></label>
			<input id="hb-accom-linked-page" name="hb-accom-linked-page" type="text" size="4" value="<?php echo( get_post_meta( $post->ID, 'accom_linked_page', true ) ); ?>"/>
		</p>
		
		<?php endif; ?>
		
        <p class="hb-accom-select-template">
            <label for="hb-accom-page-template" class="hb-accom-settings-label"><?php _e( 'Select a template:', 'hbook-admin' ); ?></label>
            <?php
			if ( function_exists( 'htw_is_active' ) ) { 
				$page_templates = array(
					'post' => __( 'Accommodation post', 'hbook-admin' ),
					'template-advanced-layout.php' => __( 'Advanced layout Template', 'hbook-admin' ),
				);
			} else {
				$post_page_templates = array(
					'post' => __( 'Post', 'hbook-admin' ),
					'page.php' => __( 'Page', 'hbook-admin' ),
				);
				$page_templates = wp_get_theme()->get_page_templates();
				$page_templates = array_merge( $post_page_templates, $page_templates );
			}
            $current_template = get_post_meta( $post->ID, 'accom_page_template', true ); 
            ?>
            <select id="hb-accom-page-template" name="hb-accom-page-template">
                <?php foreach ( $page_templates as $template_file => $template_name ) : ?>
                <option value="<?php echo( $template_file ); ?>"<?php if ( $template_file == $current_template ) : ?> selected<?php endif; ?>>
                    <?php echo( $template_name );?>
                </option>
                <?php endforeach; ?>
            </select>
        </p>
        
    <?php
    }
    
	public function accommodation_meta_box_display( $post ) {
		if ( $this->is_accom_main_language( $post->ID ) ) :
		?>

        <p>
            <label for="hb-accom-occupancy" class="hb-accom-settings-label">
				<?php _e( 'Normal occupancy:', 'hbook-admin' ); ?>
				<br/>
				<small><?php _e( 'Extra people might pay a fee as defined in the "Rates page".', 'hbook-admin' ); ?></small>
			</label>
            <input id="hb-accom-occupancy" name="hb-accom-occupancy" type="text" size="2" value="<?php echo( get_post_meta( $post->ID, 'accom_occupancy', true ) ); ?>"/>
        </p>
        <p>
            <label for="hb-accom-max-occupancy" class="hb-accom-settings-label"><?php _e( 'Maximum occupancy:', 'hbook-admin' ); ?></label>
            <input id="hb-accom-max-occupancy" name="hb-accom-max-occupancy" type="text" size="2" value="<?php echo( get_post_meta( $post->ID, 'accom_max_occupancy', true ) ); ?>"/>
        </p>
		
		<?php endif; ?>
		
        <p>
            <label for="hb-accom-search-result-desc" class="hb-accom-settings-label"><?php _e( 'Description displayed in search results:', 'hbook-admin' ); ?></label>
            <textarea id="hb-accom-search-result-desc" name="hb-accom-search-result-desc" class="widefat i18n-multilingual" rows="3"><?php echo( get_post_meta( $post->ID, 'accom_search_result_desc', true ) ); ?></textarea>
        </p>
        <p>
            <label for="hb-accom-list-desc" class="hb-accom-settings-label"><?php _e( 'Description displayed in Accommodation list:', 'hbook-admin' ); ?></label>
            <textarea id="hb-accom-list-desc" name="hb-accom-list-desc" class="widefat i18n-multilingual" rows="3"><?php echo( get_post_meta( $post->ID, 'accom_list_desc', true ) ); ?></textarea>
        </p>
		
		<?php if ( $this->is_accom_main_language( $post->ID ) ) : ?>
			
        <p>
            <label for="hb-accom-starting-price" class="hb-accom-settings-label"><?php _e( 'Starting price:', 'hbook-admin' ); ?></label>
            <input id="hb-accom-starting-price" name="hb-accom-starting-price" type="text" size="4" value="<?php echo( get_post_meta( $post->ID, 'accom_starting_price', true ) ); ?>"/>
        </p>
        <?php
        $accom_quantity = get_post_meta( $post->ID, 'accom_quantity', true );
        if ( $accom_quantity == '' ) {
            $accom_quantity = 0;
        }
        $accom_quantity = intval( $accom_quantity );
        $accom_num_name_index = get_post_meta( $post->ID, 'accom_num_name_index', true );
		if ( $accom_num_name_index == '' ) {
			$accom_num_name_index = 0;
		}
        $accom_num_name = $this->hbdb->get_accom_num_name( $post->ID );
        ?>

        <p>
            <label for="hb-accom-quantity" class="hb-accom-settings-label"><?php _e( 'Number of accommodation of this type:', 'hbook-admin' ); ?></label>
            <input id="hb-accom-quantity" name="hb-accom-quantity" type="text" size="2" value="<?php echo( $accom_quantity ); ?>"/>
        </p>

        <p>
            <a href="#" class="hb-edit-accom-numbering"><?php _e( 'Edit accommodation numbering', 'hbook-admin' ); ?></a>
        </p>

        <div id="hb-accom-num-name-wrapper">

            <input type="hidden" id="hb-accom-num-name-json" name="hb-accom-num-name-json" />
            <input type="hidden" id="hb-accom-num-name-index" name="hb-accom-num-name-index" value="<?php echo( $accom_num_name_index ); ?>" />

            <?php foreach ( $accom_num_name as $num => $name ) { ?>

                <p class="hb-accom-num-name">
                    <input data-id="<?php echo( $num ); ?>" type="text" value="<?php echo( esc_attr( $name ) ); ?>" />
                    <a class="hb-accom-num-name-delete" href="#"><?php _e( 'Delete', 'hbook-admin' ); ?></a>
                </p>

            <?php } ?>

        </div>

        <?php
		endif;
    }

	public function save_accommodation_meta( $post_id ) {
		if ( isset( $_REQUEST['hb-accom-quantity'] ) ) {
			update_post_meta( $post_id, 'accom_quantity', sanitize_text_field( $_REQUEST['hb-accom-quantity'] ) );
		}
		if ( isset( $_REQUEST['hb-accom-occupancy'] ) ) {
			update_post_meta( $post_id, 'accom_occupancy', sanitize_text_field( $_REQUEST['hb-accom-occupancy'] ) );
		}
		if ( isset( $_REQUEST['hb-accom-max-occupancy'] ) ) {
			update_post_meta( $post_id, 'accom_max_occupancy', sanitize_text_field( $_REQUEST['hb-accom-max-occupancy'] ) );
		}
		if ( isset( $_REQUEST['hb-accom-search-result-desc'] ) ) {
			update_post_meta( $post_id, 'accom_search_result_desc', wp_filter_post_kses( $_REQUEST['hb-accom-search-result-desc'] ) );
		}
		if ( isset( $_REQUEST['hb-accom-list-desc'] ) ) {
			update_post_meta( $post_id, 'accom_list_desc', wp_filter_post_kses( $_REQUEST['hb-accom-list-desc'] ) );
		}
		if ( isset( $_REQUEST['hb-accom-starting-price'] ) ) {
			update_post_meta( $post_id, 'accom_starting_price', sanitize_text_field( $_REQUEST['hb-accom-starting-price'] ) );
		}
        if ( isset( $_REQUEST['hb-accom-num-name-index'] ) ) {
            update_post_meta( $post_id, 'accom_num_name_index', sanitize_text_field( ( $_REQUEST['hb-accom-num-name-index'] ) ) );
        }
        if ( isset( $_REQUEST['hb-accom-num-name-json'] ) ) {
            $accom_num_name = json_decode( stripslashes( $_REQUEST['hb-accom-num-name-json'] ), true );
            if ( ! $accom_num_name ) {
                $accom_num_name = array();
            }
            $this->hbdb->update_accom_num_name( $post_id, $accom_num_name );
        }
        if ( isset( $_REQUEST['hb-accom-default-page'] ) ) {
            update_post_meta( $post_id, 'accom_default_page', sanitize_text_field( $_REQUEST['hb-accom-default-page'] ) );
        }
        if ( isset( $_REQUEST['hb-accom-page-template'] ) ) {
            update_post_meta( $post_id, 'accom_page_template', sanitize_text_field( $_REQUEST['hb-accom-page-template'] ) );
        }
        if ( isset( $_REQUEST['hb-accom-linked-page'] ) ) {
            update_post_meta( $post_id, 'accom_linked_page', sanitize_text_field( $_REQUEST['hb-accom-linked-page'] ) );
        }
        
	}

    public function redirect_hb_menu_accom_page() {
        wp_redirect( admin_url( 'edit.php?post_type=hb_accommodation' ) );
        exit;
    }

	public function display_accom_id( $post ) {
		if ( in_array( $post->ID, $this->hbdb->get_all_accom_ids() ) ) {
		?>
		<div style="padding: 10px 10px 0; color: #666">
			<strong><?php _e( 'Accommodation id: ', 'hbook-admin' ); ?></strong>
			<?php echo( $post->ID ); ?>
		</div>
		<?php
		}
	}

    public function filter_template_page( $default_template ) {
        global $post;
        if ( $post && $post->post_type == 'hb_accommodation' && get_post_meta( $post->ID, 'accom_default_page', true ) != 'no' ) {
            $accom_page_template = get_post_meta( $post->ID, 'accom_page_template', true );
            if ( $accom_page_template && $accom_page_template != 'post' ) {
                $template = get_template_directory() . '/' . $accom_page_template;
                if ( file_exists( $template ) ) {
                    return $template;
                }
            }
        }
        return $default_template;
    }

	public function admin_accom_order( $query ) {
		if( is_admin() && $query->get( 'post_type' ) == 'hb_accommodation' ) {
			$query_orderby = $query->get( 'orderby' );
			if ( ! $query_orderby ) {
				$query->set( 'order', 'ASC' );
			}
		}
	}
	
	private function is_accom_main_language( $accom_id ) {
		$all_status = true;
		$accom_ids = $this->hbdb->get_all_accom_ids( $all_status );
		return in_array( $accom_id, $accom_ids );
	}
	
}
